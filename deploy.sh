#!/bin/bash
sudo git pull git@bitbucket.org:jetcampteam/jetcamps_api.git
sudo composer install
sudo php artisan migrate
sudo supervisorctl stop laravel-worker:*
sudo supervisorctl start laravel-worker:*