<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class AccommodationScrapedContent extends Model
{
    protected $table = 'accommodation_scraped_contents';

    public function accommodation(){
        return $this->hasOne('App\Accommodation', 'id', 'accommodation_id');
    }

}