<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationPhotoCategory extends Model
{
    protected  $table= 'accommodation_photo_categories';
    protected  $guarded = [];
}