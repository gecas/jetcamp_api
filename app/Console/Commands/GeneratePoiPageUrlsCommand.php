<?php

namespace App\Console\Commands;


use App\Jobs\GeneratePoiPageUrlsJob;
use App\Jobs\GeneratePoisUrlsJob;
use Illuminate\Console\Command;
use Queue;

class GeneratePoiPageUrlsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generatePoiPageUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates POIs Urls table";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GeneratePoiPageUrlsJob());
    }
}