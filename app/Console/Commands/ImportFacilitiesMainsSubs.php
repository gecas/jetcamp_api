<?php

namespace App\Console\Commands;
use App\Jobs\ImportFacilitiesMainsSubsJob;
use Illuminate\Console\Command;
use Queue;

class ImportFacilitiesMainsSubs extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:importFacilitiesMainsSubs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import facilities mains and subs";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ImportFacilitiesMainsSubsJob());
    }

}