<?php

namespace App\Console\Commands;

use App\Jobs\GenerateCommercialPageUrlsJob;
use Illuminate\Console\Command;
use Queue;

class GenerateCommercialPageUrlsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateCommercialPageUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates Commercial Page urls";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateCommercialPageUrlsJob());
    }

}