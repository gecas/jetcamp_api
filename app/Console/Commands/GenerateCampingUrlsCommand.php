<?php

namespace App\Console\Commands;
use App\Jobs\GenerateCampingsUrlsJob;
use Illuminate\Console\Command;
use Queue;

class GenerateCampingUrlsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateCampingsUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates Campings Urls table";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateCampingsUrlsJob());
    }

}