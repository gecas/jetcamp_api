<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\MigrateImagesToS3Job;

class MigrateImagesToS3 extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateImagesToS3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates Images to Amazon Services S3";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateImagesToS3Job());
    }

}