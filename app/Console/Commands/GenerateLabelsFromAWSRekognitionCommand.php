<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\GenerateLabelsFromAWSRekognitionJob;

class GenerateLabelsFromAWSRekognitionCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateLabelsFromRekognition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates labels from Amazon Services Rekognition";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateLabelsFromAWSRekognitionJob());
    }
}