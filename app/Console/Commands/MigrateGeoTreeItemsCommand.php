<?php

namespace App\Console\Commands;
use App\Jobs\MigrateGeoItemsJob;
use Illuminate\Console\Command;
use Queue;


class MigrateGeoTreeItemsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateGeoTreeItems';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates GeoTreeItems";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateGeoItemsJob());
    }
}