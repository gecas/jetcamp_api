<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\ResizeImagesSearchAccommodationJob;

class ResizeImagesSearchAccommodationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:resizeImagesSearchAccommodation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Resizes images for S3 search accommodation bucket";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ResizeImagesSearchAccommodationJob());
    }

}