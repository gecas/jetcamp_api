<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Queue;
use App\Jobs\MigrateASCtoMysqlAccommodationsJob;

class MigrateASCtoMysqlAccommodations extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateASCtoMysqlAccomodations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates accommodations scraped content to accommodations";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateASCtoMysqlAccommodationsJob());
    }
}