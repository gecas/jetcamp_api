<?php

namespace App\Console\Commands;

use App\Jobs\Prebuild\PrebuildAllJob;
use Illuminate\Console\Command;

class PrebuildAllCommand extends Command
{
    protected $signature = "prebuild:all";

    protected $description = 'Prebuild all data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Queue::push(new PrebuildAllJob());
    }
}