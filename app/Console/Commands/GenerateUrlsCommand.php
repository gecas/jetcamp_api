<?php

namespace App\Console\Commands;
use App\Jobs\GenerateUrlsJob;
use Illuminate\Console\Command;
use Queue;
use App\ElasticSearchAPI\ESClient;
use MigrationService;

class GenerateUrlsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates Urls table";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateUrlsJob());
    }

}