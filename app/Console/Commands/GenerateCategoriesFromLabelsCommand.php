<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\GenerateCategoriesFromLabelsJob;

class GenerateCategoriesFromLabelsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateCategoriesFromLabels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates categories from labels";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateCategoriesFromLabelsJob());
    }
}