<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\MigrateImagesRemoveExifJob;

class MigrateImagesRemoveExif extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateImagesRemoveExif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Removes exif from original images in Amazon Services S3";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateImagesRemoveExifJob());
    }

}