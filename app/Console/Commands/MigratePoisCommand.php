<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\MigratePoisJob;

class MigratePoisCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migratePoiItems';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates Places of Interest";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigratePoisJob());
    }

}