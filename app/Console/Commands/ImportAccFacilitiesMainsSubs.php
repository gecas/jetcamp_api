<?php

namespace App\Console\Commands;
use App\Jobs\ImportAccFacilitiesMainsSubsJob;
use Illuminate\Console\Command;
use Queue;


class ImportAccFacilitiesMainsSubs extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:importAccFacilitiesMainsSubs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import accommodations facilities mains and subs";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ImportAccFacilitiesMainsSubsJob());
    }
}