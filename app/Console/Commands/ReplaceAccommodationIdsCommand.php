<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\ReplaceAccommodationIdsJob;

class ReplaceAccommodationIdsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'geo:replaceAccommodationIds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Replace accommodation id for photos";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
//    public function fire()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ReplaceAccommodationIdsJob());
    }

}