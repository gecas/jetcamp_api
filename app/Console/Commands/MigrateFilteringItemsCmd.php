<?php

namespace App\Console\Commands;
use App\Jobs\MigrateFilteringOnAccChildrenJob;
use App\Jobs\MigrateFilteringItemsJob;
use Illuminate\Console\Command;
use Queue;

class MigrateFilteringItemsCmd extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateFilteringItems';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates accommodation filtering items";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateFilteringItemsJob());
    }
}