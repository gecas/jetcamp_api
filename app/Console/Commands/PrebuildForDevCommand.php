<?php

namespace App\Console\Commands;

use Basemkhirat\Elasticsearch\Facades\ES;
use App\Jobs\Prebuild\PrebuildAccommodationListJob;
use App\Jobs\Prebuild\PrebuildGeoItemBranchJob;
use App\Jobs\Prebuild\PrebuildGeoItemListJob;
use App\Models\EsGeoItem;
use Illuminate\Console\Command;
use Queue;
use DB;

class PrebuildForDevCommand extends Command
{
    protected $signature = "prebuild:rebuild-for-dev";

    protected $description = 'Prebuild sample data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Continue? This will truncate urls table and ES and will queue jobs that will build sample data (Klaipeda)')) {
            //Truncate jobs table
            DB::statement('TRUNCATE jobs');

            $this->truncateES();

            //Truncate urls table
            DB::statement('TRUNCATE urls');

            //Truncate failed jobs tables
            DB::statement('TRUNCATE failed_jobs');

            //Prebuild Europe
            Queue::push(new PrebuildGeoItemListJob([29468]));

            $this->prebuildCountries();

            //Prebuild klaipeda geo branch
            Queue::push(new PrebuildGeoItemBranchJob('geotree_europe_republicoflithuania_klaipedacounty'));

            //Accommodations for klaipeda
            Queue::push(new PrebuildAccommodationListJob([12951, 15468, 13528, 19711, 11044, 21521, 18128]));
        }
    }

    private function prebuildCountries()
    {
        //Countries
        Queue::push(new PrebuildGeoItemListJob([
            29591,
            287576,
            411790,
            55,
            1936,
            6641,
            17891,
            25152,
            46589,
            63675,
            69809,
            83891,
            98753,
            112538,
            127337,
            185608,
            223710,
            224832,
            237313,
            283664,
            328635,
            383519,
            420072,
            429692,
            438279,
            447454,
            456095,
            466761,
            526072,
            529896,
            546137,
            548993,
            551577,
            552277,
            553895,
            557440,
            560148,
            563188,
            571387,
            601218,
            629899,
            667157,
            677429,
            814196,
            870117,
            901388,
            909792,
            912479,
            916084
        ]));
    }

    private function truncateES()
    {
        $proceed = true;
        while ($proceed) {
            sleep(1);
            $esItems = EsGeoItem::take(1000)->get();
            if (count($esItems) > 0) {
                foreach ($esItems as $item) {
                    $item->delete();
                }
            } else {
                $proceed = false;
            }
        }
    }
}