<?php

namespace App\Console\Commands;

use App\Jobs\ResizeImagesAccommodationDesktopLargeJob;
use Illuminate\Console\Command;
use Queue;

class ResizeImagesAccommodationDesktopLargeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:resizeImagesAccommodationDesktopLarge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Resizes images for S3 accommodation desktop large bucket";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ResizeImagesAccommodationDesktopLargeJob());
    }

}