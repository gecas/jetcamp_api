<?php

namespace App\Console\Commands;

use App\Jobs\Prebuild\PrebuildGeoItemListJob;
use Illuminate\Console\Command;

class PrebuildGeoItemCommand extends Command
{
    protected $signature = "prebuild:geoitem {id*}";

    protected $description = 'Prebuild accommodation data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = $this->argument('id');
        \Queue::push(new PrebuildGeoItemListJob($ids));
    }
}