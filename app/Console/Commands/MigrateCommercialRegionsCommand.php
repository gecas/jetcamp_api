<?php

namespace App\Console\Commands;
use App\Jobs\MigrateCommercialRegionsJob;
use Illuminate\Console\Command;
use Queue;


class MigrateCommercialRegionsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateCommercialRegions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates Commercial Regions";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateCommercialRegionsJob());
    }

}