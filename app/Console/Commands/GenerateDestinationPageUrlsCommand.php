<?php

namespace App\Console\Commands;

use App\Jobs\GenerateDestinationPageUrlsJob;
use Illuminate\Console\Command;
use Queue;

class GenerateDestinationPageUrlsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateDestinationPageUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates Destination Page urls";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateDestinationPageUrlsJob());
    }

}