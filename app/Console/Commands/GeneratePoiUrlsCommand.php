<?php

namespace App\Console\Commands;


use App\Jobs\GeneratePoisUrlsJob;
use Illuminate\Console\Command;
use Queue;

class GeneratePoiUrlsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generatePoisUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates POIs Urls table";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GeneratePoisUrlsJob());
    }
}