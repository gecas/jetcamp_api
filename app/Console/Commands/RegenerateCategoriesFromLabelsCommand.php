<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Queue;
use App\Jobs\RegenerateCategoriesFromLabelsJob;

class RegenerateCategoriesFromLabelsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'geo:regenerateCategoriesFromLabels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Re-Generates categories from labels";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new RegenerateCategoriesFromLabelsJob());
    }
}