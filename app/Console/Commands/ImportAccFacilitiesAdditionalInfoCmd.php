<?php

namespace App\Console\Commands;
use App\Jobs\ImportAccFacilitiesAdditionalInfoJob;
use Illuminate\Console\Command;
use Queue;


class ImportAccFacilitiesAdditionalInfoCmd extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:importAccFacilitiesAdditionalInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import accommodations facilities additional information";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ImportAccFacilitiesAdditionalInfoJob());
    }
}