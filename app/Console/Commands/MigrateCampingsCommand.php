<?php

namespace App\Console\Commands;
use App\Jobs\MigrateCampingsJob;
use Illuminate\Console\Command;
use Queue;


class MigrateCampingsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:migrateCampings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrates campings";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new MigrateCampingsJob());
    }

}