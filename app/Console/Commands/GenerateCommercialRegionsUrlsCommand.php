<?php

namespace App\Console\Commands;
use App\Jobs\GenerateCommercialRegionsUrlsJob;
use Illuminate\Console\Command;
use Queue;


class GenerateCommercialRegionsUrlsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateCommercialRegionsUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates Commercial Regions Urls table";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateCommercialRegionsUrlsJob());
    }
}