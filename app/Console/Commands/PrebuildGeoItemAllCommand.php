<?php

namespace App\Console\Commands;

use App\Jobs\Prebuild\PrebuildGeoItemAllJob;
use Illuminate\Console\Command;
use Queue;

class PrebuildGeoItemAllCommand extends Command
{
    protected $signature = 'prebuild:geoitem-all';

    protected $description = "Prebuild all geo tree items data";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Queue::push(new PrebuildGeoItemAllJob);
    }
}