<?php

namespace App\Console\Commands;

use App\Jobs\GenerateCampingsPageUrlsJob;
use Illuminate\Console\Command;
use Queue;

class GenerateCampingPageUrlsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:generateCampingsPageUrls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates Campings Page urls";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new GenerateCampingsPageUrlsJob());
    }

}