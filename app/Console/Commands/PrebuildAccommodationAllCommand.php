<?php

namespace App\Console\Commands;

use App\Jobs\Prebuild\PrebuildAccommodationAllJob;
use Illuminate\Console\Command;
use Queue;

class PrebuildAccommodationAllCommand extends Command
{
    protected $signature = 'prebuild:accommodation-all';

    protected $description = "Prebuild all accommodations data";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Queue::push(new PrebuildAccommodationAllJob());
    }
}