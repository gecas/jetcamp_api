<?php

namespace App\Console\Commands;

use App\Jobs\ResizeImagesAccommodationDesktopThumbJob;
use Illuminate\Console\Command;
use Queue;

class ResizeImagesAccommodationDesktopThumbCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'geo:resizeImagesAccommodationDesktopThumb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Resizes images for S3 accommodation desktop thumb bucket";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('command '.$this->name.' is started!');
        Queue::push(new ResizeImagesAccommodationDesktopThumbJob());
    }

}