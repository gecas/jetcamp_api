<?php

namespace App\Console\Commands;

use App\Jobs\Prebuild\PrebuildAccommodationListJob;
use Illuminate\Console\Command;

class PrebuildAccommodationCommand extends Command
{
    protected $signature = "prebuild:accommodation {id*}";

    protected $description = 'Prebuild accommodation data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = $this->argument('id');
        \Queue::push(new PrebuildAccommodationListJob($ids));
    }
}