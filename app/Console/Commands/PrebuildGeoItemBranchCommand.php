<?php

namespace App\Console\Commands;

use App\Jobs\Prebuild\PrebuildGeoItemBranchJob;
use Illuminate\Console\Command;
use Queue;

class PrebuildGeoItemBranchCommand extends Command
{
    protected $signature = "prebuild:geoitem-branch {key}";

    protected $description = 'Prebuild geo item branch data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->argument('key');
        Queue::push(new PrebuildGeoItemBranchJob($key));
    }
}