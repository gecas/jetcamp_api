<?php

namespace App\Console;

use App\Console\Commands\PrebuildAccommodationCommand;
use App\Console\Commands\PrebuildAccommodationAllCommand;
use App\Console\Commands\PrebuildAllCommand;
use App\Console\Commands\PrebuildForDevCommand;
use App\Console\Commands\PrebuildGeoItemAllCommand;
use App\Console\Commands\PrebuildGeoItemBranchCommand;
use App\Console\Commands\PrebuildGeoItemCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\GenerateUrlsCommand',
        'App\Console\Commands\GenerateCampingUrlsCommand',
        'App\Console\Commands\MigrateCampingsCommand',
        'App\Console\Commands\MigrateGeoTreeItemsCommand',
        //'App\Console\Commands\CalculateCampingCountsCommand',
        'App\Console\Commands\MigratePoisCommand',
        'App\Console\Commands\GeneratePoiUrlsCommand',
        'App\Console\Commands\MigrateCommercialRegionsCommand',
        'App\Console\Commands\GenerateCommercialRegionsUrlsCommand',
        'App\Console\Commands\ImportFacilitiesMainsSubs',
        'App\Console\Commands\MigrateASCtoMysqlAccommodations',
        'App\Console\Commands\ImportAccFacilitiesMainsSubs',
        'App\Console\Commands\GenerateCampingPageUrlsCommand',
        'App\Console\Commands\GenerateDestinationPageUrlsCommand',
        'App\Console\Commands\GeneratePoiPageUrlsCommand',
        'App\Console\Commands\GenerateCommercialPageUrlsCommand',
        'App\Console\Commands\ImportAccFacilitiesAdditionalInfoCmd',
        'App\Console\Commands\MigrateFilteringItemsCmd',
        'App\Console\Commands\GenerateCategoriesFromLabelsCommand',
        'App\Console\Commands\GenerateLabelsFromAWSRekognitionCommand',
        'App\Console\Commands\ResumeLabelsFromAWSRekognitionCommand',
        'App\Console\Commands\ResizeImagesSearchAccommodationCommand',
        'App\Console\Commands\ResizeImagesAccommodationMobileLargeCommand',
        'App\Console\Commands\ResizeImagesAccommodationDesktopThumbCommand',
        'App\Console\Commands\MigrateImagesToS3',
        'App\Console\Commands\MigrateImagesRemoveExif',
        'App\Console\Commands\GenerateCategoriesFromLabelsCommand',
        'App\Console\Commands\RegenerateCategoriesFromLabelsCommand',
        'App\Console\Commands\ReplaceAccommodationIdsCommand',
        PrebuildAccommodationAllCommand::class,
        PrebuildAccommodationCommand::class,
        PrebuildGeoItemAllCommand::class,
        PrebuildGeoItemCommand::class,
        PrebuildGeoItemBranchCommand::class,
        PrebuildForDevCommand::class,
        PrebuildAllCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
