<?php

namespace App\Observers\Accommodation;

use App\Jobs\ES\UpdateAccommodationsIndirectConnectionsCountAroundGpsJob;
use App\Models\Accommodation;
use Illuminate\Support\Facades\Queue;

class UpdateAccommodationNearbyAccommodationsIndirectConnectionsCountsObserver
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  Accommodation $accommodation
     * @return void
     */
    public function updated(Accommodation $accommodation)
    {
        if ($accommodation->isDirty('address_gps_lat')
            || $accommodation->isDirty('address_gps_lon')
            || $accommodation->isDirty('status')
        ) {
            if ($accommodation->address_gps_lat && $accommodation->address_gps_lon) {
                Queue::push(new UpdateAccommodationsIndirectConnectionsCountAroundGpsJob(
                    $accommodation->address_gps_lat,
                    $accommodation->address_gps_lon
                ));
            }

            if ($accommodation->isDirty('address_gps_lat')
                || $accommodation->isDirty('address_gps_lon')
                && ($accommodation->getOriginal('address_gps_lat') && $accommodation->getOriginal('address_gps_lon'))
            ) {
                Queue::push(new UpdateAccommodationsIndirectConnectionsCountAroundGpsJob(
                    $accommodation->getOriginal('address_gps_lat'),
                    $accommodation->getOriginal('address_gps_lon')
                ));
            }


        }
    }
}
