<?php

namespace App\Observers\Accommodation;

use App\Jobs\Prebuild\PrebuildAccommodationJob;
use App\Models\Accommodation;
use Illuminate\Support\Facades\Queue;

class UpdateAccommodationPrebuildDataObserver
{
    /**
     * Handle the event.
     *
     * @param  Accommodation $accommodation
     * @return void
     */
    public function updated(Accommodation $accommodation)
    {
        if ($accommodation->status) {
            Queue::push(new PrebuildAccommodationJob($accommodation->id));
        }
    }
}
