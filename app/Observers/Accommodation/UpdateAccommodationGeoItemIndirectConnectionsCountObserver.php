<?php

namespace App\Observers\Accommodation;

use App\Jobs\ES\UpdateGeoItemIndirectConnectionsCountsJob;
use App\Models\Accommodation;
use Illuminate\Support\Facades\Queue;

class UpdateAccommodationGeoItemIndirectConnectionsCountObserver
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  Accommodation $accommodation
     * @return void
     */
    public function updated(Accommodation $accommodation)
    {
        if ($accommodation->isDirty('address_gps_lat')
            || $accommodation->isDirty('address_gps_lon')
            || $accommodation->isDirty('status')
            || $accommodation->isDirty('geo_trees_id')
        ) {

            if ($accommodation->geo_trees_id) {
                Queue::push(new UpdateGeoItemIndirectConnectionsCountsJob($accommodation->geo_trees_id));
            }

            if ($accommodation->isDirty('geo_trees_id') && $accommodation->getOriginal('geo_trees_id')) {
                Queue::push(new UpdateGeoItemIndirectConnectionsCountsJob($accommodation->getOriginal('geo_trees_id')));
            }
        }
    }
}
