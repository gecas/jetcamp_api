<?php

namespace App\Observers\Accommodation;

use App\Models\Accommodation;
use App\Jobs\ES\UpdateGeoItemAndParentsDirectConnectionsCountsJob;
use Illuminate\Support\Facades\Queue;

class UpdateAccommodationGeoItemAndParentsDirectConnectionsCountObserver
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  Accommodation $accommodation
     * @return void
     */
    public function updated(Accommodation $accommodation)
    {
        if ($accommodation->isDirty('geo_trees_id')
            || $accommodation->isDirty('status')
        ) {
            if ($accommodation->geo_trees_id) {
                Queue::push(new UpdateGeoItemAndParentsDirectConnectionsCountsJob($accommodation->geo_trees_id));
            }

            //If geo tree item for accommodation change update old geo tree item and its parents direct connection count
            if ($accommodation->isDirty('geo_trees_id') && $accommodation->getOriginal('geo_trees_id')) {
                Queue::push(new UpdateGeoItemAndParentsDirectConnectionsCountsJob($accommodation->getOriginal('geo_trees_id')));
            }

        }
    }
}
