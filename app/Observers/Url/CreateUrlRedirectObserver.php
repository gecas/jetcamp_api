<?php

namespace App\Observers\Url;

use App\Models\Url;
use App\Services\UrlRedirectService;

class CreateUrlRedirectObserver
{
    protected $urlRedirectService;

    public function __construct(UrlRedirectService $urlRedirectService)
    {
        $this->urlRedirectService = $urlRedirectService;
    }

    public function updated(Url $url)
    {
        if ($url->url != $url->getOriginal('url')) {
            $this->urlRedirectService->add(
                $url->lang . '/' . $url->getOriginal('url'),
                $url->lang . '/' . $url->url
            );
        }
    }

}