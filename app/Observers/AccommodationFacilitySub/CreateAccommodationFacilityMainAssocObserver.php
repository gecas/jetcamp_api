<?php

namespace App\Observers\AccommodationFacilitySub;

use App\Models\AccommodationFacilitySub;

class CreateAccommodationFacilityMainAssocObserver
{
    public function __construct()
    {
    }

    /**
     * Create facility main and accommodation assoc if it does not exist and assoc between facility sub and accommodation is created
     *
     * @param  AccommodationFacilitySub $pivot Accommodation facility sub pivot
     * @return void
     */
    public function created(AccommodationFacilitySub $pivot)
    {
        $facilityMainId = $pivot->facility_sub->facility_main_id;
        if (!$pivot->accommodation->facility_mains()->where('id', $facilityMainId)->first()) {
            $pivot->accommodation->facility_mains()->attach($facilityMainId);
        }
    }
}
