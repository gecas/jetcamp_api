<?php

namespace App\Observers\GeoTree;

use App\Models\GeoTree;
use Illuminate\Support\Facades\Queue;
use App\Jobs\GeoTree\UpdateGeoItemChildrenKeysJob;

class UpdateGeoTreeKeysObserver
{

    /**
     * Handle the event.
     *
     * @param  $geoTree GeoTree
     * @return void
     */

    public function updated(GeoTree $geoTree)
    {
        if ($geoTree->isDirty('key_short')) {
            Queue::push(new UpdateGeoItemChildrenKeysJob($geoTree->getOriginal('key_short'), $geoTree->key_short));
        }
    }
}
