<?php

namespace App\Observers\CommercialRegion;

use App\Jobs\MigrateCommercialRegionsJob;
use App\Models\CommercialRegion;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Queue;

class SyncCommercialRegionToEsObserver
{
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function updated(CommercialRegion $commercialRegion)
    {
        if ($commercialRegion->isDirty('popularity')
            && $commercialRegion->active) {

            $commercialRegion->withoutEvents(function (CommercialRegion $commercialRegion) {
                $commercialRegion->migrated = null;
                $commercialRegion->save();
            });

            Queue::push(new MigrateCommercialRegionsJob());
        }
    }
}
