<?php

namespace App\Observers\CommercialRegionTranslation;

use App\Models\CommercialRegionTranslation;
use App\Models\Url;
use App\Services\UrlGenerators\CommercialRegionUrlForSingleItemPageGenerateService;

class UpdateCommercialRegionUrlForSingleItemPageObserver
{
    protected $urlGenerateService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(CommercialRegionUrlForSingleItemPageGenerateService $urlGenerateService)
    {
        $this->urlGenerateService = $urlGenerateService;
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function saved(CommercialRegionTranslation $translation)
    {
        if ($translation->isDirty('translation')
            && $translation->language->active_frontend) {
            $match = [
                'lang' => $translation->language->abbrevation,
                'subject_id' => $translation->commercial_region->id . '-3',
                'page_type' => Url::PAGE_TYPE_COMMERCIAL_PAGE,
            ];


            $url = Url::updateOrCreate($match,
                ['url' => $this->urlGenerateService->generate($translation)]
            );
        }
    }
}
