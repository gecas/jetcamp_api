<?php

namespace App\Observers\CommercialRegionTranslation;

use App\Jobs\MigrateCommercialRegionsJob;
use App\Models\CommercialRegionTranslation;
use Illuminate\Support\Facades\Queue;
use App\Models\CommercialRegion;

class SyncCommercialRegionToEsObserver
{
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function updated(CommercialRegionTranslation $translation)
    {
        if ($translation->isDirty('translation')
            && $translation->language->active_frontend) {
            $commercialRegion = $translation->commercial_region;
            $commercialRegion->withoutEvents(function (CommercialRegion $commercialRegion) {
                $commercialRegion->migrated = null;
                $commercialRegion->save();
            });
            Queue::push(new MigrateCommercialRegionsJob());
        }
    }
}
