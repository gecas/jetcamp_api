<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class AccommodationAdditionalDistance extends Model
{
    protected $table = 'accommodation_additional_distances';

    protected $fillable = [
        'accommodation_id', 'facility_main_id', 'facility_sub_id', 'distance', 'facility_setting_distancetypes_id'
    ];

    public function facilitySettingDistanceType(){
        return $this->hasOne('App\FacilitySettingDistanceType', 'id', 'facility_setting_distancetypes_id');
    }

    public function facilityMain() {
        return $this->belongsTo('App\FacilityMain', 'facility_main_id');
    }

    public function facilitySub() {
        return $this->belongsTo('App\FacilitySub', 'facility_sub_id');
    }

    public function accommodation() {
        return $this->belongsTo('App\Accommodation', 'accommodation_id');
    }
}