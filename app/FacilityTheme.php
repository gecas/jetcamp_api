<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class FacilityTheme extends Model
{
    protected $table = 'facility_themes';

    protected $fillable = [
        'description',
    ];
}