<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use App\Models\AccommodationThemeCount;
use Illuminate\Http\Response;

class AccommodationThemeCountController extends Controller
{
    /**
     * Get accommodation themes count
     *
     * @param $id Accommodation ID
     *
     * @return mixed
     */
    public function index()
    {
        $query = AccommodationThemeCount::filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

}