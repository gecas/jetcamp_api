<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function transformToResponse(
        $status = null
    ) {
        if ($status) {
            $status = 201;
        }
        return $status;
    }
}
