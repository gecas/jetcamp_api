<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * Get single user info
     *
     * @param integer $id
     * @return mixed
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return Response::jsonApi($user);
    }
}