<?php

namespace App\Http\Controllers;

use App\Models\CommercialRegionTranslation;
use App\Models\Language;
use App\Responses\JsonApiResponseBuilder;
use App\Models\CommercialRegion;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommercialRegionTranslationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Request $request
     * @param $commercialRegionId
     * @return mixed
     */
    public function index(Request $request, $commercialRegionId)
    {
        $commercialRegion = CommercialRegion::where('id', $commercialRegionId)
            ->with('commercial_region_translations')
            ->first();
        return Response::jsonApi($commercialRegion);
    }

    /**
     * @param Request $request
     * @param $commercialRegionId
     * @param $languageAbbr
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $commercialRegionId, $languageAbbr)
    {
        $commercialRegion = CommercialRegion::findOrFail($commercialRegionId);
        $language = Language::where('abbrevation', $languageAbbr)->firstOrFail();

        $this->validate($request, [
            'translation' => 'present',
        ]);

        $translation = CommercialRegionTranslation::where('key', $commercialRegion->key)
            ->where('language_id', $language->id)
            ->first();

        //if translation is empty string
        if (!$request->input('translation')) {
            //if translation already exist delete it
            if ($translation) {
                $translation->delete();
            }

            return Response::jsonApi('', null, 204);
        }

        $translation = CommercialRegionTranslation::updateOrCreate(
            ['key' => $commercialRegion->key, 'language_id' => $language->id],
            [
                'translation' => $request->input('translation'),
                'language_id' => $language->id,
                'key' => $commercialRegion->key
            ]
        );

        return Response::jsonApi($translation);
    }
}
