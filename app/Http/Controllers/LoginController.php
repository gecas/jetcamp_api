<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiValidationException;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * Retrieves a token for the user, creates token if it doesn't exist
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\ApiValidationException When users email is not found
     * @throws \App\Exceptions\ApiValidationException When user credentials are invalid
     * @throws \Exception
     */
    public function login(Request $request)
    {
        $validation = app('validator')->make($request->input(), [
            'email' => 'required|exists:users',
            'password' => 'required|min:2',
        ]);

        if ($validation->fails()) {
            throw new ApiValidationException($validation->errors()->toArray());
        }

        $user = User::where('email', $request->input('email'));
        //dd($user);
        /*if (!app('hash')->check($request->input('password'), $user->password)) {
            throw new ApiValidationException(['password' => [trans('id')]]);
        }*/

        /*if (is_null($user->api_token)) {
            $user->api_token = User::generateApiToken(app(\App\RandomGenerator::class));
            $user->save();
        }*/

        return $this->transformToResponse($user, 201);
    }

}