<?php

namespace App\Http\Controllers;

use App\Responses\JsonApiResponseBuilder;
use App\Models\CommercialRegion;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

class CommercialRegionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get accommodation list
     *
     * @return mixed
     */
    public function index()
    {
        $query = CommercialRegion::
        addSelect(
            '*',
            DB::raw('ST_AsText(gpsPolygon) AS gpsPolygon')
        )
            ->filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();
        return Response::jsonApi($items, $count);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $item = CommercialRegion::fieldsFromInput()
            ->withRelationsFromInput()
            ->findOrFail($id);

        return Response::jsonApi($item);
    }

    /**
     * Update accommodations table entry
     * @param Request $request
     * @param int Id
     * @return mixed
     *
     */
    public function update(Request $request, $id)
    {
        $commercial_region = CommercialRegion::findOrException($id);

        $this->validate($request, [
            'key' => 'present|required',
            'parent_id' => 'present|required',
            'active' => 'present|max:11',
            'popularity' => 'present|max:11',
            'category_id' => 'present|exists:commercial_region_categories,id'
        ]);

        $commercial_region->update([
            'key' => $request->input('key'),
            'parent_id' => $request->input('parent_id'),
            'active' => $request->input('active'),
            'popularity' => $request->input('popularity'),
            'category_id' => $request->input('category_id'),
            'migrated' => null,
        ]);

        return Response::jsonApi($commercial_region);


    }
}