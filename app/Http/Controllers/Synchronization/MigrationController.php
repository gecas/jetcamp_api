<?php

namespace App\Http\Controllers\Synchronization;

use App\ElasticSearchAPI\ESSearchGeoItemResponse;
use App\ElasticSearchAPI\GeoTreeItem;
use App\Http\Controllers\Controller;
use App\ElasticSearchAPI\ESClient;
use App\Services\MigrationService;

class MigrationController extends Controller
{

    private $esClient;
    private $migrationHelper;

    public function __construct(ESClient $esClient, MigrationService $migrationHelper)
    {
        $this->esClient = $esClient;
        $this->migrationHelper = $migrationHelper;
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    public function migrateCampings() {
        $this->esClient->createGeoItemIndex();
        //ini_set("memory_limit","-1");
        $cResults = app('db')->connection('mysql2')
                            ->select('SELECT * FROM accommodation_scraped_contents where migrated is null order by id limit 8000');
        $rows = [];
        $translationKeys = [];
        $campaignIds = [];
        $geoTreeIds = [];
        foreach ($cResults as $cResult) {
            $row = [];
            $row["c_id"] = $cResult->id;
            $campaignIds[] = $cResult->id;
            $row["c_name"] = $cResult->basics_namecamping;
            $row["c_street"] = $cResult->address_street;
            $row["c_geo_tree_id"] = $cResult->geo_trees_id;
            $row["c_rating"] = (float)$cResult->ratings_rating;
            $row["c_gps_lat"] = (float)$cResult->address_gps_lat;
            $row["c_gps_lon"] = (float)$cResult->address_gps_lon;
            $gResult = app('db')->connection('mysql2')
                                ->select('SELECT * FROM geo_trees where id = :id', ['id' => $row["c_geo_tree_id"]]);
            $geoTreeIds[] = $gResult[0]->id;
            $row["g_id"] = $gResult[0]->id;
            $row["g_type"] = $gResult[0]->geonamesFcode;
            $row["g_key"] = $gResult[0]->key;
            $row["g_gps_lat"] = (float)$gResult[0]->gpsLat;
            $row["g_gps_lon"] = (float)$gResult[0]->gpsLon;
            $translationKeys[] = '"'.$row["g_key"].'"';
            $rows[] = $row;
        }

        $geoTreeItems = [];
        foreach ($rows as $row) {
            $geoTreeItem = new GeoTreeItem();
            $geoTreeItem->type = GeoTreeItem::TYPE_CAMPING;
            $geoTreeItem->geo_id = $row["c_id"];
            $geoTreeItem->calculateId();
            $geoTreeItem->subtype = $row["g_type"];
            $geoTreeItem->key = $this->migrationHelper->stripKey($row["g_key"]);
            if ($row["g_gps_lon"] and $row["g_gps_lat"]) {
                $geoTreeItem->gps_coords["lat"] = $row["g_gps_lat"];
                $geoTreeItem->gps_coords["lon"] = $row["g_gps_lon"];
            } else
                $geoTreeItem->gps_coords = null;
            //$geoTreeItem->tags = $this->migrationHelper->retrieveTags($geoTreeItem->key);
            /*echo "<br>";
            var_dump($geoItem["tags"]);
            echo "<br>";*/

            $geoTreeItem->item->name = trim($row["c_name"]);
            $geoTreeItem->item->rating = $row["c_rating"];
            if (mb_strtolower($geoTreeItem->item->name) != $this->migrationHelper->normalizeString($row["c_name"], " "))
                $geoTreeItem->item->name_translations = [mb_strtolower($geoTreeItem->item->name), $this->migrationHelper->normalizeString($row["c_name"], " ")];
            else
                $geoTreeItem->item->name_translations = [mb_strtolower($geoTreeItem->item->name)];
            if ($row["c_street"]) {
                $geoTreeItem->item->address["street"] = $row["c_street"];
                //$geoTreeItem->item->address["street_translations"] = [mb_strtolower($geoTreeItem->item->name), $this->migrationHelper->normalizeString($row["c_street"], " ")];
            } else
                $geoTreeItem->item->address = null;
            if ($row["c_gps_lon"] and $row["c_gps_lat"]) {
                $geoTreeItem->item->gps_coords["lat"] = $row["c_gps_lat"];
                $geoTreeItem->item->gps_coords["lon"] = $row["c_gps_lon"];
            } else
                $geoTreeItem->item->gps_coords = null;

            $geoTreeItems[] = $geoTreeItem;
            $this->migrationHelper->logGeoItem($geoTreeItem);
        }

        $groupedTranslationItems = [];
        if (!empty($translationKeys)) {
            // resolving translations
            $groupedTranslationItems = $this->migrationHelper->getGroupedTranslations($translationKeys);
        }

        if (!empty($geoTreeItems)) {
            $this->migrationHelper->appendGeoItemsWithTranslations($geoTreeItems, $groupedTranslationItems);

            $result = $this->esClient->bulk($geoTreeItems);
            if (!$result["es_response"]["errors"]) {
                $r = app('db')->connection('mysql2')
                    ->select('update accommodation_scraped_contents set migrated=1 where id in ('.implode(", ", $campaignIds).')');
                $r = app('db')->connection('mysql2')
                    ->select('update geo_trees set migrated=1 where id in ('.implode(", ", $geoTreeIds).')');
                echo "<strong>Success!</strong>";
            } else {
                echo "<strong>Failed!</strong>";
                echo "<pre>";
                print_r($result);
                echo "</pre>";
            }
        } else {
            echo "Empty run";
        }

    }

    public function migrateGeoTree() {
        /*ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);*/
        $this->esClient->createGeoItemIndex();
        $gResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM geo_trees t where t.migrated is null and t.key like "geotree_europe%" order by id limit 300000;');
            //->select('SELECT * FROM geo_trees order by id limit 20;');

        $geoTreeItems = [];
        $translationKeys = [];
        $geoTreeIds = [];
        foreach ($gResults as $gResult) {
            //var_dump($gResult);
            $geoTreeIds[] = $gResult->id;
            $geoTreeItem = new GeoTreeItem();
            $geoTreeItem->type = GeoTreeItem::TYPE_GEOITEM;
            $geoTreeItem->geo_id = $gResult->id;
            $geoTreeItem->calculateId();
            $geoTreeItem->subtype = $gResult->geonamesFcode;
            $geoTreeItem->key = $this->migrationHelper->stripKey($gResult->key);
            //$geoTreeItem->tags = $this->migrationHelper->retrieveTags($geoTreeItem->key);
            $translationKeys[] = '"'.$gResult->key.'"';

            if ($gResult->gpsLon and $gResult->gpsLat) {
                $geoTreeItem->gps_coords["lat"] = (float)$gResult->gpsLat;
                $geoTreeItem->gps_coords["lon"] = (float)$gResult->gpsLon;
            } else
                $geoTreeItem->gps_coords = null;

            /*$campingCnt = $this->esClient->getGeoItemCampingCnt($geoTreeItem);
            $geoTreeItem->camping_cnt = $campingCnt;*/
            unset($geoTreeItem->item);
            $geoTreeItems[] = $geoTreeItem;
            //echo "-".($geoItem)."<br>";
            $this->migrationHelper->logGeoItem($geoTreeItem);
        }

        /*foreach ($geoItems as $geoItem) {
            var_dump($geoItem);
            echo "<br><br>";
        }*/

        $groupedTranslationItems = [];
        if (!empty($translationKeys)) {
            $groupedTranslationItems = $this->migrationHelper->getGroupedTranslations($translationKeys);
        }
        //var_dump($groupedTranslationItems);

        if (!empty($geoTreeItems)) {
            $this->migrationHelper->appendGeoItemsWithTranslations($geoTreeItems, $groupedTranslationItems);
            /*foreach ($geoItems as $geoItem) {
                var_dump($geoItem);
                echo "<br><br>";
            }*/

            $result = $this->esClient->bulk($geoTreeItems);

            if (!$result["es_response"]["errors"]) {
                $cnt = 5000;
                $start = 0;
                while ($start < count($geoTreeIds)) {
                    $sliceGeoTreeIds = array_slice($geoTreeIds, $start, $cnt);
                    $r = app('db')->connection('mysql2')
                                    ->select('update geo_trees set migrated=1 where id in (' . implode(", ", $sliceGeoTreeIds) . ')');
                    $start = $start + $cnt;
                }

                echo "<strong>Success!</strong>";
            } else {
                echo "<strong>Failed!</strong>";
                echo "<pre>";
                print_r($result);
                echo "</pre>";
            }
        } else {
            echo "Empty run<br>";
        }
    }

    public function calculateCampingCounts() {
//        $cResults = app('db')->connection('mysql2')
//            ->select('SELECT camp.id as camp_id, t.id as geo_id, t.key as geo_key FROM accommodation_scraped_contents camp Join geo_trees t on camp.geo_trees_id=t.id '.
//                'where camp.migrated is null order by camp.id limit 20000');
//
//        $campingsCounts = [];
//        $campaignIds = [];
//        foreach ($cResults as $cResult) {
//            $campaignIds[] = $cResult->camp_id;
//            $mysqlGeoKey = $cResult->geo_key;
//            $geoItemKey = $this->migrationHelper->stripKey($mysqlGeoKey);
//            $tags = $this->migrationHelper->retrieveTags($geoItemKey);
//            while (count($tags) > 1) {
//                array_pop($tags);
//                $newGeoItemKey = implode("_", $tags);
//                if (array_key_exists($newGeoItemKey, $campingsCounts))
//                    $campingsCounts[$newGeoItemKey]++;
//                else
//                    $campingsCounts[$newGeoItemKey] = 1;
//            }
//        }
//
//        $newGeoTreeItems = [];
//        foreach ($campingsCounts as $key => $value) {
//            echo "---".$key." = ".$value."---<br>";
//            $data = [
//                "index" => "geotree",
//                "type" => "geoitem",
//                "body" => [
//                    "query" => [
//                        "term" => [
//                            "key" => $key
//                        ]
//                    ]
//                ]
//            ];
//            $response = new ESSearchGeoItemResponse($this->esClient->getEsClient()->search($data));
//            $geoTreeItems = $response->getItems();
//            foreach ($geoTreeItems as $geoTreeItem) {
//                $geoTreeItem->camping_cnt = $value;
//                $newGeoTreeItems[] = $geoTreeItem;
//            }
//        }
//
//        //echo "---".count($newGeoTreeItems)."---<br>";
//        if (!empty($newGeoTreeItems)) {
//            $result = $this->esClient->bulk($newGeoTreeItems);
//            if (!$result["es_response"]["errors"]) {
//                $r = app('db')->connection('mysql2')
//                    ->select('update accommodation_scraped_contents set migrated=1 where id in ('.implode(", ", $campaignIds).')');
//                echo "<strong>Success!</strong>";
//            } else {
//                echo "<strong>Failed!</strong>";
//                echo "<pre>";
//                print_r($result);
//                echo "</pre>";
//            }
//        } else {
//            echo "Empyt run";
//        }
    }

    public function generateUrlsTable() {
        /*ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);*/
//        $gResults = app('db')->connection('mysql2')
//            //->select('SELECT * FROM geo_trees where migrated is null order by id limit 100;');
//            //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_germany%" and migrated is null order by id limit 5000;');
//            //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_netherlands%" and migrated is null order by id limit 5000;');
//            ->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_france%" and migrated is null order by id limit 5000;');
//            //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_%" and migrated is null order by id limit 5000;');
//        $geoTreeIds = [];
//        $esUrls = [];
//        foreach ($gResults as $gResult) {
//            $mysqlGeoKey = $gResult->key;
//            $geoTreeIds[] = $gResult->id;
//            $keyParties = explode("_", $mysqlGeoKey);
//            $trans = [];
//            $newKey = "";
//            while (count($keyParties) > 2) { // no need to include 1st and 2nd key part (geotree and continent)
//                $newKey = implode("_", $keyParties);
//                $tResults = app('db')->connection('mysql2')
//                            ->select('SELECT * FROM geo_translations t where t.key="'.$newKey.'";');
//                foreach ($tResults as $tResult) {
//                    $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
//                }
//                array_pop($keyParties);
//            }
//            $trans = array_reverse($trans);
//            if (!empty($newKey)){
//                /*echo "latest ".$newKey."<br>";
//                foreach ($trans as $key => $value) {
//                    echo "kai ".$key."<br>";
//                    var_dump($value);
//                    echo "<br>";
//                }*/
//                $urls = [];
//                foreach ($trans[$newKey] as $lng_abbr => $translation) {
//                    if (!empty(str_slug($translation)))
//                        $url = str_slug($translation)."_";
//                    else
//                        $url = str_slug($trans[$newKey]["en"])."_"; // for zh language unable to slugify
//                    foreach ($trans as $key => $value) {
//                        if ($newKey != $key) {
//                            if (array_key_exists($lng_abbr, $trans[$key])) {
//                                $url .= str_slug($trans[$key][$lng_abbr])."_";
//                            } else if (array_key_exists("en", $trans[$key])) {
//                                $url .= str_slug($trans[$key]["en"])."_";
//                            } else
//                                $url .= str_slug(reset($trans[$key]))."_";
//                        }
//                    }
//                    //echo "kai ".$lng_abbr." url '".$url."'<br>";
//
//                    $urls[$lng_abbr] = str_replace("-", "__", $url); //
//                }
//
//                $needToUnset = [];
//                foreach ($urls as $lng_abbr => $url) {
//                    if ($lng_abbr != "en") {
//                        if ($url == $urls["en"])
//                            $needToUnset[] = $lng_abbr;
//                    }
//                }
//                foreach ($needToUnset as $lng_abbr) {
//                    unset($urls[$lng_abbr]);
//                }
//                /*echo "<br>";
//                foreach ($urls as $lng_abbr => $url) {
//                    echo "kaiaa ".$lng_abbr.", ".$url."<br>";
//                }*/
//
//                $geoItemKey = $this->migrationHelper->stripKey($mysqlGeoKey);
//                //echo "cia key ".$mysqlGeoKey." ".$geoItemKey."<br><br>";
//                if (count($urls) > 0) {
//                    $data = [
//                        "index" => "geotree",
//                        "type" => "geoitem",
//                        "body" => [
//                            "query" => [
//                                "bool" => [
//                                    "must" => [
//                                        ["term" => [
//                                            "key" => $geoItemKey
//                                        ]],
//                                        ["term" => [
//                                            "type" => [
//                                                "value" => GeoTreeItem::TYPE_GEOITEM
//                                            ]
//                                        ]]
//                                    ]
//                                ]
//                            ]
//                        ]
//                    ];
//                    $response = new ESSearchGeoItemResponse($this->esClient->getEsClient()->search($data));
//                    $geoTreeItems = $response->getItems();
//                    //echo "item rado ".count($response->getItems())."<br>";
//                    foreach ($geoTreeItems as $geoTreeItem) {
//                        $esUrl = [];
//                        foreach ($urls as $abbr => $url) {
//                            $obj = [];
//                            $obj["lng"] = $abbr;
//                            $obj["url_key"] = $url;
//                            $esUrl["urls"][] = $obj;
//                        }
//                        $esUrl['id'] = $geoTreeItem->id;
//                        $esUrl['key'] = $geoItemKey;
//                        $esUrl['type'] = $geoTreeItem->type;
//                        //$geoTreeItem->redirect_urls = array_flip($urls);
//                        $esUrls[] = $esUrl;
//                    }
//                }
//            }
//        }
//
//        //echo "aaa ".count($esUrls)."<br>";
//        if (!empty($esUrls)) {
//            $result = $this->esClient->bulkUpdate($esUrls, "geourls", "urls");
//            if (!$result["es_response"]["errors"]) {
//                $r = app('db')->connection('mysql2')
//                    ->select('update geo_trees set migrated=1 where id in ('.implode(", ", $geoTreeIds).')');
//                echo "<strong>Success!</strong>";
//            } else {
//                echo "<strong>Failed!</strong>";
//                echo "<pre>";
//                print_r($result);
//                echo "</pre>";
//            }
//        } else {
//            echo "Empyt run";
//        }
    }

}