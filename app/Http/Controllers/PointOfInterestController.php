<?php

namespace App\Http\Controllers;

use App\Responses\JsonApiResponseBuilder;
use App\Models\PointOfInterest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PointOfInterestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get points of interest
     *
     * @return mixed
     */
    public function index()
    {
        $query = PointOfInterest::filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();
        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

    /**
     * Get point of interest
     * @param int $id
     *
     * @return mixed
     */
    public function show($id)
    {
        $item = PointOfInterest::fieldsFromInput()
            ->withRelationsFromInput()
            ->findOrFail($id);

        return Response::jsonApi($item);
    }


}
