<?php

namespace App\Http\Controllers;

use App\Responses\JsonApiResponseBuilder;
use App\Models\Accommodation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AccommodationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get accommodation list
     *
     * @return mixed
     */
    public function index()
    {
        $query = Accommodation::filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

    /**
     * Get accommodation
     * @param int $id
     *
     * @return mixed
     */
    public function show($id)
    {
        $item = Accommodation::fieldsFromInput()
            ->withRelationsFromInput()
            ->findOrFail($id);

        return Response::jsonApi($item);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'present|required',
            'address_website' => 'present',
            'address_street' => 'present|required',
            'address_housenumber' => 'present|required',
            'address_housenumber_addition' => 'present',
            'address_postcode' => 'present|required',
            'address_gps_lat' => 'present|numeric|nullable',
            'address_gps_lon' => 'present|numeric|nullable',
            'address_phone_1' => 'present|max:50',
            'address_phone_2' => 'present|max:50',
            'address_email_public' => 'present',
            'social_facebook' => 'present',
            'social_twitter' => 'present',
            'social_foursquare' => 'present',
            'customer_rating_average' => 'present',
            'customer_rating_count' => 'present',
            'official_rating' => 'present',
            'popularity' => 'present',
            'size_nr_total' => 'present',
            'size_nr_recreational_pitches' => 'present',
            'size_nr_year_pitches' => 'present',
            'size_nr_mobilehomerentals' => 'present',
            'size_nr_houseorappartments' => 'present',
            'price_highseason' => 'present',
            'price_adult' => 'present',
            'price_animal' => 'present',
            'price_camper' => 'present',
            'price_caravan' => 'present',
            'price_tent' => 'present',
            'general_nudist_acco' => 'present',
            'distance_to_highway' => 'present',
            'business_address_street' => 'present',
            'business_address_housenumber' => 'present',
            'business_address_housenumber_addition' => 'present',
            'business_address_postcode' => 'present',
            'business_address_city' => 'present',
            'business_address_country' => 'present',
            'business_address_phone_1' => 'present',
            'business_address_phone_2' => 'present',
            'business_address_email' => 'present',
            'size_area_squaremeter' => 'present',
            'geo_trees_id' => 'present|exists:geo_trees,id',
            'status' => 'present|between:0,2'
        ]);

        $accommodation = Accommodation::create([
            'name' => $request->input('name'),
            'address_website' => $request->input('address_website'),
            'address_street' => $request->input('address_street'),
            'address_housenumber' => $request->input('address_housenumber'),
            'address_housenumber_addition' => $request->input('address_housenumber_addition'),
            'address_postcode' => $request->input('address_postcode'),
            'address_gps_lat' => $request->input('address_gps_lat') ? $request->input('address_gps_lat') : null,
            'address_gps_lon' => $request->input('address_gps_lon') ? $request->input('address_gps_lon') : null,
            'address_phone_1' => $request->input('address_phone_1'),
            'address_phone_2' => $request->input('address_phone_2'),
            'address_email_public' => $request->input('address_email_public'),
            'social_facebook' => $request->input('social_facebook'),
            'social_twitter' => $request->input('social_twitter'),
            'social_foursquare' => $request->input('social_foursquare'),
            'customer_rating_average' => $request->input('customer_rating_average'),
            'customer_rating_count' => $request->input('customer_rating_count') ? $request->input('customer_rating_count') : null,
            'official_rating' => $request->input('official_rating') ? $request->input('official_rating') : null,
            'popularity' => $request->input('popularity'),
            'size_nr_total' => $request->input('size_nr_total') ? $request->input('size_nr_total') : null,
            'size_nr_recreational_pitches' => $request->input('size_nr_recreational_pitches') ? $request->input('size_nr_recreational_pitches') : null,
            'size_nr_year_pitches' => $request->input('size_nr_year_pitches') ? $request->input('size_nr_year_pitches') : null,
            'size_nr_mobilehomerentals' => $request->input('size_nr_mobilehomerentals') ? $request->input('size_nr_mobilehomerentals') : null,
            'size_nr_houseorappartments' => $request->input('size_nr_houseorappartments') ? $request->input('size_nr_houseorappartments') : null,
            'price_highseason' => $request->input('price_highseason'),
            'price_adult' => $request->input('price_adult'),
            'price_animal' => $request->input('price_animal'),
            'price_camper' => $request->input('price_camper'),
            'price_caravan' => $request->input('price_caravan'),
            'price_tent' => $request->input('price_tent'),
            'general_nudist_acco' => $request->input('general_nudist_acco') ? $request->input('general_nudist_acco') : null,
            'distance_to_highway' => $request->input('distance_to_highway') ? $request->input('distance_to_highway') : null,
            'business_address_street' => $request->input('business_address_street'),
            'business_address_housenumber' => $request->input('business_address_housenumber'),
            'business_address_housenumber_addition' => $request->input('business_address_housenumber_addition'),
            'business_address_postcode' => $request->input('business_address_postcode'),
            'business_address_city' => $request->input('business_address_city'),
            'business_address_country' => $request->input('business_address_country'),
            'business_address_phone_1' => $request->input('business_address_phone_1'),
            'business_address_phone_2' => $request->input('business_address_phone_2'),
            'business_address_email' => $request->input('business_address_email'),
            'size_area_squaremeter' => $request->input('size_area_squaremeter') ? $request->input('size_area_squaremeter') : null,
            'status' => $request->input('status'),
            'geo_trees_id' => $request->input('geo_trees_id'),
            'migrated' => null,
        ]);

        return Response::jsonApi($accommodation);
    }
    
    /**
     * Update accommodations table entry
     * @param Request $request
     * @param int Id
     * @return mixed
     *
     */
    public function update(Request $request, $id)
    {
        $accommodation = Accommodation::findOrException($id);
        $this->validate($request, [
            'name' => 'present|required',
            'address_website' => 'present',
            'address_street' => 'present|required',
            'address_housenumber' => 'present|required',
            'address_housenumber_addition' => 'present',
            'address_postcode' => 'present|required',
            'address_gps_lat' => 'present|numeric|nullable',
            'address_gps_lon' => 'present|numeric|nullable',
            'address_phone_1' => 'present|max:50',
            'address_phone_2' => 'present|max:50',
            'address_email_public' => 'present',
            'social_facebook' => 'present',
            'social_twitter' => 'present',
            'social_foursquare' => 'present',
            'customer_rating_average' => 'present',
            'customer_rating_count' => 'present',
            'official_rating' => 'present',
            'popularity' => 'present',
            'size_nr_total' => 'present',
            'size_nr_recreational_pitches' => 'present',
            'size_nr_year_pitches' => 'present',
            'size_nr_mobilehomerentals' => 'present',
            'size_nr_houseorappartments' => 'present',
            'price_highseason' => 'present',
            'price_adult' => 'present',
            'price_animal' => 'present',
            'price_camper' => 'present',
            'price_caravan' => 'present',
            'price_tent' => 'present',
            'general_nudist_acco' => 'present',
            'distance_to_highway' => 'present',
            'business_address_street' => 'present',
            'business_address_housenumber' => 'present',
            'business_address_housenumber_addition' => 'present',
            'business_address_postcode' => 'present',
            'business_address_city' => 'present',
            'business_address_country' => 'present',
            'business_address_phone_1' => 'present',
            'business_address_phone_2' => 'present',
            'business_address_email' => 'present',
            'size_area_squaremeter' => 'present',
            'geo_trees_id' => 'present|exists:geo_trees,id',
            'status' => 'present|between:0,2'
        ]);

        $accommodation->update([
            'name' => $request->input('name'),
            'address_website' => $request->input('address_website'),
            'address_street' => $request->input('address_street'),
            'address_housenumber' => $request->input('address_housenumber'),
            'address_housenumber_addition' => $request->input('address_housenumber_addition'),
            'address_postcode' => $request->input('address_postcode'),
            'address_gps_lat' => $request->input('address_gps_lat') ? $request->input('address_gps_lat') : null,
            'address_gps_lon' => $request->input('address_gps_lon') ? $request->input('address_gps_lon') : null,
            'address_phone_1' => $request->input('address_phone_1'),
            'address_phone_2' => $request->input('address_phone_2'),
            'address_email_public' => $request->input('address_email_public'),
            'social_facebook' => $request->input('social_facebook'),
            'social_twitter' => $request->input('social_twitter'),
            'social_foursquare' => $request->input('social_foursquare'),
            'customer_rating_average' => $request->input('customer_rating_average'),
            'customer_rating_count' => $request->input('customer_rating_count') ? $request->input('customer_rating_count') : null,
            'official_rating' => $request->input('official_rating') ? $request->input('official_rating') : null,
            'popularity' => $request->input('popularity'),
            'size_nr_total' => $request->input('size_nr_total') ? $request->input('size_nr_total') : null,
            'size_nr_recreational_pitches' => $request->input('size_nr_recreational_pitches') ? $request->input('size_nr_recreational_pitches') : null,
            'size_nr_year_pitches' => $request->input('size_nr_year_pitches') ? $request->input('size_nr_year_pitches') : null,
            'size_nr_mobilehomerentals' => $request->input('size_nr_mobilehomerentals') ? $request->input('size_nr_mobilehomerentals') : null,
            'size_nr_houseorappartments' => $request->input('size_nr_houseorappartments') ? $request->input('size_nr_houseorappartments') : null,
            'price_highseason' => $request->input('price_highseason'),
            'price_adult' => $request->input('price_adult'),
            'price_animal' => $request->input('price_animal'),
            'price_camper' => $request->input('price_camper'),
            'price_caravan' => $request->input('price_caravan'),
            'price_tent' => $request->input('price_tent'),
            'general_nudist_acco' => $request->input('general_nudist_acco') ? $request->input('general_nudist_acco') : null,
            'distance_to_highway' => $request->input('distance_to_highway') ? $request->input('distance_to_highway') : null,
            'business_address_street' => $request->input('business_address_street'),
            'business_address_housenumber' => $request->input('business_address_housenumber'),
            'business_address_housenumber_addition' => $request->input('business_address_housenumber_addition'),
            'business_address_postcode' => $request->input('business_address_postcode'),
            'business_address_city' => $request->input('business_address_city'),
            'business_address_country' => $request->input('business_address_country'),
            'business_address_phone_1' => $request->input('business_address_phone_1'),
            'business_address_phone_2' => $request->input('business_address_phone_2'),
            'business_address_email' => $request->input('business_address_email'),
            'size_area_squaremeter' => $request->input('size_area_squaremeter') ? $request->input('size_area_squaremeter') : null,
            'status' => $request->input('status'),
            'geo_trees_id' => $request->input('geo_trees_id'),
            'migrated' => null,
        ]);

        return Response::jsonApi($accommodation);
    }
}
