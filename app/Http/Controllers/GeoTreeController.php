<?php

namespace App\Http\Controllers;

use App\Models\GeoTree;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class GeoTreeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get geo tree items list
     *
     * @return mixed
     */
    public function index()
    {


        $query = GeoTree::filtersFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

    /**
     * Get geo tree
     * @param int $id
     *
     * @return mixed
     */
    public function show($id)
    {
        $item = GeoTree::fieldsFromInput()
            ->withRelationsFromInput()
            ->findOrFail($id);
        return Response::jsonApi($item);
    }

    /**
     * Update accommodations table entry
     * @param Request $request
     * @param int Id
     * @return mixed
     *
     */
    public function update(Request $request, $id)
    {

        $geoTree = GeoTree::findOrException($id);

        $gpsPoint = null;
        $gpsPolygon = null;
        $keyShort = $geoTree->key_short;

        $this->validate($request, [
            'name' => 'present|required',
            'parent_short' => 'present|required|integer',
            'idGeonames' => 'present|required|integer',
            'geonamesFcl' => 'present',
            'geonamesFcode' => 'present|required',
            'continentCode' => 'present|required|size:2',
            'countryCode' => 'present|required|size:2',
            'population' => 'present|max:10000000000',
            'elevation' => 'present|max:10000000000',
            'timezoneGmtOffset' => 'present|between:-12,14',
            'timezoneDstOffset' => 'present|between:-12,14',
            'wikipediaLink' => 'present',
            'popularity' => 'present|between:-12,14',
            'show_on_acco_whattodo' => 'present|integer',
            'gpsLat' => 'present|numeric',
            'gpsLon' => 'present|numeric',
            'gpsBboxEast' => 'present|numeric',
            'gpsBboxSouth' => 'present|numeric',
            'gpsBboxNorth' => 'present|numeric',
            'gpsBboxWest' => 'present|numeric',
        ]);

        if ($geoTree->parent_short !== $request->input('parent_short')) {
            $parentKeyShortOld = GeoTree::where('id', $geoTree->parent_short)->first()->key_short;
            $parentKeyShortNew = GeoTree::where('id', $request->input('parent_short'))->first()->key_short;
            $keyShort = replaceFirstStringMatch($geoTree->key_short, $parentKeyShortOld, $parentKeyShortNew);
        }

        if ($request->input('gpsLat') && $request->input('gpsLon')) {
            $gpsPoint = \DB::raw("GeomFromText('POINT(" . $request->input('gpsLat') . " " . $request->input('gpsLon') . ")')");
        }

        if ($request->input('gpsBboxNorth') && $request->input('gpsBboxSouth') &&
            $request->input('gpsBboxEast') && $request->input('gpsBboxWest')) {
            $gpsPolygon = \DB::raw("GeomFromText('POLYGON((" .
                $request->input('gpsBboxSouth') . " " . $request->input('gpsBboxWest') . ", " .
                $request->input('gpsBboxSouth') . " " . $request->input('gpsBboxEast') . ", " .
                $request->input('gpsBboxNorth') . " " . $request->input('gpsBboxEast') . ", " .
                $request->input('gpsBboxNorth') . " " . $request->input('gpsBboxWest') . ", " .
                $request->input('gpsBboxSouth') . " " . $request->input('gpsBboxWest') . "))')");
        }

        $geoTree->update([
            'name' => $request->input('name'),
            'parent_short' => $request->input('parent_short'),
            'key_short' => $keyShort,
            'idGeonames' => $request->input('idGeonames'),
            'geonamesFcl' => $request->input('geonamesFcl'),
            'geonamesFcode' => $request->input('geonamesFcode'),
            'continentCode' => $request->input('continentCode'),
            'countryCode' => $request->input('countryCode'),
            'population' => $request->input('population'),
            'elevation' => $request->input('elevation'),
            'timezoneGmtOffset' => $request->input('timezoneGmtOffset'),
            'timezoneDstOffset' => $request->input('timezoneDSToffset'),
            'wikipediaLink' => $request->input('wikipediaLink'),
            'popularity' => $request->input('popularity'),
            'show_on_acco_whattodo' => $request->input('show_on_acco_whattodo'),
            'gpsLat' => $request->input('gpsLat') ? $request->input('gpsLat') : null,
            'gpsLon' => $request->input('gpsLon') ? $request->input('gpsLon') : null,
            'gpsBboxNorth' => $request->input('gpsBboxNorth') ? $request->input('gpsBboxNorth') : null,
            'gpsBboxSouth' => $request->input('gpsBboxSouth') ? $request->input('gpsBboxSouth') : null,
            'gpsBboxEast' => $request->input('gpsBboxEast') ? $request->input('gpsBboxEast') : null,
            'gpsBboxWest' => $request->input('gpsBboxWest') ? $request->input('gpsBboxWest') : null,
            'gpsPoint' => $gpsPoint,
            'gpsPolygon' => $gpsPolygon,
            'migrated' => 0,
        ]);

        return Response::jsonApi($geoTree);

    }


}