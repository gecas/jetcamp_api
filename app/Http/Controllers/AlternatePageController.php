<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Url;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AlternatePageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Get alternate urls for provided url and lang pair
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'filter.lang' => 'required|size:2',
            'filter.url' => 'required',
        ]);

        $url = Url::where('lang', $request->input('filter.lang'))
            ->where('url', $request->input('filter.url'))
            ->firstOrFail();

        $activeLanguages = Language::frontActive()->get()->pluck('abbrevation');

        $alternates = Url::whereIn('lang', $activeLanguages)
            ->where('subject_id', $url->subject_id)
            ->where('page_type', $url->page_type)
            ->where('lang', '!=', $url->lang)
            ->get();

        return Response::jsonApi($alternates);
    }
}
