<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiValidationException;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Stores user register params
     *
     */
    public function register(Request $request)
    {
        $validation = app('validator')->make($request->input(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:2',
        ]);

        if ($validation->fails()) {
            throw new ApiValidationException($validation->errors()->toArray());
        }
       // dd(app('hash')->make($request->input('password')));
        $user = User::create([
            'name' => strtolower($request->input('name')),
            'email' => strtolower($request->input('email')),
            'password' => app('hash')->make($request->input('password')),
        ]);

        //$user->api_token = User::generateRememberToken(app(\App\RandomGenerator::class));
        $user->save();
       // return response()->json($user);
        return response(json_encode($user), 201);
    }
}
