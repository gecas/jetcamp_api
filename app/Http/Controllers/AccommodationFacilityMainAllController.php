<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use App\Models\AccommodationFacilityMainAll;
use Illuminate\Http\Response;

class AccommodationFacilityMainAllController extends Controller
{
    /**
     * Get list of accommodation and all facility mains list
     *
     * @param $id Accommodation ID
     *
     * @return mixed
     */
    public function index()
    {
        $query = AccommodationFacilityMainAll::filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

}