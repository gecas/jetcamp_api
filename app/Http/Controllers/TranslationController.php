<?php

namespace App\Http\Controllers;

use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TranslationController extends Controller
{
    public function index(Request $request)
    {
        $builder = Translation::query();

        if ($request->input('filter.key')) {
            $builder->where('key', 'like', "%{$request->input('filter.key')}%");
        }

        if ($request->input('filter.group')) {
            $builder->where('group', 'like', "%{$request->input('filter.group')}%");
        }

        if ($request->input('filter.project')) {
            $builder->where('project', 'like', "%{$request->input('filter.project')}%");
        }

        if ($request->input('filter.locale')) {
            $builder->where('locale', 'like', "%{$request->input('filter.locale')}%");
        }

        return Response::jsonApi($builder->get());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            '*.project' => 'required|max:3',
            '*.locale' => 'required|max:2',
            '*.group' => 'required|max:100',
            '*.key' => 'required|max:255',
            '*.text' => 'required',
        ]);

        $translations = [];
        $items = $request->all();
        foreach ($items as $item) {
            $translations[] = Translation::updateOrCreate([
                'project' => trim($item['project']),
                'locale' => trim($item['locale']),
                'group' => trim(strtolower($item['group'])),
                'key' => trim(strtolower($item['key']))
            ],
                [
                    'text' => $item['text']
                ]
            );
        }

        return Response::jsonApi($translations);
    }
}