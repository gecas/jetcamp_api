<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use App\Models\AccommodationFacilitySub;
use Illuminate\Http\Response;
use App\Models\FacilitySub;

class AccommodationFacilitySubController extends Controller
{
    /**
     * Assign facility sub to accommodation
     *
     * @param integer $accommodationId Accommodation ID
     * @param integer $facilitySubId Facility sub ID
     * @return mixed
     */
    public function store($accommodationId, $facilitySubId)
    {
        $accommodation = Accommodation::findOrFail($accommodationId);
        $facilitySub = FacilitySub::findOrFail($facilitySubId);
        $pivot = new AccommodationFacilitySub();
        $pivot->accommodation_id = $accommodation->id;
        $pivot->facility_sub_id = $facilitySub->id;
        $pivot->save();
        return Response::jsonApi('', null, 201);
    }

    /**
     * Detach facility sub from accommodation
     *
     * @param integer $accommodationId Accommodation ID
     * @param integer $facilitySubId Facility sub ID
     * @return mixed
     */
    public function destroy($accommodationId, $facilitySubId)
    {
        $accommodation = Accommodation::findOrFail($accommodationId);
        $facilitySub = FacilitySub::findOrFail($facilitySubId);
        $accommodation->facility_subs()->detach($facilitySub->id);
        return Response::jsonApi('', null, 204);
    }

}