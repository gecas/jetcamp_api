<?php

namespace App\Http\Controllers;

use App\Models\FacilityTheme;
use Illuminate\Http\Response;

class FacilityThemeController extends Controller
{
    /**
     * Get facilities list
     *
     * @return mixed
     */
    public function index()
    {
        $query = FacilityTheme::fieldsFromInput()->withRelationsFromInput();
        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

}