<?php

namespace App\Http\Controllers;

use App\Models\GeoTreeTranslation;
use App\Models\Language;
use App\Responses\JsonApiResponseBuilder;
use App\Models\GeoTree;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class GeoTreeTranslationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Update translation. Delete translation if value is empty
     *
     * @return mixed
     */
    public function update(Request $request, $geoTreeId, $languageAbbr)
    {

        $geoTree = GeoTree::findOrFail($geoTreeId);
        $language = Language::where('abbrevation', $languageAbbr)->firstOrFail();

        $this->validate($request, [
            'translation' => 'present',
        ]);

        $translation = GeoTreeTranslation::where('key', $geoTree->key)
            ->where('language_id', $language->id)
            ->first();

        //if translation is empty string
        if (!$request->input('translation')) {
            //if translation already exist delete it
            if ($translation) {
                $translation->delete();
            }

            return Response::jsonApi('', null, 204);
        }

        $translation = GeoTreeTranslation::updateOrCreate(
            ['key' => $geoTree->key, 'language_id' => $language->id],
            [
                'translation' => $request->input('translation'),
                'language_id' => $language->id,
                'key' => $geoTree->key,
                'user_id' => Auth::id(),
                'language_abbrevation' => $languageAbbr,
                'useEn' => 0,
                'idGeonames' => $geoTree->idGeonames,
                'translation_simple' => str_slug($request->input('translation'), "")
            ]
        );

        return Response::jsonApi($translation);
    }
}