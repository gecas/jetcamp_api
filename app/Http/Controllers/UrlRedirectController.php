<?php

namespace App\Http\Controllers;

use App\Models\UrlRedirect;
use App\Responses\JsonApiResponseBuilder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UrlRedirectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get redirect
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        $query = UrlRedirect::filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }
}