<?php

namespace App\Http\Controllers;

use App\Models\PointOfInterestTranslation;
use App\Models\Language;
use App\Responses\JsonApiResponseBuilder;
use App\Models\PointOfInterest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PointOfInterestTranslationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Update translation. Delete translation if value is empty
     *
     * @return mixed
     */
    public function update(Request $request, $poiId, $languageAbbr)
    {

        $poi = PointOfInterest::findOrFail($poiId);
        $language = Language::where('abbrevation', $languageAbbr)->firstOrFail();

        $this->validate($request, [
            'translation' => 'present',
        ]);

        $translation = PointOfInterestTranslation::where('key', $poi->key)
            ->where('language_id', $language->id)
            ->first();

        //if translation is empty string
        if (!$request->input('translation')) {
            //if translation already exist delete it
            if ($translation) {
                $translation->delete();
            }

            return Response::jsonApi('', null, 204);
        }

        $translation = PointOfInterestTranslation::updateOrCreate(
            ['key' => $poi->key, 'language_id' => $language->id],
            [
                'translation' => $request->input('translation'),
                'language_id' => $language->id,
                'key' => $poi->key,
                'user_id' => Auth::id(),
                'pois_id' => $poi->id,
            ]
        );

        return Response::jsonApi($translation);
    }
}