<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use App\Models\AccommodationFacilitySubAll;
use Illuminate\Http\Response;

class AccommodationFacilitySubAllController extends Controller
{
    /**
     * Get list of accommodation and all facility subs list
     *
     * @param $id Accommodation ID
     *
     * @return mixed
     */
    public function index()
    {
        $query = AccommodationFacilitySubAll::filtersFromInput()
            ->fieldsFromInput()
            ->withRelationsFromInput();

        $count = $query->count();
        $items = $query->constraintsFromInput()->get();

        return Response::jsonApi($items, $count);
    }

}