<?php

namespace App\Http\Controllers;

use App\DTOs\AccommodationFacilityDTO;
use App\Models\Accommodation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AccommodationFacilitiesController extends Controller
{
    /**
     * Get accommodation facilities in hierarchical view
     *
     * @param $id Accommodation ID
     *
     * @return mixed
     */
    public function show($accommodationId)
    {
        $accommodation = \App\Models\Accommodation::findOrFail($accommodationId);

        $accommodationSubs = $accommodation->facility_subs->transform(function ($sub) {
            return [
                'facility_sub_id' => $sub->id,
                'description' => $sub->description,
                'facility_main_id' => $sub->facility_main_id,
            ];
        });

        $accommodatioMains = $accommodation->facility_mains()
            ->with('facility_theme')//So we don't need to query for facility name later on
            ->get()
            ->transform(function ($main) {
                return [
                    'facility_main_id' => $main->id,
                    'description' => $main->description,
                    'theme_description' => $main->facility_theme->description,
                    'facility_theme_id' => $main->facility_theme_id,
                ];
            });

        //By grouping mains we get themes with mains in it
        $themes = $accommodatioMains->groupBy('facility_theme_id');

        $themes->transform(function ($mains, $themeId) use (&$accommodationSubs) {
            $theme = [
                'theme_id' => $themeId,
                'description' => $mains[0]['theme_description'], //This is eager loaded
                'mains' => [],
            ];

            $mains->transform(function ($main) use (&$accommodationSubs) {
                $main['subs'] = [];

                //After checking whether the sub needs to go into specific main
                //we remove it from subs list so that it does not need to checked again
                $accommodationSubs = $accommodationSubs->filter(function ($sub) use (&$main) {
                    if ($sub['facility_main_id'] == $main['facility_main_id']) {
                        $main['subs'][] = $sub;
                        return true;
                    } else {
                        return false;
                    }
                });

                return $main;
            });

            $theme['mains'] = $mains->toArray();

            return $theme;
        });

        return Response::jsonApi(new AccommodationFacilityDTO($accommodation->id, $themes));
    }

}