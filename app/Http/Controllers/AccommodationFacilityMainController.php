<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use App\Models\FacilityMain;
use Illuminate\Http\Response;

class AccommodationFacilityMainController extends Controller
{
    /**
     * Assign facility main to accommodation
     *
     * @param integer $accommodationId Accommodation ID
     * @param integer $facilityMainId Facility main  ID
     * @return mixed
     */
    public function store($accommodationId, $facilityMainId)
    {
        $accommodation = Accommodation::findOrFail($accommodationId);
        $facilityMain = FacilityMain::findOrFail($facilityMainId);
        $accommodation->facility_mains()->attach($facilityMain->id);
        return Response::jsonApi('', null, 201);
    }

    /**
     * Detach facility main from accommodation
     *
     * @param integer $accommodationId Accommodation ID
     * @param integer $facilityMainId Facility main  ID
     * @return mixed
     */
    public function destroy($accommodationId, $facilityMainId)
    {
        $accommodation = Accommodation::findOrFail($accommodationId);
        $facilityMain = FacilityMain::findOrFail($facilityMainId);
        $accommodation->facility_mains()->detach($facilityMain->id);
        return Response::jsonApi('', null, 204);
    }

}