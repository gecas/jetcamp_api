<?php

namespace App\Http\Controllers;

use App\AccommodationPhoto;
use App\ElasticSearchAPI\ESClient;
use App\ElasticSearchAPI\ESSearchResponse;
use App\ElasticSearchAPI\GeoTreeItem;
use App\ElasticSearchAPI\GeoTreeItemHelper;
use App\JetCamp\JCCampingPageResponse;
use App\JetCamp\JCRequest;
use App\JetCamp\JCSearchResponse;
use App\Services\BreadcrumbService;
use App\Services\UrlsService;
use App\Models\Url;
use Illuminate\Http\Request;
use App\ElasticSearchAPI\ESSearchGeoItemResponse;
use DB;
use App\Accommodation;
use Illuminate\Support\Facades\Storage;
use App\Responses\JsonApiResponseBuilder;

class SearchController extends Controller
{

    private $esClient;
    private $urlsService;
    private $breadcrumbService;

    public function __construct(ESClient $esClient, UrlsService $urlsService, BreadcrumbService $breadcrumbService)
    {
        $this->esClient = $esClient;
        $this->urlsService = $urlsService;
        $this->breadcrumbService = $breadcrumbService;
    }

    public function test(Request $request) {

        $result["results"] = [
            ["basics_namecamping" => "Geo location 1", "address_street" => "Geo location 1 description"],
            ["basics_namecamping" => "Geo location 2", "address_street" => "Geo location 2 description"],
            ["basics_namecamping" => http_build_query($request->input()), "address_street" => "desc"]
        ];
        $params = $request->input();

      /*  $params["lng"] = 'sl';
        $jcSearchResponse = $this->esClient->filterSearchFaceded($params);
        $jcSearchResponse = $this->esClient->searchWithAggregatedRatings($params);
        $jcSearchResponse = $this->esClient->searchAutocomplete($params);
        $jcSearchResponse = $this->esClient->searchForCommercialRegionLandingPage(["item_id" => "1-3"]);
        $jcSearchResponse = $this->esClient->searchForCampingLandingPage(["item_id" => "1-2"]);
        $params["url_parts"] = ["germany"];
        $jcSearchResponse = $this->esClient->searchByUrl($params);

        $params = [
            ["52.3157","5.0603"],
            ["51.8566","5.0603"],
            ["51.8566","4.3133"],
            ["52.3157","4.3133"],
            ["52.3157","5.0603"]
        ];
        $params = [
            [5.0603, 52.3157],
            [5.0603, 51.8566],
            [4.3133, 51.8566],
            [4.3133, 52.3157],
            [5.0603, 52.3157]
        ];
        $jcResponse = $this->esClient->zoomSearchFaceted($params);

        $jcRequest = new JCRequest($request->input());
        $jcRequest->setLng('en');
//        $jcSearchResponse = $this->esClient->filterSearchFaceded($params);
//        $jcSearchResponse = $this->esClient->searchWithAggregatedRatings($params);
        $campingsCountries = $this->CampingsCountries($request);
        echo "<pre>";
        var_dump($campingsCountries);
        echo "\n";
        echo "</pre>";
        die;
//        $jcSearchResponse = $this->esClient->searchAutocomplete($jcRequest);
//        $jcSearchResponse = $this->esClient->searchForCommercialRegionLandingPage(["item_id" => "1-3"]);
//        $jcSearchResponse = $this->esClient->searchForCampingLandingPage(["item_id" => "1-2"]);
//        $params["url_parts"] = ["germany"];
//        $jcSearchResponse = $this->esClient->searchByUrl($params);

//        $params = [
//            ["52.3157","5.0603"],
//            ["51.8566","5.0603"],
//            ["51.8566","4.3133"],
//            ["52.3157","4.3133"],
//            ["52.3157","5.0603"]
//        ];
//        $params = [
//            [5.0603, 52.3157],
//            [5.0603, 51.8566],
//            [4.3133, 51.8566],
//            [4.3133, 52.3157],
//            [5.0603, 52.3157]
//        ];
//        $jcResponse = $this->esClient->zoomSearchFaceted($params);

//        var_dump($campingsCountries->original["items"]);
        $ctrl = [];
        $countries = $campingsCountries->original["items"];
        foreach ($countries as $key => $item)
        {
            $type = explode("-", $item->id);
            $type = $type[1];

            if($type == 1)
            {
                $ctrl[$key]["id"] = $item->id;
                $ctrl[$key]["key"] = $item->key;
                $ctrl[$key]["subtype"] = $item->subType;

                $url = DB::table('urls')->where('subject_id', $item->id)->where('lang', "en")->first();

                if(isset($url->url))
                {
                    $ctrl[$key]["url"] = $url->url;
                }else{
                    $ctrl[$key]["url"] = "";
                }


                foreach($item->translations as $trans)
                {

                    if($trans["lng"] == "en")
                    {
                        $ctrl[$key]["trans"] = $trans["trans"];
                    }
                }
            }


        }
        echo "<pre>";
        var_dump($ctrl);
        echo "\n";
        echo "</pre>";
*/

//        foreach ($campingsCountries->params["items"] as $key => $item){
//            echo "<pre>";
//            var_dump($item["id"]);
//            echo "\n";
//            echo "</pre>";
//        }
//        print_r($jcSearchResponse->getResponseParameters());

        // campings urls
        //$p["url_parts"] = explode("/", "germany/blankenheim/blankenheim/kreis-euskirchen/cologne-district/north-rhine-westphalia/eifel-camp-freilinger-see");
        //$p["url_parts"] = explode("/", "germany/campen/krummhoern/landkreis-aurich/lower-saxony/camping-am-deich-nordsee");
        //$p["url_parts"] = explode("/", "germany/fehmarn/fehmarn/kreis-ostholstein/schleswig-holstein/camping-suedstrand");
        //$p["url_parts"] = explode("/", "republic-of-france/le-grau-du-roi/arrondissement-de-nimes/gard/languedoc-roussillon/occitanie/camping-bon-sejour-nbsp"); // with lots of campings near by
        //$p["url_parts"] = explode("/", "frantsiya/camping-municipalcamping-municipal"); // camping page
        //$p["url_parts"] = explode("/", "campings/andorra/canillo/el-tarter"); // camping destination page
        $p["url_parts"] = explode("/", "campings/andorra/la-massana/xixerella"); // camping destination page
//        $p["url_parts"] = explode("/", "andorra/la-massana/la-massana/camping-santa-catarina"); // camping result page with commercial region
//        $p["url_parts"] = explode("/", "netherlands/south-holland/noordwijkerhout");
//        $p["url_parts"] = explode("/", "italy/veneto/provincia-di-verona/valeggio-sul-mincio/camping-family-park-altomincio");
//        $p["url_parts"] = explode("/", "italy");

        //$p["url_parts"] = explode("/", "andorra/la-massana");
        //$p["url_parts"] = explode("/", "italy/rossetti/provincia-di-ferrara/emilia-romagna/campingiserand");
        //$p["url_parts"] = explode("/", "belgium/de-bijl/arrondissement-sint-niklaas/east-flanders-province/flanders/campingboerderijwelkom");
        //$p["url_parts"] = explode("/", "italy/olmi/provincia-di-vicenza/veneto/campeggio-kursaal");
//        $p["url_parts"] = explode("/", "italy/lake-garda");
//
//        $query = "sort=popularity-desc";
//        parse_str($query, $url_query_parts);
//        $p["url_query_parts"] = $url_query_parts;



        // destination urls
        //$p["url_parts"] = explode("/", "germany");

        // POI urls
        //$p["url_parts"] = explode("/", "spain/other-fun-games/eifelpark");

        // Commercial region url
        //$p["url_parts"] = explode("/", "italy/lake-garda");

        //$query = "rating=5&facilities=kidsandfamilyfriendly&sort=popularity-desc";
        //$query = "rating=1&facilities=pooloutdoor";
       // $query = "plotsize=100-400";
//        $query = "sportgames=tennis";
      //  parse_str($query, $url_query_parts);
       // $p["url_query_parts"] = $url_query_parts;

        $p["lng"] = "en";
        $req = app('Illuminate\Http\Request');
        $req->request->add($p);
        $rsp = $this->searchByUrl($req);
        echo "<pre>";
        print_r(json_decode($rsp->content()));
        echo "</pre>";
        /*echo "<pre>";
        print_r($esSearchResponse->getRawResponse());
        echo "</pre>";*/
//        $params = [
//            [5.0603, 52.3157],
//            [5.0603, 51.8566],
//            [4.3133, 51.8566],
//            [4.3133, 52.3157],
//            [5.0603, 52.3157]
//        ];
//        $params['coords'] = [
//            [10.8811, 45.7946],
//            [10.8811, 45.5335],
//            [10.5075, 45.5335],
//            [10.5075, 45.7946],
//            [10.8811, 45.7946]
//        ];
//        $params['coords'] = [
//            [10.6963, 45.5691],
//            [10.6963, 45.4382],
//            [10.5096, 45.4382],
//            [10.5096, 45.5691],
//            [10.6963, 45.5691]
//        ];

//        $params['coords'] = [
//                [10.8811, 45.7946],
//                [10.8811, 45.5335],
//                [10.5075, 45.5335],
//                [10.5075, 45.7946],
//                [10.8811, 45.7946]
//        ];

//        $params['zoom'] = 11;
//        $params['rating'] = 3;
//
//        $jcSearchResponse = $this->esClient->updateSearchFaceted($params);
//        dd($jcSearchResponse);
//
//        echo "<pre>";
//        var_dump($jcSearchResponse);
//        $params['zoom'] = 12;
//        $params['rating'] = 3;
//
//        $jcSearchResponse = $this->esClient->updateSearchFaceted($params);
//
//        dd($jcSearchResponse);
//        $slug = "republic-of-france/camping-municipalcamping-municipal/";

    }

    public function searchAutocomplete(Request $request) {
        $jcRequest = new JCRequest($request->input());
        $jcSearchResponse = $this->esClient->searchAutocomplete($jcRequest);
        return response()->json($jcSearchResponse->getResponseParameters());
    }

//    public function searchFaceted(Request $request) {
//        $jcSearchResponse = $this->esClient->searchWithAggregatedRatings($request->input());
//        return response()->json($jcSearchResponse->getResponseParameters());
//    }

//    public function filterSearchFaceted(Request $request) {
//        $jcSearchResponse = $this->esClient->filterSearchFaceded($request->input());
//        return response()->json($jcSearchResponse->getResponseParameters());
//    }

    public function updateSearchFaceted(Request $request) {
        $jcRequest = new JCRequest($request->input());
        $jcSearchResponse = $this->esClient->updateSearchFaceted($jcRequest);
        return response()->json($jcSearchResponse->getResponseParameters());
    }

    public function poisUpdateSearchFaceted(Request $request)
    {
        $jcRequest = new JCRequest($request->input());
        $jcSearchResponse = $this->esClient->poisSearchFaceted($jcRequest);
        return response()->json($jcSearchResponse->getResponseParameters());
    }

    public function searchRedirectUrl(Request $request) {
        //$input = $request->input();
        $jcRequest = new JCRequest($request->input());
        //$jcRequest->getItemId();
        $redirectUrl = $this->urlsService->getUrlByItemId($jcRequest->getItemId(), $jcRequest->getLng());
        $rsp["lng"] = $jcRequest->getLng();
        $rsp["redirect_url"] = $redirectUrl->url;
        $rsp["status"] = 1;
        //$rsp["input"] = $input;

        return response()->json($rsp);
    }

    public function searchByUrl(Request $request) {
        //$jcSearchResponse = $this->esClient->searchByUrl($request->input());
        $jcRequest = new JCRequest($request->input());
        if (!$jcRequest->getSize())
            $jcRequest->setParamValue("size", 20);
        $url = $jcRequest->getUrl();
        $urls = $this->urlsService->getUrlsByUrl($url);
        /*$jcSearchResponse = new JCSearchResponse(null);
        $jcCampingPageResponse = new JCCampingPageResponse(null);*/

        if (count($urls) > 0) {
            $item = $this->esClient->getGeoItemById($urls[0]->subject_id);
            $jcRequest->setItem($item);
            //var_dump($urls[0]);
            $breadcrumbs = $this->breadcrumbService->returnItemBreadcrumbs($jcRequest->getItem(), $jcRequest->getLng());
            switch ($urls[0]->page_type) {
                case Url::PAGE_TYPE_GEOITEM_SEARCH:
                    $jcResponse = $this->esClient->searchForDestinationLandingPage($jcRequest);
                    break;
                case Url::PAGE_TYPE_CAMPING_SEARCH:
                    $jcResponse = $this->esClient->searchForCampingLandingPage($jcRequest);
                break;
                case Url::PAGE_TYPE_POI_SEARCH:
                    $jcResponse = $this->esClient->searchForPoiLandingPage($jcRequest);
                    break;
                case Url::PAGE_TYPE_COMMERCIAL_SEARCH:
                    $jcResponse = $this->esClient->searchForCommercialRegionLandingPage($jcRequest);
                    break;
                case Url::PAGE_TYPE_CAMPING_PAGE:
                    $jcResponse = $this->esClient->retrieveCampingPagePoi($jcRequest);
//                    $jcCampingPageResponse->campingPageCampings = $this->esClient->retrieveCampingPageCampings($params);

                    /*$jcCampingPageResponse->setType($urls[0]->page_type);
                    return response()->json($jcCampingPageResponse->getResponseParameters());*/
                    break;
                    case Url::PAGE_TYPE_DESTINATION_PAGE:
                $jcResponse = $this->esClient->retrieveDestinationLandingPage($jcRequest);
                        break;
                case Url::PAGE_TYPE_POI_PAGE:
                    $jcResponse = $this->esClient->retrievePoiLandingPage($jcRequest);
                    break;
                case Url::PAGE_TYPE_COMMERCIAL_PAGE:
                    $jcResponse = $this->esClient->retrievePoiLandingPage($jcRequest);
                    break;
            }
            $jcResponse->setBreadcrumbs($breadcrumbs);
            $jcResponse->setType($urls[0]->page_type);
        }

        return response()->json($jcResponse->getResponseParameters());
    }

    public function retrieveCommercialRegions(Request $request) {
        $jcRequest = new JCRequest($request->input());
        $jcSearchResponse = $this->esClient->retrieveCommercialRegions($jcRequest);
        return response()->json($jcSearchResponse->getResponseParameters());
    }
    public function campingCnt(Request $request)
    {
        $campingsCnt = Accommodation::count();

        return response()->json(["count" => $campingsCnt]);
    }

    public function getMainItemsAttributes(Request $request)
    {
        $params = $request->input();
        $mainItem = $params["mainItem"];
        $size = null;
        $items = [];

        $item_id = explode("-", $mainItem["id"])[0];

        $item = DB::table('accommodations')->where('id', $item_id)->first();
        $subs = DB::table('accommodation_facility_subs')->where('accommodation_id', $item_id)->get();
        $sub_items = [];
        $sub_items2 = [];

        $dogs = [46,47,48,49,50];
        $payments = [7,8,9,10,11,12,13,14];

        foreach ($subs as $key => $sub)
        {
            $sub_db = DB::table('facility_subs')->where('id', $sub->facility_sub_id)->get();
            foreach ($sub_db as $v => $dbitem)
            {
                if(in_array($dbitem->id, $payments))
                {
                    array_push($sub_items, $dbitem);
                }

                elseif(in_array($dbitem->id, $dogs))
                {
                    array_push($sub_items2, $dbitem);
                }

            }
        }
        $items["facilities_payments"] = $sub_items;
        $items["facilities_dogs"] = $sub_items2;

        if($item->address_email_public)
            $items["email"] = $item->address_email_public;
        else
            $items["email"] = null;

        if($item->address_website)
            $items["website"] = $item->address_website;
        else
            $items["website"] = null;

        if($item->social_facebook)
            $items["facebook"] = $item->social_facebook;
        else
            $items["facebook"] = null;

        if($item->checkin_pre)
            $items["checkin_time"] = $item->checkin_pre;
        else
            $items["checkin_time"] = null;

        if($item->checkout_pre)
            $items["checkout_time"] = $item->checkout_pre;
        else
            $items["checkout_time"] = null;

        if($item->size_nr_total)
            $items["size"] = $item->size_nr_total;
        else
            $items["size"] = null;

        if ($item->customer_rating_count)
            $items["reviews"] = $item->customer_rating_count;
        else
        $items["reviews"] = null;

        if ($item->customer_rating_average)
            $items["average"] = $item->customer_rating_average;
        else
            $items["average"] = null;

        $items["address"] = "";
        if($item->address_street){
            $items["address"] = $items["address"].$item->address_street . ", ";
        }
        if($item->address_housenumber){
            $items["address"] =  $items["address"].$item->address_housenumber . ", ";
        }
        if($item->address_housenumber_addition){
            $items["address"] =  $items["address"].$item->address_housenumber_addition . ", ";
        }
        if($item->address_postcode){
            $items["address"] =  $items["address"].$item->address_postcode . ", ";
        }
        if($item->address_city){
            $items["address"] =  $items["address"].$item->address_city . ", ";
        }
        if($item->address_country){
            $items["address"] =  $items["address"].$item->address_country;
        }

        return response()->json($items);
    }

    public function getMainItemImages(Request $request)
    {
        $params = $request->input();
        $mainItemId = $params["mainItem_id"];
        $mainItemName = $params["mainItem_name"];

        $images = DB::table('accommodation_photos')
                            ->where('accommodation_id', $mainItemId)
                                ->leftJoin('accommodation_photo_categories', 'accommodation_photo_categories.id', '=', 'accommodation_photos.photo_category_id')
                                    ->select('accommodation_photos.*', 'accommodation_photo_categories.key AS photo_category_key')
                                    ->get();

        $images= json_decode(json_encode($images), true);

        foreach ($images as $key => $image)
        {
            $category = str_replace("acco_photo_cat_", "", $image["photo_category_key"]);
            $name = str_slug($mainItemName)."-".$category."-".$image["id"].".".$image["extension"];

            $images[$key]["original_image_name"] = $name;
            $images[$key]["original_url"] = Storage::url(env('AWS_OBJECT_KEY').'/'.$image["id"].".".$image["extension"]);
            $images[$key]["fullsized_url"] = Storage::url(env('AWS_OBJECT_KEY_FULLSIZED').'/'.$image["id"].".".$image["extension"]);
            $images[$key]["search-accommodation-800_url"] = Storage::url(env('AWS_RESIZE_ACCO_800').'/'.$image["id"].".".$image["extension"]);
            $images[$key]["accommodation-desktop-large_url"] = Storage::url(env('AWS_RESIZE_ACCO_DESKTOP_LARGE').'/'.$image["id"].".".$image["extension"]);
            $images[$key]["accommodation-mobile-large_url"] = Storage::url(env('AWS_RESIZE_ACCO_MOBILE_LARGE').'/'.$image["id"].".".$image["extension"]);
            $images[$key]["accommodation-thumb_url"] = Storage::url(env('AWS_RESIZE_ACCO_DESKTOP_THUMB').'/'.$image["id"].".".$image["extension"]);
        }

        return response()->json($images);
    }

    public function getItemsImages(Request $request)
    {
        $params = $request->input();
        $items = $params["items"];

        foreach ($items as $key => $item)
        {
            $images = AccommodationPhoto::where('accommodation_id', $item["id"])
                ->get()->toArray();

            foreach ($images as $index => $image)
            {
                $images[$index]["original_url"] = Storage::url(env('AWS_OBJECT_KEY').'/'.$image["id"].".".$image["extension"]);
                $images[$index]["fullsized_url"] = Storage::url(env('AWS_OBJECT_KEY_FULLSIZED').'/'.$image["id"].".".$image["extension"]);
                $images[$index]["search-accommodation-800_url"] = Storage::url(env('AWS_RESIZE_ACCO_800').'/'.$image["id"].".".$image["extension"]);
                $images[$index]["accommodation-desktop-large_url"] = Storage::url(env('AWS_RESIZE_ACCO_DESKTOP_LARGE').'/'.$image["id"].".".$image["extension"]);
                $images[$index]["accommodation-mobile-large_url"] = Storage::url(env('AWS_RESIZE_ACCO_MOBILE_LARGE').'/'.$image["id"].".".$image["extension"]);
                $images[$index]["accommodation-thumb_url"] = Storage::url(env('AWS_RESIZE_ACCO_DESKTOP_THUMB').'/'.$image["id"].".".$image["extension"]);
            }
            $items[$key]["images"] = $images;
        }
        return response()->json($items);
    }

    public function ItemNiceUrl(Request $request)
    {
        $params = $request->input();
        $items = $params["items"];

        foreach ($items as $key => $item)
        {
            $item_url = DB::table('urls')->where('subject_id', $item["id"])->where('lang', $params["lng"])->where('page_type', 5)->first();

            if($item_url)
                $items[$key]["url"] = "/".$params["lng"]."/".$item_url->url;
            else
                $items[$key]["url"] = null;
        }

        return response()->json($items);

    }

    public function PoiNiceUrl(Request $request)
    {
        $params = $request->input();
        $pois = $params["pois"];

        foreach ($pois as $key => $poi)
        {
            $poi_url = DB::table('urls')->where('subject_id', $poi["id"])->where('lang', $params["lng"])->where('page_type', 4)->first();

            if($poi_url)
                $pois[$key]["url"] = "/".$params["lng"]."/".$poi_url->url;
            else
                $pois[$key]["url"] = null;
        }

        return response()->json($pois);

    }

    public function CampingsCountries(Request $request)
    {
        $params = $request->input();

        $jcRequest = new JCRequest($params);
        $jcSearchResponse = $this->esClient->getCountriesList($jcRequest);
        $campingsCountries = response()->json($jcSearchResponse->getResponseParameters());

        $ctrl = [];
        $countries = $campingsCountries->original["items"];

        foreach ($countries as $key => $item)
        {
            $type = explode("-", $item->id);
            $type = $type[1];

            if($type == 1)
            {
                $ctrl[$key]["id"] = $item->id;
                $ctrl[$key]["key"] = $item->key;
                $ctrl[$key]["subtype"] = $item->subType;

//                $url = $this->urlsService->getUrlByItemId($item->id, $params["lng"]);
                $url = DB::table('urls')->where('subject_id', $item->id)->where('lang', $params["lng"])->first();

                if(isset($url->url))
                {
                    $ctrl[$key]["url"] = $url->url;
                }
                else{
                    $ctrl[$key]["url"] = "";
                }

                foreach($item->translations as $trans)
                {

                    if($trans["lng"] == $params["lng"])
                    {
                        if($trans["trans"])
                        {
                            $ctrl[$key]["trans"] = $trans["trans"];
                        }else{
                            $ctrl[$key]["trans"] = $item->key;
                        }
                    }
                }
            }


        }
        return response()->json($ctrl);
    }

    public function CountryUrl(Request $request)
    {
        $params = $request->input();
//        $url = $this->urlsService->getUrlByItemId($params["country_id"], $params["lng"]);
        $url = DB::table('urls')->where('subject_id', $params["country_id"])->where('lang', $params["lng"])->first();

        return response()->json($url);
    }

//    public function keyToUrl(Request $request) {
//        //$key = "europe_germany_brandenburg_landkreisdahmespreewald_koenigswusterhausen_koenigswusterhausen";
//        $key = "europe_germany_brandenburg_landkreisdahmespreewald";
//        $body = [
//            "query" => [
//                "bool" => [
//                    "must" => [
//                        ["prefix" => [
//                            "key" => $key
//                        ]]
//                    ]
//                ]
//            ],
//            "from" => 0,
//            "size" => 500
//        ];
//        $data = [
//            "index" => "geotree",
//            "type" => "geoitem",
//            "body" => $body,
//        ];
//        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->getEsClient()->search($data));
//        foreach ($esSearchGeoItemResponse->getItems() as $geoTreeItem) {
//            /*echo "<pre>";
//            print_r($geoTreeItem);
//            echo "</pre>";*/
//            $url = GeoTreeItemHelper::getInstance()->calculateUrl($geoTreeItem);
//            echo "kai ".$geoTreeItem->key." ---> tada url ".$url."<br>";
//        }
//    }

    /*public function searchByKey(Request $request) {
        $jcSearchResponse = $this->esClient->searchByKey($request->input());
        return response()->json($jcSearchResponse->getResponseParameters());
    }*/

}