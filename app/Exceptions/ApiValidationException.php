<?php

namespace App\Exceptions;

class ApiValidationException extends \Exception
{
    protected $errors;

    public function __construct(array $errors)
    {
        parent::__construct(trans('the_given_data_failed_to_pass_validation')); //add translations
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

}