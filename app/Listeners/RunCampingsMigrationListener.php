<?php

namespace App\Listeners;

use App\Jobs\MigrateCampingsJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Queue;

class RunCampingsMigrationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function handle($event)
    {
        if (config('es.enable')) {
            Queue::push(new MigrateCampingsJob());
        }
    }
}
