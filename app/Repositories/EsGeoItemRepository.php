<?php namespace App\Repositories;

use Basemkhirat\Elasticsearch\Facades\ES;

class EsGeoItemRepository
{
    /**
     * Store gey item
     * @param array $data
     */
    public function store(array $data)
    {
        if (!config('es.enable')) {
            return;
        }
        $item = new \App\Models\EsGeoItem();
        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        //This sets ID
        $item->_id = $item->id;

        $item->save();
    }

    /**
     * Update geoitem
     * @param $id
     * @param array $data
     * @return null
     */
    public function update($id, array $data)
    {
        if (!config('es.enable')) {
            return;
        }
        ES::type('geoitem')->id($id)->update($data);
    }

    /**
     * Get geoitem
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $item = \App\Models\EsGeoItem::find($id);
        return $item;
    }
}