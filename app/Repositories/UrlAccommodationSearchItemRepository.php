<?php namespace App\Repositories;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\Url;

class UrlAccommodationSearchItemRepository
{
    /**
     * Method creates url for accommodation search page
     *
     * @param $accommodationid
     * @param $abbreviation
     * @param $url
     * @return mixed
     */
    public function create($accommodationid, $abbreviation, $url)
    {
        return Url::create([
            'lang' => $abbreviation,
            'url' => $url,
            'subject_id' => $accommodationid . '-' . GeoTreeItem::TYPE_CAMPING,
            'page_type' => Url::PAGE_TYPE_CAMPING_SEARCH
        ]);
    }

    /**
     * Update accommodation search page url
     *
     * @param $accommodationid
     * @param $abbreviation
     * @param $url
     * @return mixed
     */
    public function update($accommodationid, $abbreviation, $url)
    {
        return Url::accommodationUrlSearch($accommodationid, $abbreviation)->firstOrException()->update([
            'url' => $url
        ]);
    }
}