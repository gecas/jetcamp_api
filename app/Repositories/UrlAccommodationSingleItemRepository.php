<?php namespace App\Repositories;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\Url;

class UrlAccommodationSingleItemRepository
{
    /**
     * Method creates url for accommodation destination page
     *
     * @param $accommodationid
     * @param $abbreviation
     * @param $url
     * @return mixed
     */
    public function create($accommodationid, $abbreviation, $url)
    {
        return Url::create([
            'lang' => $abbreviation,
            'url' => $url,
            'subject_id' => $accommodationid . '-' . GeoTreeItem::TYPE_CAMPING,
            'page_type' => Url::PAGE_TYPE_CAMPING_PAGE
        ]);
    }

    /**
     * Update accommodation single page url
     *
     * @param $accommodationid
     * @param $abbreviation
     * @param $url
     * @return mixed
     */
    public function update($accommodationid, $abbreviation, $url)
    {
        return Url::accommodationUrlSingle($accommodationid, $abbreviation)->firstOrException()->update([
            'url' => $url
        ]);
    }
}