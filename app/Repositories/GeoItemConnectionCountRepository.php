<?php namespace App\Repositories;

use App\Models\Accommodation;
use App\Models\GeoTree;

class GeoItemConnectionCountRepository
{
    /*
     * Find active geo tree item direct accommodations count
     * @param GeoTree $item
     * @return int $count
     */
    public function findActiveDirectCampsitesCount(GeoTree $item)
    {
        $count = GeoTree::query()
            ->join('accommodations', 'geo_trees.id', '=', 'accommodations.geo_trees_id')
            ->where('accommodations.status', 1)
            ->where(function ($query) use ($item) {
                $query->where('geo_trees.key', 'like', $item->key . '_%')
                    ->orWhere('geo_trees.key', '=', $item->key);
            })
            ->count();
        return $count;
    }
}