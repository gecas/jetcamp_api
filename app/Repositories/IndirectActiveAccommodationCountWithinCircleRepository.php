<?php namespace App\Repositories;

use App\Models\Accommodation;

class IndirectActiveAccommodationCountWithinCircleRepository
{
    /*
     * Find active indirect accommodations count in circle within given radius
     * @param Accommodation $accommodation
     * @param int $radius
     * @param array $excludeIds id of accommodations to exclude
     * @return int $count
     */
    public function get($lat, $lon, $radius, $excludeIds = [])
    {
        $count = Accommodation::whereWithin($lat, $lon, $radius, 'address_gps_lat', 'address_gps_lon')
            ->where('status', 1)
            ->whereNotIn('id', $excludeIds)
            ->count();

        return $count;
    }
}