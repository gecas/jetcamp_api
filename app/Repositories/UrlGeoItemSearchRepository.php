<?php namespace App\Repositories;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\Url;

class UrlGeoItemSearchRepository
{
    /**
     * Method creates url for geo item search page
     *
     * @param $geoItemId
     * @param $abbreviation
     * @param $url
     * @return mixed
     */
    public function create($geoItemId, $abbreviation, $url)
    {
        return Url::create([
            'lang' => $abbreviation,
            'url' => $url,
            'subject_id' => $geoItemId . '-' . GeoTreeItem::TYPE_GEOITEM,
            'page_type' => Url::PAGE_TYPE_GEOITEM_SEARCH
        ]);
    }
}