<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class AccommodationAdditionalCost extends Model
{

    protected $table = 'accommodation_additional_costs';

    protected $fillable = [
        'accommodation_id', 'facility_main_id', 'facility_sub_id', 'costs', 'currency_id', 'facility_setting_costtype_id'
    ];

    public function facilityMain() {
        return $this->belongsTo('App\FacilityMain', 'facility_main_id');
    }

    public function facilitySub() {
        return $this->belongsTo('App\FacilitySub', 'facility_sub_id');
    }

    public function accommodation() {
        return $this->belongsTo('App\Accommodation', 'accommodation_id');
    }

    public function currency() {
        return $this->belongsTo('App\Currency', 'currency_id');
    }

}