<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;

class UrlsService
{

    public function getUrlByItemId($item_id, $language = "en") {
        $urls = DB::table('urls')->where([
            ['subject_id', '=', $item_id]
        ])->get();
        $defaultUrl = null;
        $finalUrl = null;
        if (count($urls) > 0) {
            foreach ($urls as $url) {
                if ($url->lang == "en") {
                    $defaultUrl = $url;
                }
                if ($url->lang == $language) {
                    $finalUrl = $url;
                    break;
                }
            }
            if (empty($finalUrl)) {
                $finalUrl = $defaultUrl;
            }
        }
        return $finalUrl;
    }

    public function getUrlsByUrl($url) {
        return DB::table('urls')->where([
            ['url', '=', $url],
            //['lang', '=', $input["lng"]]
        ])->get();
    }

}