<?php

namespace App\Services;

use App\Models\CommercialRegionTranslation;
use App\Models\Url;

/*
 * Class generates URL for commercial region search page
 */

class UniqueUrlService
{
    public function __construct()
    {
    }

    /**
     * Generate link
     * @param string $lang
     * @param string $url
     * @return string
     */
    public function get($lang, $url)
    {
        while (true) {
            $urlModel = Url::where('lang', $lang)
                ->where('url', $url)
                ->first();
            if (!$urlModel) {
                return $url;
            }

            //Check if already with increment
            if (preg_match('/(.*)--(\d{1,3})\/$/', $url, $matches)) {
                $url = $matches[1] . '--' . ($matches[2] + 1) . '/';
            } else {
                $url = trim($url, '/') . '--1/';
            }
        }
    }
}