<?php

namespace App\Services;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\Url;
use Illuminate\Support\Facades\DB;

class MigrationService
{
    public function stripKey($key) {
        $tags = explode("_", $key);
        array_shift($tags); // removing "geotrees" tag
        return implode("_", $tags);
    }

    public function stripCategortKey($key) {
        $key = $this->stripKey($key);
        $key = $this->stripKey($key);
        return $key;
    }

    public function retrieveTags($strippedKey) {
        return array_values(array_unique(explode("_", $strippedKey)));
    }

    public function logGeoItem($esItem) {
        //echo "--- ".$esItem["id"]."\n";
    }

    public function normalizeString($string, $separator = " ") {
        return mb_strtolower(str_slug($string, $separator));
    }

    public function formatFacilityKey($startFacilityKey, $endFacilityEn) {
        $endFacilityEn = str_slug($endFacilityEn);
        if (!empty($endFacilityEn)) {
            return $startFacilityKey."_".$endFacilityEn;
        } else
            return "";
    }

    public function apendTranslationKeysForSubline($translationKeys) {
        foreach ($translationKeys as $translationKey) {
            $parties = explode("_", $translationKey);
//            if (count($parties) == 2) {
//                $k = $parties[0].'_'.$parties[1];
//                if (!in_array($k, $translationKeys)) {
//                    $translationKeys[] = $k;
//                    //echo "-".$k."- nerado \n";
//                }
//            }
            if (count($parties) > 2) {
                $k = $parties[0].'_'.$parties[1];
                if (!in_array($k, $translationKeys)) {
                    $translationKeys[] = $k;
                    //echo "-".$k."- nerado \n";
                }
            }
            if (count($parties) > 3) {
                $k = $parties[0] . '_' . $parties[1] . '_' . $parties[2];
                if (!in_array($k, $translationKeys)) {
                    $translationKeys[] = $k;
                }
            }
            if (count($parties) > 4) {
                $k = $parties[0] . '_' . $parties[1] . '_' . $parties[2] . '_' . $parties[3];
                if (!in_array($k, $translationKeys)) {
                    $translationKeys[] = $k;
                }
            }

        }
        return $translationKeys;
    }

    public function getGroupedTranslations($translationKeys, $defaultTranslation = "en") {
        for ($i=0; $i<count($translationKeys); $i++) {
            $translationKeys[$i] = '"'.$translationKeys[$i].'"';
        }
        $tResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM geo_translations gt where gt.key in ('.implode(", ", $translationKeys).')');
        $groupedTranslationItems = [];
        foreach ($tResults as $tResult) {
            $key = $this->stripKey($tResult->key);
            /*$tags = explode("_", $key);
            array_shift($tags); // removing "geotrees" from begining
            $key = implode("_", $tags);*/
            if (!array_key_exists($key, $groupedTranslationItems))
                $groupedTranslationItems[$key] = [];

//            if (!array_key_exists("grouped_translations", $groupedTranslationItems[$key]))
//                $groupedTranslationItems[$key]["grouped_translations"] = [];
            if (!array_key_exists("translations", $groupedTranslationItems[$key]))
                $groupedTranslationItems[$key]["translations"] = [];

//            $tr = mb_strtolower(trim($tResult->translation));
//            if (!in_array($tr, $groupedTranslationItems[$key]["grouped_translations"])) {
//                $groupedTranslationItems[$key]["grouped_translations"][] = $tr;
//                /*var_dump($groupedTranslationItems[$key]["geoitem_translations"]);*/
//            }
//            $normalized = $this->normalizeString($tResult->translation, " ");
//            if (!in_array($normalized, $groupedTranslationItems[$key]["grouped_translations"]) and !empty($normalized)) {
//                $groupedTranslationItems[$key]["grouped_translations"][] = $normalized;
//            }

            $groupedTranslationItems[$key]["translations"][$tResult->language_abbrevation] = trim($tResult->translation);
            if ($tResult->language_abbrevation == $defaultTranslation) {
                $groupedTranslationItems[$key]["_default"] = trim($tResult->translation);
            }
        }
        return $groupedTranslationItems;
    }

    public function appendGeoItemsWithTranslations(&$geoTreeItems, $groupedTranslations) {
        //var_dump($groupedTranslations);
        for ($i=0; $i<count($geoTreeItems); $i++) {
            $geoTreeItem = $geoTreeItems[$i];
            if (array_key_exists($geoTreeItem->key, $groupedTranslations)) {
                /*$geoTreeItem->grouped_translations = array_merge($geoTreeItem->grouped_translations,
                                                                    $groupedTranslations[$geoTreeItem->key]["grouped_translations"]);*/
                foreach ($groupedTranslations[$geoTreeItem->key]["translations"] as $lng => $trans) {
                    $geoTreeItem->translations[] = ["lng" => $lng, "trans" => $trans];
                    $this->addNameSynonym($geoTreeItem, $trans);
                }

                if (array_key_exists("_default", $groupedTranslations[$geoTreeItem->key])) {
                    $geoTreeItem->name = $groupedTranslations[$geoTreeItem->key]["_default"];
                } else {
                    $t = end($geoTreeItem->translations);
                    $geoTreeItem->name = $t["trans"];
                }
                $this->addNameSynonym($geoTreeItem, $geoTreeItem->name);

                $parts = explode("_", $geoTreeItem->key);
                if (count($parts) > 1) {
                    $k = $parts[0];
                    if (array_key_exists($k, $groupedTranslations)) {
                        foreach ($groupedTranslations[$k]["translations"] as $lng => $trans) {
                            $geoTreeItem->subline["continent_translations"][] = ["lng" => $lng, "trans" => $trans];
                        }
                    }
                }

                if (count($parts) > 2) {
                    $k = $parts[0]."_".$parts[1];
                    if (array_key_exists($k, $groupedTranslations)) {
                        foreach ($groupedTranslations[$k]["translations"] as $lng => $trans) {
                            $geoTreeItem->subline["country_translations"][] = ["lng" => $lng, "trans" => $trans];
                        }
                    }
                }
                if (count($parts) > 3) {
                    $k = $parts[0]."_".$parts[1]."_".$parts[2];
                    if (array_key_exists($k, $groupedTranslations)) {
                        foreach ($groupedTranslations[$k]["translations"] as $lng => $trans) {
                            $geoTreeItem->subline["region_translations"][] = ["lng" => $lng, "trans" => $trans];
                        }
                    }
                }

                $geoTreeItems[$i] = $geoTreeItem;
            }
        }
    }

    public function appendGeoItemWithCampingCnts($geoTreeItem) {
        /*$cResults = app('db')->connection('mysql2')
                ->select('SELECT count(*) as cnt FROM accommodation_scraped_contents where geo_trees_id=:geo_tree_id', ['geo_tree_id' => $geoTreeItem->geo_id]);
        $geoTreeItem->camping_cnt = $cResults[0]->cnt;*/

        if (!empty($geoTreeItem->gps_coords) and array_key_exists("lat", $geoTreeItem->gps_coords) and array_key_exists("lon", $geoTreeItem->gps_coords)){
            $distance_query = "SELECT
                            id, basics_namecamping,
                            (
                                6371 *
                                acos(
                                    cos( radians( :lat ) ) *
                                    cos( radians( address_gps_lat ) ) *
                                    cos(
                                        radians( address_gps_lon ) - radians( :lon )
                                    ) +
                                    sin(radians( :lat1 )) *
                                    sin(radians( address_gps_lat ))
                                )
                            ) distance
                            FROM
                                accommodation_scraped_contents
                            HAVING
                                distance < :distance";

            $cResults = app('db')->connection('mysql2')
                ->select($distance_query, ['lat' => $geoTreeItem->gps_coords["lat"], 'lat1'=> $geoTreeItem->gps_coords["lat"],
                    'lon' => $geoTreeItem->gps_coords["lon"], 'distance' => 10]);
            $geoTreeItem->camping_cnt_10 = count($cResults);
            $cResults = app('db')->connection('mysql2')
                ->select($distance_query, ['lat' => $geoTreeItem->gps_coords["lat"], 'lat1'=> $geoTreeItem->gps_coords["lat"],
                    'lon' => $geoTreeItem->gps_coords["lon"], 'distance' => 25]);
            $geoTreeItem->camping_cnt_25 = count($cResults);
            $cResults = app('db')->connection('mysql2')
                ->select($distance_query, ['lat' => $geoTreeItem->gps_coords["lat"], 'lat1'=> $geoTreeItem->gps_coords["lat"],
                    'lon' => $geoTreeItem->gps_coords["lon"], 'distance' => 50]);
            $geoTreeItem->camping_cnt_50 = count($cResults);
        }
        return $geoTreeItem;
    }

    public static function instance() {
        return new MigrationService();
    }

    public function getAllDirectCampingCounts() {
        $cResults = app('db')->connection('mysql2')
            ->select('SELECT camp.id as camp_id, t.id as geo_id, t.key as geo_key FROM accommodations camp Join geo_trees t on camp.geo_trees_id=t.id');
        $campingsCounts = [];
        foreach ($cResults as $cResult) {
            //$campaignIds[] = $cResult->camp_id;
            $mysqlGeoKey = $cResult->geo_key;
            $geoItemKey = $this->stripKey($mysqlGeoKey);
            $tags = $this->retrieveTags($geoItemKey);
            while (count($tags) > 1) {
                array_pop($tags);
                $newGeoItemKey = implode("_", $tags);
                if (array_key_exists($newGeoItemKey, $campingsCounts))
                    $campingsCounts[$newGeoItemKey]++;
                else
                    $campingsCounts[$newGeoItemKey] = 1;
            }
        }
        return $campingsCounts;
    }

    public function getCampingRadiusCalculateExceptions() {
        $eResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM geo_tree_radius_exceptions te');
        $exceptions = [];
        foreach ($eResults as $eResult) {
            if (!array_key_exists($eResult->countrycode, $exceptions)) {
                $exceptions[$eResult->countrycode] = [];
            }
            $exceptions[$eResult->countrycode][] = $eResult->fcode;
        }
        return $exceptions;
    }


    public function needToPerformCampingRadiusCalculations($campingRadiusCalculateExceptions, $countryCode, $geoTreeItemSubtype) {
        if (array_key_exists($countryCode, $campingRadiusCalculateExceptions)) {
            if (in_array($geoTreeItemSubtype, $campingRadiusCalculateExceptions[$countryCode]))
                return false;
        }
        return true;
    }

    public function needToMigrateGeoTreeItemToEs($geoTreeItem) {
        if ($geoTreeItem->camping_cnt or $geoTreeItem->camping_cnt_10 or $geoTreeItem->camping_cnt_25 or $geoTreeItem->camping_cnt_50) {
            return true;
        } else
            return false;
    }

    public function migrateGeoTreeItemsToEs($geoTreeItems, \App\ElasticSearchAPI\ESClient $esClient, $translationKeys, $geoTreeIds, $movedToEsGeoTreeIds = []) {
        if (!empty($geoTreeItems)) {
            $groupedTranslationItems = [];
            if (!empty($translationKeys)) {
                $translationKeys = $this->apendTranslationKeysForSubline($translationKeys);
                $groupedTranslationItems = $this->getGroupedTranslations($translationKeys);
            }

            if (!empty($groupedTranslationItems))
                $this->appendGeoItemsWithTranslations($geoTreeItems, $groupedTranslationItems);

            $result = $esClient->bulk($geoTreeItems);
            if (!$result["es_response"]["errors"]) {
                echo "Successfully updated bunch of geotree items!\n";
            } else {
                echo "Failed to update bunch of items!\n";
                foreach ($result["es_response"]["items"] as $item) {
                    if ($item["index"]["status"]!=200) {
                        var_dump($item["index"]);
                        break;
                    }
                }
                return false;
            }
        } else {
            echo "Empty run, nothing to migrate to ES\n";
        }
        if (!empty($geoTreeIds)) {
            $r = app('db')->connection('mysql2')
                ->select('update geo_trees set migrated=1 where id in ('.implode(", ", $geoTreeIds).')');
        }
        if (!empty($movedToEsGeoTreeIds)) {
            $r = app('db')->connection('mysql2')
                ->select('update geo_trees set moved_to_es=1 where id in ('.implode(", ", $movedToEsGeoTreeIds).')');
        }
        return true;
    }

    function mysqlBulkInsertOrUpdate(array $rows, $table){
        $first = reset($rows);

        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );

        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                        //array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                        array_map( function( $value ) {
                            if (empty($value))
                                return "null";
                            elseif (strpos($value, 'ST_GeomFromText') === 0) {
                                return str_replace('"', '""', $value);
                            } else
                                return '"'.str_replace('"', '""', $value).'"';
                        }
                            , $row )
                    ).')';
            } , $rows )
        );

        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );

        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

        return \DB::statement( $sql );
    }

    function addNameSynonym(&$item, $name) {
        if (!property_exists($item, "name_synonyms")) {
            $item->name_synonyms = [];
        }
        $name = mb_strtolower(trim($name));
        $normalizedName = $this->normalizeString($this->processString($name), " ");
        if ($name != $normalizedName and !in_array($normalizedName, $item->name_synonyms)) {
            $item->name_synonyms[] = $this->normalizeString($name, " ");
        }

        $transliteratedName = $this->transliterateString($this->processString($name));
        if ($name != $transliteratedName and !in_array($transliteratedName, $item->name_synonyms)) {
            $item->name_synonyms[] = $transliteratedName;
        }
        return $item;
    }

    function transliterateString($string) {
        /*$transliterationTable = array('Ã¡' => 'a', 'Ã' => 'A', 'Ã ' => 'a', 'Ã€' => 'A', 'Äƒ' => 'a', 'Ä‚' => 'A', 'Ã¢' => 'a', 'Ã‚' => 'A', 'Ã¥' => 'a',
            'Ã…' => 'A', 'Ã£' => 'a', 'Ãƒ' => 'A', 'Ä…' => 'a', 'Ä„' => 'A', 'Ä' => 'a', 'Ä€' => 'A', 'Ã¤' => 'ae', 'Ã„' => 'AE', 'Ã¦' => 'ae', 'Ã†' => 'AE',
            'á¸ƒ' => 'b', 'á¸‚' => 'B', 'Ä‡' => 'c', 'Ä†' => 'C', 'Ä‰' => 'c', 'Äˆ' => 'C', 'Ä' => 'c', 'ÄŒ' => 'C', 'Ä‹' => 'c', 'ÄŠ' => 'C', 'Ã§' => 'c',
            'Ã‡' => 'C', 'Ä' => 'd', 'ÄŽ' => 'D', 'á¸‹' => 'd', 'á¸Š' => 'D', 'Ä‘' => 'd', 'Ä' => 'D', 'Ã°' => 'dh', 'Ã' => 'Dh', 'Ã©' => 'e', 'Ã‰' => 'E',
            'Ã¨' => 'e', 'Ãˆ' => 'E', 'Ä•' => 'e', 'Ä”' => 'E', 'Ãª' => 'e', 'ÃŠ' => 'E', 'Ä›' => 'e', 'Äš' => 'E', 'Ã«' => 'e', 'Ã‹' => 'E', 'Ä—' => 'e',
            'Ä–' => 'E', 'Ä™' => 'e', 'Ä˜' => 'E', 'Ä“' => 'e', 'Ä’' => 'E', 'á¸Ÿ' => 'f', 'á¸ž' => 'F', 'Æ’' => 'f', 'Æ‘' => 'F', 'ÄŸ' => 'g', 'Äž' => 'G',
            'Ä' => 'g', 'Äœ' => 'G', 'Ä¡' => 'g', 'Ä ' => 'G', 'Ä£' => 'g', 'Ä¢' => 'G', 'Ä¥' => 'h', 'Ä¤' => 'H', 'Ä§' => 'h', 'Ä¦' => 'H', 'Ã­' => 'i',
            'Ã' => 'I', 'Ã¬' => 'i', 'ÃŒ' => 'I', 'Ã®' => 'i', 'ÃŽ' => 'I', 'Ã¯' => 'i', 'Ã' => 'I', 'Ä©' => 'i', 'Ä¨' => 'I', 'Ä¯' => 'i', 'Ä®' => 'I',
            'Ä«' => 'i', 'Äª' => 'I', 'Äµ' => 'j', 'Ä´' => 'J', 'Ä·' => 'k', 'Ä¶' => 'K', 'Äº' => 'l', 'Ä¹' => 'L', 'Ä¾' => 'l', 'Ä½' => 'L', 'Ä¼' => 'l',
            'Ä»' => 'L', 'Å‚' => 'l', 'Å' => 'L', 'á¹' => 'm', 'á¹€' => 'M', 'Å„' => 'n', 'Åƒ' => 'N', 'Åˆ' => 'n', 'Å‡' => 'N', 'Ã±' => 'n', 'Ã‘' => 'N',
            'Å†' => 'n', 'Å…' => 'N', 'Ã³' => 'o', 'Ã“' => 'O', 'Ã²' => 'o', 'Ã’' => 'O', 'Ã´' => 'o', 'Ã”' => 'O', 'Å‘' => 'o', 'Å' => 'O', 'Ãµ' => 'o',
            'Ã•' => 'O', 'Ã¸' => 'oe', 'Ã˜' => 'OE', 'Å' => 'o', 'ÅŒ' => 'O', 'Æ¡' => 'o', 'Æ ' => 'O', 'Ã¶' => 'oe', 'Ã–' => 'OE', 'á¹—' => 'p', 'á¹–' => 'P',
            'Å•' => 'r', 'Å”' => 'R', 'Å™' => 'r', 'Å˜' => 'R', 'Å—' => 'r', 'Å–' => 'R', 'Å›' => 's', 'Åš' => 'S', 'Å' => 's', 'Åœ' => 'S', 'Å¡' => 's',
            'Å ' => 'S', 'á¹¡' => 's', 'á¹ ' => 'S', 'ÅŸ' => 's', 'Åž' => 'S', 'È™' => 's', 'È˜' => 'S', 'ÃŸ' => 'SS', 'Å¥' => 't', 'Å¤' => 'T', 'á¹«' => 't',
            'á¹ª' => 'T', 'Å£' => 't', 'Å¢' => 'T', 'È›' => 't', 'Èš' => 'T', 'Å§' => 't', 'Å¦' => 'T', 'Ãº' => 'u', 'Ãš' => 'U', 'Ã¹' => 'u', 'Ã™' => 'U',
            'Å­' => 'u', 'Å¬' => 'U', 'Ã»' => 'u', 'Ã›' => 'U', 'Å¯' => 'u', 'Å®' => 'U', 'Å±' => 'u', 'Å°' => 'U', 'Å©' => 'u', 'Å¨' => 'U', 'Å³' => 'u',
            'Å²' => 'U', 'Å«' => 'u', 'Åª' => 'U', 'Æ°' => 'u', 'Æ¯' => 'U', 'Ã¼' => 'ue', 'Ãœ' => 'UE', 'áºƒ' => 'w', 'áº‚' => 'W', 'áº' => 'w', 'áº€' => 'W',
            'Åµ' => 'w', 'Å´' => 'W', 'áº…' => 'w', 'áº„' => 'W', 'Ã½' => 'y', 'Ã' => 'Y', 'á»³' => 'y', 'á»²' => 'Y', 'Å·' => 'y', 'Å¶' => 'Y', 'Ã¿' => 'y',
            'Å¸' => 'Y', 'Åº' => 'z', 'Å¹' => 'Z', 'Å¾' => 'z', 'Å½' => 'Z', 'Å¼' => 'z', 'Å»' => 'Z', 'Ã¾' => 'th', 'Ãž' => 'Th', 'Âµ' => 'u', 'Ð°' => 'a',
            'Ð' => 'a', 'Ð±' => 'b', 'Ð‘' => 'b', 'Ð²' => 'v', 'Ð’' => 'v', 'Ð³' => 'g', 'Ð“' => 'g', 'Ð´' => 'd', 'Ð”' => 'd', 'Ðµ' => 'e', 'Ð•' => 'E', 'Ñ‘' => 'e',
            'Ð' => 'E', 'Ð¶' => 'zh', 'Ð–' => 'zh', 'Ð·' => 'z', 'Ð—' => 'z', 'Ð¸' => 'i', 'Ð˜' => 'i', 'Ð¹' => 'j', 'Ð™' => 'j', 'Ðº' => 'k', 'Ðš' => 'k', 'Ð»' => 'l',
            'Ð›' => 'l', 'Ð¼' => 'm', 'Ðœ' => 'm', 'Ð½' => 'n', 'Ð' => 'n', 'Ð¾' => 'o', 'Ðž' => 'o', 'Ð¿' => 'p', 'ÐŸ' => 'p', 'Ñ€' => 'r', 'Ð ' => 'r', 'Ñ' => 's',
            'Ð¡' => 's', 'Ñ‚' => 't', 'Ð¢' => 't', 'Ñƒ' => 'u', 'Ð£' => 'u', 'Ñ„' => 'f', 'Ð¤' => 'f', 'Ñ…' => 'h', 'Ð¥' => 'h', 'Ñ†' => 'c', 'Ð¦' => 'c', 'Ñ‡' => 'ch',
            'Ð§' => 'ch', 'Ñˆ' => 'sh', 'Ð¨' => 'sh', 'Ñ‰' => 'sch', 'Ð©' => 'sch', 'ÑŠ' => '', 'Ðª' => '', 'Ñ‹' => 'y', 'Ð«' => 'y', 'ÑŒ' => '', 'Ð¬' => '', 'Ñ' => 'e',
            'Ð­' => 'e', 'ÑŽ' => 'ju', 'Ð®' => 'ju', 'Ñ' => 'ja', 'Ð¯' => 'ja');*/
        $transliterationTable = array('Ã¡' => 'a', 'Ã' => 'A', 'Ã ' => 'a', 'Ã€' => 'A', 'Äƒ' => 'a', 'Ä‚' => 'A', 'Ã¢' => 'a', 'Ã‚' => 'A', 'Ã¥' => 'a',
            'Ã…' => 'A', 'Ã£' => 'a', 'Ãƒ' => 'A', 'Ä…' => 'a', 'Ä„' => 'A', 'Ä' => 'a', 'Ä€' => 'A', 'Ã¤' => 'a', 'Ã„' => 'A', 'Ã¦' => 'ae', 'Ã†' => 'A',
            'á¸ƒ' => 'b', 'á¸‚' => 'B', 'Ä‡' => 'c', 'Ä†' => 'C', 'Ä‰' => 'c', 'Äˆ' => 'C', 'Ä' => 'c', 'ÄŒ' => 'C', 'Ä‹' => 'c', 'ÄŠ' => 'C', 'Ã§' => 'c',
            'Ã‡' => 'C', 'Ä' => 'd', 'ÄŽ' => 'D', 'á¸‹' => 'd', 'á¸Š' => 'D', 'Ä‘' => 'd', 'Ä' => 'D', 'Ã°' => 'd', 'Ã' => 'D', 'Ã©' => 'e', 'Ã‰' => 'E',
            'Ã¨' => 'e', 'Ãˆ' => 'E', 'Ä•' => 'e', 'Ä”' => 'E', 'Ãª' => 'e', 'ÃŠ' => 'E', 'Ä›' => 'e', 'Äš' => 'E', 'Ã«' => 'e', 'Ã‹' => 'E', 'Ä—' => 'e',
            'Ä–' => 'E', 'Ä™' => 'e', 'Ä˜' => 'E', 'Ä“' => 'e', 'Ä’' => 'E', 'á¸Ÿ' => 'f', 'á¸ž' => 'F', 'Æ’' => 'f', 'Æ‘' => 'F', 'ÄŸ' => 'g', 'Äž' => 'G',
            'Ä' => 'g', 'Äœ' => 'G', 'Ä¡' => 'g', 'Ä ' => 'G', 'Ä£' => 'g', 'Ä¢' => 'G', 'Ä¥' => 'h', 'Ä¤' => 'H', 'Ä§' => 'h', 'Ä¦' => 'H', 'Ã­' => 'i',
            'Ã' => 'I', 'Ã¬' => 'i', 'ÃŒ' => 'I', 'Ã®' => 'i', 'ÃŽ' => 'I', 'Ã¯' => 'i', 'Ã' => 'I', 'Ä©' => 'i', 'Ä¨' => 'I', 'Ä¯' => 'i', 'Ä®' => 'I',
            'Ä«' => 'i', 'Äª' => 'I', 'Äµ' => 'j', 'Ä´' => 'J', 'Ä·' => 'k', 'Ä¶' => 'K', 'Äº' => 'l', 'Ä¹' => 'L', 'Ä¾' => 'l', 'Ä½' => 'L', 'Ä¼' => 'l',
            'Ä»' => 'L', 'Å‚' => 'l', 'Å' => 'L', 'á¹' => 'm', 'á¹€' => 'M', 'Å„' => 'n', 'Åƒ' => 'N', 'Åˆ' => 'n', 'Å‡' => 'N', 'Ã±' => 'n', 'Ã‘' => 'N',
            'Å†' => 'n', 'Å…' => 'N', 'Ã³' => 'o', 'Ã“' => 'O', 'Ã²' => 'o', 'Ã’' => 'O', 'Ã´' => 'o', 'Ã”' => 'O', 'Å‘' => 'o', 'Å' => 'O', 'Ãµ' => 'o',
            'Ã•' => 'O', 'Ã¸' => 'oe', 'Ã˜' => 'O', 'Å' => 'o', 'ÅŒ' => 'O', 'Æ¡' => 'o', 'Æ ' => 'O', 'Ã¶' => 'o', 'Ã–' => 'O', 'á¹—' => 'p', 'á¹–' => 'P',
            'Å•' => 'r', 'Å”' => 'R', 'Å™' => 'r', 'Å˜' => 'R', 'Å—' => 'r', 'Å–' => 'R', 'Å›' => 's', 'Åš' => 'S', 'Å' => 's', 'Åœ' => 'S', 'Å¡' => 's',
            'Å ' => 'S', 'á¹¡' => 's', 'á¹ ' => 'S', 'ÅŸ' => 's', 'Åž' => 'S', 'È™' => 's', 'È˜' => 'S', 'ÃŸ' => 'S', 'Å¥' => 't', 'Å¤' => 'T', 'á¹«' => 't',
            'á¹ª' => 'T', 'Å£' => 't', 'Å¢' => 'T', 'È›' => 't', 'Èš' => 'T', 'Å§' => 't', 'Å¦' => 'T', 'Ãº' => 'u', 'Ãš' => 'U', 'Ã¹' => 'u', 'Ã™' => 'U',
            'Å­' => 'u', 'Å¬' => 'U', 'Ã»' => 'u', 'Ã›' => 'U', 'Å¯' => 'u', 'Å®' => 'U', 'Å±' => 'u', 'Å°' => 'U', 'Å©' => 'u', 'Å¨' => 'U', 'Å³' => 'u',
            'Å²' => 'U', 'Å«' => 'u', 'Åª' => 'U', 'Æ°' => 'u', 'Æ¯' => 'U', 'Ã¼' => 'u', 'Ãœ' => 'U', 'áºƒ' => 'w', 'áº‚' => 'W', 'áº' => 'w', 'áº€' => 'W',
            'Åµ' => 'w', 'Å´' => 'W', 'áº…' => 'w', 'áº„' => 'W', 'Ã½' => 'y', 'Ã' => 'Y', 'á»³' => 'y', 'á»²' => 'Y', 'Å·' => 'y', 'Å¶' => 'Y', 'Ã¿' => 'y',
            'Å¸' => 'Y', 'Åº' => 'z', 'Å¹' => 'Z', 'Å¾' => 'z', 'Å½' => 'Z', 'Å¼' => 'z', 'Å»' => 'Z', 'Ã¾' => 'th', 'Ãž' => 'Th', 'Âµ' => 'u', 'Ð°' => 'a',
            'Ð' => 'a', 'Ð±' => 'b', 'Ð‘' => 'b', 'Ð²' => 'v', 'Ð’' => 'v', 'Ð³' => 'g', 'Ð“' => 'g', 'Ð´' => 'd', 'Ð”' => 'd', 'Ðµ' => 'e', 'Ð•' => 'E', 'Ñ‘' => 'e',
            'Ð' => 'E', 'Ð¶' => 'z', 'Ð–' => 'z', 'Ð·' => 'z', 'Ð—' => 'z', 'Ð¸' => 'i', 'Ð˜' => 'i', 'Ð¹' => 'j', 'Ð™' => 'j', 'Ðº' => 'k', 'Ðš' => 'k', 'Ð»' => 'l',
            'Ð›' => 'l', 'Ð¼' => 'm', 'Ðœ' => 'm', 'Ð½' => 'n', 'Ð' => 'n', 'Ð¾' => 'o', 'Ðž' => 'o', 'Ð¿' => 'p', 'ÐŸ' => 'p', 'Ñ€' => 'r', 'Ð ' => 'r', 'Ñ' => 's',
            'Ð¡' => 's', 'Ñ‚' => 't', 'Ð¢' => 't', 'Ñƒ' => 'u', 'Ð£' => 'u', 'Ñ„' => 'f', 'Ð¤' => 'f', 'Ñ…' => 'h', 'Ð¥' => 'h', 'Ñ†' => 'c', 'Ð¦' => 'c', 'Ñ‡' => 'ch',
            'Ð§' => 'c', 'Ñˆ' => 's', 'Ð¨' => 's', 'Ñ‰' => 'sch', 'Ð©' => 'sch', 'ÑŠ' => '', 'Ðª' => '', 'Ñ‹' => 'y', 'Ð«' => 'y', 'ÑŒ' => '', 'Ð¬' => '', 'Ñ' => 'e',
            'Ð­' => 'e', 'ÑŽ' => 'ju', 'Ð®' => 'ju', 'Ñ' => 'ja', 'Ð¯' => 'ja',
            'Ñ›' => 'h', 'Ñ™' => 'l');
        return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $string);
    }

    function processString($string) {
        $chars = ["(", ")"];
        $string = trim($string);
        $string = str_replace($chars, " ", $string);
        $string = preg_replace('!\s+!', ' ', $string); // replacing larger spaces with one space
        return $string;
    }

    public function arrayContainsAnyValue($needleArray, $inArray) {
        foreach ($needleArray as $needle) {
            if (in_array($needle, $inArray))
                return true;
        }
        return false;
    }

    public function facilitiesContainsAnyValue($needleArray, $facilitiesArray) {
        foreach ($needleArray as $needle) {
            foreach ($facilitiesArray as $facility) {
                if ($facility->description == $needle)
                    return $facility;
            }
        }
        return null;
    }

    public function migrateAccFilteringItems($accommodationIds, $esClient, $accommodationsMysqlIds = []) {
        $accIds = [];
        foreach ($accommodationIds as $key => $ids) {
            $accIds = array_merge($accIds, $ids);
        }
        $items = $esClient->getGeoItemsByIds(array_keys($accIds), 0, count($accIds));
        $geoTreeItems = [];
        if (!empty($items)) {
            foreach ($items as $item) {
                if (property_exists($item, "item")) {
                    if (property_exists($item->item, "facilities")) {
                        unset($item->item->facilities);
                    }
                    if (!property_exists($item->item, "filtering")) {
                        $item->item->filtering = [];
                    }
                    $needToUpdate = false;
                    foreach ($accommodationIds as $key => $ids) {
                        if (array_key_exists($item->id, $ids)) {
                            $item->item->filtering[$key] = $ids[$item->id];
                            $needToUpdate = true;
                        }
                    }

                    if ($needToUpdate) {
                        $geoTreeItems[] = $item;
                    }

                }
            }
        }

        if (!empty($geoTreeItems)) {
            echo "Sending to ES ".count($geoTreeItems)." ...\n";
            $result = $esClient->bulk($geoTreeItems);
            if (!$result["es_response"]["errors"]) {
                echo "Successfully updated bunch of geotree items!\n";
                if (count($accommodationsMysqlIds) > 0)
                    $r = app('db')->connection('mysql2')
                        ->select('update accommodations set migrated=1 where id in ('.implode(", ", $accommodationsMysqlIds).')');
            } else {
                echo "Failed to update bunch of items!";
                print_r($result);
                return false;
            }
        } else {
            //echo "Skipping ES ".count($geoTreeItems)."\n";
        }
        return true;
    }

    public function getAccommodationsFacilitiesFilterItems($main_facilities, $sub_facilities) {
        $facilities = [];

        $mains = ['facility_swim-beach_indoor-swimmingpool', 'facility_swim-beach_indoor-swimmingpool-2',
            'facility_swim-beach_indoor-swimmingpool-3', 'facility_swim-beach_indoor-swimmingpool-4',
            'facility_swim-beach_indoor-swimmingpool-5'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $facilities[] = 'poolindoor';

        $mains = ['facility_swim-beach_outdoor-swimmingpool', 'facility_swim-beach_outdoor-swimmingpool-2',
            'facility_swim-beach_outdoor-swimmingpool-3', 'facility_swim-beach_outdoor-swimmingpool-4',
            'facility_swim-beach_outdoor-swimmingpool-5'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $facilities[] = 'pooloutdoor';

        $mains = ['facility_location-and-general-information_reception-payment-deposit', 'facility_location-and-general-information_reception-services',
            'facility_location-and-general-information_reception-reservations'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $facilities[] = 'reception';

        $mains = ['facility_swim-beach_indoor-swimmingpool', 'facility_swim-beach_indoor-swimmingpool-2', 'facility_swim-beach_indoor-swimmingpool-3',
            'facility_swim-beach_indoor-swimmingpool-4', 'facility_swim-beach_indoor-swimmingpool-5'];
        $subs = ['facility_swim-beach_indoor-swimmingpool_large-slide-with-mandatory-swim-tires', 'facility_swim-beach_indoor-swimmingpool_large-slide-8m',
            'facility_swim-beach_indoor-swimmingpool_swimming-paradise', 'facility_swim-beach_outdoor-swimmingpool_water-slide'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) and $this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'poolwaterslide';
        $mains = ['facility_swim-beach_outdoor-swimmingpool', 'facility_swim-beach_outdoor-swimmingpool-2', 'facility_swim-beach_outdoor-swimmingpool-3',
            'facility_swim-beach_outdoor-swimmingpool-4', 'facility_swim-beach_outdoor-swimmingpool-5'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) and $this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'poolwaterslide';

        $mains = ['facility_kids-teens_animation-program', 'facility_kids-teens_adventure-park', 'facility_kids-teens_disco', 'facility_kids-teens_nursery',
            'facility_kids-teens_kids-farm', 'facility_kids-teens_cooking-classes', 'facility_kids-teens_recreation-room', 'facility_kids-teens_mini-train',
            'facility_kids-teens_skelter-pedal-car', 'facility_kids-teens_indoor-playground', 'facility_kids-teens_outdoor-playground',
            'facility_kids-teens_bouncer-inflatable-castle', 'facility_kids-teens_skelter-pedal-car', 'facility_kids-teens_bouncer-inflatable-castle'];
        $subs = ['facility_kids-teens_animation-program_for-0-5-years', 'facility_kids-teens_animation-program_for-6-12-years',
            'facility_kids-teens_animation-program_for-13-18-years', 'facility_kids-teens_disco_dance-nights', 'facility_kids-teens_nursery_mini-club',
            'facility_kids-teens_nursery_certified-child-care-provider', 'facility_kids-teens_recreation-room_tv-space',
            'facility_kids-teens_outdoor-playground_extra-large', 'facility_kids-teens_outdoor-playground_water-playground'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) or $this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'kidsandfamilyfriendly';

        $subs = ['facility_general-facilities_internet_wifi', 'facility_general-facilities_internet_wifi-in-public-areas-only',
            'facility_general-facilities_internet_wifi-full-coverage', 'facility_general-facilities_internet_wifi-partial-coverage',
            'facility_general-facilities_internet_wifi-zones', 'facility_general-facilities_internet_wifi-in-accommodation-partially'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'wifi';

        $subs = ['facility_general-facilities_internet'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'publicspacewithtv';

        $subs = ['facility_general-facilities_to-rent-exchange-available_air-conditioner', 'facility_general-facilities_units_units-with-airconditioning-available'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'airco';

        $subs = ['facility_location-and-general-information_parking_special-parking-spaces-for-disabled', 'facility_location-and-general-information_grounds_facilities-for-disabled-guests',
            'facility_general-facilities_public-sanitation_canteen-restaurant-with-disabled-accessible-toilet', 'facility_general-facilities_public-sanitation_disabled-accessible-sanitary-facilities-in-the-same-building-as-for-the-other-campers',
            'facility_general-facilities_public-sanitation_wheelchair-friendly', 'facility_location-and-general-information_grounds_ramps-for-wheelchairs'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'wheelchairfriendly';

        $subs = ['facility_activities-sports_biking_equipment-for-rent', 'facility_activities-sports_atb-mountain-biking_mountain-bike-rental',
            'facility_activities-sports_cycling_equipment-for-rent'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $facilities[] = 'bikerental';

        return $facilities;
    }

    public function getAccommodationsChildrenFilterItems($main_facilities, $sub_facilities) {
        $children_items = [];
        $mains = ['facility_kids-teens_animation-program'];
        $subs = ['facility_kids-teens_animation-program_for-0-5-years', 'facility_kids-teens_animation-program_for-6-12-years',
            'facility_kids-teens_animation-program_for-13-18-years'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) or $this->arrayContainsAnyValue($subs, $sub_facilities))
            $children_items[] = 'animation';

        $mains = ['facility_kids-teens_indoor-playground', 'facility_kids-teens_outdoor-playground'];
        $subs = ['facility_kids-teens_outdoor-playground_extra-large', 'facility_kids-teens_outdoor-playground_water-playground'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) or $this->arrayContainsAnyValue($subs, $sub_facilities))
            $children_items[] = 'playground';

        $subs = ['facility_swim-beach_indoor-swimmingpool_toddlers-pool', 'facility_swim-beach_outdoor-swimmingpool_toddlers-pool'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $children_items[] = 'babypool';

        $mains = ['facility_kids-teens_disco'];
        $subs = ['facility_kids-teens_disco_dance-nights'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) or $this->arrayContainsAnyValue($subs, $sub_facilities))
            $children_items[] = 'disco';

        return $children_items;

    }

    public function getAccommodationsSanitaryFilterItems($sub_facilities) {
        $sanitary_items = [];
        $subs = ['facility_general-facilities_public-sanitation_private-sanitation-available'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'privatesanitary';

        $subs = ['facility_general-facilities_wash-dry_washing-machines-laundromat'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'washinmachine';

        $subs = ['facility_general-facilities_wash-dry_dryers'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'dryer';

        $subs = ['facility_general-facilities_public-sanitation_baby-room', 'facility_general-facilities_public-sanitation_family-shower'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'babylaundry';

        $subs = ['facility_general-facilities_public-sanitation_childrens-sanitary'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'sanitarianforchildren';

        $subs = ['facility_general-facilities_public-sanitation_wheelchair-friendly', 'facility_general-facilities_public-sanitation_disabled-accessible-sanitary-facilities-in-the-same-building-as-for-the-other-campers'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'sanitarianfordisabled';

        $subs = ['facility_general-facilities_public-sanitation_family-shower'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $sanitary_items[] = 'familyshower';

        return $sanitary_items;
    }

    public function getAccommodationsLocationFilterItems($accommodation, $mainFacilities, $subFacilities){
        $location_items = [];

        $subs = ['facility_swim-beach_beach_sandy-beach'];
        $facility = $this->facilitiesContainsAnyValue($subs, $subFacilities);
        if ($facility) {
            $accommodationAdditionalDistance = $this->getAccommodationAdditionalDistanceByFacility($accommodation, $facility);
            if (!empty($accommodationAdditionalDistance)) {
                $distance_in_meters = $this->getDistanceInMeters($accommodationAdditionalDistance);
                if ($distance_in_meters <= 500){
                    $location_items[] = 'beachsandcloserthen500m';
                }
            }
        }

        $subs = ['facility_swim-beach_beach_pebble-beach'];
        $facility = $this->facilitiesContainsAnyValue($subs, $subFacilities);
        if ($facility) {
            $accommodationAdditionalDistance = $this->getAccommodationAdditionalDistanceByFacility($accommodation, $facility);
            if (!empty($accommodationAdditionalDistance)) {
                $distance_in_meters = $this->getDistanceInMeters($accommodationAdditionalDistance);
                if ($distance_in_meters <= 500){
                    $location_items[] = 'beachstonescloserthen500m';
                }
            }
        }

        $mains = ['facility_swim-beach_beach', 'facility_swim-beach_beach-2'];
        $facility = $this->facilitiesContainsAnyValue($mains, $mainFacilities);
        if ($facility) {
            $accommodationAdditionalDistance = $this->getAccommodationAdditionalDistanceByFacility($accommodation, $facility);
            if (!empty($accommodationAdditionalDistance)) {
                $distance_in_meters = $this->getDistanceInMeters($accommodationAdditionalDistance);
                if ($distance_in_meters <= 10000){
                    $location_items[] = 'beachcloserthen10km';
                }
            }
        }

        $subs = ['facility_location-and-general-information_location_in-a-forest'];
        if ($this->facilitiesContainsAnyValue($subs, $subFacilities)){
            $location_items[] = 'inforrest';
        }

        return $location_items;
    }

    public function getAccommodationsRulesFilteringItems($main_facilities, $sub_facilities) {
        $rule_items = [];

        $subs = ['facility_location-and-general-information_dogs-pets_dogs-pets-permitted-on-leach', 'facility_location-and-general-information_dogs-pets_dogs-permitted-on-leash-max-height',
            'facility_location-and-general-information_dogs-pets_cats-permitted', 'facility_location-and-general-information_dogs-pets_designated-pet-waste-areafield',
            'facility_location-and-general-information_dogs-pets_bath-for-pets', 'facility_location-and-general-information_dogs-pets_shower-for-pets'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $rule_items[] = 'petsallowed';

        $subs = ['facility_location-and-general-information_dogs-pets_dogs-are-not-permitted'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $rule_items[] = 'petsnotallowed';

        $subs = ['facility_location-and-general-information_camping-regulations_nudist-camping'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $rule_items[] = 'nudistcamping';

        $subs = ['facility_location-and-general-information_camping-regulations_bbq-allowed-near-accommodation-or-on-pitch', 'facility_general-facilities_cooking-facilities_common-bbq-place'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $rule_items[] = 'bbqallowed';

        $subs = ['facility_location-and-general-information_camping-regulations_adults-only'];
        if ($this->arrayContainsAnyValue($subs, $sub_facilities))
            $rule_items[] = 'adultonly';

        return $rule_items;
    }

    public function getAccommodationsSportsAndGamesFilteringItems($main_facilities, $sub_facilities) {
        $sports_games_items = [];

        $mains = ['facility_activities-sports_table-tennis'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $sports_games_items[] = 'tabletennis';

        $mains = ['facility_activities-sports_tennis'];
        $subs = ['facility_activities-sports_tennis_hardcourt-outdoor', 'facility_activities-sports_tennis_hardcourt-indoor', 'facility_activities-sports_tennis_gravel',
            'facility_activities-sports_tennis_grass', 'facility_activities-sports_tennis_lessons-available', 'facility_activities-sports_tennis_equipment-for-rent',
            'facility_activities-sports_tennis_with-lighting'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) or $this->arrayContainsAnyValue($subs, $sub_facilities))
            $sports_games_items[] = 'tennis';

        $mains = ['facility_activities-sports_petanque-court'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $sports_games_items[] = 'petanque';

        $mains = ['facility_activities-sports_horse-pony-riding'];
        $subs = ['facility_activities-sports_horse-pony-riding_lessons-available'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities) or $this->arrayContainsAnyValue($subs, $sub_facilities))
            $sports_games_items[] = 'horsebackriding';

        $mains = ['facility_activities-sports_minigolf'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $sports_games_items[] = 'minigolf';

        $mains = ['facility_activities-sports_beach-volleyball'];
        if ($this->arrayContainsAnyValue($mains, $main_facilities))
            $sports_games_items[] = 'beachvolleybal';

        return $sports_games_items;
    }


    public function getAccommodationAdditionalDistanceByFacility($accommodation, $facility) {
        $accommodationAdditionalDistances = $accommodation->accommodationAdditionalDistance;
        foreach ($accommodationAdditionalDistances as $accommodationAdditionalDistance) {
            if ($accommodationAdditionalDistance->facility_sub_id == $facility->id
                or $accommodationAdditionalDistance->facility_main_id == $facility->id) {
                return $accommodationAdditionalDistance;
            }
        }
        return null;
    }

    public function getDistanceInMeters(\App\AccommodationAdditionalDistance $accommodationAdditionalDistance) {
        $distanceType = $accommodationAdditionalDistance->facilitySettingDistanceType;
        return (float)$distanceType->conversion_meter * $accommodationAdditionalDistance->distance;
    }

    public function generateGeoTreeItemUrl($geo_item_id) {
        $gResults = app('db')->connection('mysql2')
            ->select('SELECT g.id as id, g.key as k, g.key_short as key_short FROM geo_trees g where g.id = '.$geo_item_id);
        if (count($gResults) > 0) {
            $gResult = $gResults[0];
            $mysqlGeoKey = $gResult->key_short;
            $mysqlGeoKeyLong = $gResult->k;
            $geoTreeIds[] = $gResult->id;
            $keyParties = explode("_", $mysqlGeoKey);
            $trans = [];
            $newKey = "";
            $subject_id = $gResult->id."-".GeoTreeItem::TYPE_COMMERCIAL;
            while (count($keyParties) > 2) { // no need to include 1st and 2nd key part (geotree and continent)
                $newKey = implode("_", $keyParties);
                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_translations t where t.key="'.$newKey.'";');
                if (!empty($tResults)) {
                    foreach ($tResults as $tResult) {
                        $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                    }
                } elseif ($newKey == $mysqlGeoKey) { // if it didn't find any translation for key_short when need to check translation for key
                    $tResults = app('db')->connection('mysql2')
                        ->select('SELECT * FROM geo_translations t where t.key="'.$mysqlGeoKeyLong.'";');
                    foreach ($tResults as $tResult) {
                        $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                    }
                } else
                    $trans[$newKey]["en"] = end($keyParties);
                array_pop($keyParties);
            }
            $trans = array_reverse($trans);
            if (!empty($newKey)){
                $urls = [];
                foreach ($trans[$newKey] as $lng_abbr => $translation) {
                    if (!empty(str_slug($translation)))
                        $url = str_slug($translation)."/";
                    else
                        $url = str_slug($trans[$newKey]["en"])."/"; // for zh language unable to slugify
                    foreach ($trans as $key => $value) {
                        if ($newKey != $key) {
                            if (array_key_exists($lng_abbr, $trans[$key])) {
                                $url .= str_slug($trans[$key][$lng_abbr])."/";
                            } else if (array_key_exists("en", $trans[$key])) {
                                $url .= str_slug($trans[$key]["en"])."/";
                            } else
                                $url .= str_slug(reset($trans[$key]))."/";
                        }
                    }

                    //$urls[$lng_abbr] = str_replace("-", "__", $url); //
                    $urls[$lng_abbr] = $url; //
                    //echo " kai ".$lng_abbr." - ".$url."\n";
                }

                $needToUnset = [];
                foreach ($urls as $lng_abbr => $url) {
                    if ($lng_abbr != "en") {
                        if ($url == $urls["en"])
                            $needToUnset[] = $lng_abbr;
                    }
                }
                foreach ($needToUnset as $lng_abbr) {
                    unset($urls[$lng_abbr]);
                }

                foreach ($urls as $lng_abbr => $url) {
                    $urlParams = [
                        'lang' => $lng_abbr,
                        'url' => $url,
                        'subject_id' => $subject_id,
                        'page_type' => Url::PAGE_TYPE_COMMERCIAL_PAGE
                    ];
                    $urlParamsArr[] = $urlParams;
                }
                //echo "Processed key ".$mysqlGeoKey." (".$subject_id.")\n";
                if (count($urlParamsArr) > 0) {
                    //DB::table('urls')->insert($urlParamsArr);
                    $res = $this->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                    echo "generating urls for key ".$mysqlGeoKey." (".$subject_id."): saving ".count($urlParamsArr)."\n";
                    $urlParamsArr = [];
                }
            }
        }
    }

}
/*
         $start_time = time();
        $pResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM pois p where p.migrated is null and p.active = 1 and geotree_id>0 order by id limit 50000;');

        $i = 1;
        foreach ($pResults as $pResult) {
            $subjectId = $pResult->id."-".GeoTreeItem::TYPE_POI;

            $cResults = app('db')->connection('mysql2')
                ->select('SELECT * FROM poi_cats p_cats join poi_categories p_c on (p_cats.categories_id = p_c.id) where p_cats.pois_id = '.$pResult->id.' order by p_c.priority limit 1;');
            $categoryKey = "";
            if (count($cResults) > 0) {
                $categoryKey = $migrationService->stripCategortKey($cResults[0]->key);
            }

            $tResults = app('db')->connection('mysql2')
                ->select('SELECT * FROM poi_translations t join languages l on (t.language_id = l.id) where t.key="'.$pResult->key.'";');
            $poiName = "";
            foreach ($tResults as $tResult) {
                if ($tResult->abbrevation == "en") {
                    $poiName = trim($tResult->translation);
                }
            }
            //echo $i.") processing ".$poiName.", ".$categoryKey."\n";
            $geoResult = null;
            if (is_numeric($pResult->geotree_id))
                $geoResult = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_trees where id = :id', ['id' => $pResult->geotree_id]);

            if (!empty($geoResult)) {
                $mysqlGeoKey = $geoResult[0]->key_short;

                $keyParties = explode("_", $mysqlGeoKey);
                if (count($keyParties) > 3) {
                    $mysqlGeoCountryKey = $keyParties[0]."_".$keyParties[1]."_".$keyParties[2];
                } else {
                    $mysqlGeoCountryKey = $mysqlGeoKey;
                }
                $trans = [];
                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_translations t where t.key="'.$mysqlGeoCountryKey.'";');
                foreach ($tResults as $tResult) {
                    $trans[$mysqlGeoCountryKey][$tResult->language_abbrevation] = $tResult->translation;
                }
                array_pop($keyParties);
                $urls = [];
                $a = "poi";
                foreach ($trans[$mysqlGeoCountryKey] as $lng_abbr => $translation) {
                    if (!empty(str_slug($translation)))
                        $url =  $a . "/" . str_slug($translation)."/";
                    else
                        $url =  $a . "/" . str_slug($trans[$mysqlGeoCountryKey]["en"])."/"; // for zh language unable to slugify
                    foreach ($trans as $key => $value) {
                        if ($mysqlGeoCountryKey != $key) {
                            if (array_key_exists($lng_abbr, $trans[$key])) {
                                $url .= str_slug($trans[$key][$lng_abbr])."/";
                            } else if (array_key_exists("en", $trans[$key])) {
                                $url .= str_slug($trans[$key]["en"])."/";
                            } else
                                $url .= str_slug(reset($trans[$key]))."/";
                        }
                    }

                    //$urls[$lng_abbr] = str_replace("-", "__", $url); //
                    $urls[$lng_abbr] = $url.str_slug($categoryKey)."/".str_slug($poiName)."/"; //
                }
                $needToUnset = [];
                foreach ($urls as $lng_abbr => $url) {
                    if ($lng_abbr != "en") {
                        if ($url == $urls["en"])
                            $needToUnset[] = $lng_abbr;
                    }
                }
                foreach ($needToUnset as $lng_abbr) {
                    unset($urls[$lng_abbr]);
                }

                foreach ($urls as $lng_abbr => $url) {
                    $urlParams = [
                        'lang' => $lng_abbr,
                        'url' => $url,
                        'subject_id' => $subjectId,
                        'page_type' => Url::PAGE_TYPE_POI_PAGE
                    ];
                    //echo "---".implode(" ", $urlParams)."\n";
                    $urlParamsArr[] = $urlParams;
                }

                if (count($urlParamsArr) > 0) {
                    //DB::table('urls')->insert($urlParamsArr);
                    $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                    echo $i.") ".$poiName." (".$subjectId."): saving ".count($urlParamsArr)."\n";
                    $r = app('db')->connection('mysql2')
                        ->select('update pois set migrated=1 where id ='.$pResult->id);
                    $urlParamsArr = [];
                }
            }
            $i++;
        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";*/