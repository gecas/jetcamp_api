<?php

namespace App\Services;

use App\Breadcrumb;
use App\ElasticSearchAPI\ESClient;
use App\ElasticSearchAPI\GeoTreeItem;
use Illuminate\Support\Facades\Lang;

class BreadcrumbService
{

    private $esClient;
    private $urlsService;

    public function __construct(ESClient $esClient, UrlsService $urlsService)
    {
        $this->esClient = $esClient;
        $this->urlsService = $urlsService;
    }

    public function returnItemBreadcrumbs(GeoTreeItem $geoTreeItem, $language = "en") {
        if (property_exists($geoTreeItem, "key_short") and !empty($geoTreeItem->key_short)) {
            $main_key = $geoTreeItem->key_short;
        } else {
            $main_key = $geoTreeItem->key;
        }
        $keys = explode("_", $main_key);
        $terms = [];
        while (count($keys) > 2) {
            array_pop($keys);
            $key = implode("_", $keys);
            $terms[] = $key;
        }
        sort($terms);

        $items = $this->esClient->getGeoItemsByTerms("key", $terms, 0, 100);

        // adding main item key to the end of array (important) after items were retrieved
        // because getGeoItemsByTerms could return not needed items by main item key
        $terms[] = $main_key;
        $items[] = $geoTreeItem; // adding main item to the end
//        echo "<pre>";
//        var_dump($terms);
//        echo "</pre>";
//        echo "<pre>";
//        var_dump($items);
//        echo "</pre>";
        $breadcrumbItems = [];
        foreach ($terms as $index => $item_key) {
            $item = $this->returnItemByKey($items, $item_key);
            if (!empty($item)) {
                $url = $this->urlsService->getUrlByItemId($item->id, $language);
                switch ($item->type){
                    case GeoTreeItem::TYPE_CAMPING:
                    case GeoTreeItem::TYPE_COMMERCIAL:
                    case GeoTreeItem::TYPE_POI:
                        $name = $item->getItemNameTranslation($language);
                        if (!empty($item->parent_geo_id)) { // if commercial region's or camping's parent exists then need to add parent to breadcrumb list
                            $parentItemUrl = $this->urlsService->getUrlByItemId($item->parent_geo_id, $language);
                            if ($parentItemUrl) {
                                $parentItem = $this->esClient->getGeoItemById($item->parent_geo_id);
                                if ($parentItem) {
                                    $parentBreadCrumb = new Breadcrumb();
                                    $parentBreadCrumb->setName($parentItem->getTranslation($parentItem->translations, $language));
                                    $parentBreadCrumb->setUrl($parentItemUrl->url);
                                    $parentBreadCrumb->setId($parentItem->id);
                                    $parentBreadCrumb->setType($parentItem->type);
                                    $breadcrumbItems[] = $parentBreadCrumb;
                                }
                            }
                        }
                        break;
                    default:
                        $name = $item->getTranslation($item->translations, $language);
                        break;
                }

                if ($url) {
                    $breadcrumb = new Breadcrumb();
                    $breadcrumb->setName($name);
                    $breadcrumb->setUrl($url->url);
                    $breadcrumb->setId($item->id); // id and type is used in website data layer
                    $breadcrumb->setType($item->type);
                    $breadcrumbItems[] = $breadcrumb;
                }
            }
        }
        return $breadcrumbItems;
    }

    private function returnItemByKey($items, $key) {
        foreach ($items as $item) {
            if ($item->key == $key) {
                return $item;
            }
        }
        return null;
    }
}