<?php

namespace App\Services\Prebuild;

use App\Models\Accommodation;
use App\Models\GeoTree;
use App\Services\Prebuild\Models\Accommodation\UpdateAccommodationSearchUrl;
use App\Services\Prebuild\Models\Accommodation\UpdateAccommodationSingleItemUrl;
use App\Services\Prebuild\Models\Accommodation\SyncAccommodationToEs;
use App\Services\Prebuild\Models\GeoTree\CreateGeoTreeSearchUrl;
use App\Services\Prebuild\Models\GeoTree\SyncGeoTreeToEs;

/**
 * Class SyncToEsService
 * Class responsible for defined entity prebuild tasks.
 *
 * @package App\Services\SyncToEs
 */
class PrebuildService
{
    /**
     * @var array Mappings for model and tasks
     */
    protected $models = [
        Accommodation::class => [
            UpdateAccommodationSingleItemUrl::class,
            UpdateAccommodationSearchUrl::class,
            SyncAccommodationToEs::class,
        ],
        GeoTree::class => [
            CreateGeoTreeSearchUrl::class,
            SyncGeoTreeToEs::class,
        ]
    ];

    /**
     * Method responsible for figure out tasks for the model and executes them.
     *
     * @param $model
     * @throws \ReflectionException
     */
    public function make($model)
    {
        $reflect = new \ReflectionClass($model);

        if (!isset($this->models[$reflect->getName()])) {
            throw new \Exception('Model ' . $reflect->getName() . ' can not be made available');
        }

        foreach ($this->models[$reflect->getName()] as $jobClass) {
            $job = \app($jobClass);
            if ($job->isApplicable($model)) {
                $job->handle($model);
            }
        }
    }
}