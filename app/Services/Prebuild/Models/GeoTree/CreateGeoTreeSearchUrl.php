<?php

namespace App\Services\Prebuild\Models\GeoTree;

use App\Models\GeoTree;
use App\Models\Language;
use App\Models\Url;
use App\Repositories\UrlGeoItemSearchRepository;
use App\Services\UrlGenerators\GeoTreeUrlForSearchPageGeneratorService;

class CreateGeoTreeSearchUrl
{
    protected $urlGenerator;
    protected $urlGeoItemSearchRepository;

    /**
     * CreateGeoTreeSearchUrl constructor.
     * @param GeoTreeUrlForSearchPageGeneratorService $urlGenerator
     * @param UrlGeoItemSearchRepository $urlGeoItemSearchRepository
     */
    public function __construct(
        GeoTreeUrlForSearchPageGeneratorService $urlGenerator,
        UrlGeoItemSearchRepository $urlGeoItemSearchRepository
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->urlGeoItemSearchRepository = $urlGeoItemSearchRepository;
    }

    /**
     * Rebuild URL.
     *
     * @param GeoTree $geoTree
     */
    public function handle(GeoTree $geoTree)
    {
        Language::frontActive()->get()->each(function (Language $language) use ($geoTree) {
            if (!Url::GeoItemUrlSearch($geoTree->id, $language->abbrevation)->first()) {
                $this->urlGeoItemSearchRepository->create($geoTree->id
                    , $language->abbrevation
                    , $this->urlGenerator->generate($geoTree, $language));
            }
        });
    }

    /**
     * Whether to execute task
     *
     * @param GeoTree $geoTree
     * @return bool
     */
    public function isApplicable(GeoTree $geoTree)
    {
        return true;
    }
}