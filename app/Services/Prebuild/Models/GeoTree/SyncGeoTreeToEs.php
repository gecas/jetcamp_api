<?php

namespace App\Services\Prebuild\Models\GeoTree;

use App\Models\GeoTree;
use App\Services\SyncToEs\SyncToEsService;

class SyncGeoTreeToEs
{
    protected $syncToEsService;

    /**
     * SyncGeoTreeToEs constructor.
     * @param SyncToEsService $syncToEsService
     */
    public function __construct(SyncToEsService $syncToEsService)
    {
        $this->syncToEsService = $syncToEsService;
    }

    /**
     * Sync geo tree item to ES
     *
     * @param GeoTree $geoTree
     * @throws \ReflectionException
     */
    public function handle(GeoTree $geoTree)
    {
        $this->syncToEsService->sync($geoTree);
    }

    /**
     * Whether to execute task
     *
     * @param GeoTree $geoTree
     * @return bool
     */
    public function isApplicable(GeoTree $geoTree)
    {
        return true;
    }
}