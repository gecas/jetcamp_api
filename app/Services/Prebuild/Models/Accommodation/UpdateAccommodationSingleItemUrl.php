<?php

namespace App\Services\Prebuild\Models\Accommodation;

use App\Models\Accommodation;
use App\Models\Language;
use App\Models\Url;
use App\Repositories\UrlAccommodationSingleItemRepository;
use App\Services\UrlGenerators\AccommodationUrlForSingleItemPageGenerator;

class UpdateAccommodationSingleItemUrl
{
    protected $urlGenerator;
    protected $urlAccommodationSingleItemRepository;

    /**
     * UpdateAccommodationSingleItemUrl constructor.
     * @param AccommodationUrlForSingleItemPageGenerator $urlGenerator
     * @param UrlAccommodationSingleItemRepository $urlAccommodationSingleItemRepository
     */
    public function __construct(
        AccommodationUrlForSingleItemPageGenerator $urlGenerator,
        UrlAccommodationSingleItemRepository $urlAccommodationSingleItemRepository
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->urlAccommodationSingleItemRepository = $urlAccommodationSingleItemRepository;
    }

    /**
     * Rebuild URL.
     *
     * @param Accommodation $accommodation
     */
    public function handle(Accommodation $accommodation)
    {
        Language::frontActive()->get()->each(function (Language $language) use ($accommodation) {
            $urlModel = Url::accommodationUrlSingle($accommodation->id, $language->abbrevation)->first();
            if (!$urlModel) {
                $this->urlAccommodationSingleItemRepository->create($accommodation->id
                    , $language->abbrevation
                    , $this->urlGenerator->generate($accommodation, $language));
            } else {
                $newUrl = $this->urlGenerator->generate($accommodation, $language, false);
                if ($urlModel->urlSuffixLess != $newUrl) {
                    echo 123;
                    $this->urlAccommodationSingleItemRepository->update($accommodation->id
                        , $language->abbrevation
                        , $this->urlGenerator->generate($accommodation, $language)
                    );
                }


            }
        });
    }

    /**
     * Whether to execute task
     *
     * @param Accommodation $accommodation
     * @return bool
     */
    public function isApplicable(Accommodation $accommodation)
    {
        return (bool)$accommodation->status;
    }
}