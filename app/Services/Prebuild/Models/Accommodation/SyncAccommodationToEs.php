<?php

namespace App\Services\Prebuild\Models\Accommodation;

use App\Models\Accommodation;
use App\Services\SyncToEs\SyncToEsService;

class SyncAccommodationToEs
{
    protected $syncToEsService;

    /**
     * SyncAccommodationToEs constructor.
     * @param SyncToEsService $syncToEsService
     */
    public function __construct(SyncToEsService $syncToEsService)
    {
        $this->syncToEsService = $syncToEsService;
    }

    /**
     * Sync accommodation to ES
     *
     * @param Accommodation $accommodation
     */
    public function handle(Accommodation $accommodation)
    {
        $this->syncToEsService->sync($accommodation);
    }

    /**
     * Whether to execute task
     *
     * @param Accommodation $accommodation
     * @return bool
     */
    public function isApplicable(Accommodation $accommodation)
    {
        return (bool)$accommodation->status;
    }
}