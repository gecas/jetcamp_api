<?php

namespace App\Services\UrlGenerators;

use App\Models\GeoTree;
use App\Models\Language;
use App\Services\UniqueUrlService;

/*
 * Class generates URL for commercial region search page
 */

class GeoTreeUrlForSearchPageGeneratorService
{
    protected $uniqueUrlService;

    /**
     * @param UniqueUrlService $uniqueUrlService
     * @return void
     */
    public function __construct(UniqueUrlService $uniqueUrlService)
    {
        $this->uniqueUrlService = $uniqueUrlService;
    }

    /**
     * Generate link
     * en/italy
     * @param GeoTree $geoTree
     * @param Language $language
     * @return string
     */
    public function generate(GeoTree $geoTree, Language $language)
    {
        if ($geoTree->depth == 1) {
            $url = $this->getContinentLink($geoTree, $language);
        } else {
            $url = $this->getDefaultLink($geoTree, $language);
        }

        return $this->uniqueUrlService->get($language->abbrevation, $url);
    }

    /**
     * @param GeoTree $geoTree
     * @param Language $language
     * @return string
     */
    protected function getDefaultLink(GeoTree $geoTree, Language $language)
    {
        $items = $geoTree->withParents(true)->get();

        //Remove continent
        $parents = $items->slice(1);

        $url = '';

        $parents->each(function (GeoTree $item) use (&$url, $language) {
            $translationModel = $item->translations()->language($language->abbrevation)->first();

            //Fallback to english
            if (!$translationModel) {
                $translationModel = $item->translations()->language('en')->first();
            }

            $translation = $translationModel->translation ?? $item->name;

            $url = $url . str_slug($translation) . '/';
        });

        return $url;
    }

    /**
     * @param GeoTree $geoTree
     * @param Language $language
     * @return string
     */
    protected function getContinentLink(GeoTree $geoTree, Language $language)
    {
        $translation = $geoTree->translations()->language($language->abbrevation)->first();

        //Fallback to english
        if (!$translation) {
            $translation = $geoTree->translations()->language('en')->firstOrException();
        }

        return str_slug($translation->translation) . '/';
    }
}