<?php

namespace App\Services\UrlGenerators;

use App\Models\CommercialRegionTranslation;
use App\Services\UniqueUrlService;

/*
 * Class generates URL for commercial region landing page
 */

class CommercialRegionUrlForSingleItemPageGenerateService
{
    protected $uniqueUrlService;

    protected $prefix = 'commercial/';

    /**
     * @param UniqueUrlService $uniqueUrlService
     * @return void
     */
    public function __construct(UniqueUrlService $uniqueUrlService)
    {
        $this->uniqueUrlService = $uniqueUrlService;
    }

    /**
     * Generate link
     * @param CommercialRegionTranslation $translation
     * @return string
     */
    public function generate(CommercialRegionTranslation $translation)
    {
        $commercialRegion = $translation->commercial_region()->firstOrException();
        $language = $translation->language()->firstOrException();
        $geoItem = $commercialRegion->geo_tree()->firstOrException();
        $country = $geoItem->withParents()->get()->get(1);
        $countryTranslation = $country->translations()->language($language->abbrevation)->firstOrException();

        $url = $this->prefix . str_slug($countryTranslation->translation) . '/' . str_slug($translation->translation) . '/';

        return $this->uniqueUrlService->get($language->abbrevation, $url);

    }
}