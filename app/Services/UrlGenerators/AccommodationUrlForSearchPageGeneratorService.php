<?php

namespace App\Services\UrlGenerators;

use App\Models\Accommodation;
use App\Models\GeoTree;
use App\Models\Language;
use App\Services\UniqueUrlService;

/*
 * Class generates URL for commercial region search page
 */

class AccommodationUrlForSearchPageGeneratorService
{

    protected $uniqueUrlService;

    /**
     * @param UniqueUrlService $uniqueUrlService
     * @return void
     */
    public function __construct(UniqueUrlService $uniqueUrlService)
    {
        $this->uniqueUrlService = $uniqueUrlService;
    }

    /**
     * Generate link
     * netherlands/provincie-overijssel/denekamp/erve-wezenberg/
     * @param Accommodation $accommodation
     * @param Language $language
     * @param bool $unique
     * @return string
     */
    public function generate(Accommodation $accommodation, Language $language, $unique = true)
    {
        $geo = $accommodation->geo_tree;
        $parents = $geo->withParents()->get();
        //Remove continent
        $parents = $parents->slice(1);

        $url = '';

        $parents->each(function (GeoTree $item) use (&$url, $language) {
            $translation = $item->translations()->language($language->abbrevation)->first();

            //Fallback to english
            if (!$translation) {
                $translation = $item->translations()->language('en')->firstOrException();
            }

            $url = $url . str_slug($translation->translation) . '/';
        });

        $url = $url . str_slug($accommodation->name) . '/';

        if ($unique) {
            return $this->uniqueUrlService->get($language->abbrevation, $url);
        } else {
            return $url;
        }
    }
}