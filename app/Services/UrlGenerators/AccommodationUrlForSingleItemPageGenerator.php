<?php

namespace App\Services\UrlGenerators;

use App\Models\Accommodation;
use App\Models\Language;
use App\Services\UniqueUrlService;

/*
 * Class generates URL for commercial region landing page
 */

class AccommodationUrlForSingleItemPageGenerator
{
    protected $uniqueUrlService;

    /**
     * @param UniqueUrlService $uniqueUrlService
     * @return void
     */
    public function __construct(UniqueUrlService $uniqueUrlService)
    {
        $this->uniqueUrlService = $uniqueUrlService;
    }

    /**
     * Generate link
     * @param Accommodation $accommodation
     * @param Language $language
     * @param bool $unique
     * @return string
     */
    public function generate(Accommodation $accommodation, Language $language, $unique = true)
    {
        $geo = $accommodation->geo_tree;

        //Get all parents and take second item
        $country = $geo->withParents()->get()->get(1);

        $translation = $country->translations()->language($language->abbrevation)->first();

        if (!$translation) {
            $countryTranslation = $country->translations()->language('en')->first()->translation ?? $country->name;
        } else {
            $countryTranslation = $translation->translation;
        }

        $url = str_slug($countryTranslation) . '/' . str_slug($accommodation->name) . '/';

        if ($unique) {
            return $this->uniqueUrlService->get($language->abbrevation, $url);
        } else {
            return $url;
        }
    }
}