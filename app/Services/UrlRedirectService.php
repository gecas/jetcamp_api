<?php

namespace App\Services;

use App\Models\UrlRedirect;

/**
 * Service allows to store redirect link.
 *
 * Redirect links are stored in "url_redirects table" where fields "from", "to"
 * defines where given link should redirect.
 *
 * It also handles cases when there is redirect a->b and when trying create redirect b->a
 * the previous redirect is deleted because link a is available again.
 *
 * It also handles cases when there is redirect a->b and when trying create redirect b->c
 * it will update a->b redirect to a->c.
 *
 * Class UrlRedirectService
 * @package App\Services
 */
class UrlRedirectService
{
    /**
     * Add redirect url
     * @param string $from Link from
     * @param string $to Link to
     * @return \App\Models\UrlRedirect
     */
    public function add($from, $to)
    {
        $this->deleteLinkBack($from, $to);
        $this->updateToLinks($from, $to);
        return $this->createLink($from, $to);
    }

    /**
     * Persist a link
     * @param string $from
     * @param string $to
     * @return \App\Models\UrlRedirect
     */
    protected function createLink($from, $to)
    {
        return UrlRedirect::create([
            'from' => $from,
            'to' => $to
        ]);
    }

    /**
     * Delete a back link
     * This handles the case when there is a redirect a->b and link
     * b->a is beeing created so a->b is not needed anymore.
     * @param $from
     * @param $to
     */
    protected function deleteLinkBack($from, $to)
    {
        $redirect = UrlRedirect::where('from', $to)
            ->where('to', $from)->first();
        if ($redirect) {
            $redirect->delete();
        }
    }

    /**
     * Update to links
     * This handles the case when there is a redirect a->b and redirect b->c
     * is being created so a->b becomes a->c
     * @param $from
     * @param $to
     */
    protected function updateToLinks($from, $to)
    {
        UrlRedirect::where('to', $from)->get()->each(function (UrlRedirect $redirect) use ($to) {
            $redirect->to = $to;
            $redirect->save();
        });
    }
}