<?php

namespace App\Services\SyncToEs\Schemas;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\GeoTreeTranslation;
use App\Models\Language;
use App\Repositories\GeoItemConnectionCountRepository;
use App\Repositories\IndirectActiveAccommodationCountWithinCircleRepository;

class GeoTreeSchema
{
    protected $geoItemConnectionCountRepository;
    protected $indirectActiveAccommodationCountWithinCircleRepository;

    public function __construct(
        GeoItemConnectionCountRepository $geoItemConnectionCountRepository,
        IndirectActiveAccommodationCountWithinCircleRepository $indirectActiveAccommodationCountWithinCircleRepository
    ) {
        $this->geoItemConnectionCountRepository = $geoItemConnectionCountRepository;
        $this->indirectActiveAccommodationCountWithinCircleRepository = $indirectActiveAccommodationCountWithinCircleRepository;
    }

    /**
     * Get model ID in ES
     *
     * @param $geoTree
     * @return string
     */
    public function getId($geoTree)
    {
        return (string)($geoTree->id . '-1');
    }

    /**
     * Prepare data to be sent to ES.
     *
     * @param $geoTree
     * @return array
     */
    public function getData($geoTree)
    {
        return [
            'type' => GeoTreeItem::TYPE_GEOITEM,
            'geo_id' => $geoTree->id,
            'id' => $this->getId($geoTree),
            'subtype' => $geoTree->geonamesFcode,
            'key' => $geoTree->key,
            'name' => $geoTree->name,
            'gps_coords' => [
                'lat' => $geoTree->gpsLat,
                'lon' => $geoTree->gpsLon,
            ],
            'translations' => $this->getTranslations($geoTree),
            'camping_cnt' => $this->geoItemConnectionCountRepository->findActiveDirectCampsitesCount($geoTree),
            'camping_cnt_10' => $this->indirectActiveAccommodationCountWithinCircleRepository->get($geoTree->gpsLat,
                $geoTree->gpsLon, 10),
            'camping_cnt_25' => $this->indirectActiveAccommodationCountWithinCircleRepository->get($geoTree->gpsLat,
                $geoTree->gpsLon, 25),
            'camping_cnt_50' => $this->indirectActiveAccommodationCountWithinCircleRepository->get($geoTree->gpsLat,
                $geoTree->gpsLon, 50),
            'rating' => '',
            'popularity' => $geoTree->popularity,
            'search_score' => '',
            'parent_geo_id' => $geoTree->parent . '-1',
            'show_on_acco_whattodo' => $geoTree->show_on_acco_whattodo,
            'name_synonyms' => '',
            'subline' => $this->getSubline($geoTree),
        ];
    }

    /**
     * Get translations.
     *
     * @param $geoTree
     * @return mixed
     */
    private function getTranslations($geoTree)
    {
        $activeLanguages = Language::frontActive()->get(['id'])->pluck('id')->toArray();
        return $geoTree->translations()
            ->whereIn('language_id', $activeLanguages)
            ->get()
            ->transform(function (GeoTreeTranslation $translation) {
                return [
                    'lng' => $translation->language_abbrevation,
                    'trans' => $translation->translation,
                ];
            })->toArray();
    }

    /**
     * Get subline.
     *
     * @param $geoTree
     * @return mixed
     */
    private function getSubline($geoTree)
    {
        $activeLanguages = Language::frontActive()->get(['id', 'abbrevation']);
        $geoChain = $geoTree->withParents()->with([
            'translations' => function ($query) use ($activeLanguages) {
                $query->whereIn('language_id', $activeLanguages->pluck('id')->toArray());
            }
        ])->get();

        return collect([
            'continent_translations' => function () use ($geoChain, $activeLanguages) {
                $geoItem = $geoChain[0];
                $fallbackTrans = $geoItem->translations->where('language_id',
                        1)->first()->translation ?? $geoItem->name;

                return $activeLanguages->map(function (Language $language) use ($geoItem, $fallbackTrans) {
                    return [
                        'lng' => $language->abbrevation,
                        'trans' => $geoItem->translations->where('language_id',
                                $language->id)->first()->translation ?? $fallbackTrans,
                    ];
                });
            },
            'country_translations' => function () use ($geoChain, $activeLanguages) {
                if (!isset($geoChain[1])) {
                    return [];
                }
                $geoItem = $geoChain[1];
                $fallbackTrans = $geoItem->translations->where('language_id',
                        1)->first()->translation ?? $geoItem->name;

                return $activeLanguages->map(function (Language $language) use ($geoItem, $fallbackTrans) {
                    return [
                        'lng' => $language->abbrevation,
                        'trans' => $geoItem->translations->where('language_id',
                                $language->id)->first()->translation ?? $fallbackTrans,
                    ];
                });
            },
            'region_translations' => function () use ($geoChain, $activeLanguages) {
                if (!isset($geoChain[2])) {
                    return [];
                }
                $geoItem = $geoChain[2];
                $fallbackTrans = $geoItem->translations->where('language_id',
                        1)->first()->translation ?? $geoItem->name;

                return $activeLanguages->map(function (Language $language) use ($geoItem, $fallbackTrans) {
                    return [
                        'lng' => $language->abbrevation,
                        'trans' => $geoItem->translations->where('language_id',
                                $language->id)->first()->translation ?? $fallbackTrans,
                    ];
                });
            },
        ])->callableToValue()->toArray();
    }
}