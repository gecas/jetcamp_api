<?php

namespace App\Services\SyncToEs\Schemas;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\GeoTree;
use App\Models\GeoTreeTranslation;
use App\Models\Language;
use App\Models\Url;
use App\Repositories\IndirectActiveAccommodationCountWithinCircleRepository;
use App\Repositories\UrlAccommodationSearchItemRepository;
use App\Repositories\UrlGeoItemSearchRepository;
use App\Services\UrlGenerators\GeoTreeUrlForSearchPageGeneratorService;

class AccommodationSchema
{
    protected $indirectActiveAccommodationCountWithinCircleRepository;
    protected $geoTreeUrlForSearchPageGeneratorService;
    protected $urlGeoItemSearchRepository;
    protected $geoItemSearchPageUrlGeneratorService;

    /**
     * AccommodationSchema constructor.
     * @param IndirectActiveAccommodationCountWithinCircleRepository $indirectActiveAccommodationCountWithinCircleRepository
     * @param GeoTreeUrlForSearchPageGeneratorService $geoTreeUrlForSearchPageGeneratorService
     * @param UrlGeoItemSearchRepository $urlGeoItemSearchRepository
     * @param GeoTreeUrlForSearchPageGeneratorService $geoItemSearchPageUrlGeneratorService
     */
    public function __construct(
        IndirectActiveAccommodationCountWithinCircleRepository $indirectActiveAccommodationCountWithinCircleRepository,
        GeoTreeUrlForSearchPageGeneratorService $geoTreeUrlForSearchPageGeneratorService,
        UrlGeoItemSearchRepository $urlGeoItemSearchRepository,
        GeoTreeUrlForSearchPageGeneratorService $geoItemSearchPageUrlGeneratorService
    ) {
        $this->indirectActiveAccommodationCountWithinCircleRepository = $indirectActiveAccommodationCountWithinCircleRepository;
        $this->geoTreeUrlForSearchPageGeneratorService = $geoTreeUrlForSearchPageGeneratorService;
        $this->urlGeoItemSearchRepository = $urlGeoItemSearchRepository;
        $this->geoItemSearchPageUrlGeneratorService = $geoItemSearchPageUrlGeneratorService;

    }

    /**
     * Get model ID in ES
     *
     * @param $accommodation
     * @return string
     */
    public function getId($accommodation)
    {
        return (string)($accommodation->id . '-2');
    }

    /**
     * Prepare data to be sent to ES.
     *
     * @param $accommodation
     * @return array
     */
    public function getData($accommodation)
    {
        return [
            'type' => GeoTreeItem::TYPE_CAMPING,
            'geo_id' => $accommodation->geo_trees_id,
            'id' => $this->getId($accommodation),
            'subtype' => $accommodation->geo_tree->geonamesFcode,
            'key' => $accommodation->geo_tree->key,
            'item' => (object)[
                'name' => $accommodation->name,
                'name_synonyms' => [],
                'rating' => (float)$accommodation->customer_rating_average,
                'rating_count' => (int)$accommodation->customer_rating_count,
                'rating_stars' => (int)$accommodation->official_rating,
                'popularity' => (int)$accommodation->popularity,
                'size_nr_total' => (int)$accommodation->size_nr_total,
                'address' => (object)[
                    'street' => $accommodation->address_street,
                    'housenumber' => $accommodation->address_housenumber,
                    'city' => $accommodation->address_city,
                    'postcode' => $accommodation->address_postcode,
                ],
                'gps_coords' => (object)[
                    'lat' => (float)$accommodation->address_gps_lat,
                    'lon' => (float)$accommodation->address_gps_lon,
                ],
            ],
            'name' => $accommodation->geo_tree->name,
            'gps_coords' => (object)[
                'lat' => $accommodation->geo_tree->gpsLat,
                'lon' => $accommodation->geo_tree->gpsLon,
            ],
            'translations' => [
                (object)[
                    'lng' => 'en',
                    'trans' => $accommodation->name
                ]
            ],
            'camping_cnt' => 0,
            'camping_cnt_10' => $this->getIndirectActiveAccommodationCountWithinCircle($accommodation, 10),
            'camping_cnt_25' => $this->getIndirectActiveAccommodationCountWithinCircle($accommodation, 25),
            'camping_cnt_50' => $this->getIndirectActiveAccommodationCountWithinCircle($accommodation, 50),
            'rating' => '',
            'popularity' => $accommodation->geo_tree->po,
            'search_score' => '',
            'parent_geo_id' => $accommodation->geo_tree->id . '-1',
            'breadcrumbs' => $this->getBreadcrumbs($accommodation),
            'key_short' => $accommodation->geo_tree->key_short,
            'name_synonyms' => [],//@todo
            'subline' => [] //@todo breadcrumbs can be used instead
        ];
    }

    /**
     * Get indirect counts values.
     *
     * @param $accommodation
     * @param $radius
     * @return mixed
     */
    private function getIndirectActiveAccommodationCountWithinCircle($accommodation, $radius)
    {
        return $this->indirectActiveAccommodationCountWithinCircleRepository->get(
            $accommodation->address_gps_lat,
            $accommodation->address_gps_lon,
            $radius,
            [$accommodation->id]
        );
    }

    /**
     * Form breadcrumbs part.
     *
     * @param $accommodation
     * @return mixed
     */
    private function getBreadcrumbs($accommodation)
    {
        $activeLanguages = Language::frontActive()->get(['id', 'abbrevation']);
        $geoItems = $accommodation->geo_tree->withParents()->get();

        return $geoItems->transform(function (GeoTree $geoItem) use ($activeLanguages) {
            $enTitle = $geoItem->translations()->where('language_abbrevation',
                    'en')->first()->translation ?? $geoItem->name;

            return [
                'key' => $geoItem->key,
                'translations' => $activeLanguages->mapWithKeys(function (Language $language) use (
                    $enTitle,
                    $geoItem
                ) {
                    if ($language->abbrevation == 'en') {
                        $title = $enTitle;
                    } else {
                        $title = $geoItem->translations()
                                ->where('language_abbrevation', $language->abbrevation)
                                ->first()
                                ->translation ?? $enTitle;
                    }

                    $urlModel = Url::geoItemUrlSearch($geoItem->id, $language->abbrevation)->first();

                    if (!$urlModel) {
                        $urlModel = $this->urlGeoItemSearchRepository->create($geoItem->id
                            , $language->abbrevation
                            , $this->geoTreeUrlForSearchPageGeneratorService->generate($geoItem, $language));
                    }

                    return [
                        $language->abbrevation => [
                            'url' => $urlModel->fullUrl,
                            'title' => $title,
                        ]
                    ];
                })->toArray()
            ];
        })->toArray();
    }
}