<?php

namespace App\Services\SyncToEs;

use App\Models\EsGeoItem;
use App\Repositories\EsGeoItemRepository;

/**
 * Class SyncToEsService
 * Class responsible for creating/updating item to ES. It uses "schemas" which are responsible for getting
 * data for different types of models.
 * @package App\Services\SyncToEs
 */
class SyncToEsService
{
    protected $esRepository;

    /**
     * SyncToEsService constructor.
     * @param EsGeoItemRepository $esRepository
     */
    public function __construct(EsGeoItemRepository $esRepository)
    {
        $this->esRepository = $esRepository;
    }

    /**
     * @param $model
     * @throws \ReflectionException
     */
    public function sync($model)
    {
        $reflect = new \ReflectionClass($model);
        $schemaName = '\\' . __NAMESPACE__ . '\Schemas\\' . $reflect->getShortName() . 'Schema';
        $schema = \app($schemaName);
        $id = $schema->getId($model);
        $data = $schema->getData($model);

        if (EsGeoItem::id($id)->first()) {
            $this->esRepository->update($id, $data);
        } else {
            $this->esRepository->store($data);
        }
    }
}