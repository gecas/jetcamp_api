<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    protected $table = 'accommodations';

    protected $fillable = [
        'accommodation_scraped_id', 'name', 'address_phone', 'address_website', 'address_street', 'address_housenumber', 'address_housenumber_addition',
        'address_postcode', 'address_city', 'address_country', 'address_gps_lat', 'address_gps_lon', 'address_gps_point', 'address_phone_1', 'address_phone_2',
        'address_email_public', 'social_facebook', 'social_twitter', 'social_foursquare', 'customer_rating_count', 'customer_rating_average', 'official_rating',
        'popularity', 'size_nr_total', 'size_nr_recreational_pitches', 'size_nr_year_pitches', 'size_nr_mobilehomerentals', 'size_nr_houseorappartments',
        'geo_trees_id', 'price_highseason', 'price_adult', 'price_animal', 'price_camper', 'price_caravan', 'price_tent', 'general_nudist_acco'
        ];

    public function mainFacilities() {
        return $this->belongsToMany('App\FacilityMain', 'accommodation_facility_mains', 'accommodation_id', 'facility_main_id');
    }

    public function subFacilities() {
        return $this->belongsToMany('App\FacilitySub', 'accommodation_facility_subs', 'accommodation_id', 'facility_sub_id');
    }

    public function accommodationAdditionalDistance(){
        return $this->hasMany('App\AccommodationAdditionalDistance', 'accommodation_id');
    }

    public function accommodationAdditionalCost(){
        return $this->hasMany('App\AccommodationAdditionalCost', 'accommodation_id');
    }
}