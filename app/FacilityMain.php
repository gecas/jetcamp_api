<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class FacilityMain extends Model
{
    protected $table = 'facility_mains';

    protected $fillable = [
        'facility_theme_id', 'show_add_costs', 'show_add_distances', 'show_add_openingdates', 'show_add_openinghours', 'show_add_size_in_cm',
        'show_add_size_in_m_lwh', 'show_add_numbers', 'show_add_language', 'show_add_age', 'show_add_weekdays', 'show_add_frequency',
        'description'
    ];

}