<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class PointOfInterestTranslationSchema extends BaseSchema
{
    protected $resourceType = 'poi_translations';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'translation',
            'language_id',
            'key',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}