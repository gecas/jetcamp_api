<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class GeoItemSchema extends BaseSchema
{
    protected $resourceType = 'geo_items';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return [
            'title' => $model->title
        ];
    }
}