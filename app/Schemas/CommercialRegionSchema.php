<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class CommercialRegionSchema extends BaseSchema
{
    protected $resourceType = 'commercial_regions';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'key',
            'parent_id',
            'active',
            'popularity',
            'category_id',
            'gpsPolygon',
            'created_at',
            'updated_at',
            'migrated'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('translations_in_all_languages', $includeList)) {
            $res['translations_in_all_languages'][self::DATA] = $model->translations_in_all_languages;
        }

        return $res;
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}