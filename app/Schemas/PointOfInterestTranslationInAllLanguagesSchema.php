<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class PointOfInterestTranslationInAllLanguagesSchema extends BaseSchema
{
    protected $resourceType = 'poi_translations_in_all_languages';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'poi_id',
            'poi_key',
            'language_id',
            'language_abbr',
            'translation'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}