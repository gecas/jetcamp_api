<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class FacilityThemeSchema extends BaseSchema
{
    protected $resourceType = 'facility_themes';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'description'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('translation', $includeList)) {
            $res['translation'][self::DATA] = $model->translation;
        }

        return $res;
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}