<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class UrlSchema extends BaseSchema
{
    protected $resourceType = 'url';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return [
            'lang' => $model->lang,
            'url' => $model->url,
            'subject_id' => $model->subject_id,
            'page_type' => $model->page_type,
        ];
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}