<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class AccommodationThemeCountSchema extends BaseSchema
{
    protected $resourceType = 'accommodation_theme_count';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'theme_id',
            'theme_key',
            'accommodation_id',
            'c'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('translation', $includeList)) {
            $res['translation'][self::DATA] = $model->translation;
        }

        return $res;
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}