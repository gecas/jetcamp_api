<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class UrlRedirectSchema extends BaseSchema
{
    protected $resourceType = 'url_redirects';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return [
            'from' => $model->from,
            'to' => $model->to,
        ];
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}