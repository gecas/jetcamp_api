<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class CommercialRegionTranslationSchema extends BaseSchema
{
    protected $resourceType = 'commercial_region_translations';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'key',
            'language_id',
            'translation',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}