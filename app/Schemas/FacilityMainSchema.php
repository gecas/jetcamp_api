<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class FacilityMainSchema extends BaseSchema
{
    protected $resourceType = 'facility_mains';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return [
            'description' => $model->description
        ];
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('translation', $includeList)) {
            $res['translation'][self::DATA] = $model->translation;
        }

        return $res;
    }
}