<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class GeoTreeTranslationInAllLanguagesSchema extends BaseSchema
{
    protected $resourceType = 'geo_tree_translations_in_all_languages';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'geo_tree_id',
            'geo_tree_key',
            'language_id',
            'language_abbr',
            'translation'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}