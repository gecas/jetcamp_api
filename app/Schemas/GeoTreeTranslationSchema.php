<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class GeoTreeTranslationSchema extends BaseSchema
{
    protected $resourceType = 'geo_translations';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'translation',
            'language_id',
            'key',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}