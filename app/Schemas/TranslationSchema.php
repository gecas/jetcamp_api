<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class TranslationSchema extends BaseSchema
{
    protected $resourceType = 'translation';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return [
            'project' => $model->project,
            'locale' => $model->locale,
            'group' => $model->group,
            'key' => $model->key,
            'text' => $model->text,
            'created_at' => $model->created_at ? $model->created_at->toDateTimeString() : null,
            'updated_at' => $model->updated_at ? $model->updated_at->toDateTimeString() : null,
        ];
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}