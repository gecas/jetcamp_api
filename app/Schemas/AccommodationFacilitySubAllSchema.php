<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class AccommodationFacilitySubAllSchema extends BaseSchema
{
    protected $resourceType = 'accommodation_facility_subs_all';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'accommodation_id',
            'facility_sub_id',
            'facility_main_id',
            'checked',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('facility_sub', $includeList)) {
            $res['facility_sub'][self::DATA] = $model->facility_sub;
        }

        return $res;
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}