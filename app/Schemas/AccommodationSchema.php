<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class AccommodationSchema extends BaseSchema
{
    protected $resourceType = 'accommodations';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'accommodation_scraped_id',
            'name',
            'address_phone',
            'address_website',
            'address_street',
            'address_housenumber',
            'address_housenumber_addition',
            'address_postcode',
            'address_city',
            'address_country',
            'address_gps_lat',
            'address_gps_lon',
            'address_phone_1',
            'address_phone_2',
            'address_email_public',
            'social_facebook',
            'social_twitter',
            'social_foursquare',
            'customer_rating_count',
            'customer_rating_average',
            'official_rating',
            'popularity',
            'size_nr_total',
            'size_nr_recreational_pitches',
            'size_nr_year_pitches',
            'size_nr_mobilehomerentals',
            'size_nr_houseorappartments',
            'geo_trees_id',
            'price_highseason',
            'price_adult',
            'price_animal',
            'price_camper',
            'price_caravan',
            'price_tent',
            'general_nudist_acco',
            'distance_to_highway',
            'business_address_street',
            'business_address_housenumber',
            'business_address_housenumber_addition',
            'business_address_postcode',
            'business_address_city',
            'business_address_country',
            'business_address_phone_1',
            'business_address_phone_2',
            'business_address_email',
            'size_area_squaremeter',
            'created_at',
            'updated_at',
            'migrated',
            'status'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('geo_tree', $includeList)) {
            $res['geo_tree'][self::DATA] = $model->geo_tree;
        }

        return $res;
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}