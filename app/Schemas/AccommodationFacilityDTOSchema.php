<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class AccommodationFacilityDTOSchema extends BaseSchema
{
    protected $resourceType = 'accommodation_facility_dto';

    public function getId($model): ?string
    {
        return $model->accommodationId;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return ['facilities' => $model->facilities];
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}