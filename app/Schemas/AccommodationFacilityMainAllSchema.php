<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class AccommodationFacilityMainAllSchema extends BaseSchema
{
    protected $resourceType = 'accommodation_facility_mains_all';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'accommodation_id',
            'facility_main_id',
            'facility_theme_id',
            'checked',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('facility_main', $includeList)) {
            $res['facility_main'][self::DATA] = $model->facility_main;
        }

        return $res;
    }

    public function getResourceLinks($resource): array
    {
        return [];
    }
}