<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class PointOfInterestSchema extends BaseSchema
{
    protected $resourceType = 'pois';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {

        $fields = [
            'name_en',
            'city',
            'street',
            'streetnumber',
            'postalcode',
            'geotree_id',
            'phone',
            'domain',
            'lat',
            'lon',
            'rating_count',
            'rating_value',
            'active',
            'popularity',
            'key',
            'open_monday',
            'open_tuesday',
            'open_wednesday',
            'open_thursday',
            'open_friday',
            'open_saturday',
            'open_sunday',
            'close_monday',
            'close_tuesday',
            'close_wednesday',
            'close_thursday',
            'close_friday',
            'close_saturday',
            'close_sunday',
            'open_always',
            'permantly_closed',
            'ta_id',
            'google_id',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('translations', $includeList)) {
            $res['translations'][self::DATA] = $model->translations;
        }
        if (in_array('translations_in_all_languages', $includeList)) {
            $res['translations_in_all_languages'][self::DATA] = $model->translations_in_all_languages;
        }

        return $res;
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}