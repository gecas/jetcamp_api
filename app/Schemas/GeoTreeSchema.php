<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class GeoTreeSchema extends BaseSchema
{
    protected $resourceType = 'geo_trees';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {

        $fields = [
            'name',
            'asciname',
            'alternatenames',
            'parent',
            'parent_short',
            'key',
            'key_short',
            'idGeonames',
            'geonamesFcl',
            'geonamesFcode',
            'continentCode',
            'countryCode',
            'population',
            'elevation',
            'timezoneGmtOffset',
            'timezoneDstOffset',
            'wikipediaLink',
            'popularity',
            'show_on_acco_whattodo',
            'migrated',
            'gpsLat',
            'gpsLon',
            'gpsBboxEast',
            'gpsBboxSouth',
            'gpsBboxNorth',
            'gpsBboxWest',
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }

    public function getRelationships($model, bool $isPrimary, array $includeList): ?array
    {
        $res = [];
        if (in_array('translations', $includeList)) {
            $res['translations'][self::DATA] = $model->translations;
        }
        if (in_array('translations_in_all_languages', $includeList)) {
            $res['translations_in_all_languages'][self::DATA] = $model->translations_in_all_languages;
        }

        return $res;
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}