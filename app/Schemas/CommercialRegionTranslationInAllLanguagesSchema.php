<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class CommercialRegionTranslationInAllLanguagesSchema extends BaseSchema
{
    protected $resourceType = 'commercial_region_translations_in_all_languages';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        $fields = [
            'commercial_region_id',
            'commercial_region_key',
            'language_id',
            'language_abbr',
            'translation'
        ];

        return getRequiredData($fields, $model, $fieldKeysFilter);
    }


    public function getResourceLinks($resource): array
    {
        return [];
    }
}