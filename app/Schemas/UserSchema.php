<?php namespace App\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;

class UserSchema extends BaseSchema
{
    protected $resourceType = 'user';

    public function getId($model): ?string
    {
        return (string)$model->id;
    }

    public function getAttributes($model, array $fieldKeysFilter = null): ?array
    {
        return [
            'name' => $model->name,
            'email' => $model->email,
        ];
    }
}