<?php

namespace App\DTOs;

class AccommodationFacilityDTO
{
    public $accommodationId;
    public $facilities;

    public function __construct($accommodationId, $facilities)
    {
        $this->accommodationId = $accommodationId;
        $this->facilities = $facilities;
    }
}