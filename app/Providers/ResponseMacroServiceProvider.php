<?php

namespace App\Providers;

use App\Renderables\JsonApiRenderable;
use App\Responses\JsonApiResponse;
use App\Responses\JsonApiResponseBuilder;
use Neomerx\JsonApi\Encoder\Encoder;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Response;
use Neomerx\JsonApi\Document\Error;


class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        //Macro for Response object
        Response::macro('jsonApi', function ($items, $count = null, $status = 200, $mappings = null) {
            if (!$items) {
                return \Illuminate\Http\Response::create(
                    '',
                    $status,
                    [
                        'Content-Type' => 'application/vnd.api+json'
                    ]
                );
            }

            $renderable = new JsonApiRenderable(
                $items,
                app('request')->fields(),
                app('request')->includes(),
                $mappings
            );
            //array_merge(app('request')->fields(), ['accommodations'=>['geo_tree']]),
            if (isset($count)) {
                $renderable->withLinks(
                    config('app.url'),
                    app('request')->input(),
                    $count,
                    app('request')->input('page.offset') ?: 0,
                    app('request')->input('page.limit') ?: $count
                );
            }

            return \Illuminate\Http\Response::create(
                $renderable,
                $status,
                [
                    'Content-Type' => 'application/vnd.api+json'
                ]
            );
        });

        Response::macro('jsonApiValidationError', function ($e) {
            $errors = [];
            foreach ($e->errors() as $field => $fieldErrors) {
                foreach ($fieldErrors as $errorString) {
                    $errors[] = new Error(null, null, 422, null, $errorString, $field);
                }
            }

            return \Illuminate\Http\Response::create(
                Encoder::instance()->encodeErrors($errors),
                422,
                [
                    'Content-Type' => 'application/vnd.api+json'
                ]
            );
        });

        Response::macro('jsonApiError', function ($message, $status) {
            $errors[] = new Error(null, null, $status, null, $message);

            return \Illuminate\Http\Response::create(
                Encoder::instance()->encodeErrors($errors),
                $status,
                [
                    'Content-Type' => 'application/vnd.api+json'
                ]
            );
        });
    }


}

