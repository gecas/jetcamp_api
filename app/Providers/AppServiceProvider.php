<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);
        \Dusterio\LumenPassport\LumenPassport::routes($this->app, ['prefix' => 'api/v1/oauth']);
        \Dusterio\LumenPassport\LumenPassport::allowMultipleTokens();
    }
}
