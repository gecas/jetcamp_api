<?php

namespace App\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class CollectionMacroServiceProvider extends ServiceProvider
{
    /**
     * Boot collection macro service provider.
     *
     * @return void
     */
    public function boot()
    {
        //Collection macro that converts array elements that are callable to the value that callable returns.
        Collection::macro('callableToValue', function () {
            return $this->map(function ($value) {
                return is_callable($value) ? $value() : $value;
            });
        });
    }
}
