<?php

namespace App\Providers;

use App\Models\Accommodation;
use App\Models\AccommodationFacilitySub;
use App\Models\CommercialRegion;
use App\Models\CommercialRegionTranslation;
use App\Models\Url;
use App\Models\GeoTree;
use Illuminate\Support\ServiceProvider;

class ObserversServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Accommodation::observe([
            \App\Observers\Accommodation\UpdateAccommodationGeoItemAndParentsDirectConnectionsCountObserver::class,
            \App\Observers\Accommodation\UpdateAccommodationGeoItemIndirectConnectionsCountObserver::class,
            \App\Observers\Accommodation\UpdateAccommodationNearbyAccommodationsIndirectConnectionsCountsObserver::class,
            \App\Observers\Accommodation\UpdateAccommodationNearbyPoisIndirectConnectionsCountsObserver::class,
            \App\Observers\Accommodation\UpdateAccommodationPrebuildDataObserver::class
        ]);

        Url::observe([
            \App\Observers\Url\CreateUrlRedirectObserver::class
        ]);
        CommercialRegionTranslation::observe([
            \App\Observers\CommercialRegionTranslation\UpdateCommercialRegionUrlForSearchPageObserver::class,
            \App\Observers\CommercialRegionTranslation\UpdateCommercialRegionUrlForSingleItemPageObserver::class,
            \App\Observers\CommercialRegionTranslation\SyncCommercialRegionToEsObserver::class
        ]);
        CommercialRegion::observe([
            \App\Observers\CommercialRegion\SyncCommercialRegionToEsObserver::class
        ]);
        AccommodationFacilitySub::observe([
            \App\Observers\AccommodationFacilitySub\CreateAccommodationFacilityMainAssocObserver::class
        ]);
        GeoTree::observe([
            \App\Observers\GeoTree\UpdateGeoTreeKeysObserver::class,
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}