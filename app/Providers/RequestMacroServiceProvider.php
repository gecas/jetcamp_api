<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class RequestMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Request::macro('fields', function () {
            $res = [];
            $fields = $this->input('fields');
            if (isset($fields) && is_array($fields) && count($fields) > 0) {
                foreach ($fields as $table => $string) {
                    if (!$string) {
                        continue;
                    }
                    $cols = explode(',', $string);
                    foreach ($cols as $col) {
                        $res[$table][] = $col;
                    }
                }
            }
            return $res;
        });

        Request::macro('includes', function ($level = 0) {
            $includes = [];
            if ($this->input('include')) {
                $includes = explode(',', $this->input('include'));

                if ($level > 0) {
                    //@todo this find 1st level only but this is enough for me
                    $newIncludes = [];
                    foreach ($includes as $include) {
                        $newIncludes[] = substr($include, 0, strpos($include, '.'));
                    }
                    $includes = $newIncludes;
                }

            }

            return $includes;
        });

    }
}

