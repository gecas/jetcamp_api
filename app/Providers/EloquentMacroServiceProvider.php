<?php

namespace App\Providers;

use App\Helpers\EloquentFilterBuilderFromArray;
use Illuminate\Support\ServiceProvider;
use \Illuminate\Database\Eloquent\Builder;


class EloquentMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        //Apply offset and limit if appropriate query inputs exist
        Builder::macro('constraintsFromInput', function () {
            if (($offset = app('request')->input('page.offset')) == true) {
                $this->offset($offset);
            }

            if (($limit = app('request')->input('page.limit')) == true) {
                $this->limit($limit);
            }
            return $this;
        });

        //Select fields that are in fields input
        Builder::macro('fieldsFromInput', function () {
            $tableName = $this->getModel()->getTable();
            $fields = app('request')->input('fields');
            $includes = app('request')->includes(1);

            if (isset($fields[$tableName]) && !in_array($fields[$tableName], $includes)) {
                $values = explode(',', $fields[$tableName]);
                foreach ($values as $field) {
                    if (in_array($field, $includes)) {
                        continue;
                    }
                    $this->addSelect($tableName . '.' . $field);
                }
            }
            return $this;
        });

        //Build query from input
        Builder::macro('filtersFromInput', function () {
            $filters = app('request')->input('filter');

            if (is_array($filters) && count($filters) > 0) {
                //Filter allowed
                $allowed = config('allowed_query_filters');
                $filtered = [];

                foreach ($filters as $table => $columns) {
                    foreach ($columns as $column => $operands) {
                        foreach ($operands as $operand => $value) {
                            if (!$value) {
                                continue;
                            }
                            if (in_array($operand, $allowed[$table][$column])) {
                                $filtered[$table][$column][$operand] = $value;
                            }
                        }
                    }
                }

                return (new EloquentFilterBuilderFromArray\Manager($this, $filtered))->build();
            } else {
                return $this;
            }
        });

        Builder::macro('findOrException', function ($id) {
            $result = $this->find($id, ['*']);

            if (is_array($id)) {
                if (count($result) == count(array_unique($id))) {
                    return $result;
                }
            } elseif (!is_null($result)) {
                return $result;
            }

            throw new \Exception('Model "' . get_class($this->model) . "\" not found ID=$id");
        });

        Builder::macro('firstOrException', function ($columns = ['*']) {
            if (!is_null($model = $this->first($columns))) {
                return $model;
            }

            throw new \Exception('Model "' . get_class($this->model) . "\" not found");
        });

        Builder::macro('withRelationsFromInput', function () {
            if (($includeString = app('request')->input('include')) == true) {
                $includes = explode(',', $includeString);
                foreach ($includes as $include) {
                    $this->with($include);
                }
            }
            return $this;
        });

        Builder::macro('whereWithin', function ($lat, $lon, $radius, $name_lat, $name_lon) {
            $distanceRaw = "(
            6371 * acos(
                cos( radians($lat) ) * cos( radians(`$name_lat`) ) *
                cos( radians(`$name_lon`) - radians($lon) ) +
                sin( radians($lat) ) * sin( radians(`$name_lat`) )
            )
        )";
            $this->whereRaw("$distanceRaw <= {$radius}");
            return $this;
        });
    }
}

