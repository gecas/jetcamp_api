<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationPhoto extends Model
{
    protected  $table= 'accommodation_photos';
    protected  $guarded = [];

    public function accommodation()
    {
        return $this->belongsTo('App\Accommodation', 'accommodation_id');
    }

    public function category()
    {
        return $this->belongsTo('App\AccommodationPhotoCategory', 'photo_category_id');
    }
}