<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationPhotoLabel extends Model
{
    protected  $table= 'accommodation_photo_labels';
    protected  $guarded = [];

    public function accommodation()
    {
        return $this->belongsTo('App\Accommodation', 'accommodation_id');
    }
}