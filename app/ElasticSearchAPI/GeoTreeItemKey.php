<?php

namespace App\ElasticSearchAPI;

class GeoTreeItemKey
{

    private $key;
    private $parties;

    public function __construct($key)
    {
        $this->key = $key;
        $this->parties = explode("_", $this->key);
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    public function getContinent() {
        return $this->getParty(0);
    }

    public function getCountry() {
        return $this->getParty(1);
    }

    public function getMainRegion() {
        return $this->getParty(2);
    }
    public function getRegion() {
        return $this->getParty(3);
    }
    public function getDestin() {
        return $this->getParty(4);
    }
    public function getDestin1() {
        return $this->getParty(5);
    }

    public function getLastParty() {
        return end($this->parties);
    }

    private function getParty($index) {
        if (count($this->parties) > $index) {
                return $this->parties[$index];
        } else
            return false;
    }
    public function getCountryKey() {
        if (count($this->parties) > 1) {
            return $this->getParty(0)."_".$this->getParty(1)."_".$this->getParty(2);
        } else {
            return null;
        }
    }
    public function getCountryKey0() {
        if (count($this->parties) > 1) {
            return $this->getParty(0)."/".$this->getParty(1)."/".$this->getParty(2);
        } else {
            return null;
        }
    }
    public function getCountryKey1() {
        if (count($this->parties) > 1) {
            return $this->getParty(0)."_".$this->getParty(1)."_".$this->getParty(2)."_".$this->getParty(3);
        } else {
            return null;
        }
    }

   /* public function getCountryKey1() {
        if (count($this->parties) > 0) {
            return $this->getParty(0)."_".$this->getParty(1)."_".$this->getParty(2)."_".$this->getParty(3);
        } else {
            return null;
        }
    }

    public function getCountryKey2() {
        if (count($this->parties) > 0) {
            return $this->getParty(0)."_".$this->getParty(1)."_".$this->getParty(2)."_".$this->getParty(3)."_".$this->getParty(4)."_".$this->getParty(5);
        } else {
            return null;
        }
    }*/
}