<?php

namespace App\ElasticSearchAPI;


class ESSearchResponse
{
    private $rawEsResponse;
    private $items = [];
    private $status;

    public function __construct($rawEsResponse)
    {
        $this->rawEsResponse = $rawEsResponse;
        $this->parseESResponse();
    }

    private function parseESResponse() {
        if (!array_key_exists("errors", $this->rawEsResponse)) {
            $items = [];
            foreach ($this->rawEsResponse["hits"]["hits"] as $hit) {
                $_source = $hit["_source"];
                $items[] = $_source;
            }
            $this->items = $items;
            $this->setStatus(1);
        } else
            $this->setStatus(0);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function getRawEsResponse()
    {
        return $this->rawEsResponse;
    }

    /**
     * @param mixed $rawEsResponse
     */
    public function setRawEsResponse($rawEsResponse)
    {
        $this->rawEsResponse = $rawEsResponse;
    }

    public function getItemTotal() {
        return $this->rawEsResponse["hits"]["total"];
    }
}