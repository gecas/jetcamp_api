<?php

namespace App\ElasticSearchAPI;


class ESCountResponse
{

    private $rawEsResponse;
    private $count;

    public function __construct($rawEsResponse)
    {
        $this->rawEsResponse = $rawEsResponse;
        $this->parseESResponse();
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getRawEsResponse()
    {
        return $this->rawEsResponse;
    }

    /**
     * @param mixed $rawEsResponse
     */
    public function setRawEsResponse($rawEsResponse)
    {
        $this->rawEsResponse = $rawEsResponse;
    }

    private function parseESResponse() {
        $this->setCount($this->rawEsResponse["count"]);
    }

}