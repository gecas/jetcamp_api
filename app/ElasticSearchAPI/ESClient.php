<?php

namespace App\ElasticSearchAPI;

use App\FacilityTheme;
use App\JetCamp\JCCampingPageResponse;
use App\JetCamp\JCRequest;
use App\JetCamp\JCSearchResponse;
use App\Services\BreadcrumbService;
use Elasticsearch\ClientBuilder;
use App\ElasticSearchAPI\GeoTreeItem;
use Illuminate\Support\Facades\Log;
use DB;

//use Illuminate\Support\Facades\App;

class ESClient
{
    private $esHosts;
    private $esClient;

    public function __construct()
    {
        $this->esHosts = [config('elasticsearch.host') . ":" . config('elasticsearch.port')];
        $this->esClient = ClientBuilder::create()
            ->setHosts($this->esHosts)
            ->build();
    }

    public function retrieveCommercialRegions(JCRequest $jcRequest)
    {
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "term" => [
                                "type" => [
                                    "value" => GeoTreeItem::TYPE_COMMERCIAL
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "sort" => [
                [
                    "item.popularity" => [
                        "order" => "desc"
                    ]
                ],
                [
                    "item.within_camping_cnt" => [
                        "order" => "desc"
                    ]
                ]
            ]
        ];

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
        return $jcSearchResponse;
    }

    public function retrieveCampingPagePoi(JCRequest $jcRequest)
    {
        $geoTreeItem = $jcRequest->getItem();

        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "bool" => [
                                "should" => [
                                    [
                                        "bool" => [
                                            "must" => [
                                                [
                                                    "term" => [
                                                        "type" => [
                                                            "value" => GeoTreeItem::TYPE_GEOITEM
                                                        ]
                                                    ]
                                                ],
                                                [
                                                    "range" => [
                                                        "show_on_acco_whattodo" => [
                                                            "gte" => 1
                                                        ]
                                                    ]
                                                ]
                                            ],
                                            "filter" => [
                                                "geo_distance" => [
                                                    "distance" => "10km",
                                                    "gps_coords" => [
                                                        "lat" => $geoTreeItem->item->gps_coords["lat"],
                                                        "lon" => $geoTreeItem->item->gps_coords["lon"]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    [
                                        "bool" => [
                                            "must" => [
                                                [
                                                    "term" => [
                                                        "type" => [
                                                            "value" => GeoTreeItem::TYPE_POI
                                                        ]
                                                    ]
                                                ],
                                                [
                                                    "range" => [
                                                        "item.whattodo" => [
                                                            "gte" => 1
                                                        ]
                                                    ]
                                                ],
                                                [
                                                    "range" => [
                                                        "item.whattodo_min_rating" => [
                                                            "gte" => $geoTreeItem->item->rating
                                                        ]
                                                    ]
                                                ],
                                                [
                                                    "bool" => [
                                                        "must" => [
                                                            [
                                                                "bool" => [
                                                                    "should" => [
                                                                        [
                                                                            "range" => [
                                                                                "item.whattodo_min_reviews" => [
                                                                                    "gt" => 2000
                                                                                ]
                                                                            ]
                                                                        ]
                                                                    ],
                                                                    "filter" => [
                                                                        "geo_distance" => [
                                                                            "distance" => "100km",
                                                                            "item.gps_coords" => [
                                                                                "lat" => $geoTreeItem->item->gps_coords["lat"],
                                                                                "lon" => $geoTreeItem->item->gps_coords["lon"]
                                                                            ]
                                                                        ]
                                                                    ]
                                                                ]
                                                            ],
                                                            [
                                                                "bool" => [
                                                                    "should" => [
                                                                        [
                                                                            "range" => [
                                                                                "item.whattodo_min_reviews" => [
                                                                                    "gt" => 500,
                                                                                    "lt" => 2000
                                                                                ]
                                                                            ]
                                                                        ]
                                                                    ],
                                                                    "filter" => [
                                                                        "geo_distance" => [
                                                                            "distance" => "50km",
                                                                            "item.gps_coords" => [
                                                                                "lat" => $geoTreeItem->item->gps_coords["lat"],
                                                                                "lon" => $geoTreeItem->item->gps_coords["lon"]
                                                                            ]
                                                                        ]
                                                                    ]
                                                                ]
                                                            ],
                                                            [
                                                                "bool" => [
                                                                    "should" => [
                                                                        [
                                                                            "range" => [
                                                                                "item.whattodo_min_reviews" => [
                                                                                    "lt" => 500
                                                                                ]
                                                                            ]
                                                                        ]
                                                                    ],
                                                                    "filter" => [
                                                                        "geo_distance" => [
                                                                            "distance" => "25km",
                                                                            "item.gps_coords" => [
                                                                                "lat" => $geoTreeItem->item->gps_coords["lat"],
                                                                                "lon" => $geoTreeItem->item->gps_coords["lon"]
                                                                            ]
                                                                        ]
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    [
                                        "bool" => [
                                            "must" => [
                                                [
                                                    "range" => [
                                                        "item.whattodo" => [
                                                            "gte" => 1
                                                        ]
                                                    ]
                                                ]
                                            ],
                                            "filter" => [
                                                "geo_distance" => [
                                                    "distance" => "1km",
                                                    "item.gps_coords" => [
                                                        "lat" => $geoTreeItem->item->gps_coords["lat"],
                                                        "lon" => $geoTreeItem->item->gps_coords["lon"]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]

                    ]

                ]
            ],
            "sort" => [
                [
                    "show_on_acco_whattodo" => [
                        "order" => "desc"
                    ]
                ]
            ],
            "size" => 8
        ];

        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);

        $jcCampingPageResponse = new JCCampingPageResponse($esSearchGeoItemResponse);
        $jcCampingPageResponse->setStatus(1);
        $jcCampingPageResponse->setMainJcItem($geoTreeItem);

        $accommodation = $geoTreeItem->getAccommodation();

//        if ($accommodation) {
//            $mainJcItem = $jcCampingPageResponse->getMainJcItem();
//            $subsObj = $accommodation->subFacilities;
//            $subs = [];
//            foreach ($subsObj as $sub) {
//                $subs[] = $sub->description;
//            }
//            $mainJcItem->setFacilitySubs($subs);
//
//            $mainsObj = $accommodation->mainFacilities;
//            $mains = [];
//            foreach ($mainsObj as $main) {
//                $mains[] = $main->description;
//            }
//            $mainJcItem->setFacilityMains($mains);
//        }

        foreach ($esSearchGeoItemResponse->getItems() as $item) {
            $jcCampingPageResponse->addPoisItem($item);
        }

        $jcCampingsResponse = $this->retrieveCampingPageCampings($jcRequest);

        foreach ($jcCampingsResponse->getItems() as $camping) {
            $jcCampingPageResponse->addCampingsItem($camping);
        }

        return $jcCampingPageResponse;
    }

    private function retrieveCampingPageCampings(JCRequest $jcRequest)
    {
        $geoTreeItem = $jcRequest->getItem();

        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "bool" => [
                                "should" => [
                                    [
                                        "bool" => [
                                            "must" => [
                                                [
                                                    "term" => [
                                                        "type" => [
                                                            "value" => GeoTreeItem::TYPE_CAMPING
                                                        ]
                                                    ]
                                                ]
                                            ],
                                            "filter" => [
                                                "geo_distance" => [
                                                    "distance" => "10km",
                                                    "item.gps_coords" => [
                                                        "lat" => $geoTreeItem->item->gps_coords["lat"],
                                                        "lon" => $geoTreeItem->item->gps_coords["lon"]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]

                    ]

                ]
            ]
        ];

        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);

//        $jcCampingPageResponse = new JCCampingPageResponse($esSearchGeoItemResponse);
//        $jcCampingPageResponse->setStatus(1);
//
//        $jcCampingPageResponse->setMainJcItem($geoTreeItem);
        return $esSearchGeoItemResponse;
    }

    public function searchForCommercialRegionLandingPage(JCRequest $jcRequest)
    {
        $mainGeoTreeItem = $jcRequest->getItem();

        $coords = $mainGeoTreeItem->getItemPolygon();
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "term" => [
                                "type" => [
                                    "value" => GeoTreeItem::TYPE_CAMPING
                                ]
                            ]
                        ]
                    ],
                    "filter" => [
                        "geo_polygon" => [
                            "item.gps_coords" => [
                                "points" => $coords
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $this->appendBodyWithAggregation($body);
        $this->appendBodyWithFiltering($body, $jcRequest);
        $this->appendBodyWithSorting($body, $jcRequest);

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
        $jcSearchResponse->setMainJcItem($mainGeoTreeItem);
        return $jcSearchResponse;
    }

    public function searchForCampingLandingPage(JCRequest $jcRequest)
    {
        $mainGeoTreeItem = $jcRequest->getItem();

        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "term" => [
                                "type" => [
                                    "value" => GeoTreeItem::TYPE_CAMPING
                                ]
                            ]
                        ],
                        [
                            "bool" => [
                                "must_not" => [
                                    [
                                        "term" => [
                                            "_id" => [
                                                "value" => $mainGeoTreeItem->id
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "filter" => [
                        "geo_distance" => [
                            "distance" => "10km",
                            "item.gps_coords" => [
                                "lat" => $mainGeoTreeItem->item->gps_coords["lat"],
                                "lon" => $mainGeoTreeItem->item->gps_coords["lon"]
                            ]
                        ]
                    ]
                ]
            ],
        ];
        $this->appendBodyWithAggregation($body);
        $this->appendBodyWithFiltering($body, $jcRequest);
        $this->appendBodyWithSorting($body, $jcRequest);

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
        $jcSearchResponse->setMainJcItem($mainGeoTreeItem);
        $accommodation = $mainGeoTreeItem->getAccommodation();

        return $jcSearchResponse;
    }

    public function searchForPoiLandingPage(JCRequest $jcRequest)
    {
        $mainGeoTreeItem = $jcRequest->getItem();

        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "term" => [
                                "type" => [
                                    "value" => GeoTreeItem::TYPE_CAMPING
                                ]
                            ]
                        ],
                        [
                            "bool" => [
                                "must_not" => [
                                    [
                                        "term" => [
                                            "_id" => [
                                                "value" => $mainGeoTreeItem->id
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "filter" => [
                        "geo_distance" => [
                            "distance" => "50km",
                            "item.gps_coords" => [
                                "lat" => $mainGeoTreeItem->item->gps_coords["lat"],
                                "lon" => $mainGeoTreeItem->item->gps_coords["lon"]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $this->appendBodyWithAggregation($body);
        $this->appendBodyWithFiltering($body, $jcRequest);
//        $params["item_lat"] = $mainGeoTreeItem->item->gps_coords["lat"];
        $jcRequest->setParamValue("item_lat", $mainGeoTreeItem->item->gps_coords["lat"]);
//        $params["item_lon"] = $mainGeoTreeItem->item->gps_coords["lon"];
        $jcRequest->setParamValue("item_lon", $mainGeoTreeItem->item->gps_coords["lon"]);
        $this->appendBodyWithSorting($body, $jcRequest);

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
        $jcSearchResponse->setMainJcItem($mainGeoTreeItem);
        return $jcSearchResponse;
    }

    public function searchForDestinationLandingPage(JCRequest $jcRequest)
    {
        $geoItem = $jcRequest->getItem(); //$this->getGeoItemById($params["item_id"]);
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "prefix" => [
                                "key" => $geoItem->key
                            ]
                        ],
                        [
                            "term" => [
                                "type" => GeoTreeItem::TYPE_CAMPING
                            ]
                        ]
                    ]
                ]
            ],
            "sort"  => [
                [
                    "item.popularity" => [
                        "order" => "desc"
                    ]
                ],
            ]
        ];
        $this->appendBodyWithAggregation($body);
        $this->appendBodyWithFiltering($body, $jcRequest);
        $this->appendBodyWithSorting($body, $jcRequest);

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponse->getItemTotal());
        } else { // if there is no campings directly connected to geoitem then we need to search by radius
            $body = [
                "query" => [
                    "bool" => [
                        "must" => [
                            [
                                "term" => [
                                    "type" => [
                                        "value" => GeoTreeItem::TYPE_CAMPING
                                    ]
                                ]
                            ],
                            [
                                "bool" => [
                                    "must_not" => [
                                        [
                                            "term" => [
                                                "_id" => [
                                                    "value" => $geoItem->id
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        "filter" => [
                            "geo_distance" => [
                                "distance" => "50km",
                                "item.gps_coords" => [
                                    "lat" => $geoItem->gps_coords["lat"],
                                    "lon" => $geoItem->gps_coords["lon"]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $this->appendBodyWithAggregation($body);
            $this->appendBodyWithFiltering($body, $jcRequest);
            $this->appendBodyWithSorting($body, $jcRequest);

            if ($jcRequest->getFrom()) {
                $body["from"] = $jcRequest->getFrom();
            }
            if ($jcRequest->getSize()) {
                $body["size"] = $jcRequest->getSize();
            }
            $data = [
                "index" => "geotree",
                "type" => "geoitem",
                "body" => $body,
            ];

            Log::debug(json_encode($data['body']));
            $esSearchGeoItemResponseByRadius = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponseByRadius);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponseByRadius->getItemTotal());
        }
        $jcSearchResponse->setMainJcItem($geoItem);
        return $jcSearchResponse;

    }

    public function searchAutocomplete(JCRequest $jcRequest)
    {
        if (endsWithChar($jcRequest->getQuery(), " ")) {
            $query = trim($jcRequest->getQuery());
            $nonGeotreeMainCondition["should"] = [
                [
                    "term" => [
                        "item.name_translations.trans" => [
                            "value" => $query
                        ]
                    ]
                ],
                [
                    "term" => [
                        "item.name" => [
                            "value" => $query
                        ]
                    ]
                ],
                [
                    "term" => [
                        "item.name_synonyms" => [
                            "value" => $query
                        ]
                    ]
                ]
            ];
            $geotreeMainCondition["should"] = [
                [
                    "term" => [
                        "translations.trans" => [
                            "value" => $query
                        ]
                    ]
                ],
                [
                    "term" => [
                        "name" => [
                            "value" => $query
                        ]
                    ]
                ],
                [
                    "term" => [
                        "name_synonyms" => [
                            "value" => $query
                        ]
                    ]
                ]
            ];
        } else {
            $search_fields = ["item.name^3", "item.name_translations.trans^3", "item.name_synonyms"];
            $nonGeotreeMainCondition["should"] = [
                [
                    "multi_match" => [
                        "query" => $jcRequest->getQuery(),
                        "fields" => $search_fields,
                        "operator" => "and",
                        "type" => "phrase_prefix"
                    ]
                ],
                [
                    "multi_match" => [
                        "query" => $jcRequest->getQuery(),
                        "fields" => $search_fields,
                        "operator" => "and",
                        "boost" => 5,
                        "type" => "phrase_prefix"
                    ]
                ]
            ];
            $search_fields = ["translations.trans", "name^3", "name.kw", "name_synonyms"];
            $geotreeMainCondition["should"] = [
                [
                    "multi_match" => [
                        "query" => $jcRequest->getQuery(),
                        "fields" => $search_fields,
                        "operator" => "and",
                        "type" => "phrase_prefix"
                    ]
                ],
                [
                    "multi_match" => [
                        "query" => $jcRequest->getQuery(),
                        "fields" => $search_fields,
                        "operator" => "and",
                        "boost" => 5,
                        "type" => "phrase_prefix"
                    ]
                ]
            ];
        }

        $body = [
            "query" => [
                "bool" => [
                    "should" => [
                        [
                            "bool" => [
                                "must" => [
                                    [
                                        "terms" => [
                                            "type" => [
                                                GeoTreeItem::TYPE_CAMPING,
                                                GeoTreeItem::TYPE_COMMERCIAL,
                                                GeoTreeItem::TYPE_POI
                                            ]
                                        ]
                                    ],
                                    ["bool" => $nonGeotreeMainCondition]
                                ]
                            ]
                        ],
                        [
                            "bool" => [
                                "must" => [
                                    [
                                        "terms" => [
                                            "type" => [GeoTreeItem::TYPE_GEOITEM]
                                        ]
                                    ],
                                    ["bool" => $geotreeMainCondition]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "sort" => [
                [
                    "popularity" => [
                        "order" => "desc",
                        "unmapped_type" => "long"
                    ]
                ],
                [
                    "item.popularity" => [
                        "order" => "desc",
                        "unmapped_type" => "long"
                    ]
                ],
                [
                    "_score" => [
                        "order" => "desc"
                    ]
                ],
                [
                    "camping_cnt_10" => [
                        "order" => "desc"
                    ]
                ]
            ]
        ];
        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
        return $jcSearchResponse;
    }

//    public function retrieveCountriesList(JCRequest $jcRequest)
//    {
//        $body["aggs"] = [
//            "countries"=> [
//                "terms"=> [
//                    "field"=> "item.address.country"
//                ]
//            ]
//        ];
//        $data = [
//            "index" => "geotree",
//            "type" => "geoitem",
//            "body" => $body,
//        ];
//        Log::debug(json_encode($data['body']));
//        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
//        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
//        return $jcSearchResponse;
//
//    }

    public function getCountriesList(JCRequest $jcRequest)
    {
        $body = [
            "size" => 100,
            "query" => [
                "bool" => [
                    "should" => [
                        [
                            "match" => [
                                "subtype" => "PCLI"
                            ]
                        ],
                        [
                            "match" => [
                                "subtype" => "PCLD"
                            ]
                        ],
                        [
                            "match" => [
                                "subtype" => "PCLF"
                            ]
                        ],
                        [
                            "match" => [
                                "subtype" => "PCLS"
                            ]
                        ],
                        [
                            "match" => [
                                "subtype" => "PCLIX"
                            ]
                        ]
                    ]
                ]
            ]
//            "sort"=> [
//            [
////                "translations.trans.keyword"=> [
//                "key.keyword"=> [
//                "order"=> "asc"
//              ]
//            ]
//          ]
        ];

        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
        return $jcSearchResponse;
    }

//    public function searchWithAggregatedRatings($params) {
//        $body = [
//            "query" => [
//                "bool" => [
//                    "must" => [
//                        ["multi_match" => [
//                            "query" => $params["q"],
//                            "type" => "phrase_prefix",
//                            "fields" => ["translations.trans", "item.name^3",
//                                "item.name_translations.trans^3", "name^3", "name.kw",
//                                "name_synonyms", "item.name_synonyms"]
//                        ]],
//                        ["range" => [
//                            "item.rating" => [
//                                "gte" => 0
//                            ]
//                        ]],
//                        ["term" => [
//                            "type" => GeoTreeItem::TYPE_CAMPING
//                        ]]
//                    ]
//                ]
//            ],
//            "aggs" => [
//                "ratings" => [
//                    "terms" => [
//                        "field" => "item.rating",
//                        "size" => 500
//                    ]
//                ]
//            ]
//        ];
//
//        if (array_key_exists("from", $params))
//            $body["from"] = $params["from"];
//        if (array_key_exists("size", $params))
//            $body["size"] = $params["size"];
//        $data = [
//            "index" => "geotree",
//            "type" => "geoitem",
//            "body" => $body,
//        ];
//        /*echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit();*/
//        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data));
//        if (array_key_exists("lng", $params))
//            $language = $params["lng"];
//        else
//            $language = "en";
//        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse, $language);
//
//        return $jcSearchResponse;
//    }

//    public function filterSearchFaceded($params) {
//        $body = [
//            "query" => [
//                "bool" => [
//                    "must" => [
//                        ["multi_match" => [
//                            "query" => $params["q"],
//                            "type" => "phrase_prefix",
//                            "fields" => ["grouped_translations", "item.name_translations^3"]
//                        ]],
//                        ["term" => [
//                            "type" => GeoTreeItem::TYPE_CAMPING
//                        ]]
//                    ]
//                ]
//            ]
//        ];
//
//        if (array_key_exists("ratings",$params)) {
//            $ratingsBool["should"] = [];
//            foreach ($params["ratings"] as $rating) {
//                //$ratingsBool["should"][]["term"] = ["item.rating" => $rating];
//                //$ratingsBool["should"][]["range"] = ["item.rating" => [ "gte" => $rating - 0.5, "lt" => $rating + 0.5]];
//                //$ratingsBool["should"][]["range"] = ["item.rating" => [ "gte" => $rating - 0.5]];
//                $ratingsBool["should"][]["range"] = ["item.rating" => [ "gte" => $rating]];
//            }
//
//            $body["query"]["bool"]["must"][]["bool"] = $ratingsBool;
//        }
//
//        /*echo "<pre>";
//        print_r($body);
//        echo "</pre>";*/
//
//        if (array_key_exists("from", $params))
//            $body["from"] = $params["from"];
//        if (array_key_exists("size", $params))
//            $body["size"] = $params["size"];
//        $data = [
//            "index" => "geotree",
//            "type" => "geoitem",
//            "body" => $body,
//        ];
//
//        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data));
//        if (array_key_exists("lng", $params))
//            $language = $params["lng"];
//        else
//            $language = "en";
//        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse, $language);
//
//        return $jcSearchResponse;
//    }

    public function updateSearchFaceted(JCRequest $jcRequest)
    {
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        "geo_polygon" => [
                            "item.gps_coords" => [
                                "points" => $jcRequest->getCoords()
                            ]
                        ]
                    ],
                    "must_not" => [
                        "term" => [
                            "type" => [
                                "value" => GeoTreeItem::TYPE_POI
                            ]
                        ]
                    ]
                ]
            ],
//            "aggs" => [
//                "ratings" => [
//                    "terms" => [
//                        "field" => "item.rating",
//                        "size" => 500
//                    ]
//                ]
//            ]
        ];

//        if (array_key_exists("rating", $params)) {
//            $ratingsBool["should"][]["range"] = ["item.rating" => [ "gte" => $params["rating"]]];
//            $body["query"]["bool"]["must"][]["bool"] = $ratingsBool;
//        }
        $this->appendBodyWithAggregation($body);
        $this->appendBodyWithFiltering($body, $jcRequest);
        $this->appendBodyWithSorting($body, $jcRequest);

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }

        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);

        return $jcSearchResponse;
    }

    public function poisSearchFaceted(JCRequest $jcRequest)
    {
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "geo_polygon" => [
                                "item.gps_coords" => [
                                    "points" => $jcRequest->getCoords()
                                ]
                            ]
                        ],

                        [
                            "range" => [
                                "item.rating" => [
                                    "gte" => 4
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "type" => [
                                    "value" => GeoTreeItem::TYPE_POI
                                ]
                            ]
                        ]

                    ],

                ]
            ],
            "aggs" => [
                "ratings" => [
                    "terms" => [
                        "field" => "item.rating",
                        "size" => 500
                    ]
                ]
            ]
        ];

        $this->appendBodyWithAggregation($body);
        $this->appendBodyWithFiltering($body, $jcRequest);
        $this->appendBodyWithSorting($body, $jcRequest);

        if ($jcRequest->getFrom()) {
            $body["from"] = $jcRequest->getFrom();
        }
        if ($jcRequest->getSize()) {
            $body["size"] = $jcRequest->getSize();
        }

        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data), $jcRequest);
        $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);

        return $jcSearchResponse;
    }

    public function getGeoItemCampingCnt(GeoTreeItem $item)
    {
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "prefix" => [
                                "key" => $item->key
                            ]
                        ],
                        [
                            "term" => [
                                "type" => [
                                    "value" => GeoTreeItem::TYPE_CAMPING
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        $esCountResponse = new ESCountResponse($this->esClient->count($data));
        return $esCountResponse->getCount();
    }

    public function getGeoItemById($id)
    {
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            'body' => [
                'query' => [
                    'match' => [
                        '_id' => $id
                    ]
                ]
            ]
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data));
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            return $esSearchGeoItemResponse->getItems()[0];
        } else {
            return null;
        }
    }

    public function getGeoItemsByIds($ids, $from = 0, $size = 10)
    {
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            'body' => [
                'query' => [
                    'terms' => [
                        '_id' => $ids
                    ]
                ],
                'size' => $size,
                'from' => $from
            ]
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data));
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            return $esSearchGeoItemResponse->getItems();
        } else {
            return null;
        }
    }

    public function getGeoItemsByTerms($property, $terms, $from = 0, $size = 10)
    {
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            'body' => [
                'query' => [
                    'terms' => [
                        $property => $terms
                    ]
                ],
                'size' => $size,
                'from' => $from
            ]
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data));
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            return $esSearchGeoItemResponse->getItems();
        } else {
            return null;
        }
    }

    /*public function retrieveSearchRedirectUrl($params) {
        $data = [
            "index" => "geotree",
            "type" => "urls",
            'body' => [
                'query' => [
                    'term' => [
                        'key' => $id
                    ]
                ]
            ]
        ];
    }*/

    public function bulk($geoTreeItems)
    {
        for ($i = 0; $i < count($geoTreeItems); $i++) {
            $geoItem = $geoTreeItems[$i];
            $params['body'][] = [
                'index' => [
                    '_index' => 'geotree',
                    '_type' => 'geoitem',
                    '_id' => $geoItem->id
                ]
            ];
            $params['body'][] = $geoItem;

            if ($i and $i % 1000 == 0) {
                $return["es_response"] = $this->esClient->bulk($params);
                unset($params);
                echo "sending to ES " . $i . "\n";
                //echo "siuncia kai cnt = ".$i."-<br>";
            }
        }
        if ($params) {
            $return["es_response"] = $this->esClient->bulk($params);
            //echo "paskutini karta siuncia cnt = ".(count($params['body']) / 2)."<br>";
        }
        //$return["results"] = $this->getResponseResults($return["es_response"]);
        return $return;
    }

    public function bulkUpdate($items, $index, $type)
    {
        for ($i = 0; $i < count($items); $i++) {
            $item = $items[$i];
            $params['body'][] = [
                'index' => [
                    '_index' => $index,
                    '_type' => $type,
                    '_id' => $item["id"]
                ]
            ];
            $params['body'][] = $item;

            if ($i and $i % 1000 == 0) {
                $return["es_response"] = $this->esClient->bulk($params);
                unset($params);
                //echo "siuncia kai cnt = ".$i."-<br>";
            }
        }
        if ($params) {
            $return["es_response"] = $this->esClient->bulk($params);
            //echo "paskutini karta siuncia cnt = ".(count($params['body']) / 2)."<br>";
        }
        //$return["results"] = $this->getResponseResults($return["es_response"]);
        return $return;
    }

    public function createGeoItemIndex()
    {
        $params = ['index' => "geotree"];
        if (!$this->esClient->indices()->exists($params)) {
            $data = [
                'index' => 'geotree',
                'body' => [
                    'mappings' => [
                        'geoitem' => [
                            "properties" => [
                                "gps_coords" => [
                                    "type" => "geo_point"
                                ],
                                "item.gps_coords" => [
                                    "type" => "geo_point"
                                ],
                                "item.geo_location" => [
                                    "type" => "geo_shape",
                                    "tree" => "quadtree",
                                    "precision" => "1km"
                                ],
                                "name" => [
                                    "type" => "text",
                                    "fields" => [
                                        "kw" => [
                                            "analyzer" => "my_analyzer",
                                            "search_analyzer" => "autocomplete_search",
                                            "type" => "text",
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "settings" => [
                        "analysis" => [
                            "analyzer" => [
                                "my_analyzer" => [
                                    "tokenizer" => "my_tokenizer",
                                    "filter" => [
                                        "lowercase"
                                    ]
                                ],
                                "autocomplete_search" => [
                                    "tokenizer" => "lowercase"
                                ]
                            ],
                            "tokenizer" => [
                                "my_tokenizer" => [
                                    "type" => "edge_ngram",
                                    "min_gram" => 1,
                                    "max_gram" => 10,
                                    "token_chars" => [
                                        "letter",
                                        "digit"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            //$response =
            return $this->esClient->indices()->create($data);
        } else {
            return false;
        }
    }

    public function appendBodyWithAggregation(&$body)
    {
        $body["aggs"] = [
            "ratings" => [
                "terms" => [
                    "field" => "item.rating",
                    "size" => 500
                ]
            ],
            "facilities" => [
                "terms" => [
                    "field" => "item.filtering.facilities.keyword"
                ]
            ],
            "sports_and_game" => [
                "terms" => [
                    "field" => "item.filtering.sports_and_game.keyword"
                ]
            ]
        ];
    }

    public function appendBodyWithFiltering(&$body, JCRequest $jcRequest)
    {
        if (!empty($jcRequest->getRating())) {
            if ($jcRequest->getRating() == 'norating') {
                $body["query"]["bool"]["must"][]['term']['item.rating'] = 0;
            } else {
                $body["query"]["bool"]["must"][]['range']['item.rating']['gte'] = $jcRequest->getRating();
            }
        }

        if ($jcRequest->getFacilities()) {
            $facilitiesBool = [];
            foreach ($jcRequest->getFacilities() as $facility) {
                if ($facility) {
                    $facilitiesBool["must"][] = [
                        "term" => [
                            "item.filtering.facilities" => [
                                "value" => $facility
                            ]
                        ]
                    ];
                }
            }
            if (!empty($facilitiesBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $facilitiesBool;
            }
        }
        if ($jcRequest->getSportsAndGames()) {
            $sportsBool = [];
            foreach ($jcRequest->getSportsAndGames() as $sport) {
                if ($sport) {
                    $sportsBool["must"][] = [
                        "term" => [
                            "item.filtering.sports_and_game" => [
                                "value" => $sport
                            ]
                        ]
                    ];
                }
            }
            if (!empty($sportsBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $sportsBool;
            }
        }

        if ($jcRequest->getSanitary()) {
            $sanitaryBool = [];
            foreach ($jcRequest->getSanitary() as $sanitary) {
                if ($sanitary) {
                    $sanitaryBool["must"][] = [
                        "term" => [
                            "item.filtering.sanitary" => [
                                "value" => $sanitary
                            ]
                        ]
                    ];
                }
            }
            if (!empty($sanitaryBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $sanitaryBool;
            }
        }

        if ($jcRequest->getChildren()) {
            $childrenBool = [];
            foreach ($jcRequest->getChildren() as $children) {
                if ($children) {
                    $childrenBool["must"][] = [
                        "term" => [
                            "item.filtering.children" => [
                                "value" => $children
                            ]
                        ]
                    ];
                }
            }
            if (!empty($childrenBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $childrenBool;
            }
        }

        if ($jcRequest->getRatingStars()) {
            $starsBool = [];
            foreach ($jcRequest->getRatingStars() as $star) {
                if ($star && $star != 'notclassified') {
                    $starsBool["should"][] = [
//                    $starsBool["must"][] = [
                        "term" => [
                            "item.rating_stars" => [
                                "value" => $star
                            ]
                        ]
                    ];
                }
            }
            if (!empty($starsBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $starsBool;
            }
        }

        if ($jcRequest->getRules()) {
            $rulesBool = [];
            foreach ($jcRequest->getRules() as $rule) {
                if ($rule) {
                    $rulesBool["must"][] = [
                        "term" => [
                            "item.filtering.rules" => [
                                "value" => $rule
                            ]
                        ]
                    ];
                }
            }
            if (!empty($rulesBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $rulesBool;
            }
        }

        if ($jcRequest->getPlotsizes()) {
            $plotsizesBool = [];
            foreach ($jcRequest->getPlotsizes() as $plotsize) {
                if ($plotsize) {
                    if ($plotsize == "p100") {
//                        $plotsizesBool["must"][] = [
                        $plotsizesBool["should"][] = [
                            "range" => [
                                "item.size_nr_total" => [
                                    "lt" => 100
                                ]
                            ]
                        ];
                    } else {
                        if ($plotsize == "p400") {
//                        $plotsizesBool["must"][] = [
                            $plotsizesBool["should"][] = [
                                "range" => [
                                    "item.size_nr_total" => [
                                        "gt" => 400
                                    ]
                                ]
                            ];
                        } else {
                            if ($plotsize == "p100-400") {
                                //$plotsizesBool["must"][] = [
                                $plotsizesBool["should"][] = [
                                    "range" => [
                                        "item.size_nr_total" => [
                                            "gte" => 100,
                                            "lte" => 400
                                        ]
                                    ]
                                ];
                            }
                        }
                    }
                }

            }
            if (!empty($plotsizesBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $plotsizesBool;
            }
        }

        if ($jcRequest->getLocation()) {
            $locationsBool = [];
            foreach ($jcRequest->getLocation() as $location) {
                if ($location) {
                    $locationsBool["must"][] = [
                        "term" => [
                            "item.filtering.location" => [
                                "value" => $location
                            ]
                        ]
                    ];
                }
            }
            if (!empty($locationsBool)) {
                $body["query"]["bool"]["must"][]["bool"] = $locationsBool;
            }
        }
    }

    public function appendBodyWithSorting(&$body, JCRequest $jcRequest)
    {
        if ($jcRequest->getSortBy()) {
            switch ($jcRequest->getSortBy()) {
                case 'rating':
                    $body["sort"] = [
                        [
                            "item.rating" => [
                                "order" => $jcRequest->getSortOrder()
                            ]
                        ]
                    ];
                    break;
                case 'popularity':
                    $body["sort"] = [
                        [
                            "item.popularity" => [
                                "order" => $jcRequest->getSortOrder()
                            ]
                        ]
                    ];
                    break;
                case 'rating_stars':
                    $body["sort"] = [
                        [
                            "item.rating_stars" => [
                                "order" => $jcRequest->getSortOrder()
                            ]
                        ]
                    ];
                    break;
                case 'distance': // only for POI
                    $body["sort"] = [
                        [
                            "_geo_distance" => [
                                "item.gps_coords" => [
                                    "lat" => $jcRequest->getParamByName("item_lat"),
                                    "lon" => $jcRequest->getParamByName("item_lon")
                                ],
                                "order" => $jcRequest->getSortOrder()
                            ]
                        ]
                    ];
                    break;
                default:
                    break;
            }
        }
    }


    public function retrieveDestinationLandingPage(JCRequest $jcRequest)
    {
        $geoItem = $jcRequest->getItem(); //$this->getGeoItemById($params["item_id"]);
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "prefix" => [
                                "key" => $geoItem->key
                            ]
                        ],
                        [
                            "term" => [
                                "type" => GeoTreeItem::TYPE_GEOITEM
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];
        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data),
            $jcRequest); //import ESSearchGeoItemResponse
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponse->getItemTotal());
        } else { // if there is no campings directly connected to geoitem then we need to search by radius
            $body = [
                "query" => [
                    "bool" => [
                        "must" => [
                            [
                                "term" => [
                                    "type" => [
                                        "value" => GeoTreeItem::TYPE_GEOITEM
                                    ]
                                ]
                            ],
                            [
                                "bool" => [
                                    "must_not" => [
                                        [
                                            "term" => [
                                                "_id" => [
                                                    "value" => $geoItem->id
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $this->appendBodyWithAggregation($body);
            $this->appendBodyWithFiltering($body, $jcRequest);
            $this->appendBodyWithSorting($body, $jcRequest);

            if ($jcRequest->getFrom()) {
                $body["from"] = $jcRequest->getFrom();
            }
            if ($jcRequest->getSize()) {
                $body["size"] = $jcRequest->getSize();
            }
            $data = [
                "index" => "geotree",
                "type" => "geoitem",
                "body" => $body,
            ];

            $esSearchGeoItemResponseByRadius = new ESSearchGeoItemResponse($this->esClient->$data,
                $jcRequest);  // import
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponseByRadius);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponseByRadius->getItemTotal());
        }
        $jcSearchResponse->setMainJcItem($geoItem);
        return $jcSearchResponse;

    }

    public function retrievePoiLandingPage(JCRequest $jcRequest)
    {
        $geoItem = $jcRequest->getItem(); //$this->getGeoItemById($params["item_id"]);
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "prefix" => [
                                "key" => $geoItem->key
                            ]
                        ],
                        [
                            "term" => [
                                "type" => GeoTreeItem::TYPE_GEOITEM
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data)); //import ESSearchGeoItemResponse
        $language = $jcRequest->getLng() ? $jcRequest->getLng() : "en";
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse, $language);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponse->getItemTotal());
        } else { // if there is no campings directly connected to geoitem then we need to search by radius
            $body = [
                "query" => [
                    "bool" => [
                        "must" => [
                            [
                                "term" => [
                                    "type" => [
                                        "value" => GeoTreeItem::TYPE_GEOITEM
                                    ]
                                ]
                            ],
                            [
                                "bool" => [
                                    "must_not" => [
                                        [
                                            "term" => [
                                                "_id" => [
                                                    "value" => $geoItem->id
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $this->appendBodyWithAggregation($body);
            $this->appendBodyWithFiltering($body, $jcRequest);
            $this->appendBodyWithSorting($body, $jcRequest);

            if ($jcRequest->getFrom()) {
                $body["from"] = $jcRequest->getFrom();
            }
            if ($jcRequest->getSize()) {
                $body["size"] = $jcRequest->getSize();
            }
            $data = [
                "index" => "geotree",
                "type" => "geoitem",
                "body" => $body,
            ];

            $esSearchGeoItemResponseByRadius = new ESSearchGeoItemResponse($this->esClient->$data);  // import
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponseByRadius, $language);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponseByRadius->getItemTotal());
        }
        $jcSearchResponse->setMainJcItem($geoItem);
        return $jcSearchResponse;

    }

    public function retrieveCommercialLandingPage(JCRequest $jcRequest)
    {
        $geoItem = $jcRequest->getItem(); //$this->getGeoItemById($params["item_id"]);
        $body = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "prefix" => [
                                "key" => $geoItem->key
                            ]
                        ],
                        [
                            "term" => [
                                "type" => GeoTreeItem::TYPE_GEOITEM
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $data = [
            "index" => "geotree",
            "type" => "geoitem",
            "body" => $body,
        ];

        Log::debug(json_encode($data['body']));
        $esSearchGeoItemResponse = new ESSearchGeoItemResponse($this->esClient->search($data)); //import ESSearchGeoItemResponse
        $language = $jcRequest->getLng() ? $jcRequest->getLng() : "en";
        if (count($esSearchGeoItemResponse->getItems()) > 0) {
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponse, $language);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponse->getItemTotal());
        } else { // if there is no campings directly connected to geoitem then we need to search by radius
            $body = [
                "query" => [
                    "bool" => [
                        "must" => [
                            [
                                "term" => [
                                    "type" => [
                                        "value" => GeoTreeItem::TYPE_GEOITEM
                                    ]
                                ]
                            ],
                            [
                                "bool" => [
                                    "must_not" => [
                                        [
                                            "term" => [
                                                "_id" => [
                                                    "value" => $geoItem->id
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $this->appendBodyWithAggregation($body);
            $this->appendBodyWithFiltering($body, $jcRequest);
            $this->appendBodyWithSorting($body, $jcRequest);

            if ($jcRequest->getFrom()) {
                $body["from"] = $jcRequest->getFrom();
            }
            if ($jcRequest->getSize()) {
                $body["size"] = $jcRequest->getSize();
            }
            $data = [
                "index" => "geotree",
                "type" => "geoitem",
                "body" => $body,
            ];

            $esSearchGeoItemResponseByRadius = new ESSearchGeoItemResponse($this->esClient->$data);  // import
            $jcSearchResponse = new JCSearchResponse($esSearchGeoItemResponseByRadius, $language);
            $jcSearchResponse->setItemTotal($esSearchGeoItemResponseByRadius->getItemTotal());
        }
        $jcSearchResponse->setMainJcItem($geoItem);
        return $jcSearchResponse;

    }


    /**
     * @return \Elasticsearch\Client
     */
    public function getEsClient()
    {
        return $this->esClient;
    }
}

