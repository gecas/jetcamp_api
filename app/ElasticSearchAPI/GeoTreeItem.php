<?php

namespace App\ElasticSearchAPI;

use App\ElasticSearchAPI\GeoTreeItemKey;
use App\Accommodation;

class GeoTreeItem
{

    const TYPE_GEOITEM = 1;
    const TYPE_CAMPING = 2;
    const TYPE_COMMERCIAL = 3;
    const TYPE_POI = 4;

    const GEO_LOCATION_TYPE_POLYGON = "polygon";

    const SUBTYPE_CITIES = ["PPL", "PPLA", "PPLA2", "PPLA3", "PPLA4", "PPLC", "PPLL", "PPLX"];
    const SUBTYPE_REGIONS = ["ADM1", "ADM2", "ADM3", "ADM4", "ADM5", "PRSH", "TERR"];
    const SUBTYPE_COUNTRIES = ["PCLD", "PCLF", "PCLI", "PCLIX", "PCLS"];
    const SUBTYPE_CONTINENTS = ["CONT"];

    public $type;
    public $geo_id;
    public $id;
    public $subtype;
    public $key;
    //public $tags;
    public $item;
    public $name;
    public $gps_coords;
    public $translations = [];
    //public $grouped_translations = [];
    public $camping_cnt = 0;
    public $camping_cnt_10 = 0;
    public $camping_cnt_25 = 0;
    public $camping_cnt_50 = 0;
    public $rating;
    public $popularity;
    public $search_score;
    public $parent_geo_id;
    public $breadcrumbs = [];

    public function __construct()
    {
        $this->item = new \stdClass();
    }

    public function isGeoItem() {
        if ($this->type == self::TYPE_GEOITEM)
            return true;
        else
            return false;
    }

    public function isCamping() {
        if ($this->type == self::TYPE_CAMPING)
            return true;
        else
            return false;
    }

    public function getMysqlId() {
        return $this->geo_id;
    }

    public function calculateId() {
        $this->id = $this->geo_id."-".$this->type;
    }

    public function getContinent() {
        $geoTreeItemKey = new GeoTreeItemKey($this->key);
        return $geoTreeItemKey->getContinent();
    }

    public function getCountry() {
        $geoTreeItemKey = new GeoTreeItemKey($this->key);
        return $geoTreeItemKey->getCountry();
    }

    public function getMainRegion() {
        $geoTreeItemKey = new GeoTreeItemKey($this->key);
        return $geoTreeItemKey->getMainRegion();
    }

    public function getTranslation($tranlations, $language) {
        return $this->getTranslationFromList($tranlations, $language);
    }

    public function getTranslationByTrans($translations, $needle) {
        foreach ($translations as $translation) {
            if (strcasecmp($translation["trans"], $needle) == 0)
                return $translation;
        }
        return null;
    }

    public function getSublineCountryTranslation($language) {
        $trans = "";
        if (property_exists($this, "subline")){
            if (array_key_exists("country_translations", $this->subline)) {
                $trans = $this->getTranslationFromList($this->subline["country_translations"], $language);
            }
        }

        if ($trans) {
            return $trans;
        } else {
            return $this->getCountry();
        }

    }

    public function getSublineRegionTranslation($language) {
        $trans = "";
        if (property_exists($this, "subline")){
            if (array_key_exists("region_translations", $this->subline)) {
                $trans = $this->getTranslationFromList($this->subline["region_translations"], $language);
            }
        }

        if ($trans) {
            return $trans;
        } else {
            return $this->getMainRegion();
        }

    }

    public function getSublineContinentTranslation($language) {
        $trans = "";
        if (property_exists($this, "subline")){
            if (array_key_exists("continent_translations", $this->subline)) {
                $trans = $this->getTranslationFromList($this->subline["continent_translations"], $language);
            }
        }

        if ($trans) {
            return $trans;
        } else {
            return $this->getContinent();
        }

    }

    public function getItemNameTranslation($language) {
        $trans = "";
        if (property_exists($this->item, "name_translations")){
            $trans = $this->getTranslationFromList($this->item->name_translations, $language);
        }
        if (empty($trans))
            $trans = $this->item->name;
        return $trans;
    }

    public function getTranslationFromList($translationsList, $language) {
        foreach ($translationsList as $translation) {
            if ($translation["lng"] == $language)
                return $translation["trans"];
        }
        return null;
    }

    public function isCity() {
        if (in_array($this->subtype, GeoTreeItem::SUBTYPE_CITIES))
            return true;
        else
            return false;
    }

    public function isCountry() {
        if (in_array($this->subtype, GeoTreeItem::SUBTYPE_COUNTRIES))
            return true;
        else
            return false;
    }

    public function isContinent() {
        if (in_array($this->subtype, GeoTreeItem::SUBTYPE_CONTINENTS))
            return true;
        else
            return false;
    }

    public function isPoi()
    {
        if ($this->type == self::TYPE_POI)
            return true;
        else
            return false;
    }

    public function getCommercialRegionCountry() {
        if (property_exists($this->item, "key")) {
            $parts = explode("_", $this->item->key);
            return $parts[1];
        }
        return "";
    }

    public function getItemPolygon() {
        if (property_exists($this->item, "geo_location") and $this->item->geo_location["type"] == self::GEO_LOCATION_TYPE_POLYGON)
            return $this->item->geo_location["coordinates"][0];
        else
            return [];
    }

    public function getAccommodation() {
        if ($this->isCamping()){
            return Accommodation::find($this->getMysqlId());
        } else
            return null;
    }

}