<?php

namespace App\ElasticSearchAPI;

use App\ElasticSearchAPI\GeoTreeItem;
use App\JetCamp\JCRequest;

class ESSearchGeoItemResponse
{
    private $rawEsResponse;
    private $items = [];
    private $ratingsAgg;
    private $status;
    private $facilitiesAgg;
    private $sportsAndGameAgg;
    private $sanitaryAgg;
    private $childrenAgg;
    private $starRatingsAgg;
    private $rulesAgg;
    private $plotsizesAgg;
    private $locationAgg;
    private $jcRequest;
    private $searchQuery;
    private $lng;

    public function __construct($rawEsResponse, JCRequest $jcRequest = null)
    {
        $this->rawEsResponse = $rawEsResponse;
        $this->jcRequest = $jcRequest;
        $this->parseESResponse();
    }

    private function parseESResponse() {
        if (!array_key_exists("errors", $this->rawEsResponse)) {
            $items = [];
            foreach ($this->rawEsResponse["hits"]["hits"] as $hit) {
                $_source = $hit["_source"];

                $itemObj = new GeoTreeItem();
                $itemObj->type = $_source["type"];
                $itemObj->geo_id = $_source["geo_id"];
                $itemObj->id = $_source["id"];
                $itemObj->subtype = $_source["subtype"];
                $itemObj->key = $_source["key"];
                $itemObj->name = $_source["name"];
                //$itemObj->tags = $_source["tags"];
                if (array_key_exists("item", $_source))
                    $itemObj->item = (object)$_source["item"];
                $itemObj->gps_coords = $_source["gps_coords"];
                //$itemObj->grouped_translations = $_source["grouped_translations"];
                $itemObj->translations = $_source["translations"];
                if (array_key_exists("camping_cnt", $_source))
                    $itemObj->camping_cnt = $_source["camping_cnt"];
                if (array_key_exists("camping_cnt_10", $_source))
                    $itemObj->camping_cnt_10 = $_source["camping_cnt_10"];
                if (array_key_exists("camping_cnt_25", $_source))
                    $itemObj->camping_cnt_25 = $_source["camping_cnt_25"];
                if (array_key_exists("camping_cnt_50", $_source))
                    $itemObj->camping_cnt_50 = $_source["camping_cnt_50"];
                if (array_key_exists("subline", $_source))
                    $itemObj->subline = $_source["subline"];
                if (array_key_exists("show_on_acco_whattodo", $_source))
                    $itemObj->show_on_acco_whattodo = $_source["show_on_acco_whattodo"];
                if (array_key_exists("parent_geo_id", $_source))
                    $itemObj->parent_geo_id = $_source["parent_geo_id"];
                if (array_key_exists("breadcrumbs", $_source)) {
                    $itemObj->breadcrumbs = $_source["breadcrumbs"];
                }
                $itemObj->search_score = $hit["_score"];
                $items[] = $itemObj;
            }
            $this->items = $items;
            $this->parseESResponseAggregation();
            $this->setStatus(1);
            $this->setSearchQuery($this->jcRequest ? $this->jcRequest->getQuery() : '');
            $this->setLng($this->jcRequest ? $this->jcRequest->getLng() : 'en');
        } else
            $this->setStatus(0);
    }

    private function parseESResponseAggregation() {
        if (array_key_exists("aggregations", $this->rawEsResponse)) {
            if (array_key_exists("ratings", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->ratingsAgg, $this->rawEsResponse["aggregations"]["ratings"]);
            }
            if (array_key_exists("facilities", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->facilitiesAgg, $this->rawEsResponse["aggregations"]["facilities"]);
            }

//            if (array_key_exists("sports_and_game", $this->rawEsResponse["aggregations"])) {
//                $this->parseESResponseAggregationBuckets($this->sportsAndGameAgg, $this->rawEsResponse["aggregations"]["sports_and_game"]);
//            }
            if (array_key_exists("sportgames", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->sportsAndGameAgg, $this->rawEsResponse["aggregations"]["sportgames"]);
            }

            if (array_key_exists("sanitary", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->sanitaryAgg, $this->rawEsResponse["aggregations"]["sanitary"]);
            }
            if (array_key_exists("children", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->childrenAgg, $this->rawEsResponse["aggregations"]["children"]);
            }

            if (array_key_exists("rating_stars", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->starRatingsAgg, $this->rawEsResponse["aggregations"]["rating_stars"]);
            }

            if (array_key_exists("rules", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->rulesAgg, $this->rawEsResponse["aggregations"]["rules"]);
            }

            if (array_key_exists("plotsize", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->plotsizesAgg, $this->rawEsResponse["aggregations"]["plotsize"]);
            }

            if (array_key_exists("location", $this->rawEsResponse["aggregations"])) {
                $this->parseESResponseAggregationBuckets($this->locationAgg, $this->rawEsResponse["aggregations"]["location"]);
            }

        }
    }

    private function parseESResponseAggregationBuckets(&$covertedAggragetionProperty, $esResponseAggregatedData) {
        $covertedAggragetionProperty["other_documents_cnt"] = $esResponseAggregatedData["sum_other_doc_count"];
        foreach ($esResponseAggregatedData["buckets"] as $aggregationBucket) {
            $covertedAggragetionProperty["buckets"][(string)$aggregationBucket["key"]] = $aggregationBucket["doc_count"];
        }
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getRawEsResponse()
    {
        return $this->rawEsResponse;
    }

    /**
     * @param mixed $rawEsResponse
     */
    public function setRawEsResponse($rawEsResponse)
    {
        $this->rawEsResponse = $rawEsResponse;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getItemTotal() {
        return $this->rawEsResponse["hits"]["total"];
    }

    /**
     * @return mixed
     */
    public function getRatingsAgg()
    {
        return $this->ratingsAgg;
    }

    /**
     * @param mixed $ratingsAgg
     */
    public function setRatingsAgg($ratingsAgg)
    {
        $this->ratingsAgg = $ratingsAgg;
    }

    /**
     * @return mixed
     */
    public function getFacilitiesAgg()
    {
        return $this->facilitiesAgg;
    }

    /**
     * @param mixed $facilitiesAgg
     */
    public function setFacilitiesAgg($facilitiesAgg)
    {
        $this->facilitiesAgg = $facilitiesAgg;
    }

    /**
     * @return mixed
     */
    public function getSportsAndGameAgg()
    {
        return $this->sportsAndGameAgg;
    }

    /**
     * @param mixed $sportsAndGameAgg
     */
    public function setSportsAndGameAgg($sportsAndGameAgg)
    {
        $this->sportsAndGameAgg = $sportsAndGameAgg;
    }

    /**
     * @return mixed
     */
    public function getSanitaryAgg()
    {
        return $this->sanitaryAgg;
    }

    /**
     * @param mixed $sanitaryAgg
     */
    public function setSanitaryAgg($sanitaryAgg)
    {
        $this->sanitaryAgg = $sanitaryAgg;
    }

    /**
     * @return mixed
     */
    public function getChildrenAgg()
    {
        return $this->childrenAgg;
    }

    /**
     * @param mixed $childrenAgg
     */
    public function setChildrenAgg($childrenAgg)
    {
        $this->childrenAgg = $childrenAgg;
    }

    /**
     * @return mixed
     */
    public function getStarRatingsAgg()
    {
        return $this->starRatingsAgg;
    }

    /**
     * @param mixed $starRatingsAgg
     */
    public function setStarRatingsAgg($starRatingsAgg)
    {
        $this->starRatingsAgg = $starRatingsAgg;
    }

    /**
     * @return mixed
     */
    public function getRulesAgg()
    {
        return $this->rulesAgg;
    }

    /**
     * @param mixed $rulesAgg
     */
    public function setRulesAgg($rulesAgg)
    {
        $this->rulesAgg = $rulesAgg;
    }

    /**
     * @return mixed
     */
    public function getPlotsizesAgg()
    {
        return $this->plotsizesAgg;
    }

    /**
     * @param mixed $plotsizesAgg
     */
    public function setPlotsizesAgg($plotsizesAgg)
    {
        $this->plotsizesAgg = $plotsizesAgg;
    }

    /**
     * @return mixed
     */
    public function getLocationAgg()
    {
        return $this->locationAgg;
    }

    /**
     * @param mixed $locationAgg
     */
    public function setLocationAgg($locationAgg)
    {
        $this->locationAgg = $locationAgg;
    }

    /**
     * @return JCRequest
     */
    public function getJcRequest()
    {
        return $this->jcRequest;
    }

    /**
     * @param JCRequest $jcRequest
     */
    public function setJcRequest($jcRequest)
    {
        $this->jcRequest = $jcRequest;
    }

    /**
     * @return mixed
     */
    public function getSearchQuery()
    {
        return $this->searchQuery;
    }

    /**
     * @param mixed $searchQuery
     */
    public function setSearchQuery($searchQuery)
    {
        $this->searchQuery = $searchQuery;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

}