<?php

namespace App\JetCamp;


use App\ElasticSearchAPI\ESClient;

class JCRequest
{
    private $params = [];

    public function __construct($input)
    {
        if (array_key_exists("url_parts", $input)) {
            $this->params["url"] = "";
            foreach ($input["url_parts"] as $urlPart) {
                $this->params["url"] .= $urlPart . "/";
            }
        }

        if (array_key_exists("url_query_parts", $input)) {

            if (array_key_exists("rating", $input["url_query_parts"])) {
                $this->params["rating"] = $input["url_query_parts"]["rating"];
            }

            if (array_key_exists("facilities", $input["url_query_parts"])) {
                $this->params["facilities"] = explode(",", $input["url_query_parts"]["facilities"]);
            }

//            if (array_key_exists("sportgames", $input["url_query_parts"])) {
//            if (array_key_exists("sports_and_game", $input["url_query_parts"])) {
            if (array_key_exists("sportgames", $input["url_query_parts"])) {
//                $this->params["sports_and_game"] = explode(",", $input["url_query_parts"]["sportgames"]);
                $this->params["sportgames"] = explode(",", $input["url_query_parts"]["sportgames"]);
            }
            if (array_key_exists("sanitary", $input["url_query_parts"])) {
                $this->params["sanitary"] = explode(",", $input["url_query_parts"]["sanitary"]);
            }

            if (array_key_exists("rules", $input["url_query_parts"])) {
                $this->params["rules"] = explode(",", $input["url_query_parts"]["rules"]);
            }
            if (array_key_exists("children", $input["url_query_parts"])) {
                $this->params["children"] = explode(",", $input["url_query_parts"]["children"]);
            }
            if (array_key_exists("starrating", $input["url_query_parts"])) {
                $this->params["starrating"] = explode(",", $input["url_query_parts"]["starrating"]);
            }

            if (array_key_exists("plotsize", $input["url_query_parts"])) {
                $this->params["plotsize"] = explode(",", $input["url_query_parts"]["plotsize"]);
            }

            if (array_key_exists("sort", $input["url_query_parts"])) {
                $sort_parts = explode("-", $input["url_query_parts"]["sort"]);
                $this->params["sort_by"] = $sort_parts[0];
                $this->params["sort_order"] = $sort_parts[1];
            }
        }

        if (array_key_exists("size", $input)) {
            $this->params["size"] = $input["size"];
        }

        if (array_key_exists("lng", $input)) {
            $this->params["lng"] = $input["lng"];
        }

        if (array_key_exists("from", $input)) {
            $this->params["from"] = $input["from"];
        }

        if (array_key_exists("item_id", $input)) {
            $this->params["item_id"] = $input["item_id"];
        }

        if (array_key_exists("q", $input)) {
            $this->params["query"] = $input["q"];
        }

        if (array_key_exists("coords", $input)) {
            $this->params["coords"] = $input["coords"];
        }
    }

    public function getParamByName($name) {
        if (array_key_exists($name, $this->params)) {
            return $this->params[$name];
        }
        return null;
    }

    public function setParamValue($name, $value) {
        $this->params[$name] = $value;
    }

    public function getUrl() {
        return $this->getParamByName("url");
    }

    public function getItem() {
        return $this->getParamByName("item");
    }

    public function setItem($item) {
        $this->setParamValue("item", $item);
    }

    public function setLng($lng) {
        $this->setParamValue("lng", $lng);
    }

    public function getLng() {
        return $this->getParamByName("lng");
    }

    public function getRating() {
        return $this->getParamByName("rating");
    }

    public function getFacilities() {
        return $this->getParamByName("facilities");
    }

    public function getSportsAndGames() {
//        return $this->getParamByName("sports_and_game");
        return $this->getParamByName("sportgames");
    }
    public function getSanitary() {
        return $this->getParamByName("sanitary");
    }

    public function getChildren() {
        return $this->getParamByName("children");
    }

    public function getRatingStars() {
        return $this->getParamByName("starrating");
    }

    public function getRules() {
        return $this->getParamByName("rules");
    }
    public function getPlotsizes() {
        return $this->getParamByName("plotsize");
    }

    public function getLocation() {
        return $this->getParamByName("location");
    }

    public function getSortBy() {
        return $this->getParamByName("sort_by");
    }

    public function getSortOrder() {
        return $this->getParamByName("sort_order");
    }

    public function getSize() {
        return $this->getParamByName("size");
    }

    public function getFrom() {
        return $this->getParamByName("from");
    }

    public function getItemId() {
        return $this->getParamByName("item_id");
    }

    public function getQuery() {
        return $this->getParamByName("query");
    }

    public function getCoords() {
        return $this->getParamByName("coords");
    }

}