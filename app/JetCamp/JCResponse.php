<?php

namespace App\JetCamp;
use App\ElasticSearchAPI\GeoTreeItemHelper;
use App\ElasticSearchAPI\GeoTreeItem;

abstract class JCResponse
{
    protected $status = 0;
    protected $type;
    protected $rawResponse;
    protected $itemTotal = 0;
    protected $mainJcItem;
    protected $breadcrumbs;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getMainJcItem()
    {
        return $this->mainJcItem;
    }

    /**
     * @param mixed $mainJcItem
     */
    public function setMainJcItem(GeoTreeItem $geoTreeItem, $language = 'en')
    {
        $this->mainJcItem = JCItemFactory::getInstance()->createJcItem($geoTreeItem, $language);
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getResponseParameters() {
        return [];
    }

    public function setResponseParameters() {

    }

    /**
     * @return mixed
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param mixed $rawResponse
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
    }

    /**
     * @return mixed
     */
    public function getItemTotal()
    {
        return $this->itemTotal;
    }

    /**
     * @param mixed $itemTotal
     */
    public function setItemTotal($itemTotal)
    {
        $this->itemTotal = $itemTotal;
    }

    /**
     * @param mixed $breadcrumbs
     */
    public function setBreadcrumbs($breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * @return mixed
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

}