<?php

namespace App\JetCamp;

class JCItem
{

    public $type;
    public $name;
    public $desc;
    public $subType;
    public $id;
    public $key;
    public $campingCnt = 0;
    public $campingCnt10 = 0;
    public $campingCnt25 = 0;
    public $campingCnt50 = 0;
    public $campingCntWithin;
    public $rating;
    public $ratingCount;
    public $starsRating;
    public $coords;
    public $translations;
    public $geoLocation;
    public $popularity;
    public $searchScore;
    public $facilityMains;
    public $facilitySubs;
    public $facilityThemes;
    public $whattodo;
    public $address;
    public $region;
    public $country;
    public $breadcrumbs;

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSubType()
    {
        return $this->subType;
    }

    /**
     * @param mixed $subType
     */
    public function setSubType($subType)
    {
        $this->subType = $subType;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param mixed $desc
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;
    }

    /**
     * @return mixed
     */
    public function getCampingCnt()
    {
        return $this->campingCnt;
    }

    /**
     * @param mixed $campingCnt
     */
    public function setCampingCnt($campingCnt)
    {
        $this->campingCnt = $campingCnt;
    }

    /**
     * @return mixed
     */
    public function getCoords()
    {
        return $this->coords;
    }

    /**
     * @param mixed $coords
     */
    public function setCoords($coords)
    {
        $this->coords = $coords;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param mixed $translations
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;
    }

    /**
     * @return int
     */
    public function getCampingCnt10()
    {
        return $this->campingCnt10;
    }

    /**
     * @return int
     */
    public function getCampingCnt25()
    {
        return $this->campingCnt25;
    }

    /**
     * @return int
     */
    public function getCampingCnt50()
    {
        return $this->campingCnt50;
    }

    /**
     * @param int $campingCnt10
     */
    public function setCampingCnt10($campingCnt10)
    {
        $this->campingCnt10 = $campingCnt10;
    }

    /**
     * @param int $campingCnt25
     */
    public function setCampingCnt25($campingCnt25)
    {
        $this->campingCnt25 = $campingCnt25;
    }

    /**
     * @param int $campingCnt50
     */
    public function setCampingCnt50($campingCnt50)
    {
        $this->campingCnt50 = $campingCnt50;
    }

    /**
     * @return mixed
     */
    public function getGeoLocation()
    {
        return $this->geoLocation;
    }

    /**
     * @param mixed $geoLocation
     */
    public function setGeoLocation($geoLocation)
    {
        $this->geoLocation = $geoLocation;
    }

    /**
     * @return mixed
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param mixed $popularity
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
    }

    /**
     * @return mixed
     */
    public function getStarsRating()
    {
        return $this->starsRating;
    }

    /**
     * @param mixed $starsRating
     */
    public function setStarsRating($starsRating)
    {
        $this->starsRating = $starsRating;
    }

    /**
     * @return mixed
     */
    public function getCampingCntWithin()
    {
        return $this->campingCntWithin;
    }

    /**
     * @param mixed $campingCntWithin
     */
    public function setCampingCntWithin($campingCntWithin)
    {
        $this->campingCntWithin = $campingCntWithin;
    }

    /**
     * @return mixed
     */
    public function getSearchScore()
    {
        return $this->searchScore;
    }

    /**
     * @param mixed $searchScore
     */
    public function setSearchScore($searchScore)
    {
        $this->searchScore = $searchScore;
    }

    /**
     * @return mixed
     */
    public function getFacilitySubs()
    {
        return $this->facilitySubs;
    }

    /**
     * @return mixed
     */
    public function getFacilityMains()
    {
        return $this->facilityMains;
    }

    /**
     * @param mixed $facilityMains
     */
    public function setFacilitySubs($facilitySubs)
    {
        $this->facilitySubs = $facilitySubs;
    }

    /**
     * @param mixed $facilityMains
     */
    public function setFacilityMains($facilityMains)
    {
        $this->facilityMains = $facilityMains;
    }

    /**
     * @return mixed
     */
    public function getWhattodo()
    {
        return $this->whattodo;
    }

    /**
     * @param mixed $whattodo
     */
    public function setWhattodo($whattodo)
    {
        $this->whattodo = $whattodo;
    }

    /**
     * @return mixed
     */
    public function getRatingCount()
    {
        return $this->ratingCount;
    }

    /**
     * @param mixed $ratingCount
     */
    public function setRatingCount($ratingCount)
    {
        $this->ratingCount = $ratingCount;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getFacilityThemes()
    {
        return $this->facilityThemes;
    }

    /**
     * @param mixed $facilityThemes
     */
    public function setFacilityThemes($facilityThemes)
    {
        $this->facilityThemes = $facilityThemes;
    }

}