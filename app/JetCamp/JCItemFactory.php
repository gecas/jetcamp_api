<?php

namespace App\JetCamp;

use App\ElasticSearchAPI\GeoTreeItem;
use Illuminate\Support\Facades\Lang;

class JCItemFactory
{

    private static $instance = null;

    private function __construct() {}
    private function __clone() {}

    /**
     * get JCItemFactory singleton instance
     *
     * @return JCItemFactory instance
     */
    public static function getInstance()
    {
        if (JCItemFactory::$instance == null)
            JCItemFactory::$instance = new JCItemFactory();
        return JCItemFactory::$instance;
    }



    public function getCampingCntRadiusLabel(GeoTreeItem $geoTreeItem) {
        if ($geoTreeItem->camping_cnt >= 5) {
            return "direct ".$geoTreeItem->camping_cnt;
        } elseif ($geoTreeItem->camping_cnt_10 >= 5) {
            return "10 km radius ".$geoTreeItem->camping_cnt_10." campings";
        } elseif ($geoTreeItem->camping_cnt_25 >= 10) {
            return "25 km radius ".$geoTreeItem->camping_cnt_25." campings";
        } elseif ($geoTreeItem->camping_cnt_50 >= 1) {
            return "50 km radius ".$geoTreeItem->camping_cnt_50." campings";
        } else {
            return "direct ".$geoTreeItem->camping_cnt_50." campings";
        }
    }

    public function createJcItem(GeoTreeItem $geoTreeItem, $language, $searchQuery = "") {
        $jcItem = new JCItem();
        $jcItem->setId($geoTreeItem->id);
        $jcItem->setKey($geoTreeItem->key);
        $jcItem->setType($geoTreeItem->type);
        $jcItem->setSubType($geoTreeItem->subtype);
        $jcItem->setCampingCnt($geoTreeItem->camping_cnt);
        $jcItem->setCampingCnt10($geoTreeItem->camping_cnt_10);
        $jcItem->setCampingCnt25($geoTreeItem->camping_cnt_25);
        $jcItem->setCampingCnt50($geoTreeItem->camping_cnt_50);
        $jcItem->setSearchScore($geoTreeItem->search_score);

        switch ($geoTreeItem->type) {
            case GeoTreeItem::TYPE_GEOITEM:

                $country = $geoTreeItem->getSublineCountryTranslation($language);
                $region = $geoTreeItem->getSublineRegionTranslation($language);
                $continent = $geoTreeItem->getSublineContinentTranslation($language);

                if ($geoTreeItem->isContinent()) {
                    $jcItem->setDesc(Lang::get("messages.subline_continent", [], $language)." ".ucfirst($continent));
                } elseif ($geoTreeItem->isCountry()) {
                    $jcItem->setDesc(Lang::get("messages.subline_country", [], $language).' '.ucfirst($continent));
                } elseif ($geoTreeItem->isCity()) {
                    $jcItem->setDesc(Lang::get("messages.subline_city", [], $language).' '.ucfirst($region).', '.ucfirst($country));
                } else {
                    $jcItem->setDesc(Lang::get("messages.subline_region", [], $language).' '.ucfirst($country));
                }

//                $trans = $geoTreeItem->getTranslation($language);
                $trans = $geoTreeItem->getTranslation($geoTreeItem->translations, $language);
                if ($trans) {
                    $jcItem->setName($trans);
                    if ($language != "en") {
//                        $transEn = $geoTreeItem->getTranslation("en");
                        $transEn = $geoTreeItem->getTranslation($geoTreeItem->translations ,"en");
                        if ($transEn and $transEn != $trans){
                            $jcItem->setName($jcItem->getName(). " (".$transEn.")");
                        }
                    } else {
                        $translationsBySearchQuery = $geoTreeItem->getTranslationByTrans($geoTreeItem->translations, $searchQuery); // trying to resolve translation by entered search query
                        if ($translationsBySearchQuery and strcasecmp($translationsBySearchQuery["trans"], $trans) != 0) {
                            $jcItem->setName($jcItem->getName(). " (".$translationsBySearchQuery["trans"].")"); // set in bracket search query if it was not in english but selected language is EN
                        }
                    }
                }
                if (!$jcItem->getName()) {
                    $jcItem->setName($geoTreeItem->name);
                }

                $jcItem->setRating($geoTreeItem->rating);
                $coords = new \stdClass();
                $coords->lon = (float)$geoTreeItem->gps_coords["lon"];
                $coords->lat = (float)$geoTreeItem->gps_coords["lat"];
                $jcItem->setCoords($coords);

                if (property_exists($geoTreeItem, "show_on_acco_whattodo"))
                    $jcItem->setWhattodo($geoTreeItem->show_on_acco_whattodo);
                else
                    $jcItem->setWhattodo(null);
                $jcItem->setDesc($jcItem->getDesc() . " - ".$jcItem->getSubType());
                $jcItem->setDesc($jcItem->getDesc() . " (".$this->getCampingCntRadiusLabel($geoTreeItem).") ");
                $jcItem->setDesc($jcItem->getDesc()." (".$jcItem->getCampingCnt().", ".$jcItem->getCampingCnt10().", ".$jcItem->getCampingCnt25().", ".$jcItem->getCampingCnt50().")");
                $jcItem->setPopularity($geoTreeItem->popularity);
                break;

            case GeoTreeItem::TYPE_POI:

                $country = $geoTreeItem->getSublineCountryTranslation($language);
                $region = $geoTreeItem->getSublineRegionTranslation($language);

                $jcItem->setDesc(Lang::get("messages.poi_categories.".$geoTreeItem->item->category_key, [], $language).' '
                    .Lang::get("messages.in", [], $language).' '.ucfirst($region).', '.ucfirst($country));

                $jcItem->setName($geoTreeItem->item->name);

                $jcItem->setRating($geoTreeItem->rating);
                $coords = new \stdClass();
                $coords->lon = (float)$geoTreeItem->item->gps_coords["lon"];
                $coords->lat = (float)$geoTreeItem->item->gps_coords["lat"];
                $jcItem->setCoords($coords);
                $jcItem->setDesc($jcItem->getDesc() . " (".$this->getCampingCntRadiusLabel($geoTreeItem).") ");
                //$jcItem->setDesc($jcItem->getDesc() . " - ".Lang::get("messages.poi_categories.".$geoTreeItem->item->category_key));
                $jcItem->setDesc($jcItem->getDesc()." (".$jcItem->getCampingCnt().", ".$jcItem->getCampingCnt10().", ".$jcItem->getCampingCnt25().", ".$jcItem->getCampingCnt50().")");
                $jcItem->setPopularity($geoTreeItem->item->popularity);
                break;
            case GeoTreeItem::TYPE_COMMERCIAL:
                $country = $geoTreeItem->getSublineCountryTranslation($language);
                $jcItem->setDesc(Lang::get("messages.subline_commercial_region", [], $language).' '. ucfirst($country) . ' '.Lang::get("messages.word_with", [], $language). ' '
                    .$geoTreeItem->item->within_camping_cnt.' campings');
                $jcItem->setName($geoTreeItem->getItemNameTranslation($language));
                $jcItem->setGeoLocation($geoTreeItem->getItemPolygon());
                $jcItem->setPopularity($geoTreeItem->item->popularity);
                $jcItem->setCampingCntWithin($geoTreeItem->item->within_camping_cnt);
                break;
            default:

                $country = $geoTreeItem->getSublineCountryTranslation($language);
                $region = $geoTreeItem->getSublineRegionTranslation($language);

                $jcItem->setDesc(Lang::get("messages.subline_camping", [], $language).' '.ucfirst($region).', '.ucfirst($country));
                $jcItem->setName($geoTreeItem->item->name);
                $jcItem->setRating($geoTreeItem->item->rating);
                $jcItem->setRatingCount($geoTreeItem->item->rating_count);
                if (property_exists($geoTreeItem->item, "address"))
                    $jcItem->setAddress($geoTreeItem->item->address);
                $jcItem->setRegion($region);
                $jcItem->setCountry($country);

                $coords = new \stdClass();
                $coords->lon = (float)$geoTreeItem->item->gps_coords["lon"];
                $coords->lat = (float)$geoTreeItem->item->gps_coords["lat"];
                $jcItem->setCoords($coords);

                $jcItem->setDesc($jcItem->getDesc() . " - ".$jcItem->getSubType());
                $jcItem->setDesc($jcItem->getDesc()." (ccnt ".$jcItem->getCampingCnt().", ".$jcItem->getCampingCnt10().", ".$jcItem->getCampingCnt25().", ".$jcItem->getCampingCnt50().")");
                $jcItem->setPopularity($geoTreeItem->item->popularity);
                $jcItem->setStarsRating($geoTreeItem->item->rating_stars);
                $jcItem->breadcrumbs = $geoTreeItem->breadcrumbs;
                break;
        }
        $jcItem->setDesc($jcItem->getDesc() . " (".$jcItem->getPopularity()."  ".$jcItem->getSearchScore().")");
//        $jcItem->setDesc($jcItem->getDesc() . " - ".$jcItem->getSubType());
        $jcItem->setTranslations($geoTreeItem->translations);
        return $jcItem;
    }

}