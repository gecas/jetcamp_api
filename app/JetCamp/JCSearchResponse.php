<?php

namespace App\JetCamp;

use App\ElasticSearchAPI\ESSearchGeoItemResponse;
use App\JetCamp\JCItem;
use App\JetCamp\JCResponse;
use Illuminate\Support\Facades\Lang;
use App\ElasticSearchAPI\GeoTreeItemHelper;
use App\ElasticSearchAPI\GeoTreeItem;

class JCSearchResponse extends JCResponse
{
    private $jcItems = [];
    private $facilitiesGroups = [];
    private $sportsAndGameGroups = [];
    private $sanitaryGroups = [];
    private $ratingGroups = [];
    private $childrenGroups = [];
    private $starRatingsGroups = [];
    private $rulesGroups = [];
    private $plotsizeGroups = [];
    private $locationGroups = [];
    private $esSearchGeoItemResponse;

    public function __construct(ESSearchGeoItemResponse $esSearchGeoItemResponse = null)
    {
        parent::__construct();
        $this->esSearchGeoItemResponse = $esSearchGeoItemResponse;
        if ($esSearchGeoItemResponse) {
            foreach ($esSearchGeoItemResponse->getItems() as $geoTreeItem) {
                $this->addGeoTreeItem($geoTreeItem);
            }
            $this->convertGeoTreeItemsRatingsAggregation($esSearchGeoItemResponse->getRatingsAgg());
            $this->convertGeoTreeItemsFacilitiesAggregation($esSearchGeoItemResponse->getFacilitiesAgg());
            $this->convertGeoTreeItemsSportsAndGameAggregation($esSearchGeoItemResponse->getSportsAndGameAgg());
            $this->convertGeoTreeItemsSanitaryAggregation($esSearchGeoItemResponse->getSanitaryAgg());
            $this->convertGeoTreeItemsChildrenAggregation($esSearchGeoItemResponse->getChildrenAgg());
            $this->convertGeoTreeItemsStarRatingsAggregation($esSearchGeoItemResponse->getStarRatingsAgg());
            $this->convertGeoTreeItemsRulesAggregation($esSearchGeoItemResponse->getRulesAgg());
            $this->convertGeoTreeItemsPlotsizeAggregation($esSearchGeoItemResponse->getPlotsizesAgg());
            $this->convertGeoTreeItemsLocationAggregation($esSearchGeoItemResponse->getLocationAgg());
            $this->setStatus($esSearchGeoItemResponse->getStatus());
            $this->setItemTotal($esSearchGeoItemResponse->getItemTotal());
        }
    }

    public function convertGeoTreeItemsRatingsAggregation($ratingAgg) {
        if ($ratingAgg) {
            if (array_key_exists("buckets", $ratingAgg)) {
                foreach ($ratingAgg["buckets"] as $rating => $documentCount) {
                    $rounded = (int)floor($rating);
                    if (array_key_exists($rounded, $this->ratingGroups)) {
                        $this->ratingGroups[$rounded] += $documentCount;
                    } else {
                        $this->ratingGroups[$rounded] = $documentCount;
                    }
                }
            }
        }
    }

    public function convertGeoTreeItemsFacilitiesAggregation($facilitiesAgg) {
        if (!empty($facilitiesAgg)) {
            if (array_key_exists("buckets", $facilitiesAgg)) {
                $this->facilitiesGroups = $facilitiesAgg["buckets"];
            }
        }
    }

    public function convertGeoTreeItemsSportsAndGameAggregation($sportsAndGameAgg) {
        if (!empty($sportsAndGameAgg)) {
            if (array_key_exists("buckets", $sportsAndGameAgg)) {
                $this->sportsAndGameGroups = $sportsAndGameAgg["buckets"];
            }
        }
    }
    public function convertGeoTreeItemsSanitaryAggregation($sanitaryAgg) {
        if (!empty($sanitaryAgg)) {
            if (array_key_exists("buckets", $sanitaryAgg)) {
                $this->sanitaryGroups = $sanitaryAgg["buckets"];
            }
        }
    }

    public function convertGeoTreeItemsChildrenAggregation($childrenAgg) {
        if (!empty($childrenAgg)) {
            if (array_key_exists("buckets", $childrenAgg)) {
                $this->childrenGroups = $childrenAgg["buckets"];
            }
        }
    }

    public function convertGeoTreeItemsStarRatingsAggregation($starRatingsAgg) {
        if (!empty($starRatingsAgg)) {
            if (array_key_exists("buckets", $starRatingsAgg)) {
                $this->starRatingsGroups = $starRatingsAgg["buckets"];
            }
        }
    }

    public function convertGeoTreeItemsRulesAggregation($rulesAgg) {
        if (!empty($rulesAgg)) {
            if (array_key_exists("buckets", $rulesAgg)) {
                $this->rulesGroups = $rulesAgg["buckets"];
            }
        }
    }

    public function convertGeoTreeItemsPlotsizeAggregation($plotsizeAgg) {
        if (!empty($plotsizeAgg)) {
            if (array_key_exists("buckets", $plotsizeAgg)) {
                $this->plotsizeGroups = $plotsizeAgg["buckets"];
            }
        }
    }

    public function convertGeoTreeItemsLocationAggregation($locationAgg) {
        if (!empty($locationAgg)) {
            if (array_key_exists("buckets", $locationAgg)) {
                $this->locationGroups = $locationAgg["buckets"];
            }
        }
    }


    public function setMainJcItem2($jcItem) {
        $this->mainJcItem = $jcItem;
    }

    /**
     * @return array
     */
    public function getJcItems()
    {
        return $this->jcItems;
    }

    /**
     * @param array $jcItems
     */
    public function setJcItems($jcItems)
    {
        $this->jcItems = $jcItems;
    }

    public function addGeoTreeItem($geoTreeItem) {
        $this->jcItems[] = JCItemFactory::getInstance()->createJcItem($geoTreeItem, $this->esSearchGeoItemResponse->getLng(), $this->esSearchGeoItemResponse->getSearchQuery());
    }

    public function getResponseParameters() {
        $return["status"] = $this->getStatus();
        $return["items"] = $this->getJcItems();
        $return["main_item"] = $this->getMainJcItem();
        $return["items_total"] = $this->getItemTotal();
        $return["rating_groups"] = $this->getRatingGroups();
        $return["facilities_groups"] = $this->getFacilitiesGroups();
//        $return["sports_and_game_groups"] = $this->getSportsAndGameGroups();
        $return["sportgames_groups"] = $this->getSportsAndGameGroups();
        $return["sanitary_groups"] = $this->getSanitaryGroups();
        $return["children_groups"] = $this->getChildrenGroups();
        $return["rules_groups"] = $this->getRulesGroups();
        $return["star_ratings_groups"] = $this->getStarRatingsGroups();
        $return["plotsize_groups"] = $this->getPlotsizeGroups();
        $return["location_groups"] = $this->getLocationGroups();
        $return["type"] = $this->getType();
        $return["breadcrumbs"] = $this->getBreadcrumbs();
        return $return;
    }

    /**
     * @return array
     */
    public function getChildrenGroups()
    {
        return $this->childrenGroups;
    }

    /**
     * @param array $childrenGroups
     */
    public function setChildrenGroups($childrenGroups)
    {
        $this->childrenGroups = $childrenGroups;
    }

    /**
     * @return array
     */
    public function getSanitaryGroups()
    {
        return $this->sanitaryGroups;
    }

    /**
     * @param array $sanitaryGroups
     */
    public function setSanitaryGroups($sanitaryGroups)
    {
        $this->sanitaryGroups = $sanitaryGroups;
    }

    /**
     * @return array
     */
    public function getSportsAndGameGroups()
    {
        return $this->sportsAndGameGroups;
    }

    /**
     * @param array $sportsAndGameGroups
     */
    public function setSportsAndGameGroups($sportsAndGameGroups)
    {
        $this->sportsAndGameGroups = $sportsAndGameGroups;
    }

    /**
     * @return array
     */
    public function getFacilitiesGroups()
    {
        return $this->facilitiesGroups;
    }

    /**
     * @param array $facilitiesGroups
     */
    public function setFacilitiesGroups($facilitiesGroups)
    {
        $this->facilitiesGroups = $facilitiesGroups;
    }

    /**
     * @return array
     */
    public function getRatingGroups()
    {
        return $this->ratingGroups;
    }

    /**
     * @param array $ratingGroups
     */
    public function setRatingGroups($ratingGroups)
    {
        $this->ratingGroups = $ratingGroups;
    }

    /**
     * @return array
     */
    public function getStarRatingsGroups()
    {
        return $this->starRatingsGroups;
    }

    /**
     * @param array $starRatingsGroups
     */
    public function setStarRatingsGroups($starRatingsGroups)
    {
        $this->starRatingsGroups = $starRatingsGroups;
    }

    /**
     * @return array
     */
    public function getRulesGroups()
    {
        return $this->rulesGroups;
    }

    /**
     * @param array $rulesGroups
     */
    public function setRulesGroups($rulesGroups)
    {
        $this->rulesGroups = $rulesGroups;
    }

    /**
     * @return array
     */
    public function getPlotsizeGroups()
    {
        return $this->plotsizeGroups;
    }

    /**
     * @param array $plotsizeGroups
     */
    public function setPlotsizeGroups($plotsizeGroups)
    {
        $this->plotsizeGroups = $plotsizeGroups;
    }

    /**
     * @return array
     */
    public function getLocationGroups()
    {
        return $this->locationGroups;
    }

    /**
     * @param array $locationGroups
     */
    public function setLocationGroups($locationGroups)
    {
        $this->locationGroups = $locationGroups;
    }

    public function processRawResponse($rawResponse) {
        $this->setRawResponse($rawResponse);
        $params = json_decode($rawResponse, true);
        $this->setStatus($params["status"]);
        $this->setJcItems($params["items"]);
        $this->setMainJcItem2($params["main_item"]);
        $this->setItemTotal($params["items_total"]);
        $this->setRatingGroups($params["rating_groups"]);
        $this->setFacilitiesGroups($params["facilities_groups"]);
//        $this->setSportsAndGameGroups($params["sports_and_game_groups"]);
        $this->setSportsAndGameGroups($params["sportgames_groups"]);
        $this->setSanitaryGroups($params["sanitary_groups"]);
        $this->setChildrenGroups($params["children_groups"]);
        $this->setRulesGroups($params["rules_groups"]);
        $this->setStarRatingsGroups($params["star_ratings_groups"]);
        $this->setPlotsizeGroups($params["plotsize_groups"]);
        $this->setLocationGroups($params["location_groups"]);
        $this->setType($params["type"]);
        $this->setBreadcrumbs($params["breadcrumbs"]);
    }


}