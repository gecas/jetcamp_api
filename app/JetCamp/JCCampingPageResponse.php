<?php

namespace App\JetCamp;

use App\ElasticSearchAPI\ESSearchGeoItemResponse;
use App\JetCamp\JCItem;
use App\JetCamp\JCResponse;
use Illuminate\Support\Facades\Lang;
use App\ElasticSearchAPI\GeoTreeItemHelper;
use App\ElasticSearchAPI\GeoTreeItem;

class JCCampingPageResponse extends JCResponse
{
    private $campingPoisItems = [];
    private $campingCampingsItems = [];

    public function __construct(ESSearchGeoItemResponse $esSearchGeoItemResponse = null, $language = "en")
    {
        parent::__construct();
        if ($esSearchGeoItemResponse) {
            //$this->convertGeoTreeItemsRatingsAggregation($esSearchGeoItemResponse->getRatingsAgg());
            $this->setStatus($esSearchGeoItemResponse->getStatus());
            $this->setItemTotal($esSearchGeoItemResponse->getItemTotal());
            $this->setRawResponse($esSearchGeoItemResponse->getRawEsResponse());
        }
    }

    /**
     * @return array
     */
    public function getCampingPoisItems()
    {
        return $this->campingPoisItems;
    }

    /**
     * @param array $campingPoisItems
     */
    public function setCampingPoisItems($campingPoisItems)
    {
        $this->campingPoisItems = $campingPoisItems;
    }

    /**
     * @return array
     */
    public function getCampingCampingsItems()
    {
        return $this->campingCampingsItems;
    }

    /**
     * @param array $campingCampingsItems
     */
    public function setCampingCampingsItems($campingCampingsItems)
    {
        $this->campingCampingsItems = $campingCampingsItems;
    }

//    public function addGeoTreeItem($geoTreeItem, $language = 'en') {
//        $this->jcItems[] = $this->geoTreeItemHelper->convertToJcItem($geoTreeItem, $language);
//    }

    public function addCampingsItem($geoTreeItem, $language = 'en') {
        $this->campingCampingsItems[] = JCItemFactory::getInstance()->createJcItem($geoTreeItem, $language);
    }

    public function addPoisItem($geoTreeItem, $language = 'en') {
        $this->campingPoisItems[] = JCItemFactory::getInstance()->createJcItem($geoTreeItem, $language);
    }

    public function getResponseParameters() {
        $return["status"] = $this->getStatus();
        $return["camping_poi_items"] = $this->getCampingPoisItems();
        $return["camping_campings_items"] = $this->getCampingCampingsItems();
        $return["main_item"] = $this->getMainJcItem();
        $return["items_total"] = $this->getItemTotal();
//        $return["rating_groups"] = $this->getRatingGroups();
        $return["type"] = $this->getType();
        $return["breadcrumbs"] = $this->getBreadcrumbs();
        return $return;
    }

    public function processRawResponse($rawResponse) {
        $this->setRawResponse($rawResponse);
        $params = json_decode($rawResponse, true);
        $this->setStatus($params["status"]);
        $this->setCampingCampingsItems($params["camping_campings_items"]);
        $this->setCampingPoisItems($params["camping_poi_items"]);
//        $this->setRatingGroups($params["rating_groups"]);
//        $this->setFacilitiesGroups($params["facilities_groups"]);
        $this->setType($params["type"]);
    }

}