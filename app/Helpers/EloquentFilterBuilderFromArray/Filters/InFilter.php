<?php

namespace App\Helpers\EloquentFilterBuilderFromArray\Filters;

class InFilter implements FilterInterface
{
    protected $query;
    protected $table;
    protected $column;
    protected $value;

    public function __construct(
        \Illuminate\Database\Eloquent\Builder $query,
        string $table,
        string $column,
        string $value
    ) {
        $this->query = $query;
        $this->table = $table;
        $this->column = $column;
        $this->value = $value;
    }

    public function apply()
    {
        $this->query->whereIn($this->table . '.' . $this->column, explode(',', $this->value));
        return $this->query;
    }
}