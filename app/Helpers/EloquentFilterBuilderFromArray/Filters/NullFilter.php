<?php

namespace App\Helpers\EloquentFilterBuilderFromArray\Filters;

class NullFilter implements FilterInterface
{
    protected $query;
    protected $table;
    protected $column;
    protected $value;

    public function __construct(
        \Illuminate\Database\Eloquent\Builder $query,
        string $table,
        string $column,
        string $value
    ) {
        $this->query = $query;
        $this->table = $table;
        $this->column = $column;
        $this->value = $value;
    }

    public function apply()
    {
        if($this->value) {
            $this->query->whereNull($this->table . '.' . $this->column);
        } else{
            $this->query->whereNotNull($this->table . '.' . $this->column);
        }
        return $this->query;
    }
}