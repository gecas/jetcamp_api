<?php

namespace App\Helpers\EloquentFilterBuilderFromArray\Filters;

interface FilterInterface
{
    public function __construct(
        \Illuminate\Database\Eloquent\Builder $query,
        string $table,
        string $column,
        string $value
    );

    public function apply();
}