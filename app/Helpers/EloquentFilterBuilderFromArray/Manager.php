<?php

namespace App\Helpers\EloquentFilterBuilderFromArray;


/**
 * Allows to build Eloquent query from an array.
 *
 * Format of an rules array
 * $_['table']['column']['operand'] = $value
 *
 * Possible filter operands see in $filterClasses variable
 * @todo implement missing filters
 *
 * @package App\Helpers
 */
class Manager
{
    protected $query;
    protected $filters = [];

    protected $filterClasses = [
        'eq' => 'Equal',
        'neq' => 'NotEqual',
        'in' => 'In',
        'nin' => 'NotIn',
        'gt' => 'GreaterThan',
        'gte' => 'GreaterThan',
        'lt' => 'LessThan',
        'lte' => 'LessThanEqual',
        'plikep' => 'LikeBoth',
        'likep' => 'LikeRight',
        'plike' => 'LikeLeft',
        'like' => 'Like',
        'n' => 'Null',
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters Format is [table][column][rule]=value
     */
    public function __construct(\Illuminate\Database\Eloquent\Builder $query, $filters = [])
    {
        $this->query = $query;
        $this->filters = $filters;
    }

    /**
     * Loops through "filter" and new ups specific filter classes and calls "apply" method
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function build()
    {
        foreach ($this->filters as $table => $columns) {
            foreach ($columns as $column => $operands) {
                foreach ($operands as $operand => $value) {
                    $filterClass = __NAMESPACE__ . '\\Filters\\' . $this->filterClasses[$operand] . 'Filter';
                    $this->query = (new $filterClass($this->query, $table, $column, $value))->apply();
                }
            }
        }
        return $this->query;
    }
}