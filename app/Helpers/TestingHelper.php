<?php

class TestingHelper
{
    public static function instance() {
        return new TestingHelper();
    }

    public function isArraySortedByProperty($items, $property, $order = "asc") {
        $value = null;
        $i = 0;
        foreach ($items as $item) {
            if (!empty($value)) {
                switch ($order) {
                    case "asc":
                        if ($item[$property] >= $value) {
                            $i++;
                        }
                        break;
                    case "desc":
                        if ($item[$property] <= $value) {
                            $i++;
                        }
                        break;
                }
            } else {
                $i++;
            }
            $value = $item[$property];
        }

        if ($i == count($items))
            return true;
        else
            return false;
    }

    public function isArrayFilteredByProperty($items, $property, $value) {
        $i = 0;
        foreach ($items as $item) {
            if ($item[$property] >= $value)
                $i++;
        }
        if ($i == count($items))
            return true;
        else
            return false;
    }
}