<?php
if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

function endsWithChar($needle, $haystack)
{
    return ($needle[strlen($needle) - 1] === $haystack);
}


/**
 * Form array from model which is required. To be used in Schemas
 *
 * @param array $fields
 * @param $model
 * @param null|array $required
 * @return array
 */
function getRequiredData(array $fields, $model, $required = null)
{
    $res = [];

    if ($required) {
        $fields = array_intersect($fields, array_keys($required));
    }

    foreach ($fields as $field) {
        $res[$field] = $model->$field;
    }
    return $res;
}

/**
 * Replaces first found match in a string
 *
 * @param string $string
 * @param string $oldValue
 * @param string $newValue
 * @return string
 */
function replaceFirstStringMatch($string, $oldValue, $newValue)
{
    $pos = strpos($string, $oldValue);
    if ($pos !== false) {
        $string = substr_replace($string, $newValue, $pos, strlen($oldValue));
    }
    return $string;
}