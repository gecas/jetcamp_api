<?php

namespace App\Renderables;

use Illuminate\Contracts\Support\Renderable;
use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Encoder\EncoderOptions;
use Neomerx\JsonApi\Document\Link;

class JsonApiRenderable implements Renderable
{
    protected $items;
    protected $withLinks = false;
    protected $total;
    protected $skip;
    protected $take;
    protected $url;
    protected $input;
    protected $fields = [];
    protected $includes = [];
    protected $mappings = [];

    public function __construct($items, $fields = [], $includes = [], $mappings = null)
    {
        $this->items = $items;
        $this->fields = $fields;
        $this->includes = $includes;
        $this->mappings = $mappings ?? [];
    }

    /**
     * Set variables
     *
     * @param string $url Domain url
     * @param array $input
     * @param $total
     * @param $skip
     * @param $take
     */
    public function withLinks($url, $input = [], $total, $skip, $take)
    {
        $this->withLinks = true;
        $this->url = $url;
        $this->skip = $skip;
        $this->take = $take;
        $this->total = $total;
        $this->input = $input;
    }

    /**
     * Return JSON API response
     *
     * @return string
     */
    public function render()
    {
        $mappings = array_merge(config('schema_mappings'), $this->mappings);
        $encoder = Encoder::instance($mappings, new EncoderOptions(JSON_PRETTY_PRINT));
        $params = new \Neomerx\JsonApi\Encoder\Parameters\EncodingParameters(
            $this->includes,
            $this->fields
        );

        if ($this->withLinks) {
            $encoder->withLinks($this->getLinks($this->total, $this->take, $this->skip));
        }

        if (isset($this->total)) {
            $encoder->withMeta(['total' => $this->total]);
        };

        $response = $encoder->encodeData($this->items, $params);
        return $response;
    }

    /**
     * Form links array. It will be passed to Json API encoder
     *
     * @param $count
     * @param $take
     * @param $skip
     * @return array Links array
     */
    private function getLinks($count, $take, $skip)
    {
        $links = [
            Link::SELF => $this->getSelfLink(),
            Link::FIRST => $this->getFirstLink(),
            Link::LAST => $this->getLastLink(),
        ];

        if (($prevLink = $this->getPrevLink()) == true) {
            $links[Link::PREV] = $prevLink;
        }
        if (($nextLink = $this->getNextLink()) == true) {
            $links[Link::NEXT] = $nextLink;
        }
        return $links;
    }

    /**
     * @return Link
     */
    private function getSelfLink()
    {
        return new Link($this->url . '?' . http_build_query($this->input));
    }

    /**
     * @return Link
     */
    private function getFirstLink()
    {
        return new Link($this->url . '?' . http_build_query(
                array_merge($this->input, [
                        'page' => [
                            'offset' => 0,
                            'limit' => $this->take
                        ]
                    ]
                )
            )
        );
    }

    /**
     * @return Link
     */
    private function getLastLink()
    {
        return new Link($this->url . '?' . http_build_query(
                array_merge($this->input, [
                        'page' => [
                            'offset' => ($this->total || $this->take) ? (floor($this->total / $this->take) * $this->take) : 0,
                            'limit' => $this->take
                        ]
                    ]
                )
            )
        );
    }

    /**
     * @return Link|null
     */
    private function getNextLink()
    {
        $nextOffset = $this->skip + $this->take;
        if ($nextOffset < $this->total) {
            return new Link($this->url . '?' . http_build_query(
                    array_merge($this->input, [
                            'page' => [
                                'offset' => $nextOffset,
                                'limit' => $this->take
                            ]
                        ]
                    )
                )
            );
        } else {
            return null;
        }
    }

    /**
     * @return Link|null
     */
    private function getPrevLink()
    {
        $diff = $this->skip - $this->take;
        if ($diff < 0) {
            return null;
        } else {
            return new Link($this->url . '?' . http_build_query(
                    array_merge($this->input, [
                            'page' => [
                                'offset' => $diff,
                                'limit' => $this->take
                            ]
                        ]
                    )
                )
            );
        }
    }
}

