<?php

namespace App\Jobs;

use App\Services\MigrationService;
use DB;

class MigrateASCtoMysqlAccommodationsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $asc_results = app('db')->connection('mysql2')
            ->select('SELECT * FROM `accommodation_scraped_contents` order by id');

        $rows = [];
        foreach ($asc_results as $asc_result) {
            $row = [];
            $row['accommodation_scraped_id'] = (int)$asc_result->id;
            $row['name'] = $asc_result->basics_namecamping;
            $row['address_phone'] = $asc_result->address_phone;
            $row['address_website'] = $asc_result->address_domain;
            $row['address_street'] = $asc_result->address_street;
            $row['address_housenumber'] = $asc_result->address_housenumber;
            $row['address_housenumber_addition'] = $asc_result->address_housenumber_addition;
            $row['address_postcode'] = $asc_result->address_postcode;
            $row['address_city'] = $asc_result->address_city;
            $row['address_country'] = $asc_result->address_country;
            $row['address_gps_lat'] = $asc_result->address_gps_lat;
            $row['address_gps_lon'] = $asc_result->address_gps_lon;
            $row['address_gps_point'] = DB::raw("ST_GeomFromText('POINT(".$asc_result->address_gps_lon." ".$asc_result->address_gps_lat.")')");
            $row['address_phone_1'] = $asc_result->address_phone;
            $row['address_email_public'] = $asc_result->address_email;
            $row['customer_rating_count'] = $asc_result->customer_rating_count;
            $row['customer_rating_average'] = $asc_result->customer_rating_average;
            $row['official_rating'] = $asc_result->official_rating;
            $row['popularity'] = $asc_result->popularity;

            if ($asc_result->places_general) {
                $size_nr_total = $asc_result->places_general;
            } else {
                $size_nr_total = $asc_result->size_nr_camperpitches + $asc_result->size_nr_caravancomfort
                                    + $asc_result->size_nr_plots + $asc_result->size_nr_recreationpitches
                                    + $asc_result->size_nr_rentals;
            }
            $row['size_nr_total'] = $size_nr_total;

            if ($asc_result->places_touristic) {
                $size_nr_recreational_pitches = $asc_result->places_touristic;
            } else {
                $size_nr_recreational_pitches = $asc_result->size_nr_recreationpitches;
            }
            $row['size_nr_recreational_pitches'] = $size_nr_recreational_pitches;

            $row['size_nr_year_pitches'] = $asc_result->places_annual;

            if ($asc_result->places_accosforrents) {
                $size_nr_mobilehomerentals = $asc_result->places_accosforrents;
            } else {
                $size_nr_mobilehomerentals = $asc_result->size_nr_rentals;
            }
            $row['size_nr_mobilehomerentals'] = $size_nr_mobilehomerentals;
            $row['geo_trees_id'] = $asc_result->geo_trees_id;
            $row['price_highseason'] = $asc_result->price_highseason;
            $row['price_adult'] = $asc_result->price_adult;
            $row['price_animal'] = $asc_result->price_animal;
            $row['price_camper'] = $asc_result->price_camper;
            $row['price_caravan'] = $asc_result->price_caravan;
            $row['price_tent'] = $asc_result->price_tent;
            $row['general_nudist_acco'] = $asc_result->general_nudist_acco;

            $rows[] = $row;
        }
        echo "Processed ".count($rows)." items\n";
        echo "Moving them to accommodations\n";
        $migrationService->mysqlBulkInsertOrUpdate($rows, "accommodations");
        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }
}