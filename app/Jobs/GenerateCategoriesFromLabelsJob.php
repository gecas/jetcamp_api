<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;

class GenerateCategoriesFromLabelsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start_time = time();
        $labels = [];
        $labels2 = [];
        $labels3 = [];
        $labels_k = [];
        $labels_conf = [];
        $labels_text = [];
        $a_r1 = ["Yard", "Outdoors", "Backyard"];
        $a_r2 = ["Cafe", "Restaurant", "Pub"];
        $a_r3 = ["Room", "Apartment", "Indoors"];

        $ids = app('db')->connection('mysql2')
            ->select("
                    SELECT DISTINCT 
                        apl.`id` AS id
                    FROM
                        accommodation_photo_labels as apl
                    ");
        foreach ($ids as $id)
        {
            $image_labels = app('db')->connection('mysql2')
                ->select("
                    SELECT
                        apl.`label_text` AS label_text,
                        apl.`id` AS apl_id,
                        apl.`confidence` AS confidence,
                        apl.`photo_id` AS photo_id_labels,
                        ap.`id` AS photos_id,
                        ap.`photo_category_id` AS photo_category_id,
                        ap.`labels_done` AS photo_labels_done
                    FROM
                        accommodation_photo_labels AS apl
                    LEFT JOIN accommodation_photos AS ap ON ap.`id` = apl.`photo_id`
                    WHERE apl.`photo_id` = $id->id
                    ");
            foreach ($image_labels as $label)
            {
                if($label->photo_labels_done == 1)
                {
                    if(
                        strpos($label->label_text, "Caravan") !== false ||
                        strpos($label->label_text, "Cottage") !== false ||
                        strpos($label->label_text, "Mobile Home") !== false ||
                        strpos($label->label_text, "Tent") !== false
                    )
                    {
                        array_push($labels, $label);
                        foreach ($labels as $k =>$l)
                        {
                            $labels_k[$k] = $l->confidence;
                        }
                        $max_k = max($labels_k);
                        $label_key = array_search($max_k, $labels_k);
                        $label_with_highest_conf = $labels[$label_key];
                        $category_id = 1;
                        if($label_with_highest_conf->label_text == "Caravan" ||
                            $label_with_highest_conf->label_text == "Mobile Home")
                        {
                            $category_id = 2;
                        }else if($label_with_highest_conf->label_text == "Cottage")
                        {
                            $category_id = 3;
                        }
                        else if($label_with_highest_conf->label_text == "Tent")
                        {
                            $category_id = 4;
                        }
//                        DB::table('accommodation_photos')
//                            ->where('id', $label_with_highest_conf->photos_id)
//                            ->update(['photo_category_id' => $category_id, 'category_done' => 1]);
                        var_dump(count($labels));
                        var_dump($label_with_highest_conf->label_text);
                        var_dump($label_with_highest_conf->confidence);
                        echo "______"."\n";
                    }else if(
                        strpos($label->label_text, "Yard") !== false ||
                        strpos($label->label_text, "Outdoors") !== false ||
                        strpos($label->label_text, "Backyard") !== false
//                        strpos($label->label_text, "Tent") !== false
                    )
                    {
                        array_push($labels2, $label);
                        foreach ($labels2 as $k =>$l)
                        {
                            $labels_text[$k] = $l->label_text;
                            $labels_conf[$k] = $l->confidence;
                        }
                        var_dump("second array");
                        var_dump(min($labels_conf));
//                        if(count(array_intersect($a_r1, $labels_text)) >= 2 &&
//                            min($labels_text["confidence"]) >= 65)
//                        {
//                            $category_id = 5;
//                        }else if(count(array_intersect($a_r2, $labels_text)) >= 2 &&
//                            min($labels_text["confidence"]) >= 65)
//                        {
//                            $category_id = 6;
//                        }else if(count(array_intersect($a_r3, $labels_text)) >= 2 &&
//                            min($labels_text["confidence"]) >= 70)
//                        {
//                            $category_id = 7;
//                        }
//                        var_dump(array_intersect($a_r1, $labels_text));
//                        array_push($labels2, $label);
//                        arsort($labels2);
//                        var_dump(count($labels2));
//                        var_dump($labels2);
                        echo "______"."\n";
                    }
//                    var_dump(strpos($label->label_text, "Caravan"));
//                    var_dump(strpos($label->label_text, "Cottage"));
//                    var_dump(strpos($label->label_text, "Mobile Home"));
//                    var_dump(strpos($label->label_text, "Tent"));
                }else{
//                    var_dump("else");
//                    echo "______"."\n";
                    // assign to the category 1
                }
            }
        }

//        $image_labels = app('db')->connection('mysql2')
//            ->select("
//                    SELECT
//                        apl.`label_text` AS label_text,
//                        apl.`confidence` AS confidence,
//                        apl.`photo_id` AS photo_id_labels,
//                        ap.`id` AS photos_id,
//                        ap.`photo_category_id` AS photo_category_id,
//                        ap.`labels_done` AS photo_labels_done
//                    FROM
//                        accommodation_photo_labels AS apl
//                    LEFT JOIN accommodation_photos AS ap ON ap.`id` = apl.`photo_id`
//                    ");
//
//        foreach ($image_labels as $label)
//        {
//            if($label->photo_labels_done == 1)
//            {
//                var_dump($label);
//                echo "______"."\n";
//            }else{
//                // assign to the category 1
//            }
//        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}