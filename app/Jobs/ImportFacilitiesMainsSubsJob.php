<?php

namespace App\Jobs;

use App\JetCamp\JCItem;
use App\Services\MigrationService;
use Maatwebsite\Excel\Facades\Excel;
use App\FacilityMain;
use App\FacilitySub;
use App\FacilityTheme;

class ImportFacilitiesMainsSubsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        //echo "you are here -".base_path()."-\n";
        //$excel = App::make('excel');
        $excel_file = base_path()."/bin/facilities.xlsx";
        $sheet = Excel::selectSheetsByIndex(0)->load($excel_file)->get();//load($excel_file)->selectSheetsByIndex(0)->get();
        //var_dump($sheet);

        $themes = [];
        $main_facilities = [];
        $sub_facilities = [];
        $attributes = ["costs", "distances", "openingdates", "openinghours", "size_in_m_lwh", "size_in_cm", "numbers", "frequency", "weekdays", "age", "language"];
        $fillable_cols = ['show_add_costs', 'show_add_distances', 'show_add_openingdates', 'show_add_openinghours',
            'show_add_size_in_cm', 'show_add_size_in_m_lwh', 'show_add_numbers', 'show_add_language',
            'show_add_age', 'show_add_weekdays', 'show_add_frequency'];
        foreach ($sheet as $row) {
            //var_dump($row);
            //echo " ".str_slug($row->theme_en, "")."_".."_".str_slug($row->subfacility_en, "")."<br>";
            $theme = $migrationService->formatFacilityKey("facility", $row->theme_en);
            if (!empty($theme)) {
                $main_faicility = $migrationService->formatFacilityKey($theme, $row->mainfacility_en);
                $sub_facility = $migrationService->formatFacilityKey($main_faicility, $row->subfacility_en);
                /*if (!empty($row->subfacility_en))
                    $sub_facility = $main_faicility."_".str_slug($row->subfacility_en, "");
                else
                    $sub_facility = "";*/
                if (!in_array($theme, $themes))
                    $themes[] = $theme;
                if (!array_key_exists($main_faicility, $main_facilities))
                    $main_facilities[$main_faicility] = [];
                if (empty($sub_facility)) {
                    foreach ($attributes as $attribute) {
                        if (!empty($row->$attribute)) {
                            $main_facilities[$main_faicility][$attribute] = $row->$attribute;
                        }
                    }
                } else {
                    if (!array_key_exists($sub_facility, $sub_facilities))
                        $sub_facilities[$sub_facility] = [];
                    foreach ($attributes as $attribute) {
                        if (!empty($row->$attribute)) {
                            $sub_facilities[$sub_facility][$attribute] = $row->$attribute;
                        }
                    }
                }
            }
        }
        /*foreach ($themes as $theme) {

        }*/
        $i = 1;
        foreach ($themes as $theme) {
            $facilityTheme = FacilityTheme::firstOrCreate(["description" => $theme]);
            echo $i.") theme '".$theme."' \n";
            $i++;
        }
        echo "\n";
        $i = 1;
        foreach ($main_facilities as $facility_key => $values) {
            //echo "kai key ".$facility_key."<br>";
            $facility_key_parts = explode("_", $facility_key);
            $theme_key = $facility_key_parts[0]."_".$facility_key_parts[1];
            $facilityTheme = FacilityTheme::where(["description" => $theme_key])->first();
            //echo "---".$facilityTheme->id."<br>";
            $facilityMain = FacilityMain::firstOrNew(["description" => $facility_key]);
            echo $i.") main facility '".$facility_key."' (theme ".$facilityTheme->description.")\n";
            $fill_values = [];
            $fill_values["facility_theme_id"] = $facilityTheme->id;
            foreach ($fillable_cols as $fillable_col) {
                $fill_values[$fillable_col] = 0;
            }
            foreach ($values as $k => $v) {
                $table_col = "show_add_".$k;
                if (in_array($table_col, $facilityMain->getFillable())) {
                    $fill_values[$table_col] = 1;
                }
                //echo "--".$table_col."++";
            }
            $facilityMain->fill($fill_values);
            $facilityMain->save();
            $i++;
        }
        echo "\n";
        $i = 1;
        foreach ($sub_facilities as $sub_facility_key => $values) {
            $sub_facility_key_parts = explode("_", $sub_facility_key);
            array_pop($sub_facility_key_parts);
            $facility_key = implode("_",$sub_facility_key_parts);
            $facilityMain = FacilityMain::where(["description" => $facility_key])->first();
            $facilitySub = FacilitySub::firstOrNew(["description" => $sub_facility_key]);
            echo $i.") sub facility '".$sub_facility_key."' (main facility ".$facilityMain->description.")\n";
            $fill_values = [];
            $fill_values["facility_main_id"] = $facilityMain->id;
            foreach ($fillable_cols as $fillable_col) {
                $fill_values[$fillable_col] = 0;
            }
            foreach ($values as $k => $v) {
                $table_col = "show_add_".$k;
                if (in_array($table_col, $facilitySub->getFillable())) {
                    $fill_values[$table_col] = 1;
                }
                //echo "--".$table_col."++";
            }
            $facilitySub->fill($fill_values);
            $facilitySub->save();
            $i++;
        }
        /*echo "<pre>";
        print_r($themes);
        echo "</pre>";
        echo "<pre>";
        print_r($main_facilities);
        echo "</pre>";
        echo "<pre>";
        print_r($sub_facilities);
        echo "</pre>";*/
        /*foreach($result as $sheet)
        {
            // get sheet title
            var_dump($sheet->getTitle());
            echo "aaaaa<br>";
        }*/

    }
}