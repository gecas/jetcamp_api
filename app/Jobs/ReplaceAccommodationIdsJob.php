<?php

namespace App\Jobs;

use Maatwebsite\Excel\Facades\Excel;
use DB;

class ReplaceAccommodationIdsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    public function handle()
    {
        $start_time = time();
        $excel_file = base_path()."/bin/accommodation_photos.xlsx";
//        $sheets = Excel::selectSheetsByIndex(0)->load($excel_file)->get();
        $sheets = Excel::selectSheetsByIndex(0)->load($excel_file)->toArray();

        foreach ($sheets as $sheet)
        {
            echo "<pre>";
            var_dump($sheet["id"]);
            echo "\n";
            var_dump($sheet["accommodation_id"]);
            echo "\n";
            $update = DB::table('accommodation_photos')
                ->where('id', $sheet["id"])
                ->update(['accommodation_id' => $sheet["accommodation_id"]]);
            var_dump("Updated \n");
            var_dump("-------------- \n");
        }

        var_dump("Finished in: ".$start_time);
    }
}