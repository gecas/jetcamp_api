<?php

namespace App\Jobs;


use App\ElasticSearchAPI\GeoTreeItemKey;
use App\ElasticSearchAPI\GeoTreeItem;
use App\Services\MigrationService;
use App\Models\Url;

class GenerateCampingsPageUrlsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $cResults = app('db')->connection('mysql2')
            ->select('SELECT id, name, geo_trees_id FROM accommodations where migrated is null and geo_trees_id>0 order by id');
        $urlParamsArr = [];
        $campaignIds = [];
        $i = 1;
        foreach ($cResults as $cResult) {
            $subject_id = $cResult->id."-".GeoTreeItem::TYPE_CAMPING;
            $campingNameSlug = str_slug($cResult->name);
            $gResult = app('db')->connection('mysql2')
                ->select('SELECT * FROM geo_trees where id = :id', ['id' => $cResult->geo_trees_id]);

            if ($gResult) {
                $geoTreeItemKey = new GeoTreeItemKey($gResult[0]->key);
                $countryKey = $geoTreeItemKey->getCountryKey();
                $trans = [];
                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_translations t where t.key="'.$countryKey.'";');
                foreach ($tResults as $tResult) {
                    $trans[$tResult->language_abbrevation] = $tResult->translation;
                }
                //var_dump($trans);
                $urls = [];
                foreach ($trans as $lng_abbr => $translation) {
                    $urls[$lng_abbr] = str_slug($translation)."/".$campingNameSlug."/";
                }
                $needToUnset = [];
                foreach ($urls as $lng_abbr => $url) {
                    if ($lng_abbr != "en") {
                        if ($url == $urls["en"])
                            $needToUnset[] = $lng_abbr;
                    }
                }
                foreach ($needToUnset as $lng_abbr) {
                    unset($urls[$lng_abbr]);
                }
                foreach ($urls as $lng_abbr => $url) {
                    //echo "--- ".$url."\n";
                    $urlParams = [
                        'lang' => $lng_abbr,
                        'url' => $url,
                        'subject_id' => $subject_id,
                        'page_type' => Url::PAGE_TYPE_CAMPING_PAGE
                    ];
                    //echo "---".implode(" ", $urlParams)."\n";
                    $urlParamsArr[] = $urlParams;
                    $campaignIds[] = $cResult->id;
                }
                echo $i.") ".$campingNameSlug." (".$subject_id.")\n";
            }
            if ($i % 1000 == 0 and count($urlParamsArr) > 0) {
                echo "updating batch\n";
                //DB::table('urls')->insert($urlParamsArr);
                $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                if (count($campaignIds) > 0){
                    $r = app('db')->connection('mysql2')
                        ->select('update accommodations set migrated=1 where id in ('.implode(", ", $campaignIds).')');
                    $campaignIds = [];
                }
                $urlParamsArr = [];
            }
            $i++;
        }

        if (count($urlParamsArr) > 0) {
            echo "updating final batch\n";
            //DB::table('urls')->insert($urlParamsArr);
            $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
            if (count($campaignIds) > 0){
                $r = app('db')->connection('mysql2')
                    ->select('update accommodations set migrated=1 where id in ('.implode(", ", $campaignIds).')');
                $campaignIds = [];
            }
        }

        $end_time = time();
        echo "Campings Job Done in ".($end_time - $start_time)."s.\n";
    }

}