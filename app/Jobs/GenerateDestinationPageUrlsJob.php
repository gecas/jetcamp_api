<?php

namespace App\Jobs;

use App\ElasticSearchAPI\GeoTreeItemKey;
use App\ElasticSearchAPI\GeoTreeItem;
use App\Services\MigrationService;
use App\Models\Url;

class GenerateDestinationPageUrlsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {

        $start_time = time();
        $gResults = app('db')->connection('mysql2')
            ->select('SELECT g.id as id, g.name as name, g.key as "key" FROM geo_trees g where g.migrated is null order by g.id');
        $urlParamsArr = [];
        $campaignIds = [];
        $i = 1;
        foreach ($gResults as $gResult) {
            $mysqlGeoKey = $gResult->key;
            $geoTreeIds[] = $gResult->id;
            $keyParties = explode("_", $mysqlGeoKey);
            $trans = [];
            $newKey = "";
            $subject_id = $gResult->id."-".GeoTreeItem::TYPE_GEOITEM;
            while (count($keyParties) > 2) { // no need to include 1st and 2nd key part (geotree and continent)

                $newKey = implode("_", $keyParties);
                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_translations t WHERE t.key="' . $newKey . '";');
                if (!empty($tResults)) {
                    foreach ($tResults as $tResult) {
                        $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                    }
                } else
                    $trans[$newKey]["en"] = end($keyParties);
                array_pop($keyParties);
            }
            if ($subject_id) {

                $trans = array_reverse($trans);
                $destNameSlug = $gResult->name;
                if (!empty($newKey and array_key_exists("en", $trans[$newKey]))) {
                    $urls = [];
                    $a = "campings";
                    foreach ($trans[$newKey] as $lng_abbr => $translation) {
                        if (!empty(str_slug($translation)))
                            $url = $a . "/" . str_slug($translation) . "/";
                        else
                            $url = $a . "/" . str_slug($trans[$newKey]["en"]) . "/"; // for zh language unable to slugify
                        foreach ($trans as $key => $value) {
                            if ($newKey != $key) {
                                if (array_key_exists($lng_abbr, $trans[$key])) {
                                    $url .= str_slug($trans[$key][$lng_abbr]) . "/";
                                } else if (array_key_exists("en", $trans[$key])) {
                                    $url .= str_slug($trans[$key]["en"]) . "/";
                                } else
                                    $url .= str_slug(reset($trans[$key])) . "/";
                            }
                        }
                        $urls[$lng_abbr] = $url; //
                    }
                    $needToUnset = [];
                    foreach ($urls as $lng_abbr => $url) {
                        if ($lng_abbr != "en") {
                            if ($url == $urls["en"])
                                $needToUnset[] = $lng_abbr;
                        }
                    }
                    foreach ($needToUnset as $lng_abbr) {
                        unset($urls[$lng_abbr]);
                    }
                    foreach ($urls as $lng_abbr => $url) {
                        //echo "--- ".$url."\n";
                        $urlParams = [
                            'lang' => $lng_abbr,
                            'url' => $url,
                            'subject_id' => $subject_id,
                            'page_type' => Url::PAGE_TYPE_DESTINATION_PAGE
                        ];
                        //echo "---".implode(" ", $urlParams)."\n";
                        $urlParamsArr[] = $urlParams;
                        $campaignIds[] = $gResult->id;
                    }
                    echo $i . ") " . $destNameSlug . " (" . $subject_id . ")\n";
                }
            }

            if ($i % 1000 == 0 and count($urlParamsArr) > 0) {
                echo "updating batch\n";
                //DB::table('urls')->insert($urlParamsArr);
                $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                if (count($campaignIds) > 0) {
                    $r = app('db')->connection('mysql2')
                        ->select('UPDATE geo_trees SET migrated=1 WHERE id IN (' . implode(", ", $campaignIds) . ')');
                    $campaignIds = [];
                }
                $urlParamsArr = [];
            }
            $i++;
        }
        if (count($urlParamsArr) > 0) {
            echo "updating final batch\n";
            //DB::table('urls')->insert($urlParamsArr);
            $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
            if (count($campaignIds) > 0) {
                $r = app('db')->connection('mysql2')
                    ->select('UPDATE geo_trees SET migrated=1 WHERE id IN (' . implode(", ", $campaignIds) . ')');
                $campaignIds = [];
            }
        }

        $end_time = time();
        echo "Destination Job Done in ".($end_time - $start_time)."s.\n";
    }

}
