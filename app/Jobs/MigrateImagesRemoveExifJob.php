<?php

namespace App\Jobs;

use Icyboy\LumenAws\Aws;
use Aws\S3\S3Client;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class MigrateImagesRemoveExifJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    public function handle()
    {
        $start_time = time();
//        $s3 = Aws::createClient('s3');

        // Instantiate the client.
        $s3 = S3Client::factory(["region" => env('AWS_REGION'), "version" => "2006-03-01"]);

        $bucket = env('AWS_BUCKET');
        $obj_key = env('AWS_OBJECT_KEY');

        // Use the high-level iterators (returns ALL of your objects).
        $objects = $s3->getIterator('ListObjects', array('Bucket' => $bucket, 'Prefix' => 'original'));
        echo "Keys retrieved!\n";
        foreach ($objects as $key => $object) {
            if ($key > 0) {
                $assetPath = Storage::disk('s3')->url($object['Key']);
                $ext = explode('.', $assetPath);
                $title = explode('/', $assetPath);
                $title = end($title);
                $title_no_ext = explode('.', $title);
                $title_no_ext = reset($title_no_ext);
                $ext = end($ext);
                $img = null;
//                if($title_no_ext > 32078)
//                {
                $exists = Storage::disk('s3')->exists($object['Key']);
                if ($exists) {
                    $file = Storage::disk('s3')->get($object['Key']);
//                    $visibility = Storage::disk('s3')->getMetaData($object['Key']);
                    $save_path = public_path('img_s3') . '/';
                    if (!file_exists($save_path)) {
                        mkdir($save_path, 775, true);
                    }
                    $exif = Image::make($file)->save($save_path . $title);
                }
                $iexif = $save_path . $title;


                $images = app('db')->connection('mysql2')
                    ->select("
                    SELECT
                        ac.`name` AS name,
                        ac.`address_gps_lat` AS gps_lat,
                        ac.`address_gps_lon` AS gps_lon,
                        ap.`accommodation_id` AS accommodation_id,
                        ap.`id` AS id,
                        ap.`extension` AS extension
                    FROM accommodation_photos AS ap
                    LEFT JOIN accommodations AS ac ON ac.`accommodation_scraped_id` = ap.`accommodation_id`
                    WHERE ap.`id` = $title_no_ext
                    ORDER BY ap.`id`
                    ");

                if (count($images) > 0) {
                    $remove_exif = exec(base_path() . "/bin/ExifTool/exiftool -all= $iexif");
                    $gps = exec(base_path() . "/bin/ExifTool/exiftool -XMP:GPSLongitude='" . $images[0]->gps_lon . "'  -XMP:GPSLatitude='" . $images[0]->gps_lat . "'  -GPSLongitudeRef='West' -GPSLatitudeRef='North' $iexif");
                }

                $s3_key = env('AWS_OBJECT_KEY_FULLSIZED') . '/' . $title;
                if (file_exists($iexif)) {
                    $to_s3 = $s3->putObject(array(
                        'Bucket' => env('AWS_BUCKET'),
                        'ACL' => 'public-read',
                        'Key' => $s3_key,
                        'SourceFile' => $iexif
                    ));

                    exec("rm -f '" . $iexif . '_original' . "'");
//                    exec("rm -f '".$iexif."'");
                }
                $updated = DB::table('accommodation_photos')
                    ->where('id', $title_no_ext)
                    ->update(['resized_fullsize' => 1]);

                var_dump('updated: '.$updated);
                var_dump(file_exists($iexif));
                var_dump($key);
                echo "------------" . "\n";
//                }

            }
        }
        exec("rm -rf '" . $save_path . "'");
        echo $start_time . ' time spent';
        echo "\n";
    }
}