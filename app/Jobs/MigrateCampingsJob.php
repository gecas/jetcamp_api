<?php

namespace App\Jobs;

use App\ElasticSearchAPI\ESClient;
use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\GeoTree;
use App\Services\MigrationService;

class MigrateCampingsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ESClient $esClient, MigrationService $migrationService)
    {
        $start_time = time();
        $esClient->createGeoItemIndex();
        //ini_set("memory_limit","-1");
        $cResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM `accommodations` where migrated is NULL AND status=1 AND address_gps_lat!=0.00000000 and address_gps_lon!=99.99999999 and geo_trees_id>0 order by id limit 15000');
        $rows = [];
        $translationKeys = [];
        $campaignIds = [];
        $geoTreeIds = [];
        foreach ($cResults as $cResult) {
            if ($cResult->geo_trees_id) {
                $row = [];
                $row["c_id"] = $cResult->id;
                $campaignIds[] = $cResult->id;
                $row["c_name"] = $cResult->name;
                $row["c_street"] = $cResult->address_street;
                $row["c_housenumber"] = $cResult->address_housenumber;
                $row["c_city"] = $cResult->address_city;
                $row["c_postcode"] = $cResult->address_postcode;
                $row["c_geo_tree_id"] = $cResult->geo_trees_id;
                $row["c_rating"] = (float)$cResult->customer_rating_average;
                $row["c_rating_count"] = (int)$cResult->customer_rating_count;
                $row["c_rating_stars"] = (int)$cResult->official_rating;
                $row["c_popularity"] = (int)$cResult->popularity;
                $row["c_gps_lat"] = (float)$cResult->address_gps_lat;
                $row["c_gps_lon"] = (float)$cResult->address_gps_lon;
                $row["c_size_nr_total"] = (int)$cResult->size_nr_total;
                $gResult = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_trees where id = :id', ['id' => $row["c_geo_tree_id"]]);
                if ($gResult) {
                    $geoTreeIds[] = $gResult[0]->id;
                    $row["g_id"] = $gResult[0]->id;
                    $row["g_type"] = $gResult[0]->geonamesFcode;
                    $row["g_key"] = $gResult[0]->key;
                    $row["g_key_short"] = $gResult[0]->key_short;
                    $row["g_gps_lat"] = (float)$gResult[0]->gpsLat;
                    $row["g_gps_lon"] = (float)$gResult[0]->gpsLon;
                    $translationKeys[] = $row["g_key"];

                    $geoItem = new GeoTree;
                    $geoItem->forceFill((array)$gResult[0]);
                    $rows[] = $row;
                }
            }
        }

        echo "Finished adding campings (".count($rows).")\n";
        $geoTreeItems = [];
        foreach ($rows as $row) {
            $geoTreeItem = new GeoTreeItem();
            $geoTreeItem->type = GeoTreeItem::TYPE_CAMPING;
            $geoTreeItem->geo_id = $row["c_id"];
            $geoTreeItem->calculateId();
            $geoTreeItem->subtype = $row["g_type"];
            $geoTreeItem->parent_geo_id = $row["c_geo_tree_id"]."-".GeoTreeItem::TYPE_GEOITEM; // setting parent's geo tree item id
            $geoTreeItem->key = $migrationService->stripKey($row["g_key"]);
            if ($row["g_key_short"] != $row["g_key"])
                $geoTreeItem->key_short = $migrationService->stripKey($row["g_key_short"]);
            if ($row["g_gps_lon"] and $row["g_gps_lat"]) {
                $geoTreeItem->gps_coords["lat"] = $row["g_gps_lat"];
                $geoTreeItem->gps_coords["lon"] = $row["g_gps_lon"];
            } else
                $geoTreeItem->gps_coords = null;

            $geoTreeItem->item->name = trim($row["c_name"]);
            $migrationService->addNameSynonym($geoTreeItem->item, $geoTreeItem->item->name);
            $geoTreeItem->item->rating = $row["c_rating"];
            $geoTreeItem->item->rating_count = $row["c_rating_count"];
            $geoTreeItem->item->rating_stars = $row["c_rating_stars"];
            $geoTreeItem->item->popularity = $row["c_popularity"];
            $geoTreeItem->item->size_nr_total = $row["c_size_nr_total"];

            if ($row["c_street"]) {
                $geoTreeItem->item->address["street"] = $row["c_street"];
                //$geoTreeItem->item->address["street_translations"] = [mb_strtolower($geoTreeItem->item->name), $this->migrationHelper->normalizeString($row["c_street"], " ")];
            }
            $geoTreeItem->item->address["housenumber"] = $row["c_housenumber"];
            $geoTreeItem->item->address["city"] = $row["c_city"];
            $geoTreeItem->item->address["postcode"] = $row["c_postcode"];

            if ($row["c_gps_lon"] and $row["c_gps_lat"]) {
                $geoTreeItem->item->gps_coords["lat"] = $row["c_gps_lat"];
                $geoTreeItem->item->gps_coords["lon"] = $row["c_gps_lon"];
            } else
                $geoTreeItem->item->gps_coords = null;

            $geoTreeItems[] = $geoTreeItem;

            //$migrationService->generateGeoTreeItemUrl($row["c_geo_tree_id"]);
            $migrationService->logGeoItem($geoTreeItem);
        }
        echo "Finished adding geo tree items (".count($geoTreeItems).")\n";

        if ($migrationService->migrateGeoTreeItemsToEs($geoTreeItems, $esClient, $translationKeys, $geoTreeIds)) {
            if (count($campaignIds) > 0)
                $r = app('db')->connection('mysql2')
                    ->select('update accommodations set migrated=1 where id in ('.implode(", ", $campaignIds).')');
        }
        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}