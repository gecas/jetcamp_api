<?php

namespace App\Jobs;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\Url;
use Illuminate\Support\Facades\DB;
use App\Services\MigrationService;


class GenerateUrlsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $gResults = app('db')->connection('mysql2')
            //->select('SELECT * FROM geo_trees where migrated is null order by id limit 100;');
            //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_germany%" and migrated is null order by id limit 15000;');
            //->select('SELECT * FROM geo_trees g where migrated is null order by id limit 15000;');
            ->select('SELECT g.id as id, g.key as k, g.key_short as key_short FROM geo_trees g where migrated is null and moved_to_es = 1 order by id limit 400000;');
            //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_netherlands%" and migrated is null order by id limit 15000;');
            //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_france%" and migrated is null order by id limit 100;');
        //->select('SELECT * FROM geo_trees g where g.key like "geotree_europe_%" and migrated is null order by id limit 5000;');
        $urlParamsArr = [];
        $i = 1;
        foreach ($gResults as $gResult) {
            $mysqlGeoKey = $gResult->key_short;
            $mysqlGeoKeyLong = $gResult->k;
            $geoTreeIds[] = $gResult->id;
            $keyParties = explode("_", $mysqlGeoKey);
            $trans = [];
            $newKey = "";
            $subject_id = $gResult->id."-".GeoTreeItem::TYPE_GEOITEM;
            while (count($keyParties) > 2) { // no need to include 1st and 2nd key part (geotree and continent)
                $newKey = implode("_", $keyParties);
                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_translations t where t.key="'.$newKey.'";');
                if (!empty($tResults)) {
                    foreach ($tResults as $tResult) {
                        $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                    }
                } elseif ($newKey == $mysqlGeoKey) { // if it didn't find any translation for key_short when need to check translation for key
                    $tResults = app('db')->connection('mysql2')
                        ->select('SELECT * FROM geo_translations t where t.key="'.$mysqlGeoKeyLong.'";');
                    foreach ($tResults as $tResult) {
                        $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                    }
                } else
                    $trans[$newKey]["en"] = end($keyParties);
                array_pop($keyParties);
            }
            $trans = array_reverse($trans);
            if (!empty($newKey and array_key_exists("en", $trans[$newKey]))){
                $urls = [];
                foreach ($trans[$newKey] as $lng_abbr => $translation) {
                    if (!empty(str_slug($translation)))
                        $url = str_slug($translation)."/";
                    else
                        $url = str_slug($trans[$newKey]["en"])."/"; // for zh language unable to slugify
                    foreach ($trans as $key => $value) {
                        if ($newKey != $key) {
                            if (array_key_exists($lng_abbr, $trans[$key])) {
                                $url .= str_slug($trans[$key][$lng_abbr])."/";
                            } else if (array_key_exists("en", $trans[$key])) {
                                $url .= str_slug($trans[$key]["en"])."/";
                            } else
                                $url .= str_slug(reset($trans[$key]))."/";
                        }
                    }

                    //$urls[$lng_abbr] = str_replace("-", "__", $url); //
                    $urls[$lng_abbr] = $url; //
                }

                $needToUnset = [];
                foreach ($urls as $lng_abbr => $url) {
                    if ($lng_abbr != "en") {
                        if ($url == $urls["en"])
                            $needToUnset[] = $lng_abbr;
                    }
                }
                foreach ($needToUnset as $lng_abbr) {
                    unset($urls[$lng_abbr]);
                }

                foreach ($urls as $lng_abbr => $url) {
                    $urlParams = [
                        'lang' => $lng_abbr,
                        'url' => $url,
                        'subject_id' => $subject_id,
                        'page_type' => Url::PAGE_TYPE_GEOITEM_SEARCH
                    ];
                    $urlParamsArr[] = $urlParams;
                }
                //echo "Processed key ".$mysqlGeoKey." (".$subject_id.")\n";
                if (count($urlParamsArr) > 0) {
                    //DB::table('urls')->insert($urlParamsArr);
                    $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                    echo $i.") key ".$mysqlGeoKey." (".$subject_id."): saving ".count($urlParamsArr)."\n";
                    $r = app('db')->connection('mysql2')
                        ->select('update geo_trees set migrated=1 where id in ('.implode(", ", $geoTreeIds).')');
                    $geoTreeIds = [];
                    $urlParamsArr = [];
                }
            }
            $i++;
        }
        /*if (count($urlParamsArr) > 0) {
            DB::table('urls')->insert($urlParamsArr);
            echo "saving ".count($urlParamsArr)."\n";
            $r = app('db')->connection('mysql2')
                ->select('update geo_trees set migrated=1 where id in ('.implode(", ", $geoTreeIds).')');
            $geoTreeIds = [];
        }*/
        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}