<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;

class RegenerateCategoriesFromLabelsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start_time = time();
        $labels = [];
        $labels2 = [];
        $labels3 = [];
        $labels4 = [];
        $labels_k = [];
        $labels_conf = [];
        $labels_text = [];
        $a_r1 = ["Yard", "Outdoors", "Backyard"];
        $a_r2 = ["Cafe", "Restaurant", "Pub"];
        $a_r3 = ["Room", "Apartment", "Indoors"];
        $a_r4 = ["Crowd", "Swimming Pool", "Shop", "Boat", "Tennis", "Beach", "Parade", "Aerial View", "Pool", "Fitness", "Playground", "Camping", "Sport", "Sports"];
        $r4_labels_array = [
            [
                "label_text" => "Crowd",
                "confidence" => 75,
                "prio" => 2,
                "category_id" => 10
            ],
            [
                "label_text" => "Swimming Pool",
                "confidence" => 60,
                "prio" => 2,
                "category_id" => 12
            ],
            [
                "label_text" => "Shop",
                "confidence" => 70,
                "prio" => 2,
                "category_id" => 16
            ],
            [
                "label_text" => "Tennis",
                "confidence" => 75,
                "prio" => 1,
                "category_id" => 8
            ],
            [
                "label_text" => "Boat",
                "confidence" => 60,
                "prio" => 2,
                "category_id" => 17
            ],
            [
                "label_text" => "Beach",
                "confidence" => 65,
                "prio" => 1,
                "category_id" => 9
            ],
            [
                "label_text" => "Parade",
                "confidence" => 50,
                "prio" => 1,
                "category_id" => 10
            ],
            [
                "label_text" => "Aerial View",
                "confidence" => 50,
                "prio" => 1,
                "category_id" => 11
            ],
            [
                "label_text" => "Pool",
                "confidence" => 60,
                "prio" => 1,
                "category_id" => 12
            ],
            [
                "label_text" => "Fitness",
                "confidence" => 80,
                "prio" => 1,
                "category_id" => 13
            ],
            [
                "label_text" => "Playground",
                "confidence" => 75,
                "prio" => 1,
                "category_id" => 14
            ],
            [
                "label_text" => "Camping",
                "confidence" => 60,
                "prio" => 1,
                "category_id" => 15
            ]
        ];

        $sortArray = array();

        foreach($r4_labels_array as $r4_array){
            foreach($r4_array as $key=>$value){
                if(!isset($sortArray[$key])){
                    $sortArray[$key] = array();
                }
                $sortArray[$key][] = $value;
            }
        }

        $orderby = "prio"; //change this to whatever key you want from the array
        $orderby2 = "confidence"; //change this to whatever key you want from the array

        array_multisort($sortArray[$orderby],SORT_DESC,$r4_labels_array);
        array_multisort($sortArray[$orderby], SORT_DESC, $sortArray[$orderby2], SORT_ASC, $r4_labels_array);

        $ids = app('db')->connection('mysql2')
            ->select("
                    SELECT DISTINCT 
                        apl.`photo_id` AS id
                    FROM
                        accommodation_photo_labels as apl
                    WHERE apl.`photo_id` > 229 and apl.`photo_id` < 235
                    ");

        foreach ($ids as $id) {
            $image_labels = app('db')->connection('mysql2')
                ->select("
                    SELECT
                        apl.`label_text` AS label_text,
                        apl.`id` AS apl_id,
                        apl.`confidence` AS confidence,
                        apl.`photo_id` AS photo_id_labels,
                        ap.`id` AS photos_id,
                        ap.`photo_category_id` AS photo_category_id,
                        ap.`labels_done` AS photo_labels_done
                    FROM
                        accommodation_photo_labels AS apl
                    LEFT JOIN accommodation_photos AS ap ON ap.`id` = apl.`photo_id`
                    WHERE apl.`photo_id` = $id->id AND ap.`labels_done` = 1
                    ");
            var_dump(count($image_labels));
            die;
            foreach ($image_labels as $key => $label) {

                if ($label->photo_labels_done == 1) {

                    if (
                    strpos($label->label_text, "Caravan") !== false ||
                    strpos($label->label_text, "Cottage") !== false ||
                    strpos($label->label_text, "Mobile Home") !== false ||
                    strpos($label->label_text, "Tent") !== false
                ) {
//                    var_dump("if 1");
//                    var_dump($label->label_text);

                    array_push($labels, $label);
                    foreach ($labels as $k => $l) {
                        $labels_k[$k] = $l->confidence;
                    }
                    $max_k = max($labels_k);
                    $label_key = array_search($max_k, $labels_k);
                    $label_with_highest_conf = $labels[$label_key];
                    $category_id = 1;
                    if ($label_with_highest_conf->label_text == "Caravan" ||
                        $label_with_highest_conf->label_text == "Mobile Home") {
                        $category_id = 2;
                    } else if ($label_with_highest_conf->label_text == "Cottage") {
                        $category_id = 3;
                    } else if ($label_with_highest_conf->label_text == "Tent") {
                        $category_id = 4;
                    }
//                    var_dump('category: '.$category_id);
//                    echo "______"."\n";
//                    break;
//                        DB::table('accommodation_photos')
//                            ->where('id', $label_with_highest_conf->photos_id)
//                            ->update(['photo_category_id' => $category_id, 'category_done' => 1]);
//                        var_dump(count($labels));
//                        var_dump($label_with_highest_conf->label_text);
//                        var_dump($label_with_highest_conf->confidence);
                } else if (
                    strpos($label->label_text, "Yard") !== false ||
                    strpos($label->label_text, "Outdoors") !== false ||
                    strpos($label->label_text, "Backyard") !== false
//                        strpos($label->label_text, "Tent") !== false
                ) {
                    array_push($labels2, $label);
                    var_dump(count($labels2));
                    echo "____________"."\n";

                    foreach ($labels2 as $k => $l) {
                        $labels_text[$k]["text"]= $l->label_text;
                        $labels_text[$k]["confidence"] = $l->confidence;
                    }
//                    var_dump($labels_text["text"]);
//                    var_dump($labels_text["confidence"]);
//                    var_dump($a_r1);
//                    var_dump(count(array_intersect($a_r1, array_column($labels_text, "text")))); // take array column text after the call

//                        var_dump("second array");
//                        var_dump(min($labels_conf));
//                    if (count(array_intersect($a_r1, $labels_text["text"])) >= 2 &&
//                        min($labels_text["confidence"]) >= 65) {
//                        $category_id = 5;
//                        var_dump($labels2);
//                        var_dump('category: '.$category_id);
//                        echo "___________"."\n";
//                    } else if (count(array_intersect($a_r2, $labels_text["text"])) >= 2 &&
//                        min($labels_text["confidence"]) >= 65) {
//                        $category_id = 6;
//                        var_dump($labels2);
//                        var_dump('category: '.$category_id);
//                        echo "___________"."\n";
//                    } else if (count(array_intersect($a_r3, $labels_text["text"])) >= 2 &&
//                        min($labels_text["confidence"]) >= 70) {
//                        $category_id = 7;
//                        var_dump($labels2);
//                        var_dump('category: '.$category_id);
//                        echo "___________"."\n";
//                    }
//                    var_dump('category: '.$category_id);
//                    echo "______"."\n";
                } else if (
                in_array($label->label_text, $a_r4)
                ) {
//                        var_dump("if 3");
//                        var_dump($label->label_text);
//                        echo "_________" . "\n";
//                    $db_text = array_column($image_labels, "label_text");
                    $db_text = array_column($r4_labels_array, "label_text");

                        if($label->label_text == "Sport" || $label->label_text == "Sports")
                    {
                        $category_id = 18;
                    }else {
                            $category_id  = 1;
//                            var_dump($label->label_text);
//                            var_dump($db_text);
//                            var_dump(array_search($label->label_text,$db_text));
                            if(array_search($label->label_text,$db_text) != false)
                            {
                                $kei = array_search($label->label_text,$db_text);
//                                var_dump($label->label_text);
//                                var_dump($r4_labels_array[$kei]);
                                if($label->confidence > $r4_labels_array[$kei]["confidence"])
                                {
//                                    var_dump('category: '.$r4_labels_array[$kei]["category_id"]);
                                }
                            }
                            echo "________"."\n";
                            foreach ($r4_labels_array as $itm) {
                                if (in_array($itm['label_text'], $db_text) && $label->confidence > $itm["confidence"]) {
                                    $category_id  = $itm['category_id'];
                                    break;
                                }
                            }
//                            foreach ($image_labels as $itm) {
//                                if (in_array($itm->label_text, $db_text)) {
//                                    $category_id  = $itm->category_id;
//                                    break;
//                                }
//                            }
                        }

                } else {
                    $category_id = 1;
//                    var_dump("else: " . $label->label_text.', category: '.$category_id);
//                    echo "______"."\n";
                    // assign to the category 1
                }
            } else{
                    $category_id = 1;
//                    var_dump("else/labels not done");
//                    echo "______"."\n";
                }
          }
        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}