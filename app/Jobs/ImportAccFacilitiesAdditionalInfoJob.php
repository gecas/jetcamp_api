<?php

namespace App\Jobs;

use App\Accommodation;
use App\AccommodationAdditionalCost;
use App\AccommodationAdditionalDistance;
use App\AccommodationScrapedContent;
use App\Currency;
use App\FacilitySettingDistanceType;
use App\Services\MigrationService;
use Maatwebsite\Excel\Facades\Excel;
use App\FacilitySub;
use App\FacilityMain;
use App\FacilitySettingAdditional;

class ImportAccFacilitiesAdditionalInfoJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $excel_file = base_path()."/bin/facilities.xlsx";
        $sheet = Excel::selectSheetsByIndex(0)->load($excel_file)->get();//load($excel_file)->selectSheetsByIndex(0)->get();
        $i = 1;

        $distanceTypeInMeters = FacilitySettingDistanceType::where(['description' => 'backoffice_database_distancetype_meter'])->first();
        $distanceTypeInKilometers = FacilitySettingDistanceType::where(['description' => 'backoffice_database_distancetype_kilometer'])->first();
        $currencyInEur = Currency::where(['description' => 'backoffice_database_currency_eur'])->first();
        $costTypePerDay = FacilitySettingAdditional::where(['description' => 'backoffice_database_coststype_PerPersonPerDay'])->first();

        //var_dump($distanceTypeInMeters);
        foreach ($sheet as $row) {
            if (!empty($row["method"])) {
                $theme = $migrationService->formatFacilityKey("facility", $row->theme_en);
                $main_faicility = $migrationService->formatFacilityKey($theme, $row->mainfacility_en);
                $sub_facility = $migrationService->formatFacilityKey($main_faicility, $row->subfacility_en);

                switch ($row["method"]) {
                    case 1:
                    case 2:
                        // If value of column is bigger then 1 then: activate theme/main/subfacility + add row in accommodation_additional_distances.
                        // Use the value found in the column CustomOnes as value. The value is in meters/kilometers.

//                        If: CustomOnes --> if set use this value
//                        Elseif: CustomOnes2 --> if set use this value
//                        Elseif: CustomOnes3 --> if it's a 1 use 5000 as value
//                        Elseif : CustomOnes4 --> if it's an integer above 0, it holds the distance in KM
                        $distance = 0;
                        echo $i.") method type ".$row["method"]."\n";
                        $column = $row->customones;
                        $ascs = AccommodationScrapedContent::where($column, '>=', 1)->get();
                        echo "--- checking column ".$column.", found ".count($ascs)." ASCs\n";
                        if (count($ascs) == 0) {
                            $column = $row->customones2;
                            if (!empty($column)) {
                                $ascs = AccommodationScrapedContent::where($column, '>=', 1)->get();
                                echo "--- checking column ".$column.", found ".count($ascs)." ASCs\n";
                            } else {
                                $column = $row->customones3;
                                if (!empty($column)) {
                                    if ($row["method"] == 1)
                                        $distance = 5000;
                                    else
                                        $distance = 5;
                                    $ascs = AccommodationScrapedContent::where($column, '=', 1)->get();
                                    echo "--- checking column ".$column.", found ".count($ascs)." ASCs\n";
                                } else {
                                    $column = $row->customones4;
                                    if (!empty($column)) {
                                        $ascs = AccommodationScrapedContent::where($column, '>=', 1)->get();
                                        echo "--- checking column ".$column.", found ".count($ascs)." ASCs\n";
                                    }
                                }
                            }
                        }

                        if (count($ascs) > 0) {
                            $facilitySub = null;
                            $facilityMain = null;
                            if (!empty($sub_facility)) {
                                $facilitySub = FacilitySub::where(["description" => $sub_facility])->first();
                            } else {
                                $facilityMain = FacilityMain::where(["description" => $main_faicility])->first();
                            }
                            if ($facilitySub) {
                                echo "Facility sub ".$facilitySub->description."\n";
                            }
                            if ($facilityMain) {
                                echo "Facility main ".$facilityMain->description."\n";
                            }

                            $rows = [];
                            foreach ($ascs as $asc) {
                                $accommodation = Accommodation::where(["accommodation_scraped_id" => $asc->id])->first();
                                //echo "-- adding for accommodation (".$accommodation->id.") distance (".$asc->$column.")\n";
                                //$accommodationAdditionalDistance = new AccommodationAdditionalDistance();
                                $values = [];
                                $values['accommodation_id'] = $accommodation->id;
                                if ($facilitySub) {
                                    $values['facility_sub_id'] = $facilitySub->id;
                                    $accommodation->subFacilities()->sync($facilitySub, false); // adding sub facility if accommodation didn't have it yet
                                }
                                if ($facilityMain) {
                                    $values['facility_main_id'] = $facilityMain->id;
                                    $accommodation->mainFacilities()->sync($facilityMain, false); // adding main facility if accommodation didn't have it yet
                                }
                                if ($distance)
                                    $values['distance'] = $distance;
                                else
                                    $values['distance'] = $asc->$column;

                                if ($row["method"] == 1)
                                    $values['facility_setting_distancetypes_id'] = $distanceTypeInMeters->id;
                                else
                                    $values['facility_setting_distancetypes_id'] = $distanceTypeInKilometers->id;
                                $rows[] = $values;
                                /*$accommodationAdditionalDistance->fill($values);
                                try {
                                    $accommodation->accommodationAdditionalDistance()->save($accommodationAdditionalDistance);
                                } catch (\Exception $e) {
                                    // duplicate exceptions could be throwed, do nothing
                                }*/
                            }
                            $migrationService->mysqlBulkInsertOrUpdate($rows, 'accommodation_additional_distances');
                        }
                        break;
                    case 3:
                        // If value of column is 1 AND the theme/main/subfacility is activated (because column a, b, c or d is a 1) then add row in
                        // accommodation_additional_cost where the costs are 0 and the type is euro.
                        $excel_asc_columns = ["column_names_scraped_content_1", "column_names_scraped_content_2", "column_names_scraped_content_3", "column_names_scraped_content_4"];
                        $column = $row->customones;
                        foreach ($excel_asc_columns as $excel_asc_column) {
                            if (!empty($row->$excel_asc_column)) {
                                $ascs = AccommodationScrapedContent::where($column, '=', 1)->get();
                                echo $i.") method type ".$row["method"].", column ".$column.", found ".count($ascs)." ASCs\n";
                                if (count($ascs) > 0) {
                                    $facilitySub = null;
                                    $facilityMain = null;
                                    if (!empty($sub_facility)) {
                                        $facilitySub = FacilitySub::where(["description" => $sub_facility])->first();
                                    } else {
                                        $facilityMain = FacilityMain::where(["description" => $main_faicility])->first();
                                    }
                                    if ($facilitySub) {
                                        echo "Facility sub ".$facilitySub->description."\n";
                                    }
                                    if ($facilityMain) {
                                        echo "Facility main ".$facilityMain->description."\n";
                                    }
                                    $rows = [];
                                    foreach ($ascs as $asc) {
                                        $accommodation = Accommodation::where(["accommodation_scraped_id" => $asc->id])->first();

                                        //$accommodationAdditionalCost = new AccommodationAdditionalCost();
                                        $values = [];
                                        $values['accommodation_id'] = $accommodation->id;
                                        if ($facilitySub) {
                                            $values['facility_sub_id'] = $facilitySub->id;
                                            $accommodation->subFacilities()->sync($facilitySub, false); // adding sub facility if accommodation didn't have it yet
                                        }
                                        if ($facilityMain) {
                                            $values['facility_main_id'] = $facilityMain->id;
                                            $accommodation->mainFacilities()->sync($facilityMain, false); // adding main facility if accommodation didn't have it yet
                                        }
                                        $values['costs'] = 0;
                                        $values['currency_id'] = $currencyInEur->id;
                                        $values['facility_setting_costtype_id'] = $costTypePerDay->id;
                                        $rows[] = $values;
                                        /*$accommodationAdditionalCost->fill($values);
                                        try {
                                            $accommodation->accommodationAdditionalCost()->save($accommodationAdditionalCost);
                                        } catch (\Exception $e) {
                                            // duplicate exceptions could be throwed, do nothing
                                        }*/
                                    }
                                    $migrationService->mysqlBulkInsertOrUpdate($rows, 'accommodation_additional_costs');
                                }
                                break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            //echo $i.")\n";
            $i++;
        }
        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}