<?php

namespace App\Jobs;

use App\Services\MigrationService;
use App\Models\Url;
use Illuminate\Support\Facades\DB;
use App\ElasticSearchAPI\GeoTreeItem;

class GenerateCampingsUrlsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $cResults = app('db')->connection('mysql2')
            ->select('SELECT id, name, geo_trees_id FROM `accommodations` where migrated is null and geo_trees_id>0 order by id limit 100000');
        $urlParamsArr = [];
        $campaignIds = [];
        $i = 1;
        foreach ($cResults as $cResult) {
            $subject_id = $cResult->id."-".GeoTreeItem::TYPE_CAMPING;
            $campingNameSlug = str_slug($cResult->name);
            $gResult = app('db')->connection('mysql2')
                ->select('SELECT * FROM `geo_trees` where id = :id', ['id' => $cResult->geo_trees_id]);

            if ($gResult) {
                $mysqlGeoKey = $gResult[0]->key_short;
                $mysqlGeoKeyLong = $gResult[0]->key;
                $keyParties = explode("_", $mysqlGeoKey);
                $trans = [];
                $newKey = "";
                while (count($keyParties) > 2) { // no need to include 1st and 2nd key part (geotree and continent)
                    $newKey = implode("_", $keyParties);
                    $tResults = app('db')->connection('mysql2')
                        ->select('SELECT * FROM geo_translations t where t.key="'.$newKey.'";');
                    if (!empty($tResults)) {
                        foreach ($tResults as $tResult) {
                            $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                        }
                    } elseif ($newKey == $mysqlGeoKey) { // if it didn't find any translation for key_short when need to check translation for key
                        $tResults = app('db')->connection('mysql2')
                            ->select('SELECT * FROM geo_translations t where t.key="'.$mysqlGeoKeyLong.'";');
                        foreach ($tResults as $tResult) {
                            $trans[$newKey][$tResult->language_abbrevation] = $tResult->translation;
                        }
                    } else
                        $trans[$newKey]["en"] = end($keyParties);
                    array_pop($keyParties);
                }
                array_pop($keyParties);
                $trans = array_reverse($trans);
                if (!empty($newKey)){
                    $urls = [];
                    foreach ($trans[$newKey] as $lng_abbr => $translation) {
                        if (!empty(str_slug($translation)))
                            $url = str_slug($translation)."/";
                        else
                            $url = str_slug($trans[$newKey]["en"])."/"; // for zh language unable to slugify
                        foreach ($trans as $key => $value) {
                            if ($newKey != $key) {
                                if (array_key_exists($lng_abbr, $trans[$key])) {
                                    $url .= str_slug($trans[$key][$lng_abbr])."/";
                                } else if (array_key_exists("en", $trans[$key])) {
                                    $url .= str_slug($trans[$key]["en"])."/";
                                } else
                                    $url .= str_slug(reset($trans[$key]))."/";
                            }
                        }

                        //$urls[$lng_abbr] = str_replace("-", "__", $url); //
                        $urls[$lng_abbr] = $url.$campingNameSlug."/"; //
                    }


                    $needToUnset = [];
                    foreach ($urls as $lng_abbr => $url) {
                        if ($lng_abbr != "en") {
                            if ($url == $urls["en"])
                                $needToUnset[] = $lng_abbr;
                        }
                    }
                    foreach ($needToUnset as $lng_abbr) {
                        unset($urls[$lng_abbr]);
                    }
                    foreach ($urls as $lng_abbr => $url) {
                        $urlParams = [
                            'lang' => $lng_abbr,
                            'url' => $url,
                            'subject_id' => $subject_id,
                            'page_type' => Url::PAGE_TYPE_CAMPING_SEARCH
                        ];
                        //echo "---".implode(" ", $urlParams)."\n";
                        $urlParamsArr[] = $urlParams;
                        $campaignIds[] = $cResult->id;
                    }
                    echo $i.") ".$campingNameSlug." (".$subject_id.")\n";
                }
            }
            if ($i % 1000 == 0 and count($urlParamsArr) > 0) {
                echo "updating batch\n";
                //DB::table('urls')->insert($urlParamsArr);
                $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                if (count($campaignIds) > 0){
                    $r = app('db')->connection('mysql2')
                        ->select('update accommodations set migrated=1 where id in ('.implode(", ", $campaignIds).')');
                    $campaignIds = [];
                }
                $urlParamsArr = [];
            }
            $i++;
        }

        if (count($urlParamsArr) > 0) {
            echo "updating final batch\n";
            //DB::table('urls')->insert($urlParamsArr);
            $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
            if (count($campaignIds) > 0){
                $r = app('db')->connection('mysql2')
                    ->select('update accommodations set migrated=1 where id in ('.implode(", ", $campaignIds).')');
                $campaignIds = [];
            }
        }

        $end_time = time();
        echo "Campings Job Done in ".($end_time - $start_time)."s.\n";
    }

}