<?php

namespace App\Jobs;

use App\Accommodation;
use App\AccommodationScrapedContent;
use App\Services\MigrationService;
use Maatwebsite\Excel\Facades\Excel;
use App\FacilityMain;
use App\FacilitySub;

class ImportAccFacilitiesMainsSubsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $excel_file = base_path()."/bin/facilities.xlsx";
        $sheet = Excel::selectSheetsByIndex(0)->load($excel_file)->get();//load($excel_file)->selectSheetsByIndex(0)->get();

        $excel_asc_columns = ["column_names_scraped_content_1", "column_names_scraped_content_2", "column_names_scraped_content_3", "column_names_scraped_content_4"];
        $mains_list = [];
        $subs_list = [];
        $i = 2;
        $table_columns = [];
        foreach ($sheet as $row) {
            foreach ($excel_asc_columns as $excel_asc_column) {
                if (!empty($row->$excel_asc_column)) {
                    //echo $i . ") kai " . $excel_asc_column . ", " . $row->$excel_asc_column . "\n";
                    /*if (!array_key_exists($row->$excel_asc_column, $table_columns)) {
                        $table_columns[$row->$excel_asc_column]["subs"] = [];
                        $table_columns[$row->$excel_asc_column]["mains"] = [];
                    }*/
                    $theme = $migrationService->formatFacilityKey("facility", $row->theme_en);
                    if (!empty($theme)) {
                        $main_faicility = $migrationService->formatFacilityKey($theme, $row->mainfacility_en);
                        $sub_facility = $migrationService->formatFacilityKey($main_faicility, $row->subfacility_en);
                        if (!empty($sub_facility)) {
                            //if (!in_array($sub_facility, $subs_list))
                                $subs_list[$sub_facility][] = $row->$excel_asc_column;
                        }
                        $mains_list[$main_faicility][] = $row->$excel_asc_column;
                    }
                }
            }
            $i++;
            /**/
        }
        if (!empty($mains_list)) {
            $mains_keys = array_keys($mains_list);
            $facilityMains = FacilityMain::whereIn("description", $mains_keys)->get();
            $i = 1;
            echo "---- Facility Mains\n";
            foreach ($facilityMains as $facilityMain) {
                echo $i.") facility main description ".$facilityMain->description."\n";
                $columns = $mains_list[$facilityMain->description];
                foreach ($columns as $column) {
                    $accommodationScrapedContents = AccommodationScrapedContent::where([$column => 1])->get();
                    echo "found ASCs ".count($accommodationScrapedContents)." for column ".$column."\n";
                    foreach ($accommodationScrapedContents as $accommodationScrapedContent) {
                        $accommodation = Accommodation::where(["accommodation_scraped_id" => $accommodationScrapedContent->id])->first();
                        $accommodation->mainFacilities()->sync($facilityMain, false);
                    }
                }
                $i++;
            }
        }

        if (!empty($subs_list)) {
            $subs_keys = array_keys($subs_list);
            $facilitySubs = FacilitySub::whereIn("description", $subs_keys)->get();
            $i = 1;
            echo "---- Facility Subs\n";
            foreach ($facilitySubs as $facilitySub) {
                echo $i.") facility sub description ".$facilitySub->description."\n";
                $columns = $subs_list[$facilitySub->description];
                foreach ($columns as $column) {
                    $accommodationScrapedContents = AccommodationScrapedContent::where([$column => 1])->get();
                    echo "found ASCs ".count($accommodationScrapedContents)." for column ".$column."\n";
                    foreach ($accommodationScrapedContents as $accommodationScrapedContent) {
                        $accommodation = Accommodation::where(["accommodation_scraped_id" => $accommodationScrapedContent->id])->first();
                        $accommodation->subFacilities()->sync($facilitySub, false);
                    }
                }
                $i++;
            }
        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }
}