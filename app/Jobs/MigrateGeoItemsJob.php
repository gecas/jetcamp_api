<?php

namespace App\Jobs;

use App\ElasticSearchAPI\ESClient;
use App\ElasticSearchAPI\ESSearchGeoItemResponse;
use App\ElasticSearchAPI\GeoTreeItem;
use App\Models\GeoTree;
use App\Models\GeoTreeTranslation;
use App\Services\MigrationService;
use App\Models\Url;
use Illuminate\Support\Facades\DB;

class MigrateGeoItemsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ESClient $esClient, MigrationService $migrationService)
    {
        $start_time = time();
        $esClient->createGeoItemIndex();

        $gResults = GeoTree::query()
            ->addSelect('*')
            ->addSelect([
                'key_short as k_s',
                'key as k'
            ])
            ->whereNull('migrated')
            ->orderBy('id')
            ->limit(400000)
            ->get();

        $directCampingCnts = $migrationService->getAllDirectCampingCounts();
        $campingRadiusCalculateExceptions = $migrationService->getCampingRadiusCalculateExceptions();
        $newGeoTreeItems = [];
        $geoTreeIds = [];
        $movedToEsGeoTreeIds = [];
        $translationKeys = [];
        echo "Count " . count($gResults) . "\n";
        if (count($gResults) > 0) {
            echo "Starting from " . $gResults[0]->k . "\n";
        }
        $i = 1;
        foreach ($gResults as $gResult) {
            $geoTreeIds[] = $gResult->id;
            //$lat = $gResult->gpsLat;
            //$lon = $gResult->gpsLon;
            //$key = $gResult->key;
            //$id = $gResult->id."-".GeoTreeItem::TYPE_GEOITEM;

            $geoTreeItem = new GeoTreeItem();
            $geoTreeItem->type = GeoTreeItem::TYPE_GEOITEM;
            $geoTreeItem->geo_id = $gResult->id;
            $geoTreeItem->calculateId();
            $geoTreeItem->subtype = $gResult->geonamesFcode;
            $geoTreeItem->key = $migrationService->stripKey($gResult->k);
            if ($gResult->k_s != $gResult->k) {
                $geoTreeItem->key_short = $migrationService->stripKey($gResult->k_s);
            }
            $geoTreeItem->popularity = $gResult->popularity;
            $geoTreeItem->show_on_acco_whattodo = $gResult->show_on_acco_whattodo;
            //$geoTreeItem->tags = $this->migrationHelper->retrieveTags($geoTreeItem->key);
            $translationKeys[] = $gResult->k;

            if ($gResult->gpsLon and $gResult->gpsLat) {
                $geoTreeItem->gps_coords["lat"] = (float)$gResult->gpsLat;
                $geoTreeItem->gps_coords["lon"] = (float)$gResult->gpsLon;
            } else {
                $geoTreeItem->gps_coords = null;
            }

            if (array_key_exists($geoTreeItem->key, $directCampingCnts)) {
                $geoTreeItem->camping_cnt = $directCampingCnts[$geoTreeItem->key];
            }

            $calculatedCampingRadius = "RadiusCalc=No";
            if ($migrationService->needToPerformCampingRadiusCalculations($campingRadiusCalculateExceptions,
                $gResult->countryCode, $geoTreeItem->subtype)) {
                $geoTreeItem = $migrationService->appendGeoItemWithCampingCnts($geoTreeItem);
                $calculatedCampingRadius = "RadiusCalc=Yes";
            }

            /*$campingCnt = $this->esClient->getGeoItemCampingCnt($geoTreeItem);
            $geoTreeItem->camping_cnt = $campingCnt;*/
            unset($geoTreeItem->item);
            echo $i . ") item.key = " . $geoTreeItem->key . " with " . $calculatedCampingRadius . ", " . $gResult->countryCode . ", "
                . $geoTreeItem->subtype . " (" . $geoTreeItem->camping_cnt . ", " . $geoTreeItem->camping_cnt_10 . ", "
                . $geoTreeItem->camping_cnt_25 . ", " . $geoTreeItem->camping_cnt_50 . "):";
            if ($migrationService) { //mr delete from ->
                $newGeoTreeItems[] = $geoTreeItem;
                $movedToEsGeoTreeIds[] = $gResult->id;
                echo "need to migrate\n";
            } else {
                echo "skipping\n";
            }

            if ($i and $i % 5000 == 0) {
                echo "Updating bunch of geotree items (" . count($newGeoTreeItems) . ") ...\n";
                if ($migrationService->migrateGeoTreeItemsToEs($newGeoTreeItems, $esClient, $translationKeys,
                    $geoTreeIds, $movedToEsGeoTreeIds)) {
                    $newGeoTreeItems = [];
                    $geoTreeIds = [];
                    $translationKeys = [];
                    $movedToEsGeoTreeIds = [];
                }
            }
            $i++;
            //var_dump($geoTreeItem);
        }
        echo "Finished with mysql!\n";
        $migrationService->migrateGeoTreeItemsToEs($newGeoTreeItems, $esClient, $translationKeys, $geoTreeIds,
            $movedToEsGeoTreeIds);
        $end_time = time();
        echo "Done in " . ($end_time - $start_time) . "s.\n";

    }

}