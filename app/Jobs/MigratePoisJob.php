<?php

namespace App\Jobs;

use App\ElasticSearchAPI\ESClient;
use App\ElasticSearchAPI\GeoTreeItem;
use App\Services\MigrationService;

class MigratePoisJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ESClient $esClient, MigrationService $migrationService)
    {
        $start_time = time();
        $esClient->createGeoItemIndex();

        $pResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM pois p where p.migrated is null and p.active = 1 order by id limit 45000;');

        $directCampingCnts = $migrationService->getAllDirectCampingCounts();
        //$campingRadiusCalculateExceptions = $migrationHelper->getCampingRadiusCalculateExceptions();

        $geoTreeItems = [];
        $geoTreeIds = [];
        $poiIds = [];
        $i = 1;
        $translationKeys = [];
        foreach ($pResults as $pResult) {
            //echo $i.") processing ".$pResult->key.": ";
            $poiIds[] = $pResult->id;

            $geoResult = null;
            if (is_numeric($pResult->geotree_id))
                $geoResult = app('db')->connection('mysql2')
                        ->select('SELECT * FROM geo_trees where id = :id', ['id' => $pResult->geotree_id]);
            $geoTreeItem = new GeoTreeItem();

            if (!empty($geoResult)) {
                $translationKeys[] = $geoResult[0]->key;
                $geoTreeIds[] = $geoResult[0]->id;
                $geoTreeItem->parent_geo_id = $geoResult[0]->id."-".GeoTreeItem::TYPE_GEOITEM;
            }

            $geoTreeItem->type = GeoTreeItem::TYPE_POI;
            $geoTreeItem->geo_id = $pResult->id;
            $geoTreeItem->calculateId();
            $geoTreeItem->subtype = (!empty($geoResult) ? $geoResult[0]->geonamesFcode : null);
            $geoTreeItem->key = (!empty($geoResult) ? $migrationService->stripKey($geoResult[0]->key) : null);

            $geoTreeItem->gps_coords = null;
            $countryCode = "";
            if (!empty($geoResult)){
                if ($geoResult[0]->gpsLat and $geoResult[0]->gpsLon){
                    $geoTreeItem->gps_coords["lat"] = (float)$geoResult[0]->gpsLat;
                    $geoTreeItem->gps_coords["lon"] = (float)$geoResult[0]->gpsLon;
                }
                if ($geoResult[0]->key_short != $geoResult[0]->key)
                    $geoTreeItem->key_short = $migrationService->stripKey($geoResult[0]->key_short);
                $countryCode = $geoResult[0]->countryCode;
            }

            if (array_key_exists($geoTreeItem->key, $directCampingCnts)) {
                $geoTreeItem->camping_cnt = $directCampingCnts[$geoTreeItem->key];
            }

            /*$calculatedCampingRadius = "RadiusCalc=No";
            if ($migrationHelper->needToPerformCampingRadiusCalculations($campingRadiusCalculateExceptions, $countryCode, $geoTreeItem->subtype)){
                $geoTreeItem = $migrationHelper->appendGeoItemWithCampingCnts($geoTreeItem);
                $calculatedCampingRadius = "RadiusCalc=Yes";
            }*/

            $geoTreeItem = $migrationService->appendGeoItemWithCampingCnts($geoTreeItem);
            echo $i.") item.key = ".$geoTreeItem->key." with ".$countryCode.", ".$geoTreeItem->subtype." ("
                .$geoTreeItem->camping_cnt.", ".$geoTreeItem->camping_cnt_10.", ".$geoTreeItem->camping_cnt_25.", "
                .$geoTreeItem->camping_cnt_50."):\n";
            //if ($migrationHelper->needToMigrateGeoTreeItemToEs($geoTreeItem)) {
                if ($pResult->lat and $pResult->lon) {
                    $geoTreeItem->item->gps_coords["lat"] = (float)str_replace(',', '.', $pResult->lat);
                    $geoTreeItem->item->gps_coords["lon"] = (float)str_replace(',', '.', $pResult->lon);
                } else
                    $geoTreeItem->item->gps_coords = null;

                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM poi_translations t join languages l on (t.language_id = l.id) where t.key="'.$pResult->key.'";');
                foreach ($tResults as $tResult) {
                    $t = trim($tResult->translation);
                    if ($tResult->abbrevation == "en") {
                        $geoTreeItem->item->name = $t;
                    } else {
                        $geoTreeItem->item->name_translations[] = ["lng" => $tResult->abbrevation, "trans" => $t];
                    }
                    $migrationService->addNameSynonym($geoTreeItem->item, $t);
                }
                if (!property_exists($geoTreeItem->item, "name")) {
                    $last = end($geoTreeItem->item->name_translations);
                    $geoTreeItem->item->name = $last["trans"];
                }

                $migrationService->addNameSynonym($geoTreeItem->item, $geoTreeItem->item->name);
                echo "---- poi name ".$geoTreeItem->item->name."\n";

                ($pResult->street ? $geoTreeItem->item->address["street"] = $pResult->street : '');
                ($pResult->region ? $geoTreeItem->item->address["region"] = $pResult->region : '');
                ($pResult->country ? $geoTreeItem->item->address["country"] = $pResult->country: '');
                $geoTreeItem->item->rating = (float)$pResult->rating_value;
                $geoTreeItem->item->rating_count = (int)$pResult->rating_count;
                $geoTreeItem->item->popularity = (int)$pResult->popularity;

                $cResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM poi_cats p_cats join poi_categories p_c on (p_cats.categories_id = p_c.id) where p_cats.pois_id = '.$pResult->id.' order by p_c.priority limit 1;');
                if (count($cResults) > 0) {
                    $categoryKey = $migrationService->stripKey($cResults[0]->key);
                    $categoryKey = $migrationService->stripKey($categoryKey);
                    //echo "category key ".$categoryKey."\n";
                    $geoTreeItem->item->category_key = $categoryKey;
                    $geoTreeItem->item->whattodo = $cResults[0]->show_on_acco_detail_whattodo;
                    $geoTreeItem->item->whattodo_min_rating = $cResults[0]->show_on_acco_detail_whattodo_min_rating;
                    $geoTreeItem->item->whattodo_min_reviews = $cResults[0]->show_on_acco_detail_whattodo_min_revews;
                }
                //$geoTreeItem = $migrationHelper->appendGeoItemWithCampingCnts($geoTreeItem);
//                if (!empty($geoResult)) {
//                    $migrationService->generateGeoTreeItemUrl($geoResult[0]->id);
//                }
                $geoTreeItems[] = $geoTreeItem;
                //echo "need to migrate\n";
            /*}else
                echo "skipping\n";*/
            if ($i and $i % 500 == 0) {
                echo "Updating bunch of poi items (".count($geoTreeItems).") ...\n";
                if ($migrationService->migrateGeoTreeItemsToEs($geoTreeItems, $esClient, $translationKeys, $geoTreeIds)) {
                    if (count($poiIds) > 0)
                        $r = app('db')->connection('mysql2')
                            ->select('update pois set migrated=1 where id in ('.implode(", ", $poiIds).')');
                    $poiIds = [];
                    $geoTreeItems = [];
                    $geoTreeIds = [];
                    $translationKeys = [];
                }

            }
            $i++;
        }

        echo "Finished adding geo tree items (".count($geoTreeItems).")\n";

        if ($migrationService->migrateGeoTreeItemsToEs($geoTreeItems, $esClient, $translationKeys, $geoTreeIds)) {
            if (count($poiIds) > 0)
                $r = app('db')->connection('mysql2')
                    ->select('update pois set migrated=1 where id in ('.implode(", ", $poiIds).')');
        }
        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";

    }

}