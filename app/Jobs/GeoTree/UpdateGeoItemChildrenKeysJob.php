<?php

namespace App\Jobs\GeoTree;

use App\Jobs\Job;
use App\Models\GeoTree;
use App\Models\GeoTreeTranslation;


class UpdateGeoItemChildrenKeysJob extends Job
{
    protected $oldKeyShort;
    protected $newKeyShort;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oldKeyShort, $newKeyShort)
    {
        $this->oldKeyShort = $oldKeyShort;
        $this->newKeyShort = $newKeyShort;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle()
    {
        $this->regenerateGeoTreesKeyShort($this->oldKeyShort, $this->newKeyShort);
    }

    /**Regenerate child geo tree key_short after parent change
     * @param string $oldKeyShort
     * @param string $newKeyShort
     * @return void
     */

    private function regenerateGeoTreesKeyShort($oldKeyShort, $newKeyShort)
    {
        $geoTrees = GeoTree::where('key_short', 'like', $oldKeyShort . '%')->get();
        foreach ($geoTrees as $geoTree) {
            $keyShortNew = replaceFirstStringMatch($geoTree->key_short, $oldKeyShort, $newKeyShort);
            $geoTree->key_short = $keyShortNew;
            $geoTree->save();
        }
    }

}