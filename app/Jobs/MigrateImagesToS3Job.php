<?php

namespace App\Jobs;

use Icyboy\LumenAws\Aws;
use App\AccommodationPhoto;

class MigrateImagesToS3Job extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    public function handle()
    {
        $start_time = time();
//        $path1 = "'C:\Users\PC\Downloads\Nieuwe_map\'";
        $path1 = public_path().'/Nieuwe_map/';

//        $images = app('db')->connection('mysql2')
//            ->select('SELECT * FROM accommodation_photos photos where photos.migrated is null order by id limit 10');
        $images = app('db')->connection('mysql2')
            ->select('
            SELECT
                ac.`name` AS name,
                ap.`accommodation_id` AS accommodation_id,
                ap.`id` AS id,
                ap.`extension` AS extension
            FROM accommodation_photos AS ap
            LEFT JOIN accommodations AS ac ON ac.`accommodation_scraped_id` = ap.`accommodation_id`
            ');



//        $images = AccommodationPhoto::where('migrated', null)->take(10)->get()->load('accommodation');

        $s3 = Aws::createClient('s3');

        echo count($images).' images';
        echo "\n";

        foreach ($images as $k => $image)
        {
//            $photo = $image->accommodation_id.'.'.$image->extension;
//            $photo = $image->accommodation_id.'.'.$image->extension;
            $photo = $image->id.'.'.$image->extension;
            $file = $path1.$photo;
//            $name = str_slug($image->name);
            $key = env('AWS_OBJECT_KEY').'/'.$photo;
//            $key = env('AWS_OBJECT_KEY').'/'.$name.'.'.$image->extension;
            if(file_exists($file))
            {
                $s3->putObject(array(
                    'Bucket'     => env('AWS_BUCKET'),
                    'ACL' => 'public-read',
                    'Key'        => $key,
                    'SourceFile' => $file
                ));
            }
            echo "key: ".$k;
            echo "\n";
            echo $image->id;
            echo "\n";
        }
        echo count($images).' images';
        echo "\n";
        echo $start_time.' time s';
        echo "\n";
    }
}