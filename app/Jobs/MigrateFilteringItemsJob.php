<?php

namespace App\Jobs;

use App\Accommodation;
use App\ElasticSearchAPI\GeoTreeItem;
use App\ElasticSearchAPI\ESClient;
use App\Services\MigrationService;

class MigrateFilteringItemsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ESClient $esClient, MigrationService $migrationService)
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
        echo "Starting migrate facility filtering items\n";
        $start_time = time();

        //$accommodations = Accommodation::all();
        $accommodations = Accommodation::whereNull('migrated')
                            ->orderBy('id', 'asc')
                            ->take(15000)
                            ->get();
        $i = 0;
        $accommodationsIds = [];
        $accommodationsMysqlIds = [];
        foreach ($accommodations as $accommodation) {
            $mainFacilities = $accommodation->mainFacilities;
            $subFacilities = $accommodation->subFacilities;
            $accommodationId = $accommodation->id."-".GeoTreeItem::TYPE_CAMPING;
            $accommodationsMysqlIds[] = $accommodation->id;
            //echo $i.") processing ".$accommodation->name." (".$accommodationId.")\n";
            if (count($mainFacilities) > 0 or count($subFacilities) > 0) {
                //echo "found mains ".count($mainFacilities)." and subs ".count($subFacilities)."\n";
                $main_facilities = [];
                foreach ($mainFacilities as $mainFacility) {
                    $main_facilities[] = $mainFacility->description;
                }

                $sub_facilities = [];
                foreach ($subFacilities as $subFacility) {
                    $sub_facilities[] = $subFacility->description;
                }

                echo $i.") ".$accommodation->name." (".$accommodationId.") adding: ";
                $facility_items = $migrationService->getAccommodationsFacilitiesFilterItems($main_facilities, $sub_facilities);
                if (!empty($facility_items)) {
                    echo "facilities (".count($facility_items).") ";
                    $accommodationsIds["facilities"][$accommodationId] = $facility_items;
                    $facility_items = [];
                }

                $childrens_items = $migrationService->getAccommodationsChildrenFilterItems($main_facilities, $sub_facilities);
                if (!empty($childrens_items)) {
                    echo "children items (".count($childrens_items).") ";
                    $accommodationsIds["children"][$accommodationId] = $childrens_items;
                    $childrens_items = [];
                }

                $sanitary_items = $migrationService->getAccommodationsSanitaryFilterItems($sub_facilities);
                if (!empty($sanitary_items)) {
                    echo "sanitary items (".count($sanitary_items).") ";
                    $accommodationsIds["sanitary"][$accommodationId] = $sanitary_items;
                    $sanitary_items = [];
                }

                $location_items = $migrationService->getAccommodationsLocationFilterItems($accommodation, $mainFacilities, $subFacilities);
                if (!empty($location_items)) {
                    echo "location items (".count($location_items).") ";
                    $accommodationsIds["location"][$accommodationId] = $location_items;
                    $location_items = [];
                }

                $rules_items = $migrationService->getAccommodationsRulesFilteringItems($main_facilities, $sub_facilities);
                if (!empty($rules_items)) {
                    echo "rules items (".count($rules_items).") ";
                    $accommodationsIds["rules"][$accommodationId] = $rules_items;
                    $rules_items = [];
                }

                $sports_game_items = $migrationService->getAccommodationsSportsAndGamesFilteringItems($main_facilities, $sub_facilities);
                if (!empty($sports_game_items)) {
                    echo "sports and game items (".count($sports_game_items).") ";
                    $accommodationsIds["sportgames"][$accommodationId] = $sports_game_items;
                    $sports_game_items = [];
                }

                echo "\n";
            }

            if ($i and $i % 1000 == 0) {
                if (!empty($accommodationsIds)) {
                    echo "Processing accommodations \n";
                    if ($migrationService->migrateAccFilteringItems($accommodationsIds, $esClient, $accommodationsMysqlIds)){
                        $accommodationsMysqlIds = [];
                        $accommodationsIds = [];
                    } else
                        echo "Failed migrate\n";
                }
            }
            $i++;
        }
        echo "Finished adding (".$i.")\n";
        if (!empty($accommodationsIds)) {
            echo "Final batch, processing accommodations ".count($accommodationsIds)."\n";
            if ($migrationService->migrateAccFilteringItems($accommodationsIds, $esClient, $accommodationsMysqlIds)){
                $accommodationsIds = [];
            } else
                echo "Failed migrate\n";
        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}