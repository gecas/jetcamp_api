<?php

namespace App\Jobs\Prebuild;

use App\Jobs\Job;
use App\Models\GeoTree;

class PrebuildGeoItemAllJob extends Job
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        GeoTree::get(['id'])->pluck('id')->chunk(5000)->each(function ($ids) {
            \Queue::push(new PrebuildGeoItemListJob($ids->toArray()));
        });
    }
}