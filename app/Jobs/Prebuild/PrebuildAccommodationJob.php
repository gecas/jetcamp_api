<?php

namespace App\Jobs\Prebuild;

use App\Models\Accommodation;
use App\Services\Prebuild\PrebuildService;
use App\Jobs\Job;

class PrebuildAccommodationJob extends Job
{
    public $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PrebuildService $prebuildService)
    {
        $accommodation = Accommodation::findOrFail($this->id);
        $prebuildService->make($accommodation);
    }
}