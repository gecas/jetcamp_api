<?php

namespace App\Jobs\Prebuild;

use App\Models\GeoTree;
use App\Services\Prebuild\PrebuildService;
use App\Jobs\Job;

class PrebuildGeoItemJob extends Job
{
    public $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PrebuildService $prebuildService)
    {
        $geoTree = GeoTree::findOrFail($this->id);
        $prebuildService->make($geoTree);
    }
}