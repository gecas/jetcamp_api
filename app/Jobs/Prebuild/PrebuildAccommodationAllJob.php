<?php

namespace App\Jobs\Prebuild;

use App\Models\Accommodation;
use App\Jobs\Job;

class PrebuildAccommodationAllJob extends Job
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Accommodation::enabled()->get(['id'])->pluck('id')->chunk(5000)->each(function ($ids) {
            \Queue::push(new PrebuildAccommodationListJob($ids->toArray()));
        });
    }
}