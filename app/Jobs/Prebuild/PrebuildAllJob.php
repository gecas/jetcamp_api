<?php

namespace App\Jobs\Prebuild;

use App\Jobs\Job;

class PrebuildAllJob extends Job
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Queue::push(new PrebuildGeoItemAllJob);
        \Queue::push(new PrebuildAccommodationAllJob);
    }
}