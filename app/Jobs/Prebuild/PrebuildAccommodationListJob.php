<?php

namespace App\Jobs\Prebuild;

use App\Jobs\Job;

class PrebuildAccommodationListJob extends Job
{
    public $ids;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->ids as $id) {
            \Queue::push(new PrebuildAccommodationJob($id));
        }
    }
}