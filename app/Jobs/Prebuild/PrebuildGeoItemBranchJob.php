<?php

namespace App\Jobs\Prebuild;

use App\Jobs\Job;
use App\Models\GeoTree;

class PrebuildGeoItemBranchJob extends Job
{
    public $key;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        GeoTree::where(function ($query) {
            $query->where('geo_trees.key', 'like', $this->key . '_%')
                ->orWhere('geo_trees.key', '=', $this->key);
        })
            ->get(['id'])
            ->pluck('id')
            ->chunk(5000)
            ->each(function ($ids) {
                \Queue::push(new PrebuildGeoItemListJob($ids->toArray()));
            });
    }
}