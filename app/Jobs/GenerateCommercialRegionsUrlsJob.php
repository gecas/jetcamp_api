<?php

namespace App\Jobs;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Services\MigrationService;
use App\Models\Url;
use DB;

class GenerateCommercialRegionsUrlsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        echo "You are here\n";
        $cr_results = app('db')->connection('mysql2')
            ->select('SELECT * FROM commercial_regions cr where migrated is null order by id limit 50000;');

        $i = 1;
        foreach ($cr_results as $cr_result) {
            $subjectId = $cr_result->id."-".GeoTreeItem::TYPE_COMMERCIAL;
            $name_trans_results = app('db')->connection('mysql2')
                ->select("SELECT crt.translation as translation, l.abbrevation as abbrevation FROM commercial_region_translations crt 
                                        join languages l on (crt.language_id = l.id)
                                        where crt.key= :key", ["key" => $cr_result->key]);
            $name_trans = [];
            foreach ($name_trans_results as $name_trans_result) {
                $name_trans[$name_trans_result->abbrevation] = $name_trans_result->translation;
            }
            unset($name_trans_result);
            if (array_key_exists("en", $name_trans))
                $name_default = $name_trans["en"];
            else
                $name_default = end($name_trans);
            $parts = explode("_", $cr_result->key);
            $trans_key = "geotree_".$parts[0]."_".$parts[1];
            $country_trans_results = app('db')->connection('mysql2')
                ->select('SELECT * FROM geo_translations t where t.key="'.$trans_key.'";');
            //$country_trans = [];
            $urls = [];
            foreach ($country_trans_results as $country_trans_result) {
                //$country_trans[] = $trans_result_1->translation;
                if (array_key_exists($country_trans_result->language_abbrevation, $name_trans))
                    $urls[$country_trans_result->language_abbrevation] = str_slug($country_trans_result->translation)."/".str_slug(trim($name_trans[$country_trans_result->language_abbrevation]))."/";
                else
                    $urls[$country_trans_result->language_abbrevation] = str_slug($country_trans_result->translation)."/".str_slug(trim($name_default))."/";
            }

            $needToUnset = [];
            foreach ($urls as $lng_abbr => $url) {
                if ($lng_abbr != "en") {
                    if ($url == $urls["en"])
                        $needToUnset[] = $lng_abbr;
                }
            }
            foreach ($needToUnset as $lng_abbr) {
                unset($urls[$lng_abbr]);
            }

            foreach ($urls as $lng_abbr => $url) {
                $urlParams = [
                    'lang' => $lng_abbr,
                    'url' => $url,
                    'subject_id' => $subjectId,
                    'page_type' => Url::PAGE_TYPE_COMMERCIAL_SEARCH
                ];
                //echo "---".implode("+", $urlParams)."\n";
                $urlParamsArr[] = $urlParams;
            }

            if (!empty($urlParamsArr) > 0) {
                //DB::table('urls')->insert($urlParamsArr);
                $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                echo $i.") ".$cr_result->key." (".$subjectId."): saving ".count($urlParamsArr)."\n";
                $r = app('db')->connection('mysql2')
                    ->select('update commercial_regions set migrated=1 where id ='.$cr_result->id);
                $urlParamsArr = [];
            }
            $i++;
        }
        $end_time = time();
        echo "Campings Job Done in ".($end_time - $start_time)."s.\n";
    }

}