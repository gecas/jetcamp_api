<?php

namespace App\Jobs;

use App\ElasticSearchAPI\GeoTreeItem;
use App\ElasticSearchAPI\ESClient;
use App\Services\MigrationService;

class MigrateCommercialRegionsJob extends Job
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ESClient $esClient, MigrationService $migrationService)
    {
        $start_time = time();
        $esClient->createGeoItemIndex();

        $cr_results = app('db')->connection('mysql2')
            ->select('SELECT cr.id as id, cr.key as k, AsText(cr.gpsPolygon) as polygon, crc.key as cat_key, cr.popularity as popularity, cr.parent_id as parent_id '.
                    'FROM commercial_regions cr '.
                    'join commercial_region_categories crc on (crc.id = cr.category_id)'
                . 'where migrated is null AND cr.active = 1 order by cr.id limit 15000');
        $crIds = [];
        $i = 1;
        $geoTreeItems = [];
        $translationKeys = [];
        $geoTreeIds = [];
        foreach ($cr_results as $cr_result) {
            $crIds[] = $cr_result->id;
            $geoTreeItem = new GeoTreeItem();
            $geoTreeItem->type = GeoTreeItem::TYPE_COMMERCIAL;
            $geoTreeItem->geo_id = $cr_result->id;
            $geoTreeItem->calculateId();

            $geoResult = app('db')->connection('mysql2')
                ->select('SELECT * FROM geo_trees where id = :id', ['id' => $cr_result->parent_id]);

            $geoTreeItem->subtype = (!empty($geoResult) ? $geoResult[0]->geonamesFcode : null);
            $geoTreeItem->key = (!empty($geoResult) ? $migrationService->stripKey($geoResult[0]->key) : null);

            if (!empty($geoResult)){
                if ($geoResult[0]->gpsLat and $geoResult[0]->gpsLon){
                    $geoTreeItem->gps_coords["lat"] = (float)$geoResult[0]->gpsLat;
                    $geoTreeItem->gps_coords["lon"] = (float)$geoResult[0]->gpsLon;
                }
                $geoTreeItem->parent_geo_id = $geoResult[0]->id."-".GeoTreeItem::TYPE_GEOITEM; // setting parent's geo tree item id
                $geoTreeItem->popularity = $geoResult[0]->popularity;
                $geoTreeItem->show_on_acco_whattodo = $geoResult[0]->show_on_acco_whattodo;
                if ($geoResult[0]->key != $geoResult[0]->key_short)
                    $geoTreeItem->key_short = $migrationService->stripKey($geoResult[0]->key_short);

                $translationKeys[] = $geoResult[0]->key;
                $geoTreeIds[] = $geoResult[0]->id;
            }

            $trans_results = app('db')->connection('mysql2')
                            ->select("SELECT crt.translation as translation, l.abbrevation as abbrevation FROM commercial_region_translations crt 
                                        join languages l on (crt.language_id = l.id)
                                        where crt.key= :key", ["key" => $cr_result->k]);

            foreach ($trans_results as $trans_result) {
                $t = trim($trans_result->translation);
                if ($trans_result->abbrevation == "en") {
                    $geoTreeItem->item->name = $t;
                } else {
                    $geoTreeItem->item->name_translations[] = ["lng" => $trans_result->abbrevation, "trans" => $t];
                }
                $migrationService->addNameSynonym($geoTreeItem->item, $t);
            }

            if (!property_exists($geoTreeItem->item, "name")) {
                $last = end($geoTreeItem->item->name_translations);
                $geoTreeItem->item->name = trim($last["trans"]);
                $migrationService->addNameSynonym($geoTreeItem->item, $geoTreeItem->item->name);
            }

            $polygon_coords_str = $cr_result->polygon;
            $c_results = app('db')->connection('mysql2')->
                select("SELECT count(*) as cnt FROM accommodations where ST_CONTAINS(ST_GEOMFROMTEXT(:polygon), point(address_gps_lat, address_gps_lon))",
                        ["polygon" => $polygon_coords_str]);
            $geoTreeItem->item->within_camping_cnt = $c_results[0]->cnt;

            $polygon_coords_str = ltrim($polygon_coords_str, "POLYGON((");
            $polygon_coords_str = rtrim($polygon_coords_str, "))");
            $polygon_coords = explode(",",$polygon_coords_str);
            $points = [];
            foreach ($polygon_coords as $polygon_coord) {
                $parts = explode(" ", $polygon_coord);
                /*$point["lon"] = $parts[0];
                $point["lat"] = $parts[1];
                $points[] = $point;*/
                $points[] = [(float)$parts[1], (float)$parts[0]];
            }
            $geoTreeItem->item->geo_location = [
                "type" => GeoTreeItem::GEO_LOCATION_TYPE_POLYGON,
                "coordinates" => [$points]
            ];

            $geoTreeItem->item->popularity = $cr_result->popularity;
            $geoTreeItem->item->category_key = $migrationService->stripCategortKey($cr_result->cat_key);
            $geoTreeItem->item->key = $cr_result->k;
            echo $i.") processing ".$geoTreeItem->item->name." (".$geoTreeItem->item->category_key.", ".$cr_result->k."): \n";
            //$migrationService->generateGeoTreeItemUrl($cr_result->parent_id);
            $geoTreeItems[] = $geoTreeItem;
            $i++;
        }
        echo "Finished adding geo tree items (".count($geoTreeItems).")\n";

        if ($migrationService->migrateGeoTreeItemsToEs($geoTreeItems, $esClient, $translationKeys, $geoTreeIds)) {
            if (count($crIds) > 0)
                $r = app('db')->connection('mysql2')
                    ->select('update commercial_regions set migrated=1 where id in ('.implode(", ", $crIds).')');
        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}