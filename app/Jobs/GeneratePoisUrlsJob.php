<?php

namespace App\Jobs;

use App\ElasticSearchAPI\GeoTreeItem;
use App\Services\MigrationService;
use App\Models\Url;
use DB;

class GeneratePoisUrlsJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MigrationService $migrationService)
    {
        $start_time = time();
        $pResults = app('db')->connection('mysql2')
            ->select('SELECT * FROM pois p where p.migrated is null and p.active = 1 and geotree_id>0 order by id limit 50000;');

        $i = 1;
        foreach ($pResults as $pResult) {
            $subjectId = $pResult->id."-".GeoTreeItem::TYPE_POI;

            $cResults = app('db')->connection('mysql2')
                ->select('SELECT * FROM poi_cats p_cats join poi_categories p_c on (p_cats.categories_id = p_c.id) where p_cats.pois_id = '.$pResult->id.' order by p_c.priority limit 1;');
            $categoryKey = "";
            if (count($cResults) > 0) {
                $categoryKey = $migrationService->stripCategortKey($cResults[0]->key);
            }

            $tResults = app('db')->connection('mysql2')
                ->select('SELECT * FROM poi_translations t join languages l on (t.language_id = l.id) where t.key="'.$pResult->key.'";');
            $poiName = "";
            foreach ($tResults as $tResult) {
                if ($tResult->abbrevation == "en") {
                    $poiName = trim($tResult->translation);
                }
            }
            //echo $i.") processing ".$poiName.", ".$categoryKey."\n";
            $geoResult = null;
            if (is_numeric($pResult->geotree_id))
                $geoResult = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_trees where id = :id', ['id' => $pResult->geotree_id]);

            if (!empty($geoResult)) {
                $mysqlGeoKey = $geoResult[0]->key_short;

                $keyParties = explode("_", $mysqlGeoKey);
                if (count($keyParties) > 3) {
                    $mysqlGeoCountryKey = $keyParties[0]."_".$keyParties[1]."_".$keyParties[2];
                } else {
                    $mysqlGeoCountryKey = $mysqlGeoKey;
                }
                $trans = [];
                $tResults = app('db')->connection('mysql2')
                    ->select('SELECT * FROM geo_translations t where t.key="'.$mysqlGeoCountryKey.'";');
                foreach ($tResults as $tResult) {
                    $trans[$mysqlGeoCountryKey][$tResult->language_abbrevation] = $tResult->translation;
                }
                array_pop($keyParties);
                $urls = [];
                foreach ($trans[$mysqlGeoCountryKey] as $lng_abbr => $translation) {
                    if (!empty(str_slug($translation)))
                        $url = str_slug($translation)."/";
                    else
                        $url = str_slug($trans[$mysqlGeoCountryKey]["en"])."/"; // for zh language unable to slugify
                    foreach ($trans as $key => $value) {
                        if ($mysqlGeoCountryKey != $key) {
                            if (array_key_exists($lng_abbr, $trans[$key])) {
                                $url .= str_slug($trans[$key][$lng_abbr])."/";
                            } else if (array_key_exists("en", $trans[$key])) {
                                $url .= str_slug($trans[$key]["en"])."/";
                            } else
                                $url .= str_slug(reset($trans[$key]))."/";
                        }
                    }

                    //$urls[$lng_abbr] = str_replace("-", "__", $url); //
                    $urls[$lng_abbr] = $url.str_slug($categoryKey)."/".str_slug($poiName)."/"; //
                }
                $needToUnset = [];
                foreach ($urls as $lng_abbr => $url) {
                    if ($lng_abbr != "en") {
                        if ($url == $urls["en"])
                            $needToUnset[] = $lng_abbr;
                    }
                }
                foreach ($needToUnset as $lng_abbr) {
                    unset($urls[$lng_abbr]);
                }

                foreach ($urls as $lng_abbr => $url) {
                    $urlParams = [
                        'lang' => $lng_abbr,
                        'url' => $url,
                        'subject_id' => $subjectId,
                        'page_type' => Url::PAGE_TYPE_POI_SEARCH
                    ];
                    //echo "---".implode(" ", $urlParams)."\n";
                    $urlParamsArr[] = $urlParams;
                }

                if (count($urlParamsArr) > 0) {
                    //DB::table('urls')->insert($urlParamsArr);
                    $res = $migrationService->mysqlBulkInsertOrUpdate($urlParamsArr, "urls");
                    echo $i.") ".$poiName." (".$subjectId."): saving ".count($urlParamsArr)."\n";
                    $r = app('db')->connection('mysql2')
                                ->select('update pois set migrated=1 where id ='.$pResult->id);
                    $urlParamsArr = [];
                }
            }
            $i++;
        }

        $end_time = time();
        echo "Done in ".($end_time - $start_time)."s.\n";
    }

}