<?php

namespace App\Jobs;

use Aws\Rekognition\RekognitionClient;
use Aws\S3\S3Client;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class GenerateLabelsFromAWSRekognitionJob extends Job
{
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
        date_default_timezone_set('Europe/Vilnius');
    }

    public function handle()
    {
        $options = ["region"=>env('AWS_REGION'), "version"=>"2016-06-27"];

        $rekognition = new RekognitionClient($options);

        // Instantiate the client.
        $s3 = S3Client::factory(["region" => env('AWS_REGION'), "version" => "2006-03-01"]);

        $bucket = env('AWS_BUCKET');
        $obj_key = env('AWS_OBJECT_KEY_FULLSIZED');

        $images = $s3->getIterator('ListObjects', array('Bucket' => $bucket, 'Prefix' => $obj_key));
        $save_path = public_path('img_s3_rekognition') . '/'; // local path for storing image file

        $title_array = [];
        $images_db = app('db')->connection('mysql2')
            ->select("
                    SELECT id,extension
                    FROM accommodation_photos AS ap
                    WHERE ap.`labels_done` = 0
                    ");
        foreach ($images_db as $imgdb)
        {
            array_push($title_array, $imgdb->id.'.'.$imgdb->extension);
        }

        foreach ($images as $key => $image)
        {
            if($key > 0) { // first key is the name of the bucket

                $assetPath = Storage::disk('s3')->url($image['Key']);
                $ext = explode('.', $assetPath);
                $ext = end($ext);
                $title = explode('/', $assetPath);
                $title = end($title);
                $title_no_ext = explode('.', $title);
                $title_no_ext = reset($title_no_ext);

                $exists = Storage::disk('s3')->exists($image['Key']);
                if ($exists) {
                    $file = Storage::disk('s3')->get($image['Key']);
                    if (!file_exists($save_path)) {
                        mkdir($save_path, 775, true);
                    }
                    $rekognition_img = Image::make($file)->save($save_path . $title);
                }

                if($ext != "gif"){

                    if(in_array($title, $title_array)) {
                        $reko = $save_path . $title;
                        $fp_image = fopen($reko, 'r');
                        $img = fread($fp_image, filesize($reko));
                        fclose($fp_image);

                        $results = $rekognition->DetectLabels(array(
                                'Image' => array(
                                    'Bytes' => $img,
                                ),
                                'Attributes' => array('ALL')
                            )
                        );

                        if ($results["@metadata"]["statusCode"] === 200) {
                            foreach ($results["Labels"] as $k => $result) {
                                DB::table('accommodation_photo_labels')->insert(
                                    [
                                        'confidence' => $result['Confidence'],
                                        'label_text' => $result['Name'],
                                        'photo_id' => $title_no_ext,
                                        'labels_done' => 1,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ]
                                );

                                DB::table('accommodation_photos')
                                    ->where('id', $title_no_ext)
                                    ->update(['labels_done' => 1]);

                                var_dump($k);
                                var_dump('ext: '.$ext);
                                var_dump('count: '.count($title_array));
                                var_dump($result);
                                echo "---------" . "\n";
                            }
                        } else {
                            DB::table('accommodation_photo_labels')->insert(
                                [
                                    'confidence' => null,
                                    'label_text' => null,
                                    'photo_id' => $title_no_ext,
                                    'labels_done' => 2,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                ]
                            );

                            DB::table('accommodation_photos')
                                ->where('id', $title_no_ext)
                                ->update(['labels_done' => 2]);
                        }
                    }
                    else {
                        DB::table('accommodation_photo_labels')->insert(
                            [
                                'confidence' => null,
                                'label_text' => null,
                                'photo_id' => $title_no_ext,
                                'labels_done' => 3,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]
                        );

                        DB::table('accommodation_photos')
                            ->where('id', $title_no_ext)
                            ->update(['labels_done' => 2]);
                    }
                }else {
                    DB::table('accommodation_photo_labels')->insert(
                        [
                            'confidence' => null,
                            'label_text' => null,
                            'photo_id' => $title_no_ext,
                            'labels_done' => 3,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]
                    );

                    DB::table('accommodation_photos')
                        ->where('id', $title_no_ext)
                        ->update(['labels_done' => 2]);
                }
            }
        }
        exec("rm -rf '" . $save_path . "'");
    }
}