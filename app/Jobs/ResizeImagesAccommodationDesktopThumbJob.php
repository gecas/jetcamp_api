<?php

namespace App\Jobs;

use Icyboy\LumenAws\Aws;
use Aws\S3\S3Client;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class ResizeImagesAccommodationDesktopThumbJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set("memory_limit","-1");
        ini_set('max_execution_time', 0);
    }

    public function handle()
    {
        $start_time = time();
        $s3 = Aws::createClient('s3');
        $gen_width = 94;
        // Instantiate the client.
//        $s3 = S3Client::factory(["region" => env('AWS_REGION'), "version" => "2006-03-01"]);

        $bucket = env('AWS_BUCKET');
        $obj_key = env('AWS_OBJECT_KEY_FULLSIZED');
        $acco_desktop_thumb = env('AWS_RESIZE_ACCO_DESKTOP_THUMB');

        // Use the high-level iterators (returns ALL of your objects).
        $objects = $s3->getIterator('ListObjects', array('Bucket' => $bucket, 'Prefix' => $obj_key));
        echo "Keys retrieved!\n";
        foreach ($objects as $key => $object) {
            if ($key > 0) {
                $assetPath = Storage::disk('s3')->url($object['Key']);
                $ext = explode('.', $assetPath);
                $title = explode('/', $assetPath);
                $title = end($title);
                $title_no_ext = explode('.', $title);
                $title_no_ext = reset($title_no_ext);
                $ext = end($ext);
                $exists = Storage::disk('s3')->exists($object['Key']);
                if ($exists) {
                    $file = Storage::disk('s3')->get($object['Key']);
//                    $visibility = Storage::disk('s3')->getMetaData($object['Key']);
                    $save_path = public_path('img_s3_resize_desktop_thumb') . '/';
                    if (!file_exists($save_path)) {
                        mkdir($save_path, 775, true);
                    }
                    $exif = Image::make($file)->save($save_path . $title);
                    $img = $save_path . $title;
                    $img_width= getimagesize($img)[0];
                    $s3_key = $acco_desktop_thumb . '/' . $title;

                    if($img_width >= $gen_width) {
                        // create instance
                        $img_res = Image::make($img);
                        // resize the image to a width of 300 and constrain aspect ratio (auto height)
                        $img_res->resize($gen_width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($save_path . $title);

                        $img_resized = $save_path . $title;

                        var_dump($key);
                        var_dump($title);
                        var_dump($s3_key);
                        var_dump($img_width);
                        echo "________"."\n";

                        if (file_exists($img_resized)) {
                            $s3->putObject(array(
                                'Bucket' => $bucket,
                                'ACL' => 'public-read',
                                'Key' => $s3_key,
                                'SourceFile' => $img_resized
                            ));
                        }
                        $updated = DB::table('accommodation_photos')
                            ->where('id', $title_no_ext)
                            ->update(['resized_acco_thumb' => 1]);
                    }else{

                        if (file_exists($img)) {
                            $s3->putObject(array(
                                'Bucket' => $bucket,
                                'ACL' => 'public-read',
                                'Key' => $s3_key,
                                'SourceFile' => $img
                            ));
                        }

                        var_dump($key);
                        var_dump("Too small");
                        echo "------------" . "\n";
                        $updated = DB::table('accommodation_photos')
                            ->where('id', $title_no_ext)
                            ->update(['acco_desktop_thumb_too_small' => 1, 'resized_acco_thumb' => 1]);
                        // image is too small
                        // update acco_800_too_small
                    }
//
                }
//
            }
        }
        exec("rm -rf '" . $save_path . "'");
        echo $start_time . ' time spent';
        echo "\n";
    }
}