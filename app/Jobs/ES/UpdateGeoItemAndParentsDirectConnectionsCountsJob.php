<?php

namespace App\Jobs\ES;

use App\Jobs\Job;
use App\Models\GeoTree;
use Illuminate\Support\Facades\Queue;

/*
 * Job finds geo item parents and creates for each geo item in chain
 * creates UpdateGeoItemConnectionsCountsJob job
 */

class UpdateGeoItemAndParentsDirectConnectionsCountsJob extends Job
{
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle()
    {
        $item = GeoTree::findOrFail($this->id);
        $item->withParents()->get()->each(function (GeoTree $item) {
            Queue::push(new UpdateGeoItemDirectConnectionsCountsJob($item->id));
        });

    }

}