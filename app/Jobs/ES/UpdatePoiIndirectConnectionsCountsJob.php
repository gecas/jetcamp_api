<?php

namespace App\Jobs\ES;

use App\Models\Poi;
use App\Repositories\EsGeoItemRepository;
use App\Jobs\Job;
use App\Repositories\IndirectActiveAccommodationCountWithinCircleRepository;

/*
 * Job finds poi indirect accommodation connections and updates that info to ES
 */

class UpdatePoiIndirectConnectionsCountsJob extends Job
{
    protected $id;
    protected $radiuses = [10, 25, 50];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle(
        EsGeoItemRepository $esRepository,
        IndirectActiveAccommodationCountWithinCircleRepository $connectionsCountRepository
    ) {
        $item = Poi::findOrFail($this->id);

        $data = [];
        foreach ($this->radiuses as $radius) {
            $data['camping_cnt_' . $radius] = $connectionsCountRepository->get(
                $item->lat,
                $item->lon,
                $radius
            );
        }

        $esRepository->update($item->id . '-4', $data);
    }

}