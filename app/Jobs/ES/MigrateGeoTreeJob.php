<?php

namespace App\Jobs\ES;

use App\Models\GeoTree;
use App\Jobs\Job;
use App\Services\SyncToEs\SyncToEsService;

class MigrateGeoTreeJob extends Job
{
    protected $id;

    /**
     * Create a new job instance.
     * @param integer $id
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     * @param SyncToEsService $syncToEsService Service that sync model to ES
     * @return void
     */
    public function handle(
        SyncToEsService $syncToEsService
    ) {
        $item = GeoTree::findOrFail($this->id);
        $syncToEsService->sync($item);
    }

}