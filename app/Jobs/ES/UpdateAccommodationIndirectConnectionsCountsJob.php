<?php

namespace App\Jobs\ES;

use App\Models\Accommodation;
use App\Models\GeoTree;
use App\Repositories\EsGeoItemRepository;
use App\Jobs\Job;
use App\Repositories\IndirectActiveAccommodationCountWithinCircleRepository;

/*
 * Job finds geo item indirect connections and updates that info to ES
 */

class UpdateAccommodationIndirectConnectionsCountsJob extends Job
{
    protected $id;
    protected $radiuses = [10, 25, 50];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle(
        EsGeoItemRepository $esRepository,
        IndirectActiveAccommodationCountWithinCircleRepository $connectionsCountRepository
    ) {
        $item = Accommodation::findOrFail($this->id);

        $data = [];
        foreach ($this->radiuses as $radius) {
            $data['camping_cnt_' . $radius] = $connectionsCountRepository->get(
                $item->address_gps_lat,
                $item->address_gps_lon,
                $radius,
                [$this->id]
            );
        }

        $esRepository->update($item->id . '-2', $data);
    }

}