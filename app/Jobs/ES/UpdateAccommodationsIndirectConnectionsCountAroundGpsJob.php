<?php

namespace App\Jobs\ES;

use App\Jobs\Job;
use App\Models\Accommodation;
use App\Models\GeoTree;
use Illuminate\Support\Facades\Queue;

/*
 * Find accommodations around GPS coordinates and update their indirect connections
 */

class UpdateAccommodationsIndirectConnectionsCountAroundGpsJob extends Job
{
    protected $lat;
    protected $lon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle()
    {
        $accommodations = Accommodation::whereWithin(
            $this->lat,
            $this->lon,
            50,
            'address_gps_lat',
            'address_gps_lon')
            ->get();

        $accommodations->each(function (Accommodation $accommodation) {
            Queue::push(new UpdateAccommodationIndirectConnectionsCountsJob($accommodation->id));
        });
    }

}