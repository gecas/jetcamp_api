<?php

namespace App\Jobs\ES;

use App\Models\GeoTree;
use App\Repositories\EsGeoItemRepository;
use App\Repositories\IndirectActiveAccommodationCountWithinCircleRepository;
use App\Jobs\Job;

/*
 * Job finds geo item indirect connections and updates that info to ES
 */

class UpdateGeoItemIndirectConnectionsCountsJob extends Job
{
    protected $id;
    protected $radiuses = [10, 25, 50];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle(
        EsGeoItemRepository $esRepository,
        IndirectActiveAccommodationCountWithinCircleRepository $connectionsCountRepository
    ) {
        $item = GeoTree::findOrFail($this->id);

        $data = [];
        foreach ($this->radiuses as $radius) {
            $data['camping_cnt_' . $radius] = $connectionsCountRepository->get(
                $item->gpsLat,
                $item->gpsLon,
                $radius);
        }

        $esRepository->update($item->id . '-1', $data);
    }

}