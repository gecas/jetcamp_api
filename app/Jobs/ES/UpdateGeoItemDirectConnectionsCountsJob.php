<?php

namespace App\Jobs\ES;

use App\Models\GeoTree;
use App\Repositories\EsGeoItemRepository;
use App\Repositories\GeoItemConnectionCountRepository;
use App\Jobs\Job;

/*
 * Job finds geo item connections and updates that info to ES
 */

class UpdateGeoItemDirectConnectionsCountsJob extends Job
{
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle(
        EsGeoItemRepository $esRepository,
        GeoItemConnectionCountRepository $connectionsCountRepository
    ) {
        $item = GeoTree::findOrFail($this->id);

        $esRepository->update($item->id . '-1',
            [
                'camping_cnt' => $connectionsCountRepository->findActiveDirectCampsitesCount($item)
                //'camping_cnt_10': 29,
                //'camping_cnt_25': 83,
                //'camping_cnt_50': 121,
            ]
        );
    }

}