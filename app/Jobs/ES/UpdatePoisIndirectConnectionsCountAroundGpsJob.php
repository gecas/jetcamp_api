<?php

namespace App\Jobs\ES;

use App\Jobs\Job;
use App\Models\Poi;
use Illuminate\Support\Facades\Queue;

/*
 * Find accommodations around GPS coordinates and update their indirect connections
 */

class UpdatePoisIndirectConnectionsCountAroundGpsJob extends Job
{
    protected $lat;
    protected $lon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    /**
     * Execute the job.
     * @param $id
     * @return void
     */
    public function handle()
    {
        $pois = Poi::whereWithin(
            $this->lat,
            $this->lon,
            50,
            'lat',
            'lon')
            ->get();

        $pois->each(function (Poi $poi) {
            Queue::push(new UpdatePoiIndirectConnectionsCountsJob($poi->id));
        });
    }

}