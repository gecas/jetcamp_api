<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointOfInterestTranslation extends Model
{
    use WithoutEventsTrait;

    protected $table = 'poi_translations';
    protected $fillable = ['translation', 'key', 'language_id', 'pois_id', 'user_id'];

}