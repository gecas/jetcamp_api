<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommercialRegionTranslation extends Model
{
    use WithoutEventsTrait;

    protected $table = 'commercial_region_translations';
    protected $fillable = ['translation', 'key', 'language_id'];

    public function commercial_region()
    {
        return $this->belongsTo(CommercialRegion::class, 'key', 'key');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }
}