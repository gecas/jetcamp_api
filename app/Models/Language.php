<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = [
    ];

    public function scopeFrontActive($query, $is = true)
    {
        return $query->where('active_frontend', (int)$is);
    }
}