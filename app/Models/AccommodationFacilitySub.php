<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AccommodationFacilitySub extends Pivot
{
    protected $table = 'accommodation_facility_subs';

    public $timestamps = false;

    public function facility_sub()
    {
        return $this->belongsTo(FacilitySub::class, 'facility_sub_id');
    }

    public function accommodation()
    {
        return $this->belongsTo(Accommodation::class, 'accommodation_id');
    }
}