<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeoTreeTranslationInAllLanguages extends Model
{
    protected $casts = [
        'id' => 'string'
    ];
    protected $table = 'geo_tree_translations_in_all_languages';
}