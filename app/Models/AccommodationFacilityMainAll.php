<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccommodationFacilityMainAll extends Model
{
    protected $table = 'accommodation_facility_mains_all';

    protected $casts = [
        'id' => 'string'
    ];

    public function facility_main()
    {
        return $this->belongsTo(FacilityMain::class, 'facility_main_id');
    }
}