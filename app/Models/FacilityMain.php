<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilityMain extends Model
{
    protected $table = 'facility_mains';

    protected $fillable = [
    ];

    /*
     * Relation to get facility theme
     */
    public function facility_theme()
    {
        return $this->belongsTo(FacilityTheme::class,
            'facility_theme_id'
        );
    }

    public function translation()
    {
        return $this->belongsTo(Translation::class, 'description', 'key')
            ->where('group', 'jetcamp')
            ->where('locale', 'en')
            ->where('project', 'web');
    }
}