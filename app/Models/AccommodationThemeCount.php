<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccommodationThemeCount extends Model
{
    protected $table = 'accommodation_theme_count';

    protected $casts = [
        'id' => 'string'
    ];

    public function translation()
    {
        return $this->belongsTo(Translation::class, 'theme_key', 'key')
            ->where('group', 'jetcamp')
            ->where('locale', 'en')
            ->where('project', 'web');
    }
}