<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    use WithoutEventsTrait;

    protected $table = 'accommodations';

    protected $fillable = [
        'name',
        'address_website',
        'address_street',
        'address_housenumber',
        'address_housenumber_addition',
        'address_postcode',
        'address_gps_lat',
        'address_gps_lon',
        'address_phone_1',
        'address_phone_2',
        'address_email_public',
        'social_facebook',
        'social_twitter',
        'social_foursquare',
        'customer_rating_average',
        'customer_rating_count',
        'official_rating',
        'popularity',
        'size_nr_total',
        'size_nr_recreational_pitches',
        'size_nr_year_pitches',
        'size_nr_mobilehomerentals',
        'size_nr_houseorappartments',
        'price_highseason',
        'price_adult',
        'price_animal',
        'price_camper',
        'price_caravan',
        'price_tent',
        'general_nudist_acco',
        'checkin_pre',
        'checkout_pre',
        'distance_to_highway',
        'business_address_street',
        'business_address_housenumber',
        'business_address_housenumber_addition',
        'business_address_postcode',
        'business_address_city',
        'business_address_country',
        'business_address_phone_1',
        'business_address_phone_2',
        'business_address_email',
        'size_area_squaremeter',
        'migrated',
        'geo_trees_id',
        'status'
    ];

    protected $casts = [
        'address_gps_lat' => 'float',
        'address_gps_lon' => 'float'
    ];

    /**
     * Return geo tree item that accommodation belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function geo_tree()
    {
        return $this->belongsTo(GeoTree::class, 'geo_trees_id');
    }

    /**
     * Get accommodation subs list
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facility_subs()
    {
        return $this->belongsToMany(FacilitySub::class,
            'accommodation_facility_subs',
            'accommodation_id',
            'facility_sub_id'
        )->using(AccommodationFacilitySub::class);
    }

    /**
     * Get accommodation mains list
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facility_mains()
    {
        return $this->belongsToMany(FacilityMain::class,
            'accommodation_facility_mains',
            'accommodation_id',
            'facility_main_id'
        );
    }

    /**
     * Scope a query to only include active accommodations.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEnabled($query)
    {
        return $query->where('status', 1);
    }
}