<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommercialRegionTranslationInAllLanguages extends Model
{
    protected $casts = [
        'id' => 'string'
    ];
    protected $table = 'commercial_region_translations_in_all_languages';
}