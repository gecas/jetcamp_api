<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class GeoTree extends Model
{
    protected $table = 'geo_trees';

    protected $fillable = [
        'name',
        'parent_short',
        'idGeonames',
        'geonamesFcl',
        'geonamesFcode',
        'continentCode',
        'countryCode',
        'population',
        'elevation',
        'timezoneGmtOffset',
        'timezoneDstOffset',
        'elevation',
        'timezoneGmtOffset',
        'timezoneDstOffset',
        'wikipediaLink',
        'popularity',
        'show_on_acco_whattodo',
        'gpsLat',
        'gpsLon',
        'gpsBboxEast',
        'gpsBboxSouth',
        'gpsBboxNorth',
        'gpsBboxWest',
        'gpsPoint',
        'gpsPolygon',
        'key_short'
    ];

    protected $casts = [
        'gpsLat' => 'float',
        'gpsLon' => 'float'
    ];

    /**
     * Get geo item with parents query
     * @param bool $short Indicate whether to take short key parents or all
     * @return Builder
     */
    public function withParents($short = false)
    {
        $segments = [];
        $col = $short ? 'key_short' : 'key';
        $key = $this->$col;
        $parentKeys = collect(explode('_', $key))
            ->transform(function ($segment) use (&$segments) {
                $segments[] = $segment;
                return implode('_', $segments);
            })
            ->slice(1);
        return GeoTree::whereIn($col, $parentKeys);
    }


    /**
     * Get geo item translations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(GeoTreeTranslation::class, 'key', 'key');
    }

    /**
     * Get geo item translations in all languages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations_in_all_languages()
    {
        return $this->hasMany(GeoTreeTranslationInAllLanguages::class, 'geo_tree_id', 'id')
            ->orderBy('active_frontend', 'DESC')
            ->orderBy('language_abbr');
    }


    /**
     * Get depth
     *
     * @return string
     */
    public function getDepthAttribute()
    {
        return count(explode('_', $this->key)) - 1;
    }
}