<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommercialRegion extends Model
{
    use WithoutEventsTrait;

    protected $table = 'commercial_regions';

    protected $fillable = [
        'key',
        'parent_id',
        'active',
        'popularity',
        'category_id',
        'created_at',
        'updated_at',
        'migrated'
    ];

    public function translations_in_all_languages()
    {
        return $this->hasMany(CommercialRegionTranslationInAllLanguages::class, 'commercial_region_id', 'id')
            ->orderBy('active_frontend', 'DESC')
            ->orderBy('language_abbr');
    }

    public function geo_tree()
    {
        return $this->belongsTo(GeoTree::class, 'parent_id');
    }

}