<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointOfInterest extends Model
{
    use WithoutEventsTrait;

    protected $table = 'pois';

    protected $fillable = [

    ];

    public function translations()
    {
        return $this->hasMany(PointOfInterestTranslation::class, 'key', 'key');
    }

    public function translations_in_all_languages()
    {
        return $this->hasMany(PointOfInterestTranslationInAllLanguages::class, 'poi_id', 'id')
            ->orderBy('active_frontend', 'DESC')
            ->orderBy('language_abbr');
    }

}