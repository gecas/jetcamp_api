<?php

namespace App\Models;

use Basemkhirat\Elasticsearch\Model;

class EsGeoItem extends Model
{
    protected $type = "geoitem";

    /**
     * Scope a query to only include active posts.
     *
     * @param \Basemkhirat\Elasticsearch\Query $query
     * @param integer $id
     * @return \Basemkhirat\Elasticsearch\Query
     */
    public function scopeId($query, $id)
    {
        return $query->where('id.keyword', $id);
    }
}