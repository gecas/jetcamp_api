<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccommodationFacilitySubAll extends Model
{
    protected $table = 'accommodation_facility_subs_all';

    protected $casts = [
        'id' => 'string'
    ];

    public function facility_sub()
    {
        return $this->belongsTo(FacilitySub::class, 'facility_sub_id');
    }
}