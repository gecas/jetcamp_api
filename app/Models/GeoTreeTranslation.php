<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeoTreeTranslation extends Model
{
    protected $table = 'geo_translations';
    protected $fillable = ['translation', 'key', 'language_id', 'user_id', 'language_abbrevation', 'useEn', 'idGeonames', 'translation_simple'];

    /**
     * Scope a query to only include country geo items
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLanguage($query, $language)
    {
        return $query->where('language_abbrevation', $language);
    }
}