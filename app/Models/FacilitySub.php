<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilitySub extends Model
{
    protected $table = 'facility_subs';

    protected $fillable = [
    ];

    public function translation()
    {
        return $this->belongsTo(Translation::class, 'description', 'key')
            ->where('group', 'jetcamp')
            ->where('locale', 'en')
            ->where('project', 'web');
    }
}