<?php

namespace App\Models;

trait WithoutEventsTrait
{
    /**
     * Call a callback without firing events.
     *
     * @param  \Closure|string $callback
     * @return void
     */
    public function withoutEvents($callback)
    {
        $dispatcher = $this->getEventDispatcher();
        $this->unsetEventDispatcher();
        try {
            $callback($this);
        } finally {
            $this->setEventDispatcher($dispatcher);
        }
    }
}