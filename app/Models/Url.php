<?php

namespace App\Models;

use App\ElasticSearchAPI\GeoTreeItem;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    const PAGE_TYPE_GEOITEM_SEARCH = 1;
    const PAGE_TYPE_CAMPING_SEARCH = 2;
    const PAGE_TYPE_COMMERCIAL_SEARCH = 3;
    const PAGE_TYPE_POI_SEARCH = 4;
    const PAGE_TYPE_CAMPING_PAGE = 5;
    const PAGE_TYPE_DESTINATION_PAGE = 6;
    const PAGE_TYPE_POI_PAGE = 7;
    const PAGE_TYPE_COMMERCIAL_PAGE = 8;

    protected $fillable = [
        'lang',
        'url',
        'subject_id',
        'page_type'
    ];

    public $timestamps = false;

    /**
     * Scope a query to only include accommodation destination url for.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $accommodationId
     * @param string $abbreviation
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAccommodationUrlSingle($query, $accommodationId, $abbreviation)
    {
        return $query->where('subject_id', $accommodationId . '-' . GeoTreeItem::TYPE_CAMPING)
            ->where('page_type', Url::PAGE_TYPE_CAMPING_PAGE)
            ->where('lang', $abbreviation);
    }

    /**
     * Scope a query to only include accommodation search url.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $accommodationId
     * @param string $abbreviation
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAccommodationUrlSearch($query, $accommodationId, $abbreviation)
    {
        return $query->where('subject_id', $accommodationId . '-' . GeoTreeItem::TYPE_CAMPING)
            ->where('page_type', Url::PAGE_TYPE_CAMPING_SEARCH)
            ->where('lang', $abbreviation);
    }

    /**
     * Scope a query to only include geo item destination url.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $geoTreeId
     * @param string $abbreviation
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGeoItemUrlSingle($query, $geoTreeId, $abbreviation)
    {
        return $query->where('subject_id', $geoTreeId . '-' . GeoTreeItem::TYPE_GEOITEM)
            ->where('page_type', Url::PAGE_TYPE_DESTINATION_PAGE)
            ->where('lang', $abbreviation);
    }

    /**
     * Scope a query to only include geo item search url.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $geoTreeId
     * @param string $abbreviation
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGeoItemUrlSearch($query, $geoTreeId, $abbreviation)
    {
        return $query->where('subject_id', $geoTreeId . '-' . GeoTreeItem::TYPE_GEOITEM)
            ->where('page_type', Url::PAGE_TYPE_GEOITEM_SEARCH)
            ->where('lang', $abbreviation);
    }

    /**
     * Get full url
     *
     * @return string
     */
    public function getFullUrlAttribute()
    {
        return $this->lang . '/' . $this->url;
    }

    /**
     * Get full url
     *
     * @return string
     */
    public function getUrlSuffixLessAttribute()
    {
        $suffixPos = strrpos($this->url, '--');
        if (!$suffixPos) {
            return $this->url;
        } else {
            return substr($this->url, 0, $suffixPos) . '/';
        }
    }

}