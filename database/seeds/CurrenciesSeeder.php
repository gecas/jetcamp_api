<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            [
                'abbreviation' => 'EUR',
                'description' => 'backoffice_database_currency_eur',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'abbreviation' => 'USD',
                'description' => 'backoffice_database_currency_usd',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'abbreviation' => 'GBP',
                'description' => 'backoffice_database_currency_gbp',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'abbreviation' => 'CHF',
                'description' => 'backoffice_database_currency_chf',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
