<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FacilitySettingDistanceTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facility_setting_distancetypes')->insert([
            [
                'description' => 'backoffice_database_distancetype_meter',
                'conversion_meter' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'description' => 'backoffice_database_distancetype_kilometer',
                'conversion_meter' => 1000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'description' => 'backoffice_database_distancetype_mile',
                'conversion_meter' => 0.002,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'description' => 'backoffice_database_distancetype_feet',
                'conversion_meter' => 0.003,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'description' => 'backoffice_database_distancetype_yard',
                'conversion_meter' => 0.004,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
