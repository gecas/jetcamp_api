<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoiTranslationsInAllLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW poi_translations_in_all_languages AS
        SELECT
            CONCAT(pois.id, '-', languages.id) AS id, 
            pois.id as poi_id,
            pois.key as poi_key,
            languages.id as language_id,
            languages.abbrevation as language_abbr,
            pt.translation as translation,
            languages.active_frontend as active_frontend
            FROM languages
            CROSS JOIN pois
            LEFT JOIN poi_translations as pt 
            ON pt.language_id = languages.id AND pt.key = pois.key
         ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW poi_translations_in_all_languages");
    }
}
