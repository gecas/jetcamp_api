<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetGeoTreeGpsPointAndGpsPolygonNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `geo_trees` CHANGE COLUMN `gpsPolygon` `gpsPolygon` POLYGON NULL, CHANGE COLUMN `gpsPoint` `gpsPoint` POINT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
