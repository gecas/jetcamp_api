<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationThemesCountView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW accommodation_theme_count AS
            SELECT CONCAT(`ft`.`id`,'-',`a`.`id`) AS `id`,`ft`.`id` AS `theme_id`, ft.description AS `theme_key` ,`a`.`id` AS `accommodation_id`, COUNT(`afm`.`facility_main_id`) AS `c`
            FROM (((`facility_themes` `ft`
            JOIN `accommodations` `a`)
            LEFT JOIN `facility_mains` `fm` ON((`ft`.`id` = `fm`.`facility_theme_id`)))
            LEFT JOIN `accommodation_facility_mains` `afm` ON(((`a`.`id` = `afm`.`accommodation_id`) AND (`afm`.`facility_main_id` = `fm`.`id`))))
            GROUP BY `a`.`id`,`ft`.`id`
         ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW accommodation_theme_count");
    }
}
