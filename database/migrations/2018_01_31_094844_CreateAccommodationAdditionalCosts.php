<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationAdditionalCosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodation_additional_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accommodation_id')->nullable();
            $table->integer('facility_main_id')->nullable();
            $table->integer('facility_sub_id')->nullable();
            $table->decimal('costs', 19, 2);
            $table->integer('currency_id');
            $table->integer('facility_setting_costtype_id');
            $table->unique(['accommodation_id', 'facility_main_id'], 'uniq_acc_id_main_id');
            $table->unique(['accommodation_id', 'facility_sub_id'], 'uniq_acc_id_sub_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accommodation_additional_costs');
    }
}
