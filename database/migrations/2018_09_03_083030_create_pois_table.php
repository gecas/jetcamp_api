<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pois', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en', 255)->index();
            $table->string('street', 255)->nullable();
            $table->string('streetnumber', 6)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('postalcode', 10)->nullable();
            $table->string('country', 100);
            $table->string('phone', 25)->nullable();
            $table->string('rating_count', 25)->nullable();
            $table->decimal('rating_value', 5, 2)->nullable();
            $table->string('domain', 100)->nullable();
            $table->string('lat', 20)->nullable();
            $table->string('lon', 20)->nullable();
            $table->tinyInteger('active')->nullable()->index();
            $table->integer('open_monday')->nullable();
            $table->integer('open_tuesday')->nullable();
            $table->integer('open_wednesday')->nullable();
            $table->integer('open_thursday')->nullable();
            $table->integer('open_friday')->nullable();
            $table->integer('open_saturday')->nullable();
            $table->integer('open_sunday')->nullable();
            $table->integer('close_monday')->nullable();
            $table->integer('close_tuesday')->nullable();
            $table->integer('close_wednesday')->nullable();
            $table->integer('close_thursday')->nullable();
            $table->integer('close_friday')->nullable();
            $table->integer('close_saturday')->nullable();
            $table->integer('close_sunday')->nullable();
            $table->integer('open_always')->nullable();
            $table->integer('permantly_closed')->nullable();
            $table->string('ta_id')->index();
            $table->string('google_id', 255)->index();
            $table->integer('poi_duaration_id');
            $table->string('ta_link', 255);
            $table->integer('pricelevel');
            $table->string('google_url', 255);
            $table->timestamps();
            $table->string('key', 255)->index();
            $table->integer('popularity')->index();
            $table->integer('geotree_id')->index();
            $table->tinyInteger('migrated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pois');
    }
}
