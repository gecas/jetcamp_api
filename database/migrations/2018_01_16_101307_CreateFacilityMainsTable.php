<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_mains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('facility_theme_id');
            $table->tinyInteger('show_add_costs');
            $table->tinyInteger('show_add_distances');
            $table->tinyInteger('show_add_openingdates');
            $table->tinyInteger('show_add_openinghours');
            $table->tinyInteger('show_add_size_in_cm');
            $table->tinyInteger('show_add_size_in_m_lwh');
            $table->tinyInteger('show_add_numbers');
            $table->tinyInteger('show_add_language');
            $table->tinyInteger('show_add_age');
            $table->tinyInteger('show_add_weekdays');
            $table->tinyInteger('show_add_frequency');
            $table->string('description');
            $table->unique('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_mains');
    }
}
