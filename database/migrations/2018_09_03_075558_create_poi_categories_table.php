<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoiCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 255);
            $table->integer('priority');
            $table->tinyInteger('show_in_autocomplete');
            $table->integer('show_in_autocomplete_min_rating')->nullable();
            $table->integer('show_in_autocomplete_min_reviews')->nullable();
            $table->integer('search_use_on_acco_zoomlevel_or_higher')->nullable();
            $table->integer('show_on_acco_search_map')->index();
            $table->integer('show_on_acco_search_map_min_rating')->nullable()->index();
            $table->integer('show_on_acco_search_map_min_reviews')->nullable()->index();
            $table->integer('show_on_acco_detail_whattodo')->nullable();
            $table->integer('show_on_acco_detail_whattodo_min_rating')->nullable();
            $table->integer('show_on_acco_detail_whattodo_min_reviews')->nullable();
            $table->integer('show_on_acco_detail_map_on_zoomlevel_or_higher');
            $table->integer('show_on_acco_detail_map_cat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poi_categories');
    }
}
