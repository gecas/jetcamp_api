<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsToAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accommodations', function (Blueprint $table) {
            $table->integer('distance_to_highway')->nullable();
            $table->string('business_address_street')->nullable();
            $table->string('business_address_housenumber')->nullable();
            $table->string('business_address_housenumber_addition')->nullable();
            $table->string('business_address_postcode')->nullable();
            $table->string('business_address_city')->nullable();
            $table->string('business_address_country')->nullable();
            $table->string('business_address_phone_1')->nullable();
            $table->string('business_address_phone_2')->nullable();
            $table->string('business_address_email')->nullable();
            $table->integer('size_area_squaremeter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accommodations', function (Blueprint $table) {
            $table->dropColumn('distance_to_highway');
            $table->dropColumn('business_address_street');
            $table->dropColumn('business_address_housenumber');
            $table->dropColumn('business_address_housenumber_addition');
            $table->dropColumn('business_address_postcode');
            $table->dropColumn('business_address_city');
            $table->dropColumn('business_address_country');
            $table->dropColumn('business_address_phone_1');
            $table->dropColumn('business_address_phone_2');
            $table->dropColumn('business_address_email');
            $table->dropColumn('size_area_squaremeter');
        });
    }
}
