<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationFacilitySubsAllView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW accommodation_facility_subs_all AS
        SELECT CONCAT(a.id, '-', fs.id) AS id 
        ,a.id AS accommodation_id
        , fs.id AS facility_sub_id
        , fs.facility_main_id
        , IF(afs.accommodation_id IS NULL, 0, 1) AS checked
        FROM facility_subs fs
        CROSS JOIN accommodations a
        LEFT JOIN accommodation_facility_subs afs ON afs.accommodation_id = a.id AND afs.facility_sub_id = fs.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW accommodation_facility_subs_all");
    }
}
