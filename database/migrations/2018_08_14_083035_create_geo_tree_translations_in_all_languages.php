<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoTreeTranslationsInAllLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW geo_tree_translations_in_all_languages AS
        SELECT
            CONCAT(geo_trees.id, '-', languages.id) AS id, 
            geo_trees.id as geo_tree_id,
            geo_trees.key as geo_tree_key,
            languages.id as language_id,
            languages.abbrevation as language_abbr,
            gt.translation as translation,
            languages.active_frontend as active_frontend
            FROM languages
            CROSS JOIN geo_trees
            LEFT JOIN geo_translations as gt 
            ON gt.language_id = languages.id AND gt.key = geo_trees.key
         ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW geo_tree_translations_in_all_languages");
    }
}
