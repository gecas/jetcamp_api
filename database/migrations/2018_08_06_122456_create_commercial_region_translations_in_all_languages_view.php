<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercialRegionTranslationsInAllLanguagesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW commercial_region_translations_in_all_languages AS
        SELECT
            CONCAT(commercial_regions.id, '-', languages.id) AS id, 
            commercial_regions.id as commercial_region_id,
            commercial_regions.key as commercial_region_key,
            languages.id as language_id,
            languages.abbrevation as language_abbr,
            crt.translation as translation,
            languages.active_frontend as active_frontend
            FROM languages
            CROSS JOIN commercial_regions
            LEFT JOIN commercial_region_translations as crt 
            ON crt.language_id = languages.id AND crt.key = commercial_regions.key
         ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW commercial_region_translations");
    }
}
