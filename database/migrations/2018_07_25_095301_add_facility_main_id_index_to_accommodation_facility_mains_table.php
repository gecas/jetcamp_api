<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacilityMainIdIndexToAccommodationFacilityMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accommodation_facility_mains', function (Blueprint $table) {
            $table->index('facility_main_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accommodation_facility_mains', function (Blueprint $table) {
            $table->dropIndex('facility_main_id');
        });
    }
}
