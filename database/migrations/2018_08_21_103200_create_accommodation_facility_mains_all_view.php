<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationFacilityMainsAllView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW accommodation_facility_mains_all AS
             SELECT
             CONCAT(a.id, '-', fm.id) AS id 
            , a.id AS accommodation_id
            , fm.id AS facility_main_id
            , fm.facility_theme_id
            , IF(afm.accommodation_id IS NULL, 0, 1) AS checked
            FROM facility_mains fm
            CROSS JOIN accommodations a
            LEFT JOIN accommodation_facility_mains afm ON afm.accommodation_id = a.id AND afm.facility_main_id = fm.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW accommodation_facility_mains_all");
    }
}
