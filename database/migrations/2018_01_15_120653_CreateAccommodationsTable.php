<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('accommodation_scraped_id');
            $table->unique('accommodation_scraped_id');
            $table->string('name');
            $table->string('address_phone')->nullable();
            $table->string('address_website')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_housenumber')->nullable();
            $table->string('address_housenumber_addition')->nullable();
            $table->string('address_postcode')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_country')->nullable();
            $table->decimal('address_gps_lat', 10, 8)->nullable();
            $table->decimal('address_gps_lon', 10, 8)->nullable();
            $table->string('address_phone_1')->nullable();
            $table->string('address_phone_2')->nullable();
            $table->string('address_email_public')->nullable();
            $table->string('social_facebook')->nullable();
            $table->string('social_twitter')->nullable();
            $table->string('social_foursquare')->nullable();
            $table->integer('customer_rating_count')->nullable();
            $table->decimal('customer_rating_average')->nullable();
            $table->integer('official_rating')->nullable();
            $table->integer('popularity')->nullable();
            $table->integer('size_nr_total')->nullable();
            $table->integer('size_nr_recreational_pitches')->nullable();
            $table->integer('size_nr_year_pitches')->nullable();
            $table->integer('size_nr_mobilehomerentals')->nullable();
            $table->integer('size_nr_houseorappartments')->nullable();
            $table->integer('geo_trees_id');
            $table->string('price_highseason')->nullable();
            $table->decimal('price_adult')->nullable();
            $table->decimal('price_animal')->nullable();
            $table->decimal('price_camper')->nullable();
            $table->decimal('price_caravan')->nullable();
            $table->decimal('price_tent')->nullable();
            $table->tinyInteger('general_nudist_acco')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE accommodations ADD address_gps_point POINT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accommodations');
    }
}
