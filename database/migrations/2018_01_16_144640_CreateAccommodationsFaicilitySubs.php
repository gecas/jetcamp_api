<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationsFaicilitySubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodation_facility_subs', function(Blueprint $table)
        {
            $table->integer('accommodation_id')->unsigned()->nullable();
            /*$table->foreign('accommodation_id')->references('id')
                ->on('accommodations')->onDelete('cascade');*/

            $table->integer('facility_sub_id')->unsigned()->nullable();
            /*$table->foreign('facility_main_id')->references('id')
                ->on('facility_mains')->onDelete('cascade');*/
            $table->unique(['accommodation_id', 'facility_sub_id'], 'unique_acc_id_fac_subs_id');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accommodation_facility_subs');
    }
}
