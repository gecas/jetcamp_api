<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Url::class, function (Faker\Generator $faker) {
    return [
        'lang' => 'en',
        'url' => 'some/link/',
        'subject_id' => '1-1',
        'page_type' => 1,
    ];
});
