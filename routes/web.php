<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->version();
});

$router->get('/hello', function () use ($router) {
    return "JetCampApi says hello!";
});

$router->get('/test', ['as' => 'test', 'uses' => 'SearchController@keyToUrl']);

$router->group(['prefix' => 'api/v1'], function ($router) {
//    $router->get('translations', ['middleware' => 'auth', 'uses' => 'TranslationController@index']);
//    $router->post('translations', ['middleware' => 'auth', 'uses' => 'TranslationController@store']);
    $router->get('translations', ['uses' => 'TranslationController@index']);
    $router->post('translations', ['uses' => 'TranslationController@store']);
    $router->get('alternate-pages', ['uses' => 'AlternatePageController@index']);

    $router->get('accommodations', ['uses' => 'AccommodationController@index']);
    $router->post('accommodations', ['uses' => 'AccommodationController@store']);
    $router->get('accommodations/{accommodationId}', ['uses' => 'AccommodationController@show']);
    $router->patch('accommodations/{accommodationId}', ['uses' => 'AccommodationController@update']);
    $router->get('accommodations/{accommodationId}/facilities', ['uses' => 'AccommodationFacilitiesController@show']);

    $router->get('accommodation_theme_count', ['uses' => 'AccommodationThemeCountController@index']);
    $router->get('accommodation_facility_mains_all', ['uses' => 'AccommodationFacilityMainAllController@index']);
    $router->get('accommodation_facility_subs_all', ['uses' => 'AccommodationFacilitySubAllController@index']);

    $router->post('accommodations/{accommodationId}/facility_mains/{facilityMainId}',
        ['uses' => 'AccommodationFacilityMainController@store']);
    $router->delete('accommodations/{accommodationId}/facility_mains/{facilityMainId}',
        ['uses' => 'AccommodationFacilityMainController@destroy']);
    $router->post('accommodations/{accommodationId}/facility_subs/{facilitySubId}',
        ['uses' => 'AccommodationFacilitySubController@store']);
    $router->delete('accommodations/{accommodationId}/facility_subs/{facilitySubId}',
        ['uses' => 'AccommodationFacilitySubController@destroy']);

    $router->get('commercial_regions', ['uses' => 'CommercialRegionController@index']);
    $router->get('commercial_regions/{commercialRegionId}', ['uses' => 'CommercialRegionController@show']);
    $router->patch('commercial_regions/{commercialRegionId}', ['uses' => 'CommercialRegionController@update']);

    $router->get('geo_trees', ['uses' => 'GeoTreeController@index']);
    $router->get('geo_trees/{geoTreeId}', ['uses' => 'GeoTreeController@show']);
    $router->patch('geo_trees/{geoTreeId}', ['uses' => 'GeoTreeController@update']);
    $router->get('users/{userId}', 'UserController@show');

    $router->get('pois', ['uses' => 'PointOfInterestController@index']);
    $router->get('pois/{poisId}', ['uses' => 'PointOfInterestController@show']);

    $router->get('url-redirects/', 'UrlRedirectController@index');

    $router->post('search', [
        'as' => 'search',
        'uses' => 'SearchController@searchAutocomplete'
    ]);

    $router->get('facility_themes', ['uses' => 'FacilityThemeController@index']);

    /*$router->post('search-by-key',[
        'as' => 'search-by-key',
        'uses' => 'SearchController@searchByKey'
    ]);*/

    $router->get('test-search', [
        'as' => 'test-search',
        'uses' => 'SearchController@test'
    ]);

    $router->post('users', 'RegisterController@register');
    $router->post('login', 'LoginController@login');

//    $router->post('search-faceted', [
//        'as' => 'search-faceted',
//        'uses' => 'SearchController@searchFaceted'
//    ]);

//    $router->post('filter-search-faceted', [
//        'as' => 'filter-search-faceted',
//        'uses' => 'SearchController@filterSearchFaceted'
//    ]);

    $router->post('update-search-faceted', [
        'as' => 'update-search-faceted',
        'uses' => 'SearchController@updateSearchFaceted'
    ]);

    $router->post('pois-search-faceted', [
        'as' => 'pois-search-faceted',
        'uses' => 'SearchController@poisUpdateSearchFaceted'
    ]);

    $router->post('search-redirect-url', [
        'as' => 'search-redirect-url',
        'uses' => 'SearchController@searchRedirectUrl'
    ]);

    $router->post('search-by-url', [
        'as' => 'search-by-url',
        'uses' => 'SearchController@searchByUrl'
    ]);

    $router->post('url-items-size', [
        'as' => 'url-items-size',
        'uses' => 'SearchController@urlItemsSize'
    ]);

    $router->post('retrieve-regions', [
        'as' => 'retrieve-regions',
        'uses' => 'SearchController@retrieveCommercialRegions'
    ]);

    $router->post('camping-cnt', [
        'as' => 'camping-cnt',
        'uses' => 'SearchController@campingCnt'
    ]);

    $router->post('campings-countries', [
        'as' => 'campings-countries',
        'uses' => 'SearchController@CampingsCountries'
    ]);

    $router->post('country-url', [
        'as' => 'country-url',
        'uses' => 'SearchController@CountryUrl'
    ]);

    $router->post('item-nice-url', [
        'as' => 'item-nice-url',
        'uses' => 'SearchController@ItemNiceUrl'
    ]);

    $router->post('poi-nice-url', [
        'as' => 'poi-nice-url',
        'uses' => 'SearchController@PoiNiceUrl'
    ]);

    $router->post('main-items-attributes', [
        'as' => 'main-items-attributes',
        'uses' => 'SearchController@getMainItemsAttributes'
    ]);
    $router->post('get-main-item-images', [
        'as' => 'get-main-item-images',
        'uses' => 'SearchController@getMainItemImages'
    ]);
    $router->post('get-items-images', [
        'as' => 'get-items-images',
        'uses' => 'SearchController@getItemsImages'
    ]);
    $router->patch('commercial_regions/{commercial_region_id}/translations/{language_id}',
        ['uses' => 'CommercialRegionTranslationController@update']);
    $router->patch('geo_trees/{geo_tree_id}/translations/{language_id}',
        ['middleware' => 'auth', 'uses' => 'GeoTreeTranslationController@update']);
    $router->patch('pois/{poi_id}/translations/{language_id}',
        ['middleware' => 'auth', 'uses' => 'PointOfInterestTranslationController@update']);
});

$router->group(['prefix' => 'test'], function ($router) {
    $router->get('bugsnag', function () {
        \Bugsnag\BugsnagLaravel\Facades\Bugsnag::notifyException(new \RuntimeException("Test error"));
    });
});


/*$router->group(['prefix' => 'synch'], function($router) {

    $router->get('/camping',[
        'as' => 'camping_migration',
        'uses' => 'Synchronization\MigrationController@migrateCampings'
    ]);

    $router->get('/geo',[
        'as' => 'geo_migration',
        'uses' => 'Synchronization\MigrationController@migrateGeoTree'
    ]);

    $router->get('/calc-campings-cnt',[
        'as' => 'calc_campings_cnt',
        'uses' => 'Synchronization\MigrationController@calculateCampingCounts'
    ]);

    $router->get('/urls',[
        'as' => 'urls',
        'uses' => 'Synchronization\MigrationController@generateUrlsTable'
    ]);

    $router->get('/urls-cmp',[
        'as' => 'urls',
        'uses' => 'Synchronization\MigrationController@generateUrlsTableForCampings'
    ]);
});*/