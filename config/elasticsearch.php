<?php

return [

    'host' => env('ES_HOST'),
    'port' => env('ES_PORT'),

    /*
     * For local development live ES is used. This is for disabling migration to ES
     * This is not in client, but rather used wherever check is done
     * I.e Listeners/RunCampingsMigrationListener.php
     */
    'enable_migration' => env('ES_ENABLE_MIGRATION')
];
