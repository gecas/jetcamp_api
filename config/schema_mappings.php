<?php

return [
    \App\Models\Translation::class => \App\Schemas\TranslationSchema::class,
    \App\Models\GeoTreeTranslation::class => \App\Schemas\GeoTreeTranslationSchema::class,
    \App\Models\Url::class => \App\Schemas\UrlSchema::class,
    \App\Models\Accommodation::class => \App\Schemas\AccommodationSchema::class,
    \App\Models\CommercialRegion::class => \App\Schemas\CommercialRegionSchema::class,
    \App\Models\CommercialRegionTranslationInAllLanguages::class => \App\Schemas\CommercialRegionTranslationInAllLanguagesSchema::class,
    \App\Models\GeoTree::class => \App\Schemas\GeoTreeSchema::class,
    \App\Models\GeoTreeTranslationInAllLanguages::class => \App\Schemas\GeoTreeTranslationInAllLanguagesSchema::class,
    \App\DTOs\AccommodationFacilityDTO::class => \App\Schemas\AccommodationFacilityDTOSchema::class,
    \App\User::class => \App\Schemas\UserSchema::class,
    \App\Models\CommercialRegionTranslation::class => \App\Schemas\CommercialRegionTranslationSchema::class,
    \App\Models\UrlRedirect::class => \App\Schemas\UrlRedirectSchema::class,
    \App\Models\AccommodationThemeCount::class => \App\Schemas\AccommodationThemeCountSchema::class,
    \App\Models\FacilityTheme::class => \App\Schemas\FacilityThemeSchema::class,
    \App\Models\AccommodationFacilityMainAll::class => \App\Schemas\AccommodationFacilityMainAllSchema::class,
    \App\Models\AccommodationFacilitySubAll::class => \App\Schemas\AccommodationFacilitySubAllSchema::class,
    \App\Models\FacilityMain::class => \App\Schemas\FacilityMainSchema::class,
    \App\Models\FacilitySub::class => \App\Schemas\FacilitySubSchema::class,
    \App\Models\PointOfInterest::class => \App\Schemas\PointOfInterestSchema::class,
    \App\Models\PointOfInterestTranslation::class => \App\Schemas\PointOfInterestTranslationSchema::class,
    \App\Models\PointOfInterestTranslationInAllLanguages::class => \App\Schemas\PointOfInterestTranslationInAllLanguagesSchema::class,
];
