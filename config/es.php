<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Elasticsearch Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the Elasticsearch connections below you wish
    | to use as your default connection for all work. Of course.
    |
    */

    'default' => env('ELASTIC_CONNECTION', 'default'),

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the Elasticsearch connections setup for your application.
    | Of course, examples of configuring each Elasticsearch platform.
    |
    */

    'connections' => [
        'default' => [
            'servers' => [
                [
                    "host" => env("ELASTIC_HOST", "127.0.0.1"),
                    "port" => env("ELASTIC_PORT", 9200),
                    'user' => env('ELASTIC_USER', ''),
                    'pass' => env('ELASTIC_PASS', ''),
                    'scheme' => env('ELASTIC_SCHEME', 'http'),
                ]
            ],
            'index' => env('ELASTIC_INDEX', 'geotree'),

            // Elasticsearch handlers
            // 'handler' => new MyCustomHandler(),
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Indices
    |--------------------------------------------------------------------------
    |
    | Here you can define your indices, with separate settings and mappings.
    | Edit settings and mappings and run 'php artisan es:index:update' to update
    | indices on elasticsearch server.
    |
    | 'my_index' is just for test. Replace it with a real index name.
    |
    */
    'indices' => [
        'geotree' => [
            'settings' => [
                "number_of_shards" => 1,
                "number_of_replicas" => 0,
            ],
            'mappings' => [
                'geoitem' => [
                    "properties" => [
                        "gps_coords" => [
                            "type" => "geo_point"
                        ],
                        "item.gps_coords" => [
                            "type" => "geo_point"
                        ],
                        "item.geo_location" => [
                            "type" => "geo_shape",
                            "tree" => "quadtree",
                            "precision" => "1km"
                        ],
                        "name" => [
                            "type" => "text",
                            "fields" => [
                                "kw" => [
                                    "analyzer" => "my_analyzer",
                                    "search_analyzer" => "autocomplete_search",
                                    "type" => "text",
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "settings" => [
                "analysis" => [
                    "analyzer" => [
                        "my_analyzer" => [
                            "tokenizer" => "my_tokenizer",
                            "filter" => [
                                "lowercase"
                            ]
                        ],
                        "autocomplete_search" => [
                            "tokenizer" => "lowercase"
                        ]
                    ],
                    "tokenizer" => [
                        "my_tokenizer" => [
                            "type" => "edge_ngram",
                            "min_gram" => 1,
                            "max_gram" => 10,
                            "token_chars" => [
                                "letter",
                                "digit"
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],

    'enable' => env('ELASTIC_ENABLE', 0)

];
