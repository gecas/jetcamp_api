<?php

/*
 * Array defines allowed filtering strategy on defined columns
 * Filter types are:
 * * eq - equal
 * * in - in
 * * gt - equal
 * * lt - equal
 * * like - like
 * * n -  null
 */
return [
    'accommodations' => [
        'id' => ['eq', 'in'],
        'name' => ['eq', 'plikep'],
        'address_city' => ['eq', 'plikep'],
        'migrated' => ['n']
    ],
    'commercial_regions' => [
        'id' => ['eq', 'in'],
        'key' => ['eq', 'plikep'],
        'gpsPolygon' => ['eq', 'plikep'],
        'migrated' => ['n'],
        'active' => ['eq', 'in', 'n'],
    ],
    'languages' => [
        'id' => ['eq', 'in'],
        'key' => ['eq', 'plikep']
    ],
    'geo_trees' => [
        'id' => ['eq', 'in'],
        'name' => ['eq', 'plikep'],
        'migrated' => ['n']
    ],
    'url_redirects' => [
        'from' => ['eq'],
        'to' => ['eq'],
    ],
    'accommodation_theme_count' => [
        'accommodation_id' => ['eq'],
        'to' => ['eq'],
    ],
    'accommodation_facility_mains_all' => [
        'accommodation_id' => ['eq'],
        'facility_theme_id' => ['eq'],
    ],
    'accommodation_facility_subs_all' => [
        'accommodation_id' => ['eq'],
        'facility_main_id' => ['eq'],
    ],
    'pois' => [
        'id' => ['eq'],
        'name_en' => ['plikep'],
        'domain' => ['plikep'],
    ],
];