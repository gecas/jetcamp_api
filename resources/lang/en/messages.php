<?php
return [
    'in' => 'in',

    'item_type_1' => 'Geoitem',
    'item_type_2' => 'Camping',
    'item_type_3' => 'Commercial region',
    'item_type_4' => 'POI',

    'subline_continent' => 'A continent',
    'subline_country' => 'A country in',
    'subline_city' => 'A place in',
    'subline_region' => 'A region in',
    'subline_camping' => 'Camping in',
    'subline_commercial_region' => 'A commercial region in',

    'poi_categories' => [
        'points_of_interest_landmarks' => 'Landmark',
        'geologic_formations' => 'Geologic formation',
        'architectural_buildings' => 'Architectural building',
        'beaches' => 'Beach',
        'performances' => 'Performance',
        'cultural_tours' => 'Cultural tour',
        'neighborhoods' => 'Neighborhood',
        'specialty_museums' => 'Specialty museum',
        'history_museums' => 'History museum',
        'art_museums' => 'Art museum',
        'islands' => 'Island'
    ],

    'campings' => '',
    'word_with' => 'with'
];