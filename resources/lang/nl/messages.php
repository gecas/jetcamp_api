<?php
return [

    'subline_continent' => 'Een continent in',
    'subline_country' => 'Een land in',
    'subline_city' => 'Een plaats in',
    'subline_region' => 'Een regio in',
    'subline_camping' => 'Kamperen in',
    'subline_commercial_region' => 'Een commerciële regio in',

    'poi_categories' => [
        'points_of_interest_landmarks' => 'Mijlpaal',
        'geologic_formations' => 'Geologische formatie',
        'architectural_buildings' => 'Architectonisch gebouw',
        'beaches' => 'Strand',
        'performances' => 'Prestatie',
        'cultural_tours' => 'Culturele tour',
        'neighborhoods' => 'Buurt',
        'specialty_museums' => 'Specialistisch museum',
        'history_museums' => 'Historisch museum',
        'art_museums' => 'Kunstmuseum',
        'islands' => 'Eiland'
    ],

    'word_with' => 'met'
];