<?php

namespace Tests\Unit\Models;

use App\Models\Url;

class UrlPreffixLessTest extends \TestCase
{
    protected $url;

    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_get_url_of_suffix_less_url()
    {
        $url = 'no-suffix/';
        $urlModel = factory(Url::class)->make([
            'lang' => 'en',
            'url' => $url
        ]);

        $this->assertEquals($url, $urlModel->urlSuffixLess);
    }

    /** @test */
    public function it_can_get_url_of_suffixed_url()
    {
        $url = 'no-suffix--1/';
        $urlModel = factory(Url::class)->make([
            'lang' => 'en',
            'url' => $url
        ]);

        $this->assertEquals('no-suffix/', $urlModel->urlSuffixLess);
    }

    /** @test */
    public function it_can_get_url_of_big_suffixed_url()
    {
        $url = 'no-suffix--150/';
        $urlModel = factory(Url::class)->make([
            'lang' => 'en',
            'url' => $url
        ]);

        $this->assertEquals('no-suffix/', $urlModel->urlSuffixLess);
    }
}
