<?php

use App\JetCamp\JCSearchResponse;
use App\JetCamp\JCCampingPageResponse;
use App\JetCampApi\JCClient;

class GeneralTest extends TestCase
{

    private $jc_rsp_json_keys = ['status', 'items', 'main_item', 'items_total', 'rating_groups', 'type'];

    public function testAutocomplete() {

        $response = $this->json('POST', 'api/v1/search', ["q" => "camping"]);
        $response->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $this->assertGreaterThan(0, count($jcSearchResponse->getJcItems()));
        $items = $jcSearchResponse->getJcItems();
        $str1 = $items[0]["desc"];

        $responseDe = $this->json('POST', 'api/v1/search', ["q" => "camping", "lng" => "de"]);
        $responseDe->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponseDe = new JCSearchResponse(null);
        $jcSearchResponseDe->processRawResponse($responseDe->response->content());
        $this->assertGreaterThan(0, count($jcSearchResponseDe->getJcItems()));
        $items = $jcSearchResponseDe->getJcItems();
        $str2 = $items[0]["desc"];

        $this->assertNotEquals($str1, $str2);
    }

    public function testRedirectUrl() {

        $response = $this->json('POST', 'api/v1/search-redirect-url', ['item_id' => "11-4", "lng" => "en"]);
        $response->seeJsonStructure(["status", "lng", "redirect_url"]);
        $params1 = json_decode($response->response->getContent(), true);

        $response = $this->json('POST', 'api/v1/search-redirect-url', ['item_id' => "11-4", "lng" => "de"]);
        $response->seeJsonStructure(["status", "lng", "redirect_url"]);
        $params2 = json_decode($response->response->getContent(), true);

        $this->assertNotEquals($params1["redirect_url"], $params2["redirect_url"]);
    }


    public function testSearchResultPages() {

        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "italy", "veneto", "provincia-di-verona", "peschiera-del-garda", "camping-bella-italia"], "lng" => "en"]);
        $response->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $jcMainItem = $jcSearchResponse->getMainJcItem();
        $name1 = $jcMainItem["name"];
        $breadcrumbs = $jcSearchResponse->getBreadcrumbs();
        $this->assertEquals("Italy", trim($breadcrumbs[0]["name"]));

        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "italien", "venetien", "provincia-di-verona", "peschiera-del-garda", "camping-bella-italia"], "lng" => "de"]); // camping
        $response->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $jcMainItem = $jcSearchResponse->getMainJcItem();
        $name2 = $jcMainItem["name"];
        $breadcrumbs = $jcSearchResponse->getBreadcrumbs();
        $this->assertEquals("Italien", trim($breadcrumbs[0]["name"]));

        $this->assertEquals($name1, $name2);

        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "spain", "catalonia", "barcelona", "sant-julia-de-cerdanyola"], "lng" => "en"]); // destination
        $response->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $jcMainItem = $jcSearchResponse->getMainJcItem();
        $breadcrumbs = $jcSearchResponse->getBreadcrumbs();
        $this->assertNotNull($jcMainItem);
        $this->assertGreaterThan(0, count($breadcrumbs));

        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "italy", "lake-garda"], "lng" => "en"]); // commercial region
        $response->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $jcMainItem = $jcSearchResponse->getMainJcItem();
        $breadcrumbs = $jcSearchResponse->getBreadcrumbs();
        $this->assertNotNull($jcMainItem);
        $this->assertGreaterThan(0, count($breadcrumbs));

        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "spain", "points-of-interest-landmarks", "torre-del-bramante"], "lng" => "en"]); // POI
        $response->seeJsonStructure($this->jc_rsp_json_keys);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $jcMainItem = $jcSearchResponse->getMainJcItem();
        $breadcrumbs = $jcSearchResponse->getBreadcrumbs();
        $this->assertNotNull($jcMainItem);
        $this->assertGreaterThan(0, count($breadcrumbs));
    }

    public function testSortingInSearchResultPage() {
        $testHelper = TestingHelper::instance();
        $sort = "popularity-desc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "italy", "veneto", "provincia-di-verona", "peschiera-del-garda",
            "camping-bella-italia"], "url_query_parts" => [
                "sort" => $sort
            ],
            "lng" => "en"]);

        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();
        $parts = explode("-", $sort);
        $isSorted = $testHelper->isArraySortedByProperty($items, $parts[0], $parts[1]);
        $this->assertTrue($isSorted);

        $sort = "popularity-asc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "italy", "veneto", "provincia-di-verona", "peschiera-del-garda",
            "camping-bella-italia"], "url_query_parts" => ["sort" => $sort],
            "lng" => "en"]); // camping
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();
        $parts = explode("-", $sort);
        $isSorted = $testHelper->isArraySortedByProperty($items, $parts[0], $parts[1]);
        $this->assertTrue($isSorted);

        $sort = "distance-desc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "spain", "educational-sites", "humboldt-university-humboldt-universitat"], "url_query_parts" => ["sort" => $sort],
            "lng" => "en"]); // poi
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();
        $this->assertGreaterThan(0, count($items));

        $sort = "rating_stars-desc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "spain", "educational-sites", "humboldt-university-humboldt-universitat"], "url_query_parts" => ["sort" => $sort],
            "lng" => "en"]); // poi
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();
        $parts = explode("-", $sort);
        $isSorted = $testHelper->isArraySortedByProperty($items, "starsRating", $parts[1]);
        $this->assertTrue($isSorted);

        $sort = "rating-asc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "netherlands", "veluwe"], "url_query_parts" => ["sort" => $sort],
            "lng" => "en"]); // commercial region
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();
        $parts = explode("-", $sort);
        $isSorted = $testHelper->isArraySortedByProperty($items, $parts[0], $parts[1]);
        $this->assertTrue($isSorted);

        $sort = "rating-desc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "denmark"], "url_query_parts" => ["sort" => $sort],
            "lng" => "en"]); // destination
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();
        $parts = explode("-", $sort);
        $isSorted = $testHelper->isArraySortedByProperty($items, $parts[0], $parts[1]);
        $this->assertTrue($isSorted);

    }

    public function testFilteringAndSortingInSearchResultPage() {
        $testHelper = TestingHelper::instance();
        $size = 7;
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "italy", "veneto", "provincia-di-verona", "peschiera-del-garda",
            "camping-bella-italia"],
            "lng" => "en", "size" => $size]);
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $ratingGroups = $jcSearchResponse->getRatingGroups();
        $valueKey = 9;
        $items = $jcSearchResponse->getJcItems();
        $this->assertEquals($size, count($items));

        $isFiltered = $testHelper->isArrayFilteredByProperty($items, "rating", $valueKey);
        $this->assertFalse($isFiltered);

        // commented out as rating filtering values was updated (from 0-5 to 6-10), need to update form filtering items itself
//        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
//            "italy", "veneto", "provincia-di-verona", "peschiera-del-garda",
//            "camping-bella-italia"],
//            "url_query_parts" => ["rating" => $valueKey],
//            "lng" => "en"]);
//        $jcSearchResponse = new JCSearchResponse(null);
//        $jcSearchResponse->processRawResponse($response->response->content());
//        $items = $jcSearchResponse->getJcItems();
//        $isFiltered = $testHelper->isArrayFilteredByProperty($items, "rating", $valueKey);
//        $this->assertTrue($isFiltered);

        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "republic-of-france", "french-alps"],
            "lng" => "en", "size" => $size]); // commercial region
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $ratingGroups = $jcSearchResponse->getRatingGroups();
        $valueKey = 4;
        $itemCountInRating = $ratingGroups[$valueKey + 1];
        $items = $jcSearchResponse->getJcItems();

        $this->assertEquals($size, count($items));
        $isFiltered = $testHelper->isArrayFilteredByProperty($items, "rating", $valueKey);
        $this->assertFalse($isFiltered);
        $isSorted = $testHelper->isArraySortedByProperty($items, "starsRating");
        $this->assertFalse($isSorted);

        $sort = "rating_stars-desc";
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "netherlands", "veluwe"],
            "url_query_parts" => ["rating" => $valueKey, "sort" => $sort],
            "lng" => "en", "size" => $size]); // commercial region
        $jcSearchResponse = new JCSearchResponse(null);
        $jcSearchResponse->processRawResponse($response->response->content());
        $items = $jcSearchResponse->getJcItems();

        // commencted out as rating values were updated
        //$this->assertEquals($itemCountInRating, count($items));
        $isFiltered = $testHelper->isArrayFilteredByProperty($items, "rating", $valueKey);
        $this->assertTrue($isFiltered);
        $parts = explode("-", $sort);
        $isSorted = $testHelper->isArraySortedByProperty($items, "starsRating", $parts[1]);
        $this->assertTrue($isSorted);

    }

    public function testMapUpdates()
    {
        $testHelper = TestingHelper::instance();

        $params['coords'] = [
            [10.6963, 45.5691],
            [10.6963, 45.4382],
            [10.5096, 45.4382],
            [10.5096, 45.5691],
            [10.6963, 45.5691]
        ];
//        $params['rating'] = 3;
        $params['zoom'] = 12;

        $params['size'] = 20;
        if($params['zoom'] >= 12){
            $response1 = $this->json('POST', '/api/v1/pois-search-faceted', $params);
        }
        $response2 = $this->json('POST', '/api/v1/update-search-faceted', $params);

        $items1 = json_decode($response1->response->content())->items;
        $items2 = json_decode($response2->response->content())->items;

        $this->assertGreaterThan(0, count($items1));
        $this->assertGreaterThan(0, count($items2));
    }

    public function testCampingPage()
    {
        $testHelper = TestingHelper::instance();
        $size = 8;
        $response = $this->json('POST', 'api/v1/search-by-url', ['url_parts' => [
            "spain", "camping-natura"],
            "lng" => "en"]);

        $jcCampingPageResponse = new JCCampingPageResponse(null);
        $jcCampingPageResponse->processRawResponse($response->response->content());
        $poi_items = $jcCampingPageResponse->getCampingPoisItems();
        $camping_items = $jcCampingPageResponse->getCampingCampingsItems();
        $this->assertGreaterThan(0, count($poi_items));
        //$this->assertGreaterThan(0, count($camping_items)); // commented out because after latest data migration can't find which camping page would have any commercial region on dev and on staging env
    }

}